/*
  ******************************************************************************
  * (C) 2018 Alvaro Lopes <alvieboy@alvie.com>
  *
  * Redistribution and use in source and binary forms, with or without modification,
  * are permitted provided that the following conditions are met:
  *   1. Redistributions of source code must retain the above copyright notice,
  *      this list of conditions and the following disclaimer.
  *   2. Redistributions in binary form must reproduce the above copyright notice,
  *      this list of conditions and the following disclaimer in the documentation
  *      and/or other materials provided with the distribution.
  *   3. Neither the name of Alvaro Lopes nor the names of contributors
  *      may be used to endorse or promote products derived from this software
  *      without specific prior written permission.
  *
  * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
  * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
  * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
  * DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
  * FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
  * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
  * SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
  * CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
  * OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
  * OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
  *
  ******************************************************************************
  */

$fn=50;
bthickness=2;
psu_height=99;
psu_width=159;
psu_align_x=-20;
psu_align_y=-20.0;
psu_hole1_y = 65.0;
psu_hole1_x = 24.0;
psu_hole_dist= 78.0;

module bholder() {
    difference() {
        translate([-4,-4,0]) cube([8,8,bthickness*2], center=false);
        // Hole 2.5mm radius
        translate([0,0,bthickness]) cylinder(r=2.75, h=bthickness);
        // Hole 2.5mm radius
        cylinder(r=1.75, h=bthickness);
    }
}

module psu_holder_hole()
{
    cylinder(r=2.75, h=bthickness*3);
}
module psu_holder() {
    difference() {
        translate([-4,-4,0]) cube([8,8,bthickness*4], center=false);
        // Hole 2.5mm radius
        translate([0,0,bthickness*3]) cylinder(r=1.75, h=bthickness);
        // Hole 2.5mm radius
        psu_holder_hole();
        //cylinder(r=2.75, h=bthickness*3);
    }
}
module psu_stand() {
    difference() {
        translate([-4,-4,0]) cube([8,8,bthickness*4], center=false);
        cylinder(r=1.5, h=bthickness*3);
    }
}

hstand=4.7;

module psu_stand_helper() {
    difference() {
        translate([-4,-4,0]) cube([8,8,hstand], center=false);
        // Hole 2.5mm radius
        cylinder(r=1.75, h=bthickness);
        // Hole 2.5mm radius
        translate([0,0,bthickness]) cylinder(r=2.75, h=bthickness*3);
    }
}

module psu_holder_helper() {
    difference() {
        translate([-4,-4,0]) cube([8,8,hstand], center=false);
        // Hole 2.5mm radius
        cylinder(r=2.75, h=hstand);
    }
}

module vsupport() {
    rotate([0,0,180])
    difference()
    {
        union() {
            cube([bthickness*4,bthickness*6,bthickness*2]);
            cube([bthickness*4,bthickness,8*2]);
        }
        union() {
        //translate([bthickness*2,0,bthickness*6]) rotate([0,90,90]) cylinder(r=2.75,h=bthickness+0.1);
        translate([bthickness*2,0,bthickness*6+1.2]) rotate([0,90,90]) cylinder(r=1.8 /*2.75*/,h=bthickness+0.1);
        }
    }
}
module vsupport_mb() {
    rotate([0,0,180])
    difference()
    {
        union() {
            cube([bthickness*4,bthickness*6,bthickness*2]);
            cube([bthickness*4,bthickness,6*2]);
        }
        union() {
        //translate([bthickness*2,0,bthickness*6]) rotate([0,90,90]) cylinder(r=2.75,h=bthickness+0.1);
        translate([bthickness*2,0,bthickness*4+1.2]) rotate([0,90,90]) cylinder(r=1.8 /*2.75*/,h=bthickness+0.1);
        }
    }
}


module toadd()
{
bholder();
translate([59.5, 0.0,  0.0]) bholder();
translate([119,  0.0,  0.0]) bholder();

translate([0.0,  38,  0.0]) bholder();
translate([59.5, 38,  0.0]) bholder();
translate([119,  38,  0.0]) bholder();

translate([0.0,  76,  0.0]) bholder();
translate([59.5, 76,  0.0]) bholder();
translate([119,  76,  0.0]) bholder();

translate([0.0,  114,  0.0]) bholder();
translate([59.5, 114,  0.0]) bholder();
translate([119,  114,  0.0]) bholder();

// Join them together
jwidth=4;

translate([3,   -(jwidth/2), 0]) cube([53.5, jwidth, bthickness]);
translate([62.5,-(jwidth/2), 0]) cube([53.5, jwidth, bthickness]);

translate([3,   -(jwidth/2)+37.5, 0]) cube([53.5, jwidth, bthickness]);
translate([62.5,-(jwidth/2)+37.5, 0]) cube([53.5, jwidth, bthickness]);

translate([3,   -(jwidth/2)+76, 0]) cube([53.5, jwidth, bthickness]);
translate([62.5,-(jwidth/2)+76, 0]) cube([53.5, jwidth, bthickness]);

translate([3,   -(jwidth/2)+114, 0]) cube([53.5, jwidth, bthickness]);
translate([62.5,-(jwidth/2)+114, 0]) cube([53.5, jwidth, bthickness]);

// V
translate([-(jwidth/2),      3, 0]) cube([jwidth, 31.5, bthickness]);
translate([-(jwidth/2)+59.5, 3, 0]) cube([jwidth, 31.5, bthickness]);
translate([-(jwidth/2)+119,  3, 0]) cube([jwidth, 31.5, bthickness]);

translate([-(jwidth/2),      3+37.5, 0]) cube([jwidth, 31.5, bthickness]);
translate([-(jwidth/2)+59.5, 3+37.5, 0]) cube([jwidth, 31.5, bthickness]);
translate([-(jwidth/2)+119,  3+37.5, 0]) cube([jwidth, 31.5, bthickness]);

translate([-(jwidth/2),      3+75, 0]) cube([jwidth, 31.5, bthickness]);
translate([-(jwidth/2)+59.5, 3+75, 0]) cube([jwidth, 31.5, bthickness]);
translate([-(jwidth/2)+119,  3+75, 0]) cube([jwidth, 31.5, bthickness]);



translate([119-psu_align_x-psu_hole1_x, (psu_hole1_y)+psu_align_y  ,  0]) psu_holder();
translate([119-psu_align_x-psu_hole1_x-psu_hole_dist, (psu_hole1_y)+psu_align_y  ,  0]) psu_holder();

// Connect the PSU holder
//translate([3+4+78,   12, 0]) cube([34.5, 2, bthickness]);
//translate([60.5,12, 0]) cube([18.5, 2, bthickness]);
//translate([-1+82,      0, 0]) cube([2, 9.5, bthickness]);


translate([29.75,    3.5+37.5, 0]) psu_stand();
translate([-4+0,     56.25, 0]) psu_stand();

translate([-4+59.5,  56.25, 0]) psu_stand();
translate([-4+119,   56.25, 0]) psu_stand();
translate([-4+59.5,  18.75, 0]) psu_stand();
translate([-4+119,   18.75, 0]) psu_stand();
translate([-4,  18.75, 0]) psu_stand();

// Helpers. Uncomment if you need them.

translate([12, 60, 0]) psu_stand_helper();
translate([22, 60, 0]) psu_stand_helper();
translate([32, 60, 0]) psu_stand_helper();
translate([42, 60, 0]) psu_stand_helper();
translate([70, 60, 0]) psu_stand_helper();
translate([80, 60, 0]) psu_holder_helper();
translate([90, 60, 0]) psu_holder_helper();


// Step-down support

stepdown_hole_distance=58.5;
stepdown_hole_clearance=5.0;
stepdown_cooler_clearance=10.25;

stepdown_clerance=stepdown_hole_clearance + stepdown_cooler_clearance;

translate([12-35,88,0]) vsupport();
translate([12-35+stepdown_hole_distance,88,0]) vsupport();
translate([(12-35),   84-(jwidth/2), 0]) cube([22, jwidth, bthickness]);

// Support for mainboard
translate([119+30-4,88,0]) vsupport_mb();
translate([55,    88,0]) vsupport_mb();
translate([55+45,    88,0]) vsupport_mb();

translate([119,   84-(jwidth/2), 0]) cube([22, jwidth, bthickness]);


}

module tosubtract()
{
    translate([119-psu_align_x-psu_hole1_x, (psu_hole1_y)+psu_align_y  ,  0]) psu_holder_hole();
    translate([119-psu_align_x-psu_hole1_x-psu_hole_dist, (psu_hole1_y)+psu_align_y  ,  0]) psu_holder_hole();
}
difference()
{
    toadd();
    tosubtract();
}

// TODO: connect step-down holder. DONE
// TODO: fix holes which are covered. DONE
// TODO: add holder for main PSU card. DONE
// TODO: add total 1-1.5mm spacing between last holes (direction where 4 holes exist). DONE
// TODO: fix hole for stepdown. Make sure up clearance is slightly smaller

