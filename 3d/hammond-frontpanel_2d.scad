$fn = 60;
thickness=1.65;
keepout=0.01;

module led() {
    union() {
        circle(r=2.75);
        // Keep-out area
        //cylinder(h=keepout,d=5.5);
    }
}

module banana() {
    circle(r=2.8);
    // Keep out
    //cylinder(h=keepout, r=5);
}



module rotary() {
    circle(r=3.55);
    // Keep out
    //rectanglerectanglerectanglerectanglerectanglerectangle([1.8,1.8,keepout],center=true);
}
/*
    front panel

hsep rotary: 84mm
vsep rotary: 21.6mm
button 1: 


relative to edge coords:

rot1:  8, 41.9
rot2:	8, 20.3
rot3: 91.8, 41.9
rot4: 91.8, 20.3
b1: 9.9, 5.0
b2: 34, 5
b3: 45.47, 5
b4: 81, 5
l4: 90.2, 5.3
l3: 55.9, 5.3
l2: 20.3, 5.3

flip x please
*/

module button()
{
    circle(d=4.0);
}
module fp()
{
    translate([-8, 41.91]) rotary(); // Eagle-fix
    translate([-8, 20.32]) rotary(); // Eagle-fix
    translate([-91.821, 41.91]) rotary(); // Eagle-fix
    translate([-91.821, 19.05]) rotary(); // Eagle-fix

    translate([-9.906, 5.08]) button(); // Eagle-fix
    translate([-34.036, 5.08]) button(); // Eagle-fix
    translate([-45.466, 5.08]) button(); // Eagle-fix
    translate([-81.026, 5.08]) button(); // Eagle-fix

    translate([-20.32, 5.334]) led(); // Eagle-fix
    translate([-55.88, 5.334]) led(); // Eagle-fix
    translate([-90.17, 5.334]) led(); // Eagle-fix
    
    // LCD hole
    translate([-23,9.2,0])
//        rectangle([55.2,40.2,thickness]);
        polygon(points=[[ 0,0 ], [-55.2,0] ,[-55.2,40.2], [0,40.2]]);
}

usbconn_width=17;
usbconn_height=14;

module center_rect(x,y)
{
    translate([-(x/2),-(y/2)])
        polygon(points=[[ 0,0 ], [x,0] ,[x,y], [0,y]]);
}


module usb_conn()
{
    center_rect(usbconn_width,usbconn_height);
    translate([-15,0]) circle(d=3.5);
    translate([15,0]) circle(d=3.5);
}

module bananas()
{
    translate([15.5, 8]) banana(); // 5V GND
    translate([34.5, 8]) banana(); // +5V
    
    translate([71,  8]) banana(); // DUAL -        
    translate([90,  8]) banana(); // DUAL GND
    translate([109, 8]) banana(); // DUAL +
    
    translate([155.5-5, 8]) banana(); // ADJ GND
    translate([174.5-5, 8]) banana(); // ADJ V+
}
// Base
//rectangle([191,69.5,thickness],false);
difference() {
    polygon(points=[[0,0],[191.30,0], [191.30,69.5] ,[0,69.5]]);
    translate([100+27,16,0]) fp();
    translate([3,0,0]) bananas();

    translate([170,55,0]) usb_conn();

}