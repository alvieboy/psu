$fn = 60;

pcon_hole_width=32.5;
pcon_hole_height=24.2;

pswitch_width=13.5;
pswitch_height=18.7;

usbconn_width=17;
usbconn_height=14;
/*
    Back panel

    Connector dimensions are 47.2 x 27.5 
    Add some margin
*/

module rect(x,y)
{
        polygon(points=[[ 0,0 ], [x,0] ,[x,y], [0,y]]);
}

module center_rect(x,y)
{
    translate([-(x/2),-(y/2)])
        polygon(points=[[ 0,0 ], [x,0] ,[x,y], [0,y]]);
}

module button()
{
    circle(d=7.0);
}

module power_connector()
{
    pcon_half_width=pcon_hole_width/2;
    pcon_half_height=pcon_hole_height/2;
    center_rect(pcon_hole_width,pcon_hole_height);
    // Holes
    translate([-20,0]) circle(d=3.5);
    translate([20,0]) circle(d=3.5);
}

module power_switch()
{
    center_rect(pswitch_width,pswitch_height);
}

module usb_conn()
{
    center_rect(usbconn_width,usbconn_height);
    translate([-15,0]) circle(d=3.5);
    translate([15,0]) circle(d=3.5);
}

// Base
difference() {
    polygon(points=[[0,0],[191.30,0], [191.30,69.5] ,[0,69.5]]);
    translate([60,50,0]) power_connector();
    translate([175,55,0]) button();
    translate([15,50,0]) power_switch();

}