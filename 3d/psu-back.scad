/*
  ******************************************************************************
  * (C) 2018 Alvaro Lopes <alvieboy@alvie.com>
  *
  * Redistribution and use in source and binary forms, with or without modification,
  * are permitted provided that the following conditions are met:
  *   1. Redistributions of source code must retain the above copyright notice,
  *      this list of conditions and the following disclaimer.
  *   2. Redistributions in binary form must reproduce the above copyright notice,
  *      this list of conditions and the following disclaimer in the documentation
  *      and/or other materials provided with the distribution.
  *   3. Neither the name of Alvaro Lopes nor the names of contributors
  *      may be used to endorse or promote products derived from this software
  *      without specific prior written permission.
  *
  * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
  * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
  * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
  * DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
  * FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
  * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
  * SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
  * CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
  * OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
  * OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
  *
  ******************************************************************************
  */

$fn = 60;
thickness=0.2;
keepout=0.01;

module psu_in()
{
    translate([0,0,thickness/2]) cube([2.75, 1.85, thickness], true);
    translate([-2,0,0]) cylinder(r=0.2, h=thickness);
    translate([+2,0,0]) cylinder(r=0.2, h=thickness);
}
module power_button()
{
    cube([1.4, 1.9, thickness]);
}
module keepout() {
        difference() {
        translate([0,0,-thickness]) cube([19.20,6.5,thickness*3]);
        translate([0.3,0.3,-thickness]) cube([19.20-0.6,6.5-0.6,thickness*3]);
         }
}


translate([19.20,0,0]) rotate([0,180,0]) 
difference() {
        union() {
            cube([19.20,6.5,thickness],false);
           // keepout();
        }
        translate([3,  5]) psu_in(); 
        translate([17.1,  4]) power_button(); 
    }

