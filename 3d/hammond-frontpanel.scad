$fn = 60;
thickness=1.65;
keepout=0.01;

module led() {
    union() {
        cylinder(h=thickness,r=2.75);
        // Keep-out area
        //cylinder(h=keepout,d=5.5);
    }
}

module banana() {
    cylinder(h=thickness, r=2.8);
    // Keep out
    //cylinder(h=keepout, r=5);
}

module rotary() {
    cylinder(h=thickness, r=3.55);
    // Keep out
    //cube([1.8,1.8,keepout],center=true);
}
/*
    front panel

hsep rotary: 84mm
vsep rotary: 21.6mm
button 1: 


relative to edge coords:

rot1:  8, 41.9
rot2:	8, 20.3
rot3: 91.8, 41.9
rot4: 91.8, 20.3
b1: 9.9, 5.0
b2: 34, 5
b3: 45.47, 5
b4: 81, 5
l4: 90.2, 5.3
l3: 55.9, 5.3
l2: 20.3, 5.3

flip x please
*/

module button()
{
    cylinder(h=thickness, d=4.0);
}
module fp()
{
    translate([8, 41.9]) rotary();
    translate([8, 20.3]) rotary();
    translate([91.8, 41.9]) rotary();
    translate([91.8, 20.3]) rotary();    

    translate([9.9, 5]) button();    
    translate([34, 5]) button();    
    translate([45.5, 5]) button();    
    translate([81, 5]) button();    

    translate([20.3, 5.3]) led();
    translate([55.9, 5.3]) led();
    translate([90.2, 5.3]) led();
    
    // LCD hole
    translate([23,9.2,0]) cube([55.2,40.2,thickness]);
}

module bananas()
{
    translate([15.5, 8]) banana(); // 5V GND
    translate([34.5, 8]) banana(); // +5V
    
    translate([71,  8]) banana(); // DUAL -        
    translate([90,  8]) banana(); // DUAL GND
    translate([109, 8]) banana(); // DUAL +
    
    translate([155.5-5, 8]) banana(); // ADJ GND
    translate([174.5-5, 8]) banana(); // ADJ V+
}
// Base
translate([191,0,0]) rotate([0,180,0]) 
difference() {
        union() {
            cube([191,69.5,thickness],false);
            //keepout();
        };
        translate([47,16,0]) fp();
        translate([3,0,0]) bananas();
    }
    
