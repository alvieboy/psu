/*
  ******************************************************************************
  * (C) 2018 Alvaro Lopes <alvieboy@alvie.com>
  *
  * Redistribution and use in source and binary forms, with or without modification,
  * are permitted provided that the following conditions are met:
  *   1. Redistributions of source code must retain the above copyright notice,
  *      this list of conditions and the following disclaimer.
  *   2. Redistributions in binary form must reproduce the above copyright notice,
  *      this list of conditions and the following disclaimer in the documentation
  *      and/or other materials provided with the distribution.
  *   3. Neither the name of Alvaro Lopes nor the names of contributors
  *      may be used to endorse or promote products derived from this software
  *      without specific prior written permission.
  *
  * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
  * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
  * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
  * DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
  * FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
  * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
  * SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
  * CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
  * OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
  * OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
  *
  ******************************************************************************
  */

$fn = 60;
thickness=0.2;
keepout=0.01;

module led() {
    union() {
        cylinder(h=thickness,r=0.275);
        // Keep-out area
        cylinder(h=keepout,r=0.3);
    }
}

module banana() {
    cylinder(h=thickness, r=0.28);
    // Keep out
    cylinder(h=keepout, r=0.5);
}

module rotary() {
    cylinder(h=thickness, r=0.355);
    // Keep out
    cube([1.8,1.8,keepout],center=true);
}
module volt_small() {
    cube([2.3, 1.05, thickness]);
}
module voltamp() {
    cube([4.55, 2.65, thickness]);
}

lcd_width=6.0;
lcd_height=4.26;

module lcd() {
    cube([6.10, 4.26, thickness]);
    // 77mm width - 10.5mm clear right, 6.5mm left
    translate([-0.65,0,0]) cube([7.70, 4.26, keepout]);

    translate([-0.35,0.25,0])     cylinder(h=thickness, r=0.18);
    translate([-0.35,4.26-0.25,0])     cylinder(h=thickness, r=0.18);
    translate([6.35,0.25,0])     cylinder(h=thickness, r=0.18);
    translate([6.35,4.26-0.25,0])     cylinder(h=thickness, r=0.18);

}

module multiturn() {
    cylinder(h=thickness, r=0.475);
    // Keep out
    cube([2.3,2.7,keepout],center=true);
    // Keep out top (for rot);
    //translate([0,0,thickness-keepout]) cylinder(h=keepout, r=0.8);
    
}

module switch_vertical() {
    cylinder(h=thickness, r=0.32);
    // Keep out
    cube([0.7, 1.3,keepout],center=true);
}

module keepout() {
        difference() {
        translate([0,0,-thickness]) cube([19.20,6.5,thickness*3]);
        translate([0.3,0.3,-thickness]) cube([19.20-0.6,6.5-0.6,thickness*3]);
         }
}

translate([19.20,0,0]) rotate([0,180,0]) 
difference() {
        union() {
            cube([19.20,6.5,thickness],false);
            //keepout();
        };
        translate([1.0,  5.5]) led(); // Power LED
        translate([1.55, 1.0]) banana(); // 5V GND
        translate([3.45, 1.0]) banana(); // +5V
        translate([1.34, 2.3]) volt_small(); // +5V status
        //translate([3.0,  3.8]) led(); // +5V status
        //translate([2.0,  3.8]) switch_vertical(); // +5V ON
        translate([2.5, 4.2]) led(); // ADJ ON LED
        translate([2.5, 5.2]) switch_vertical(); // ADJ ON switch


    
        translate([15.3,  5.0]) rotary();
        translate([17.3,  5.0]) rotary();
        translate([7.1,  1.0]) banana(); // DUAL -
        translate([9.0,  1.0]) banana(); // DUAL GND
        translate([10.9, 1.0]) banana(); // DUAL +
    //    translate([6.85, 3.0]) volt_small(); // +-V status
    
        translate([4.6,  1.75]) led(); // Dual ON
        translate([4.6, 2.75]) switch_vertical(); // ADJ ON switch
        
        translate([6, 1.8]) lcd(); // +-V status
    
  //      translate([10.8, 4.75]) multiturn();
        //translate([14.2, 3.0]) voltamp();
        translate([15.35, 2.3]) volt_small(); // +5V status
        
        translate([15.55, 1.0]) banana(); // ADJ GND
        translate([17.45, 1.0]) banana(); // ADJ V+
        translate([14, 1.75]) led(); // ADJ ON LED
        translate([14, 2.75]) switch_vertical(); // ADJ ON switch
        
    }

