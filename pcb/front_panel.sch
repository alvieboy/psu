<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE eagle SYSTEM "eagle.dtd">
<eagle version="6.4">
<drawing>
<settings>
<setting alwaysvectorfont="no"/>
<setting verticaltext="up"/>
</settings>
<grid distance="0.1" unitdist="inch" unit="inch" style="lines" multiple="1" display="no" altdistance="0.01" altunitdist="inch" altunit="inch"/>
<layers>
<layer number="1" name="Top" color="4" fill="1" visible="no" active="no"/>
<layer number="2" name="Route2" color="1" fill="3" visible="no" active="no"/>
<layer number="3" name="Route3" color="4" fill="3" visible="no" active="no"/>
<layer number="14" name="Route14" color="1" fill="6" visible="no" active="no"/>
<layer number="15" name="Route15" color="4" fill="6" visible="no" active="no"/>
<layer number="16" name="Bottom" color="1" fill="1" visible="no" active="no"/>
<layer number="17" name="Pads" color="2" fill="1" visible="no" active="no"/>
<layer number="18" name="Vias" color="2" fill="1" visible="no" active="no"/>
<layer number="19" name="Unrouted" color="6" fill="1" visible="no" active="no"/>
<layer number="20" name="Dimension" color="15" fill="1" visible="no" active="no"/>
<layer number="21" name="tPlace" color="7" fill="1" visible="no" active="no"/>
<layer number="22" name="bPlace" color="7" fill="1" visible="no" active="no"/>
<layer number="23" name="tOrigins" color="15" fill="1" visible="no" active="no"/>
<layer number="24" name="bOrigins" color="15" fill="1" visible="no" active="no"/>
<layer number="25" name="tNames" color="7" fill="1" visible="no" active="no"/>
<layer number="26" name="bNames" color="7" fill="1" visible="no" active="no"/>
<layer number="27" name="tValues" color="7" fill="1" visible="no" active="no"/>
<layer number="28" name="bValues" color="7" fill="1" visible="no" active="no"/>
<layer number="29" name="tStop" color="7" fill="3" visible="no" active="no"/>
<layer number="30" name="bStop" color="7" fill="6" visible="no" active="no"/>
<layer number="31" name="tCream" color="7" fill="4" visible="no" active="no"/>
<layer number="32" name="bCream" color="7" fill="5" visible="no" active="no"/>
<layer number="33" name="tFinish" color="6" fill="3" visible="no" active="no"/>
<layer number="34" name="bFinish" color="6" fill="6" visible="no" active="no"/>
<layer number="35" name="tGlue" color="7" fill="4" visible="no" active="no"/>
<layer number="36" name="bGlue" color="7" fill="5" visible="no" active="no"/>
<layer number="37" name="tTest" color="7" fill="1" visible="no" active="no"/>
<layer number="38" name="bTest" color="7" fill="1" visible="no" active="no"/>
<layer number="39" name="tKeepout" color="4" fill="11" visible="no" active="no"/>
<layer number="40" name="bKeepout" color="1" fill="11" visible="no" active="no"/>
<layer number="41" name="tRestrict" color="4" fill="10" visible="no" active="no"/>
<layer number="42" name="bRestrict" color="1" fill="10" visible="no" active="no"/>
<layer number="43" name="vRestrict" color="2" fill="10" visible="no" active="no"/>
<layer number="44" name="Drills" color="7" fill="1" visible="no" active="no"/>
<layer number="45" name="Holes" color="7" fill="1" visible="no" active="no"/>
<layer number="46" name="Milling" color="3" fill="1" visible="no" active="no"/>
<layer number="47" name="Measures" color="7" fill="1" visible="no" active="no"/>
<layer number="48" name="Document" color="7" fill="1" visible="no" active="no"/>
<layer number="49" name="Reference" color="7" fill="1" visible="no" active="no"/>
<layer number="50" name="dxf" color="7" fill="1" visible="no" active="no"/>
<layer number="51" name="tDocu" color="13" fill="1" visible="no" active="no"/>
<layer number="52" name="bDocu" color="7" fill="1" visible="no" active="no"/>
<layer number="53" name="tGND_GNDA" color="7" fill="1" visible="no" active="no"/>
<layer number="54" name="bGND_GNDA" color="7" fill="1" visible="no" active="no"/>
<layer number="56" name="wert" color="7" fill="1" visible="no" active="no"/>
<layer number="57" name="tCAD" color="7" fill="1" visible="no" active="no"/>
<layer number="59" name="tCarbon" color="7" fill="1" visible="no" active="no"/>
<layer number="60" name="bCarbon" color="7" fill="1" visible="no" active="no"/>
<layer number="90" name="Modules" color="5" fill="1" visible="yes" active="yes"/>
<layer number="91" name="Nets" color="2" fill="1" visible="yes" active="yes"/>
<layer number="92" name="Busses" color="1" fill="1" visible="yes" active="yes"/>
<layer number="93" name="Pins" color="2" fill="1" visible="no" active="yes"/>
<layer number="94" name="Symbols" color="4" fill="1" visible="yes" active="yes"/>
<layer number="95" name="Names" color="7" fill="1" visible="yes" active="yes"/>
<layer number="96" name="Values" color="7" fill="1" visible="yes" active="yes"/>
<layer number="97" name="Info" color="7" fill="1" visible="yes" active="yes"/>
<layer number="98" name="Guide" color="6" fill="1" visible="yes" active="yes"/>
<layer number="99" name="SpiceOrder" color="5" fill="1" visible="yes" active="yes"/>
<layer number="100" name="Muster" color="7" fill="1" visible="yes" active="yes"/>
<layer number="101" name="Patch_Top" color="7" fill="1" visible="yes" active="yes"/>
<layer number="102" name="Vscore" color="7" fill="1" visible="yes" active="yes"/>
<layer number="103" name="tMap" color="7" fill="1" visible="yes" active="yes"/>
<layer number="104" name="Name" color="7" fill="1" visible="yes" active="yes"/>
<layer number="105" name="tPlate" color="7" fill="1" visible="yes" active="yes"/>
<layer number="106" name="bPlate" color="7" fill="1" visible="yes" active="yes"/>
<layer number="107" name="Crop" color="7" fill="1" visible="yes" active="yes"/>
<layer number="108" name="centerline" color="7" fill="1" visible="yes" active="yes"/>
<layer number="109" name="fp9" color="7" fill="1" visible="no" active="yes"/>
<layer number="110" name="fp0" color="7" fill="1" visible="no" active="yes"/>
<layer number="111" name="LPC17xx" color="7" fill="1" visible="no" active="yes"/>
<layer number="112" name="tSilk" color="7" fill="1" visible="no" active="yes"/>
<layer number="113" name="ReferenceLS" color="7" fill="1" visible="no" active="no"/>
<layer number="116" name="Patch_BOT" color="7" fill="1" visible="yes" active="yes"/>
<layer number="118" name="Rect_Pads" color="7" fill="1" visible="no" active="no"/>
<layer number="121" name="_tsilk" color="7" fill="1" visible="yes" active="yes"/>
<layer number="122" name="_bsilk" color="7" fill="1" visible="yes" active="yes"/>
<layer number="123" name="tTestmark" color="7" fill="1" visible="no" active="yes"/>
<layer number="124" name="bTestmark" color="7" fill="1" visible="no" active="yes"/>
<layer number="125" name="_tNames" color="7" fill="1" visible="yes" active="yes"/>
<layer number="126" name="_bNames" color="7" fill="1" visible="no" active="yes"/>
<layer number="127" name="_tValues" color="7" fill="1" visible="no" active="yes"/>
<layer number="128" name="_bValues" color="7" fill="1" visible="no" active="yes"/>
<layer number="129" name="Mask" color="7" fill="1" visible="no" active="yes"/>
<layer number="130" name="130bmp" color="2" fill="1" visible="no" active="yes"/>
<layer number="131" name="prix" color="7" fill="1" visible="yes" active="yes"/>
<layer number="132" name="test" color="7" fill="1" visible="yes" active="yes"/>
<layer number="144" name="Drill_legend" color="7" fill="1" visible="yes" active="yes"/>
<layer number="145" name="DrillLegend_01-16" color="2" fill="9" visible="no" active="yes"/>
<layer number="150" name="Notes" color="7" fill="1" visible="no" active="yes"/>
<layer number="151" name="HeatSink" color="7" fill="1" visible="yes" active="yes"/>
<layer number="152" name="_bDocu" color="7" fill="1" visible="no" active="yes"/>
<layer number="153" name="FabDoc1" color="6" fill="1" visible="no" active="no"/>
<layer number="154" name="FabDoc2" color="2" fill="1" visible="no" active="no"/>
<layer number="155" name="FabDoc3" color="7" fill="15" visible="no" active="no"/>
<layer number="199" name="Contour" color="7" fill="1" visible="no" active="yes"/>
<layer number="200" name="200bmp" color="7" fill="1" visible="yes" active="yes"/>
<layer number="201" name="201bmp" color="7" fill="1" visible="yes" active="yes"/>
<layer number="202" name="202bmp" color="7" fill="1" visible="yes" active="yes"/>
<layer number="203" name="203bmp" color="4" fill="10" visible="yes" active="yes"/>
<layer number="204" name="204bmp" color="5" fill="10" visible="yes" active="yes"/>
<layer number="205" name="205bmp" color="6" fill="10" visible="yes" active="yes"/>
<layer number="206" name="206bmp" color="7" fill="10" visible="yes" active="yes"/>
<layer number="207" name="207bmp" color="8" fill="10" visible="yes" active="yes"/>
<layer number="208" name="208bmp" color="9" fill="10" visible="yes" active="yes"/>
<layer number="209" name="209bmp" color="7" fill="1" visible="yes" active="yes"/>
<layer number="210" name="210bmp" color="7" fill="1" visible="yes" active="yes"/>
<layer number="211" name="211bmp" color="7" fill="1" visible="yes" active="yes"/>
<layer number="212" name="212bmp" color="7" fill="1" visible="yes" active="yes"/>
<layer number="213" name="213bmp" color="7" fill="1" visible="yes" active="yes"/>
<layer number="214" name="214bmp" color="7" fill="1" visible="yes" active="yes"/>
<layer number="215" name="215bmp" color="7" fill="1" visible="yes" active="yes"/>
<layer number="216" name="216bmp" color="7" fill="1" visible="yes" active="yes"/>
<layer number="217" name="217bmp" color="18" fill="1" visible="no" active="no"/>
<layer number="218" name="218bmp" color="19" fill="1" visible="no" active="no"/>
<layer number="219" name="219bmp" color="20" fill="1" visible="no" active="no"/>
<layer number="220" name="220bmp" color="21" fill="1" visible="no" active="no"/>
<layer number="221" name="221bmp" color="22" fill="1" visible="no" active="no"/>
<layer number="222" name="222bmp" color="23" fill="1" visible="no" active="no"/>
<layer number="223" name="223bmp" color="24" fill="1" visible="no" active="no"/>
<layer number="224" name="224bmp" color="25" fill="1" visible="no" active="no"/>
<layer number="225" name="225bmp" color="7" fill="1" visible="yes" active="yes"/>
<layer number="226" name="226bmp" color="7" fill="1" visible="yes" active="yes"/>
<layer number="227" name="227bmp" color="7" fill="1" visible="yes" active="yes"/>
<layer number="228" name="228bmp" color="7" fill="1" visible="yes" active="yes"/>
<layer number="229" name="229bmp" color="7" fill="1" visible="yes" active="yes"/>
<layer number="230" name="230bmp" color="7" fill="1" visible="yes" active="yes"/>
<layer number="231" name="Eagle3D_PG1" color="14" fill="1" visible="no" active="yes"/>
<layer number="232" name="Eagle3D_PG2" color="14" fill="2" visible="no" active="yes"/>
<layer number="233" name="Eagle3D_PG3" color="14" fill="4" visible="no" active="yes"/>
<layer number="248" name="Housing" color="7" fill="1" visible="no" active="yes"/>
<layer number="249" name="Edge" color="7" fill="1" visible="no" active="yes"/>
<layer number="250" name="Descript" color="7" fill="1" visible="yes" active="yes"/>
<layer number="251" name="SMDround" color="7" fill="1" visible="yes" active="yes"/>
<layer number="254" name="cooling" color="7" fill="1" visible="yes" active="yes"/>
<layer number="255" name="routoute" color="7" fill="1" visible="yes" active="yes"/>
</layers>
<schematic xreflabel="%F%N/%S.%C%R" xrefpart="/%S.%C%R">
<libraries>
<library name="GadgetFactory">
<packages>
<package name="0201">
<description>Source: http://www.vishay.com/docs/20008/dcrcw.pdf</description>
<smd name="1" x="-0.255" y="0" dx="0.28" dy="0.43" layer="1"/>
<smd name="2" x="0.255" y="0" dx="0.28" dy="0.43" layer="1"/>
<text x="-0.4" y="0.6" size="0.8128" layer="25" font="vector" ratio="16">&gt;NAME</text>
<text x="-0.4" y="-1.4" size="0.8128" layer="27" font="vector" ratio="16">&gt;VALUE</text>
<rectangle x1="-0.3" y1="-0.15" x2="-0.15" y2="0.15" layer="51"/>
<rectangle x1="0.15" y1="-0.15" x2="0.3" y2="0.15" layer="51"/>
<rectangle x1="-0.15" y1="-0.15" x2="0.15" y2="0.15" layer="21"/>
</package>
<package name="0402">
<description>IPC 1005 chip</description>
<wire x1="-0.245" y1="0.224" x2="0.245" y2="0.224" width="0.1524" layer="51"/>
<wire x1="0.245" y1="-0.224" x2="-0.245" y2="-0.224" width="0.1524" layer="51"/>
<wire x1="-1.05" y1="0.4" x2="-1" y2="0.45" width="0" layer="39"/>
<wire x1="-1" y1="0.45" x2="1" y2="0.45" width="0" layer="39"/>
<wire x1="1" y1="0.45" x2="1.05" y2="0.4" width="0" layer="39"/>
<wire x1="1.05" y1="0.4" x2="1.05" y2="-0.4" width="0" layer="39"/>
<wire x1="1.05" y1="-0.4" x2="1" y2="-0.45" width="0" layer="39"/>
<wire x1="1" y1="-0.45" x2="-1" y2="-0.45" width="0" layer="39"/>
<wire x1="-1" y1="-0.45" x2="-1.05" y2="-0.4" width="0" layer="39"/>
<wire x1="-1.05" y1="-0.4" x2="-1.05" y2="0.4" width="0" layer="39"/>
<wire x1="-1.1" y1="0.5" x2="-0.3" y2="0.5" width="0.127" layer="21"/>
<wire x1="0.3" y1="0.5" x2="1.1" y2="0.5" width="0.127" layer="21"/>
<wire x1="1.1" y1="0.5" x2="1.1" y2="-0.5" width="0.127" layer="21"/>
<wire x1="1.1" y1="-0.5" x2="0.3" y2="-0.5" width="0.127" layer="21"/>
<wire x1="-0.3" y1="-0.5" x2="-1.1" y2="-0.5" width="0.127" layer="21"/>
<wire x1="-1.1" y1="-0.5" x2="-1.1" y2="0.5" width="0.127" layer="21"/>
<wire x1="-0.7" y1="0.1" x2="-0.5" y2="-0.1" width="0" layer="49"/>
<wire x1="-0.7" y1="-0.1" x2="-0.5" y2="0.1" width="0" layer="49"/>
<wire x1="0.5" y1="0.1" x2="0.7" y2="-0.1" width="0" layer="49"/>
<wire x1="0.5" y1="-0.1" x2="0.7" y2="0.1" width="0" layer="49"/>
<wire x1="-0.2" y1="0.3" x2="0.2" y2="0.3" width="0" layer="41"/>
<wire x1="0.2" y1="0.3" x2="0.2" y2="-0.3" width="0" layer="41"/>
<wire x1="0.2" y1="-0.3" x2="-0.2" y2="-0.3" width="0" layer="41"/>
<wire x1="-0.2" y1="-0.3" x2="-0.2" y2="0.3" width="0" layer="41"/>
<smd name="1" x="-0.6" y="0" dx="0.6" dy="0.6" layer="1" roundness="20"/>
<smd name="2" x="0.6" y="0" dx="0.6" dy="0.6" layer="1" roundness="20"/>
<text x="-1.1" y="0.8" size="0.8128" layer="25" font="vector" ratio="16">&gt;NAME</text>
<text x="-1" y="0.35" size="0.05" layer="27" font="vector" ratio="0">&gt;VALUE</text>
<text x="-1" y="-0.4" size="0.05" layer="27" font="vector" ratio="0">&gt;VALUE</text>
<text x="1" y="0.4" size="0.05" layer="27" font="vector" ratio="0" rot="R180">&gt;VALUE</text>
<text x="1" y="-0.35" size="0.05" layer="27" font="vector" ratio="0" rot="R180">&gt;VALUE</text>
<rectangle x1="-0.554" y1="-0.3048" x2="-0.254" y2="0.2951" layer="51"/>
<rectangle x1="0.2588" y1="-0.3048" x2="0.5588" y2="0.2951" layer="51"/>
<rectangle x1="-0.1999" y1="-0.4001" x2="0.1999" y2="0.4001" layer="35"/>
</package>
<package name="0402S">
<wire x1="-0.5" y1="-0.25" x2="-0.5" y2="0.25" width="0.1" layer="51"/>
<wire x1="-0.5" y1="0.25" x2="0.5" y2="0.25" width="0.1" layer="51"/>
<wire x1="0.5" y1="0.25" x2="0.5" y2="-0.25" width="0.1" layer="51"/>
<wire x1="0.5" y1="-0.25" x2="-0.5" y2="-0.25" width="0.1" layer="51"/>
<wire x1="0" y1="-0.2" x2="0" y2="0.2" width="0.127" layer="21"/>
<wire x1="-0.8" y1="0.45" x2="0.8" y2="0.45" width="0" layer="39"/>
<wire x1="0.8" y1="0.45" x2="0.8" y2="-0.45" width="0" layer="39"/>
<wire x1="0.8" y1="-0.45" x2="-0.8" y2="-0.45" width="0" layer="39"/>
<wire x1="-0.8" y1="-0.45" x2="-0.8" y2="0.45" width="0" layer="39"/>
<wire x1="-0.85" y1="0.5" x2="-0.2" y2="0.5" width="0.127" layer="21"/>
<wire x1="0.2" y1="0.5" x2="0.85" y2="0.5" width="0.127" layer="21"/>
<wire x1="0.85" y1="0.5" x2="0.85" y2="-0.5" width="0.127" layer="21"/>
<wire x1="0.85" y1="-0.5" x2="0.2" y2="-0.5" width="0.127" layer="21"/>
<wire x1="-0.2" y1="-0.5" x2="-0.85" y2="-0.5" width="0.127" layer="21"/>
<wire x1="-0.85" y1="-0.5" x2="-0.85" y2="0.5" width="0.127" layer="21"/>
<smd name="1" x="-0.4" y="0" dx="0.6" dy="0.5" layer="1" roundness="20" rot="R90"/>
<smd name="2" x="0.4" y="0" dx="0.6" dy="0.5" layer="1" roundness="20" rot="R90"/>
<text x="-0.85" y="0.7" size="0.8128" layer="25" font="vector" ratio="16">&gt;NAME</text>
<text x="-0.75" y="0.35" size="0.05" layer="27" font="vector" ratio="0">&gt;VALUE</text>
<text x="-0.75" y="-0.4" size="0.05" layer="27" font="vector" ratio="0">&gt;VALUE</text>
<text x="0.75" y="0.4" size="0.05" layer="27" font="vector" ratio="0" rot="R180">&gt;VALUE</text>
<text x="0.75" y="-0.35" size="0.05" layer="27" font="vector" ratio="0" rot="R180">&gt;VALUE</text>
<rectangle x1="-0.5" y1="-0.2" x2="-0.2" y2="0.2" layer="51"/>
<rectangle x1="0.2" y1="-0.2" x2="0.5" y2="0.2" layer="51"/>
</package>
<package name="0504">
<description>IPC 1310 chip</description>
<wire x1="-1.473" y1="0.983" x2="1.473" y2="0.983" width="0.0508" layer="39"/>
<wire x1="1.473" y1="0.983" x2="1.473" y2="-0.983" width="0.0508" layer="39"/>
<wire x1="1.473" y1="-0.983" x2="-1.473" y2="-0.983" width="0.0508" layer="39"/>
<wire x1="-1.473" y1="-0.983" x2="-1.473" y2="0.983" width="0.0508" layer="39"/>
<wire x1="-0.294" y1="0.559" x2="0.294" y2="0.559" width="0.1016" layer="51"/>
<wire x1="-0.294" y1="-0.559" x2="0.294" y2="-0.559" width="0.1016" layer="51"/>
<smd name="1" x="-0.7" y="0" dx="1" dy="1.3" layer="1"/>
<smd name="2" x="0.7" y="0" dx="1" dy="1.3" layer="1"/>
<text x="-1.2" y="1.1" size="0.8128" layer="25" font="vector" ratio="16">&gt;NAME</text>
<text x="-1.3" y="-1.9" size="0.8128" layer="27" font="vector" ratio="16">&gt;VALUE</text>
<rectangle x1="-0.6604" y1="-0.6223" x2="-0.2804" y2="0.6276" layer="51"/>
<rectangle x1="0.2794" y1="-0.6223" x2="0.6594" y2="0.6276" layer="51"/>
<rectangle x1="-0.1001" y1="-0.4001" x2="0.1001" y2="0.4001" layer="35"/>
</package>
<package name="0603">
<description>IPC 1608 chip</description>
<wire x1="-0.432" y1="-0.356" x2="0.432" y2="-0.356" width="0.1524" layer="51"/>
<wire x1="0.432" y1="0.356" x2="-0.432" y2="0.356" width="0.1524" layer="51"/>
<wire x1="-1.45" y1="0.65" x2="1.45" y2="0.65" width="0" layer="39"/>
<wire x1="1.45" y1="0.65" x2="1.45" y2="-0.65" width="0" layer="39"/>
<wire x1="1.45" y1="-0.65" x2="-1.45" y2="-0.65" width="0" layer="39"/>
<wire x1="-1.45" y1="-0.65" x2="-1.45" y2="0.65" width="0" layer="39"/>
<wire x1="-1.5" y1="0.7" x2="-0.3" y2="0.7" width="0.127" layer="21"/>
<wire x1="0.3" y1="0.7" x2="1.5" y2="0.7" width="0.127" layer="21"/>
<wire x1="1.5" y1="0.7" x2="1.5" y2="-0.7" width="0.127" layer="21"/>
<wire x1="1.5" y1="-0.7" x2="0.3" y2="-0.7" width="0.127" layer="21"/>
<wire x1="-0.3" y1="-0.7" x2="-1.5" y2="-0.7" width="0.127" layer="21"/>
<wire x1="-1.5" y1="-0.7" x2="-1.5" y2="0.7" width="0.127" layer="21"/>
<wire x1="-0.9" y1="0.1" x2="-0.7" y2="-0.1" width="0" layer="49"/>
<wire x1="-0.9" y1="-0.1" x2="-0.7" y2="0.1" width="0" layer="49"/>
<wire x1="0.7" y1="0.1" x2="0.9" y2="-0.1" width="0" layer="49"/>
<wire x1="0.7" y1="-0.1" x2="0.9" y2="0.1" width="0" layer="49"/>
<wire x1="-0.2" y1="0.4" x2="0.2" y2="0.4" width="0" layer="41"/>
<wire x1="0.2" y1="0.4" x2="0.2" y2="-0.4" width="0" layer="41"/>
<wire x1="0.2" y1="-0.4" x2="-0.2" y2="-0.4" width="0" layer="41"/>
<wire x1="-0.2" y1="-0.4" x2="-0.2" y2="0.4" width="0" layer="41"/>
<smd name="1" x="-0.8" y="0" dx="1" dy="1" layer="1" roundness="20"/>
<smd name="2" x="0.8" y="0" dx="1" dy="1" layer="1" roundness="20"/>
<text x="-1.5" y="0.9" size="0.8128" layer="25" font="vector" ratio="16">&gt;NAME</text>
<text x="-1.4" y="0.55" size="0.05" layer="27" font="vector" ratio="0">&gt;VALUE</text>
<text x="-1.4" y="-0.6" size="0.05" layer="27" font="vector" ratio="0">&gt;VALUE</text>
<text x="1.4" y="0.6" size="0.05" layer="27" font="vector" ratio="0" rot="R180">&gt;VALUE</text>
<text x="1.4" y="-0.55" size="0.05" layer="27" font="vector" ratio="0" rot="R180">&gt;VALUE</text>
<rectangle x1="0.4318" y1="-0.4318" x2="0.8382" y2="0.4318" layer="51"/>
<rectangle x1="-0.8382" y1="-0.4318" x2="-0.4318" y2="0.4318" layer="51"/>
<rectangle x1="-0.1999" y1="-0.4001" x2="0.1999" y2="0.4001" layer="35"/>
</package>
<package name="0805">
<description>IPC 2012 chip</description>
<wire x1="-0.41" y1="0.635" x2="0.41" y2="0.635" width="0.1524" layer="51"/>
<wire x1="-0.41" y1="-0.635" x2="0.41" y2="-0.635" width="0.1524" layer="51"/>
<wire x1="-1.75" y1="0.85" x2="1.75" y2="0.85" width="0" layer="39"/>
<wire x1="1.75" y1="0.85" x2="1.75" y2="-0.85" width="0" layer="39"/>
<wire x1="1.75" y1="-0.85" x2="-1.75" y2="-0.85" width="0" layer="39"/>
<wire x1="-1.75" y1="-0.85" x2="-1.75" y2="0.85" width="0" layer="39"/>
<wire x1="-1.8" y1="0.9" x2="-0.4" y2="0.9" width="0.127" layer="21"/>
<wire x1="0.4" y1="0.9" x2="1.8" y2="0.9" width="0.127" layer="21"/>
<wire x1="1.8" y1="0.9" x2="1.8" y2="-0.9" width="0.127" layer="21"/>
<wire x1="1.8" y1="-0.9" x2="0.4" y2="-0.9" width="0.127" layer="21"/>
<wire x1="-0.4" y1="-0.9" x2="-1.8" y2="-0.9" width="0.127" layer="21"/>
<wire x1="-1.8" y1="-0.9" x2="-1.8" y2="0.9" width="0.127" layer="21"/>
<wire x1="-1.1" y1="0.1" x2="-0.9" y2="-0.1" width="0" layer="49"/>
<wire x1="-1.1" y1="-0.1" x2="-0.9" y2="0.1" width="0" layer="49"/>
<wire x1="0.9" y1="0.1" x2="1.1" y2="-0.1" width="0" layer="49"/>
<wire x1="0.9" y1="-0.1" x2="1.1" y2="0.1" width="0" layer="49"/>
<wire x1="-0.3" y1="0.7" x2="0.3" y2="0.7" width="0" layer="41"/>
<wire x1="0.3" y1="0.7" x2="0.3" y2="-0.7" width="0" layer="41"/>
<wire x1="0.3" y1="-0.7" x2="-0.3" y2="-0.7" width="0" layer="41"/>
<wire x1="-0.3" y1="-0.7" x2="-0.3" y2="0.7" width="0" layer="41"/>
<smd name="1" x="-1" y="0" dx="1.2" dy="1.4" layer="1" roundness="20"/>
<smd name="2" x="1" y="0" dx="1.2" dy="1.4" layer="1" roundness="20"/>
<text x="-1.8" y="1.2" size="0.8128" layer="25" font="vector" ratio="16">&gt;NAME</text>
<text x="-1.7" y="0.75" size="0.05" layer="27" font="vector" ratio="0">&gt;VALUE</text>
<text x="-1.7" y="-0.8" size="0.05" layer="27" font="vector" ratio="0">&gt;VALUE</text>
<text x="1.7" y="0.8" size="0.05" layer="27" font="vector" ratio="0" rot="R180">&gt;VALUE</text>
<text x="1.7" y="-0.75" size="0.05" layer="27" font="vector" ratio="0" rot="R180">&gt;VALUE</text>
<rectangle x1="0.4064" y1="-0.6985" x2="1.0564" y2="0.7015" layer="51"/>
<rectangle x1="-1.0668" y1="-0.6985" x2="-0.4168" y2="0.7015" layer="51"/>
<rectangle x1="-0.1999" y1="-0.5001" x2="0.1999" y2="0.5001" layer="35"/>
</package>
<package name="01005">
<smd name="1" x="-1.27" y="0" dx="1.27" dy="1.27" layer="1"/>
<smd name="2" x="1.27" y="0" dx="1.27" dy="1.27" layer="1"/>
</package>
<package name="1206">
<description>IPC 3216 chip</description>
<wire x1="0.9525" y1="-0.8128" x2="-0.9652" y2="-0.8128" width="0.1524" layer="51"/>
<wire x1="0.9525" y1="0.8128" x2="-0.9652" y2="0.8128" width="0.1524" layer="51"/>
<wire x1="-2.35" y1="1.05" x2="2.35" y2="1.05" width="0" layer="39"/>
<wire x1="2.35" y1="1.05" x2="2.35" y2="-1.05" width="0" layer="39"/>
<wire x1="2.35" y1="-1.05" x2="-2.35" y2="-1.05" width="0" layer="39"/>
<wire x1="-2.35" y1="-1.05" x2="-2.35" y2="1.05" width="0" layer="39"/>
<wire x1="-2.4" y1="1.1" x2="-0.6" y2="1.1" width="0.127" layer="21"/>
<wire x1="0.6" y1="1.1" x2="2.4" y2="1.1" width="0.127" layer="21"/>
<wire x1="2.4" y1="1.1" x2="2.4" y2="-1.1" width="0.127" layer="21"/>
<wire x1="2.4" y1="-1.1" x2="0.6" y2="-1.1" width="0.127" layer="21"/>
<wire x1="-0.6" y1="-1.1" x2="-2.4" y2="-1.1" width="0.127" layer="21"/>
<wire x1="-2.4" y1="-1.1" x2="-2.4" y2="1.1" width="0.127" layer="21"/>
<wire x1="-1.5" y1="0.1" x2="-1.3" y2="-0.1" width="0" layer="49"/>
<wire x1="-1.5" y1="-0.1" x2="-1.3" y2="0.1" width="0" layer="49"/>
<wire x1="1.3" y1="0.1" x2="1.5" y2="-0.1" width="0" layer="49"/>
<wire x1="1.3" y1="-0.1" x2="1.5" y2="0.1" width="0" layer="49"/>
<wire x1="-0.5" y1="0.9" x2="0.5" y2="0.9" width="0" layer="41"/>
<wire x1="0.5" y1="0.9" x2="0.5" y2="-0.9" width="0" layer="41"/>
<wire x1="0.5" y1="-0.9" x2="-0.5" y2="-0.9" width="0" layer="41"/>
<wire x1="-0.5" y1="-0.9" x2="-0.5" y2="0.9" width="0" layer="41"/>
<smd name="1" x="-1.4" y="0" dx="1.6" dy="1.8" layer="1" roundness="20"/>
<smd name="2" x="1.4" y="0" dx="1.6" dy="1.8" layer="1" roundness="20"/>
<text x="-2.4" y="1.4" size="0.8128" layer="25" font="vector" ratio="16">&gt;NAME</text>
<text x="-2.3" y="0.95" size="0.05" layer="27" font="vector" ratio="0">&gt;VALUE</text>
<text x="-2.3" y="-1" size="0.05" layer="27" font="vector" ratio="0">&gt;VALUE</text>
<text x="2.3" y="1" size="0.05" layer="27" font="vector" ratio="0" rot="R180">&gt;VALUE</text>
<text x="2.3" y="-0.95" size="0.05" layer="27" font="vector" ratio="0" rot="R180">&gt;VALUE</text>
<rectangle x1="-1.6891" y1="-0.8763" x2="-0.9525" y2="0.8763" layer="51"/>
<rectangle x1="0.9525" y1="-0.8763" x2="1.6891" y2="0.8763" layer="51"/>
<rectangle x1="-0.3" y1="-0.7" x2="0.3" y2="0.7" layer="35"/>
</package>
<package name="1210">
<description>IPC 3225 chip</description>
<wire x1="-0.913" y1="1.219" x2="0.939" y2="1.219" width="0.1524" layer="51"/>
<wire x1="-0.913" y1="-1.219" x2="0.939" y2="-1.219" width="0.1524" layer="51"/>
<wire x1="-2.35" y1="1.45" x2="2.35" y2="1.45" width="0" layer="39"/>
<wire x1="2.35" y1="1.45" x2="2.35" y2="-1.45" width="0" layer="39"/>
<wire x1="2.35" y1="-1.45" x2="-2.35" y2="-1.45" width="0" layer="39"/>
<wire x1="-2.35" y1="-1.45" x2="-2.35" y2="1.45" width="0" layer="39"/>
<wire x1="-2.4" y1="1.5" x2="2.4" y2="1.5" width="0.127" layer="21"/>
<wire x1="2.4" y1="1.5" x2="2.4" y2="-1.5" width="0.127" layer="21"/>
<wire x1="2.4" y1="-1.5" x2="-2.4" y2="-1.5" width="0.127" layer="21"/>
<wire x1="-2.4" y1="-1.5" x2="-2.4" y2="1.5" width="0.127" layer="21"/>
<wire x1="-0.5" y1="1.3" x2="0.5" y2="1.3" width="0" layer="41"/>
<wire x1="0.5" y1="1.3" x2="0.5" y2="-1.3" width="0" layer="41"/>
<wire x1="0.5" y1="-1.3" x2="-0.5" y2="-1.3" width="0" layer="41"/>
<wire x1="-0.5" y1="-1.3" x2="-0.5" y2="1.3" width="0" layer="41"/>
<smd name="1" x="-1.4" y="0" dx="1.6" dy="2.6" layer="1" roundness="20"/>
<smd name="2" x="1.4" y="0" dx="1.6" dy="2.6" layer="1" roundness="20"/>
<text x="-2.4" y="1.8" size="0.8128" layer="25" font="vector" ratio="16">&gt;NAME</text>
<text x="-2.3" y="1.35" size="0.05" layer="27" font="vector" ratio="0">&gt;VALUE</text>
<text x="-2.3" y="-1.4" size="0.05" layer="27" font="vector" ratio="0">&gt;VALUE</text>
<text x="2.3" y="1.4" size="0.05" layer="27" font="vector" ratio="0" rot="R180">&gt;VALUE</text>
<text x="2.3" y="-1.35" size="0.05" layer="27" font="vector" ratio="0" rot="R180">&gt;VALUE</text>
<rectangle x1="-1.651" y1="-1.3081" x2="-0.9009" y2="1.2918" layer="51"/>
<rectangle x1="0.9144" y1="-1.3081" x2="1.6645" y2="1.2918" layer="51"/>
<rectangle x1="-0.3" y1="-0.8999" x2="0.3" y2="0.8999" layer="35"/>
</package>
<package name="1806">
<description>IPC 4516 chip</description>
<smd name="1" x="-1.27" y="0" dx="1.27" dy="1.27" layer="1"/>
<smd name="2" x="1.27" y="0" dx="1.27" dy="1.27" layer="1"/>
</package>
<package name="1812">
<description>IPC 4532 chip</description>
<wire x1="-3.05" y1="1.85" x2="3.05" y2="1.85" width="0" layer="39"/>
<wire x1="3.05" y1="-1.85" x2="-3.05" y2="-1.85" width="0" layer="39"/>
<wire x1="-3.05" y1="-1.85" x2="-3.05" y2="1.85" width="0" layer="39"/>
<wire x1="-1.4732" y1="1.6002" x2="1.4732" y2="1.6002" width="0.1016" layer="51"/>
<wire x1="-1.4478" y1="-1.6002" x2="1.4732" y2="-1.6002" width="0.1016" layer="51"/>
<wire x1="3.05" y1="1.85" x2="3.05" y2="-1.85" width="0" layer="39"/>
<wire x1="-3.1" y1="1.9" x2="3.1" y2="1.9" width="0.127" layer="21"/>
<wire x1="3.1" y1="1.9" x2="3.1" y2="-1.9" width="0.127" layer="21"/>
<wire x1="3.1" y1="-1.9" x2="-3.1" y2="-1.9" width="0.127" layer="21"/>
<wire x1="-3.1" y1="-1.9" x2="-3.1" y2="1.9" width="0.127" layer="21"/>
<wire x1="-1" y1="1.7" x2="1" y2="1.7" width="0" layer="41"/>
<wire x1="1" y1="1.7" x2="1" y2="-1.7" width="0" layer="41"/>
<wire x1="1" y1="-1.7" x2="-1" y2="-1.7" width="0" layer="41"/>
<wire x1="-1" y1="-1.7" x2="-1" y2="1.7" width="0" layer="41"/>
<smd name="1" x="-2" y="0" dx="1.8" dy="3.4" layer="1" roundness="20"/>
<smd name="2" x="2" y="0" dx="1.8" dy="3.4" layer="1" roundness="20"/>
<text x="-3.1" y="2.2" size="0.8128" layer="25" font="vector" ratio="16">&gt;NAME</text>
<text x="-3" y="1.75" size="0.05" layer="27" font="vector" ratio="0">&gt;VALUE</text>
<text x="-3" y="-1.8" size="0.05" layer="27" font="vector" ratio="0">&gt;VALUE</text>
<text x="3" y="1.8" size="0.05" layer="27" font="vector" ratio="0" rot="R180">&gt;VALUE</text>
<text x="3" y="-1.75" size="0.05" layer="27" font="vector" ratio="0" rot="R180">&gt;VALUE</text>
<rectangle x1="-2.3876" y1="-1.651" x2="-1.4376" y2="1.649" layer="51"/>
<rectangle x1="1.4478" y1="-1.651" x2="2.3978" y2="1.649" layer="51"/>
<rectangle x1="-0.4001" y1="-0.7" x2="0.4001" y2="0.7" layer="35"/>
</package>
<package name="1825">
<description>IPC 4564 chip</description>
<wire x1="-2.973" y1="3.483" x2="2.973" y2="3.483" width="0.0508" layer="39"/>
<wire x1="2.973" y1="-3.483" x2="-2.973" y2="-3.483" width="0.0508" layer="39"/>
<wire x1="-2.973" y1="-3.483" x2="-2.973" y2="3.483" width="0.0508" layer="39"/>
<wire x1="-1.4986" y1="3.2766" x2="1.4732" y2="3.2766" width="0.1016" layer="51"/>
<wire x1="-1.4732" y1="-3.2766" x2="1.4986" y2="-3.2766" width="0.1016" layer="51"/>
<wire x1="2.973" y1="3.483" x2="2.973" y2="-3.483" width="0.0508" layer="39"/>
<smd name="1" x="-1.95" y="0" dx="1.9" dy="6.8" layer="1"/>
<smd name="2" x="1.95" y="0" dx="1.9" dy="6.8" layer="1"/>
<text x="-2.9" y="3.7" size="0.8128" layer="25" font="vector" ratio="16">&gt;NAME</text>
<text x="-2.9" y="-4.6" size="0.8128" layer="27" font="vector" ratio="16">&gt;VALUE</text>
<rectangle x1="-2.413" y1="-3.3528" x2="-1.463" y2="3.3472" layer="51"/>
<rectangle x1="1.4478" y1="-3.3528" x2="2.3978" y2="3.3472" layer="51"/>
<rectangle x1="-0.5001" y1="-1" x2="0.5001" y2="1" layer="35"/>
</package>
<package name="2010">
<description>IPC 5025 chip</description>
<wire x1="-3.25" y1="1.45" x2="3.25" y2="1.45" width="0" layer="39"/>
<wire x1="3.25" y1="1.45" x2="3.25" y2="-1.45" width="0" layer="39"/>
<wire x1="3.25" y1="-1.45" x2="-3.25" y2="-1.45" width="0" layer="39"/>
<wire x1="-3.25" y1="-1.45" x2="-3.25" y2="1.45" width="0" layer="39"/>
<wire x1="-1.662" y1="1.245" x2="1.662" y2="1.245" width="0.1524" layer="51"/>
<wire x1="-1.637" y1="-1.245" x2="1.687" y2="-1.245" width="0.1524" layer="51"/>
<wire x1="-3.3" y1="1.5" x2="3.3" y2="1.5" width="0.127" layer="21"/>
<wire x1="3.3" y1="1.5" x2="3.3" y2="-1.5" width="0.127" layer="21"/>
<wire x1="3.3" y1="-1.5" x2="-3.3" y2="-1.5" width="0.127" layer="21"/>
<wire x1="-3.3" y1="-1.5" x2="-3.3" y2="1.5" width="0.127" layer="21"/>
<wire x1="-1.2" y1="1.3" x2="1.2" y2="1.3" width="0" layer="41"/>
<wire x1="1.2" y1="1.3" x2="1.2" y2="-1.3" width="0" layer="41"/>
<wire x1="1.2" y1="-1.3" x2="-1.2" y2="-1.3" width="0" layer="41"/>
<wire x1="-1.2" y1="-1.3" x2="-1.2" y2="1.3" width="0" layer="41"/>
<smd name="1" x="-2.2" y="0" dx="1.8" dy="2.6" layer="1" roundness="20"/>
<smd name="2" x="2.2" y="0" dx="1.8" dy="2.6" layer="1" roundness="20"/>
<text x="-3.3" y="1.8" size="0.8128" layer="25" font="vector" ratio="16">&gt;NAME</text>
<text x="-3.2" y="1.35" size="0.05" layer="27" font="vector" ratio="0">&gt;VALUE</text>
<text x="-3.2" y="-1.4" size="0.05" layer="27" font="vector" ratio="0">&gt;VALUE</text>
<text x="3.2" y="1.4" size="0.05" layer="27" font="vector" ratio="0" rot="R180">&gt;VALUE</text>
<text x="3.2" y="-1.35" size="0.05" layer="27" font="vector" ratio="0" rot="R180">&gt;VALUE</text>
<rectangle x1="-2.4892" y1="-1.3208" x2="-1.6393" y2="1.3292" layer="51"/>
<rectangle x1="1.651" y1="-1.3208" x2="2.5009" y2="1.3292" layer="51"/>
<rectangle x1="-0.5001" y1="-1" x2="0.5001" y2="1" layer="35"/>
</package>
<package name="2512">
<description>IPC 6332 chip</description>
<wire x1="-2.362" y1="1.473" x2="2.387" y2="1.473" width="0.1524" layer="51"/>
<wire x1="-2.362" y1="-1.473" x2="2.387" y2="-1.473" width="0.1524" layer="51"/>
<wire x1="3.9" y1="1.8" x2="3.9" y2="-1.8" width="0.127" layer="21"/>
<wire x1="3.9" y1="-1.8" x2="-3.9" y2="-1.8" width="0.127" layer="21"/>
<wire x1="-3.9" y1="-1.8" x2="-3.9" y2="1.8" width="0.127" layer="21"/>
<wire x1="-3.9" y1="1.8" x2="3.9" y2="1.8" width="0.127" layer="21"/>
<wire x1="-1.7" y1="1.3" x2="-1.5" y2="1.5" width="0" layer="41"/>
<wire x1="-1.5" y1="1.5" x2="1.5" y2="1.5" width="0" layer="41"/>
<wire x1="1.5" y1="1.5" x2="1.7" y2="1.3" width="0" layer="41"/>
<wire x1="1.7" y1="1.3" x2="1.7" y2="-1.3" width="0" layer="41"/>
<wire x1="1.7" y1="-1.3" x2="1.5" y2="-1.5" width="0" layer="41"/>
<wire x1="1.5" y1="-1.5" x2="-1.5" y2="-1.5" width="0" layer="41"/>
<wire x1="-1.5" y1="-1.5" x2="-1.7" y2="-1.3" width="0" layer="41"/>
<wire x1="-1.7" y1="-1.3" x2="-1.7" y2="1.3" width="0" layer="41"/>
<smd name="1" x="-2.8" y="0" dx="1.8" dy="3.2" layer="1" roundness="20"/>
<smd name="2" x="2.8" y="0" dx="1.8" dy="3.2" layer="1" roundness="20"/>
<text x="-3.9" y="2.1" size="0.8128" layer="25" font="vector" ratio="16">&gt;NAME</text>
<text x="-3.8" y="1.65" size="0.05" layer="27" font="vector" ratio="0">&gt;VALUE</text>
<text x="-3.8" y="-1.7" size="0.05" layer="27" font="vector" ratio="0">&gt;VALUE</text>
<text x="3.8" y="1.7" size="0.05" layer="27" font="vector" ratio="0" rot="R180">&gt;VALUE</text>
<text x="3.8" y="-1.65" size="0.05" layer="27" font="vector" ratio="0" rot="R180">&gt;VALUE</text>
<rectangle x1="-3.2004" y1="-1.5494" x2="-2.3505" y2="1.5507" layer="51"/>
<rectangle x1="2.3622" y1="-1.5494" x2="3.2121" y2="1.5507" layer="51"/>
<rectangle x1="-0.5001" y1="-1" x2="0.5001" y2="1" layer="35"/>
</package>
<package name="3616">
<smd name="1" x="-1.27" y="0" dx="1.27" dy="1.27" layer="1"/>
<smd name="2" x="1.27" y="0" dx="1.27" dy="1.27" layer="1"/>
</package>
<package name="4022">
<smd name="1" x="-1.27" y="0" dx="1.27" dy="1.27" layer="1"/>
<smd name="2" x="1.27" y="0" dx="1.27" dy="1.27" layer="1"/>
</package>
<package name="DUALRESISTOR">
<wire x1="-0.41" y1="0.635" x2="0.41" y2="0.635" width="0.1524" layer="51"/>
<wire x1="-0.41" y1="-0.635" x2="0.41" y2="-0.635" width="0.1524" layer="51"/>
<wire x1="-1.75" y1="0.85" x2="1.75" y2="0.85" width="0" layer="39"/>
<wire x1="1.75" y1="0.85" x2="1.75" y2="-0.85" width="0" layer="39"/>
<wire x1="1.75" y1="-0.85" x2="-1.75" y2="-0.85" width="0" layer="39"/>
<wire x1="-1.75" y1="-0.85" x2="-1.75" y2="0.85" width="0" layer="39"/>
<wire x1="-1.8" y1="0.9" x2="-0.4" y2="0.9" width="0.127" layer="21"/>
<wire x1="0.4" y1="0.9" x2="1.8" y2="0.9" width="0.127" layer="21"/>
<wire x1="1.8" y1="0.9" x2="1.8" y2="-0.9" width="0.127" layer="21"/>
<wire x1="1.8" y1="-0.9" x2="0.4" y2="-0.9" width="0.127" layer="21"/>
<wire x1="-0.4" y1="-0.9" x2="-1.8" y2="-0.9" width="0.127" layer="21"/>
<wire x1="-1.8" y1="-0.9" x2="-1.8" y2="0.9" width="0.127" layer="21"/>
<wire x1="-1.1" y1="0.1" x2="-0.9" y2="-0.1" width="0" layer="49"/>
<wire x1="-1.1" y1="-0.1" x2="-0.9" y2="0.1" width="0" layer="49"/>
<wire x1="0.9" y1="0.1" x2="1.1" y2="-0.1" width="0" layer="49"/>
<wire x1="0.9" y1="-0.1" x2="1.1" y2="0.1" width="0" layer="49"/>
<wire x1="-0.3" y1="0.7" x2="0.3" y2="0.7" width="0" layer="41"/>
<wire x1="0.3" y1="0.7" x2="0.3" y2="-0.7" width="0" layer="41"/>
<wire x1="0.3" y1="-0.7" x2="-0.3" y2="-0.7" width="0" layer="41"/>
<wire x1="-0.3" y1="-0.7" x2="-0.3" y2="0.7" width="0" layer="41"/>
<wire x1="-2.54" y1="0" x2="-1.27" y2="0" width="0.3048" layer="1"/>
<wire x1="2.54" y1="0" x2="1.27" y2="0" width="0.3048" layer="1"/>
<pad name="1" x="-2.5429" y="0" drill="0.8128" shape="octagon"/>
<pad name="2" x="2.5371" y="0" drill="0.8128" shape="octagon"/>
<smd name="3" x="-1" y="0" dx="1.2" dy="1.4" layer="1" roundness="20"/>
<smd name="4" x="1" y="0" dx="1.2" dy="1.4" layer="1" roundness="20"/>
<text x="-1.8" y="1.2" size="0.8128" layer="25" font="vector" ratio="16">&gt;NAME</text>
<text x="-1.7" y="0.75" size="0.05" layer="27" font="vector" ratio="0">&gt;VALUE</text>
<text x="-1.7" y="-0.8" size="0.05" layer="27" font="vector" ratio="0">&gt;VALUE</text>
<text x="1.7" y="0.8" size="0.05" layer="27" font="vector" ratio="0" rot="R180">&gt;VALUE</text>
<text x="1.7" y="-0.75" size="0.05" layer="27" font="vector" ratio="0" rot="R180">&gt;VALUE</text>
<rectangle x1="0.4064" y1="-0.6985" x2="1.0564" y2="0.7015" layer="51"/>
<rectangle x1="-1.0668" y1="-0.6985" x2="-0.4168" y2="0.7015" layer="51"/>
<rectangle x1="-0.1999" y1="-0.5001" x2="0.1999" y2="0.5001" layer="35"/>
</package>
<package name="TANT_A">
<wire x1="-1.6" y1="1.2" x2="1.6" y2="1.2" width="0.1016" layer="51"/>
<wire x1="1.6" y1="1.2" x2="1.6" y2="-1.2" width="0.1016" layer="51"/>
<wire x1="1.6" y1="-1.2" x2="-1.6" y2="-1.2" width="0.1016" layer="51"/>
<wire x1="-1.6" y1="-1.2" x2="-1.6" y2="1.2" width="0.1016" layer="51"/>
<wire x1="2.5" y1="0" x2="2.8" y2="0" width="0.127" layer="21"/>
<wire x1="2.8" y1="0" x2="3.1" y2="0" width="0.127" layer="21"/>
<wire x1="2.8" y1="0.3" x2="2.8" y2="0" width="0.127" layer="21"/>
<wire x1="2.8" y1="0" x2="2.8" y2="-0.3" width="0.127" layer="21"/>
<smd name="+" x="1.4" y="0" dx="1.8" dy="2.2" layer="1" roundness="20"/>
<smd name="-" x="-1.4" y="0" dx="1.8" dy="2.2" layer="1" roundness="20"/>
<text x="-1.6" y="1.5" size="0.8128" layer="25" font="vector" ratio="16">&gt;NAME</text>
<text x="-1.6" y="-2.3" size="0.8128" layer="27" font="vector" ratio="16">&gt;VALUE</text>
<rectangle x1="-1.75" y1="-0.6" x2="-1.625" y2="0.6" layer="51"/>
<rectangle x1="1.625" y1="-0.6" x2="1.75" y2="0.6" layer="51"/>
<rectangle x1="0.95" y1="-1.225" x2="1.25" y2="1.225" layer="51"/>
</package>
</packages>
<symbols>
<symbol name="FRAME-A4L-LOC">
<wire x1="0" y1="0" x2="50.8" y2="0" width="0.1016" layer="94"/>
<wire x1="50.8" y1="0" x2="98.425" y2="0" width="0.1016" layer="94"/>
<wire x1="98.425" y1="0" x2="146.05" y2="0" width="0.1016" layer="94"/>
<wire x1="146.05" y1="0" x2="193.675" y2="0" width="0.1016" layer="94"/>
<wire x1="193.675" y1="0" x2="241.3" y2="0" width="0.1016" layer="94"/>
<wire x1="260.35" y1="0" x2="260.35" y2="53.975" width="0.1016" layer="94"/>
<wire x1="260.35" y1="53.975" x2="260.35" y2="104.775" width="0.1016" layer="94"/>
<wire x1="260.35" y1="104.775" x2="260.35" y2="155.575" width="0.1016" layer="94"/>
<wire x1="260.35" y1="155.575" x2="260.35" y2="179.07" width="0.1016" layer="94"/>
<wire x1="0" y1="179.07" x2="0" y2="155.575" width="0.1016" layer="94"/>
<wire x1="0" y1="155.575" x2="0" y2="104.775" width="0.1016" layer="94"/>
<wire x1="0" y1="104.775" x2="0" y2="53.975" width="0.1016" layer="94"/>
<wire x1="0" y1="53.975" x2="0" y2="0" width="0.1016" layer="94"/>
<wire x1="3.175" y1="3.175" x2="50.8" y2="3.175" width="0.1016" layer="94"/>
<wire x1="50.8" y1="3.175" x2="98.425" y2="3.175" width="0.1016" layer="94"/>
<wire x1="257.175" y1="3.175" x2="257.175" y2="8.255" width="0.1016" layer="94"/>
<wire x1="257.175" y1="8.255" x2="257.175" y2="13.335" width="0.1016" layer="94"/>
<wire x1="257.175" y1="13.335" x2="257.175" y2="18.415" width="0.1016" layer="94"/>
<wire x1="257.175" y1="18.415" x2="257.175" y2="23.495" width="0.1016" layer="94"/>
<wire x1="257.175" y1="23.495" x2="257.175" y2="53.975" width="0.1016" layer="94"/>
<wire x1="257.175" y1="53.975" x2="257.175" y2="104.775" width="0.1016" layer="94"/>
<wire x1="257.175" y1="104.775" x2="257.175" y2="155.575" width="0.1016" layer="94"/>
<wire x1="257.175" y1="155.575" x2="257.175" y2="175.895" width="0.1016" layer="94"/>
<wire x1="193.675" y1="175.895" x2="146.05" y2="175.895" width="0.1016" layer="94"/>
<wire x1="146.05" y1="175.895" x2="98.425" y2="175.895" width="0.1016" layer="94"/>
<wire x1="98.425" y1="175.895" x2="50.8" y2="175.895" width="0.1016" layer="94"/>
<wire x1="50.8" y1="175.895" x2="3.175" y2="175.895" width="0.1016" layer="94"/>
<wire x1="3.175" y1="175.895" x2="3.175" y2="155.575" width="0.1016" layer="94"/>
<wire x1="3.175" y1="155.575" x2="3.175" y2="104.775" width="0.1016" layer="94"/>
<wire x1="3.175" y1="104.775" x2="3.175" y2="53.975" width="0.1016" layer="94"/>
<wire x1="3.175" y1="53.975" x2="3.175" y2="3.175" width="0.1016" layer="94"/>
<wire x1="193.675" y1="179.07" x2="146.05" y2="179.07" width="0.1016" layer="94"/>
<wire x1="146.05" y1="179.07" x2="98.425" y2="179.07" width="0.1016" layer="94"/>
<wire x1="98.425" y1="179.07" x2="50.8" y2="179.07" width="0.1016" layer="94"/>
<wire x1="50.8" y1="179.07" x2="0" y2="179.07" width="0.1016" layer="94"/>
<wire x1="193.675" y1="179.07" x2="193.675" y2="175.895" width="0.1016" layer="94"/>
<wire x1="193.675" y1="3.175" x2="193.675" y2="0" width="0.1016" layer="94"/>
<wire x1="0" y1="104.775" x2="3.175" y2="104.775" width="0.1016" layer="94"/>
<wire x1="257.175" y1="155.575" x2="260.35" y2="155.575" width="0.1016" layer="94"/>
<wire x1="98.425" y1="175.895" x2="98.425" y2="179.07" width="0.1016" layer="94"/>
<wire x1="98.425" y1="3.175" x2="98.425" y2="0" width="0.1016" layer="94"/>
<wire x1="0" y1="53.975" x2="3.175" y2="53.975" width="0.1016" layer="94"/>
<wire x1="257.175" y1="104.775" x2="260.35" y2="104.775" width="0.1016" layer="94"/>
<wire x1="0" y1="155.575" x2="3.175" y2="155.575" width="0.1016" layer="94"/>
<wire x1="50.8" y1="175.895" x2="50.8" y2="179.07" width="0.1016" layer="94"/>
<wire x1="257.175" y1="53.975" x2="260.35" y2="53.975" width="0.1016" layer="94"/>
<wire x1="146.05" y1="175.895" x2="146.05" y2="179.07" width="0.1016" layer="94"/>
<wire x1="241.3" y1="179.07" x2="241.3" y2="175.895" width="0.1016" layer="94"/>
<wire x1="241.3" y1="3.175" x2="241.3" y2="0" width="0.1016" layer="94"/>
<wire x1="146.05" y1="3.175" x2="146.05" y2="0" width="0.1016" layer="94"/>
<wire x1="50.8" y1="0" x2="50.8" y2="3.175" width="0.1016" layer="94"/>
<wire x1="161.925" y1="3.175" x2="161.925" y2="23.495" width="0.6096" layer="94"/>
<wire x1="161.925" y1="23.495" x2="215.9" y2="23.495" width="0.6096" layer="94"/>
<wire x1="215.9" y1="23.495" x2="257.175" y2="23.495" width="0.6096" layer="94"/>
<wire x1="247.015" y1="3.175" x2="247.015" y2="8.255" width="0.1016" layer="94"/>
<wire x1="247.015" y1="8.255" x2="257.175" y2="8.255" width="0.1016" layer="94"/>
<wire x1="247.015" y1="8.255" x2="215.9" y2="8.255" width="0.1016" layer="94"/>
<wire x1="215.9" y1="8.255" x2="215.9" y2="3.175" width="0.6096" layer="94"/>
<wire x1="215.9" y1="8.255" x2="215.9" y2="13.335" width="0.6096" layer="94"/>
<wire x1="215.9" y1="13.335" x2="257.175" y2="13.335" width="0.1016" layer="94"/>
<wire x1="215.9" y1="13.335" x2="215.9" y2="18.415" width="0.6096" layer="94"/>
<wire x1="215.9" y1="18.415" x2="257.175" y2="18.415" width="0.1016" layer="94"/>
<wire x1="215.9" y1="18.415" x2="215.9" y2="23.495" width="0.6096" layer="94"/>
<wire x1="257.175" y1="175.895" x2="193.675" y2="175.895" width="0.1016" layer="94"/>
<wire x1="260.35" y1="179.07" x2="193.675" y2="179.07" width="0.1016" layer="94"/>
<wire x1="98.425" y1="3.175" x2="193.548" y2="3.175" width="0.1016" layer="94"/>
<wire x1="193.929" y1="3.175" x2="241.3" y2="3.175" width="0.1016" layer="94"/>
<wire x1="241.427" y1="3.175" x2="257.175" y2="3.175" width="0.1016" layer="94"/>
<wire x1="241.427" y1="0" x2="260.35" y2="0" width="0.1016" layer="94"/>
<text x="24.384" y="0.254" size="2.54" layer="94" font="vector">A</text>
<text x="74.422" y="0.254" size="2.54" layer="94" font="vector">B</text>
<text x="121.158" y="0.254" size="2.54" layer="94" font="vector">C</text>
<text x="169.418" y="0.254" size="2.54" layer="94" font="vector">D</text>
<text x="216.916" y="0.254" size="2.54" layer="94" font="vector">E</text>
<text x="254.762" y="0.254" size="2.54" layer="94" font="vector">F</text>
<text x="258.064" y="28.702" size="2.54" layer="94" font="vector">1</text>
<text x="257.81" y="79.502" size="2.54" layer="94" font="vector">2</text>
<text x="257.81" y="130.302" size="2.54" layer="94" font="vector">3</text>
<text x="215.9" y="176.276" size="2.54" layer="94" font="vector">E</text>
<text x="168.148" y="176.276" size="2.54" layer="94" font="vector">D</text>
<text x="120.904" y="176.276" size="2.54" layer="94" font="vector">C</text>
<text x="72.898" y="176.276" size="2.54" layer="94" font="vector">B</text>
<text x="24.384" y="176.276" size="2.54" layer="94" font="vector">A</text>
<text x="0.762" y="130.302" size="2.54" layer="94" font="vector">3</text>
<text x="0.762" y="79.248" size="2.54" layer="94" font="vector">2</text>
<text x="1.016" y="26.67" size="2.54" layer="94" font="vector">1</text>
<text x="217.805" y="14.605" size="2.54" layer="94" font="vector">&gt;DRAWING_NAME</text>
<text x="217.805" y="9.525" size="2.286" layer="94" font="vector">&gt;LAST_DATE_TIME</text>
<text x="231.14" y="4.445" size="2.54" layer="94" font="vector">&gt;SHEET</text>
<text x="217.551" y="4.318" size="2.54" layer="94" font="vector">Sheet:</text>
<text x="249.682" y="176.276" size="2.54" layer="94" font="vector">F</text>
<text x="257.556" y="168.148" size="2.54" layer="94" font="vector">4</text>
<text x="0.508" y="168.148" size="2.54" layer="94" font="vector">4</text>
</symbol>
<symbol name="R-EU">
<wire x1="-2.54" y1="-0.889" x2="2.54" y2="-0.889" width="0.254" layer="94"/>
<wire x1="2.54" y1="0.889" x2="-2.54" y2="0.889" width="0.254" layer="94"/>
<wire x1="2.54" y1="-0.889" x2="2.54" y2="0.889" width="0.254" layer="94"/>
<wire x1="-2.54" y1="-0.889" x2="-2.54" y2="0.889" width="0.254" layer="94"/>
<circle x="-3.2385" y="-0.4445" radius="0.0635" width="0" layer="94"/>
<text x="-2.54" y="1.524" size="1.27" layer="95" font="vector">&gt;NAME</text>
<text x="-2.54" y="-2.794" size="1.27" layer="96" font="vector">&gt;VALUE</text>
<pin name="1" x="-5.08" y="0" visible="off" length="short" direction="pas" swaplevel="1"/>
<pin name="2" x="5.08" y="0" visible="off" length="short" direction="pas" swaplevel="1" rot="R180"/>
</symbol>
<symbol name="POWER-0.030=1/32W">
<text x="-1.9685" y="-0.4445" size="0.889" layer="94" font="vector">1/32W</text>
</symbol>
<symbol name="POWER-0.050=1/20W">
<text x="-1.9685" y="-0.4445" size="0.889" layer="94" font="vector">1/20W</text>
</symbol>
<symbol name="POWER-0.063=1/16W">
<text x="-1.9685" y="-0.4445" size="0.889" layer="94" font="vector">1/16W</text>
</symbol>
<symbol name="POWER-0.100=1/10W">
<text x="-1.9685" y="-0.4445" size="0.889" layer="94" font="vector">1/10W</text>
</symbol>
<symbol name="POWER-0.125=1/8W">
<text x="-1.524" y="-0.4445" size="0.889" layer="94" font="vector">1/8W</text>
</symbol>
<symbol name="POWER-0.167=1/6W">
<text x="-1.524" y="-0.4445" size="0.889" layer="94" font="vector">1/6W</text>
</symbol>
<symbol name="POWER-0.200=1/5W">
<text x="-1.524" y="-0.4445" size="0.889" layer="94" font="vector">1/5W</text>
</symbol>
<symbol name="POWER-0.250=1/4W">
<text x="-1.524" y="-0.4445" size="0.889" layer="94" font="vector">1/4W</text>
</symbol>
<symbol name="POWER-0.333=1/3W">
<text x="-1.524" y="-0.4445" size="0.889" layer="94" font="vector">1/3W</text>
</symbol>
<symbol name="POWER-0.500=1/2W">
<text x="-1.524" y="-0.4445" size="0.889" layer="94" font="vector">1/2W</text>
</symbol>
<symbol name="POWER-0.750=3/4W">
<text x="-1.524" y="-0.4445" size="0.889" layer="94" font="vector">3/4W</text>
</symbol>
<symbol name="POWER-1W">
<text x="-0.6985" y="-0.4445" size="0.889" layer="94" font="vector">1W</text>
</symbol>
<symbol name="POWER-2W">
<text x="-0.6985" y="-0.4445" size="0.889" layer="94" font="vector">2W</text>
</symbol>
<symbol name="POWER-3W">
<text x="-0.6985" y="-0.4445" size="0.889" layer="94" font="vector">3W</text>
</symbol>
<symbol name="POWER-4W">
<text x="-0.6985" y="-0.4445" size="0.889" layer="94" font="vector">4W</text>
</symbol>
<symbol name="POWER-5W">
<text x="-0.6985" y="-0.4445" size="0.889" layer="94" font="vector">5W</text>
</symbol>
<symbol name="POWER-10W">
<text x="-1.1429" y="-0.4445" size="0.889" layer="94" font="vector">10W</text>
</symbol>
<symbol name="POWER-15W">
<text x="-1.1429" y="-0.4445" size="0.889" layer="94" font="vector">15W</text>
</symbol>
<symbol name="POWER-20W">
<text x="-1.1429" y="-0.4445" size="0.889" layer="94" font="vector">20W</text>
</symbol>
<symbol name="POWER-25W">
<text x="-1.1429" y="-0.4445" size="0.889" layer="94" font="vector">25W</text>
</symbol>
<symbol name="POWER-35W">
<text x="-1.1429" y="-0.4445" size="0.889" layer="94" font="vector">35W</text>
</symbol>
<symbol name="POWER-40W">
<text x="-1.1429" y="-0.4445" size="0.889" layer="94" font="vector">40W</text>
</symbol>
<symbol name="POWER-100W">
<text x="-1.524" y="-0.4445" size="0.889" layer="94" font="vector">100W</text>
</symbol>
<symbol name="POWER-50W">
<text x="-1.1429" y="-0.4445" size="0.889" layer="94" font="vector">50W</text>
</symbol>
<symbol name="C-EU">
<wire x1="-2.54" y1="0" x2="-0.762" y2="0" width="0.1524" layer="94"/>
<wire x1="2.54" y1="0" x2="0.762" y2="0" width="0.1524" layer="94"/>
<circle x="-1.4605" y="-0.4445" radius="0.0635" width="0" layer="94"/>
<text x="-2.54" y="2.032" size="1.27" layer="95" font="vector">&gt;NAME</text>
<text x="-2.54" y="-3.302" size="1.27" layer="96" font="vector">&gt;VALUE</text>
<rectangle x1="-0.762" y1="-1.524" x2="-0.254" y2="1.524" layer="94"/>
<rectangle x1="0.254" y1="-1.524" x2="0.762" y2="1.524" layer="94"/>
<pin name="1" x="-2.54" y="0" visible="off" length="point" direction="pas" swaplevel="1"/>
<pin name="2" x="2.54" y="0" visible="off" length="point" direction="pas" swaplevel="1" rot="R180"/>
</symbol>
</symbols>
<devicesets>
<deviceset name="FRAME-A4L-LOC" prefix="FRAME">
<description>&lt;b&gt;FRAME&lt;/b&gt;&lt;p&gt;
DIN A4, landscape with location and doc. field</description>
<gates>
<gate name="A" symbol="FRAME-A4L-LOC" x="0" y="0"/>
</gates>
<devices>
<device name="">
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="R" prefix="R" uservalue="yes">
<description>&lt;B&gt;RESISTOR&lt;/B&gt;, European symbol</description>
<gates>
<gate name="A" symbol="R-EU" x="0" y="0"/>
<gate name="G$1" symbol="POWER-0.030=1/32W" x="-10.16" y="22.86" addlevel="request"/>
<gate name="G$2" symbol="POWER-0.050=1/20W" x="-5.08" y="22.86" addlevel="request"/>
<gate name="G$3" symbol="POWER-0.063=1/16W" x="0" y="22.86" addlevel="request"/>
<gate name="G$4" symbol="POWER-0.100=1/10W" x="5.08" y="22.86" addlevel="request"/>
<gate name="G$5" symbol="POWER-0.125=1/8W" x="10.16" y="22.86" addlevel="request"/>
<gate name="G$6" symbol="POWER-0.167=1/6W" x="15.24" y="22.86" addlevel="request"/>
<gate name="G$7" symbol="POWER-0.200=1/5W" x="-10.16" y="17.78" addlevel="request"/>
<gate name="G$8" symbol="POWER-0.250=1/4W" x="-5.08" y="17.78" addlevel="request"/>
<gate name="G$9" symbol="POWER-0.333=1/3W" x="0" y="17.78" addlevel="request"/>
<gate name="G$10" symbol="POWER-0.500=1/2W" x="5.08" y="17.78" addlevel="request"/>
<gate name="G$11" symbol="POWER-0.750=3/4W" x="10.16" y="17.78" addlevel="request"/>
<gate name="G$12" symbol="POWER-1W" x="15.24" y="17.78" addlevel="request"/>
<gate name="G$13" symbol="POWER-2W" x="-10.16" y="12.7" addlevel="request"/>
<gate name="G$14" symbol="POWER-3W" x="-5.08" y="12.7" addlevel="request"/>
<gate name="G$15" symbol="POWER-4W" x="0" y="12.7" addlevel="request"/>
<gate name="G$16" symbol="POWER-5W" x="5.08" y="12.7" addlevel="request"/>
<gate name="G$17" symbol="POWER-10W" x="10.16" y="12.7" addlevel="request"/>
<gate name="G$18" symbol="POWER-15W" x="15.24" y="12.7" addlevel="request"/>
<gate name="G$19" symbol="POWER-20W" x="-10.16" y="7.62" addlevel="request"/>
<gate name="G$20" symbol="POWER-25W" x="-5.08" y="7.62" addlevel="request"/>
<gate name="G$21" symbol="POWER-35W" x="0" y="7.62" addlevel="request"/>
<gate name="G$22" symbol="POWER-40W" x="5.08" y="7.62" addlevel="request"/>
<gate name="G$23" symbol="POWER-100W" x="15.24" y="7.62" addlevel="request"/>
<gate name="G$24" symbol="POWER-50W" x="10.16" y="7.62" addlevel="request"/>
</gates>
<devices>
<device name="0201" package="0201">
<connects>
<connect gate="A" pin="1" pad="1"/>
<connect gate="A" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="0402" package="0402">
<connects>
<connect gate="A" pin="1" pad="1"/>
<connect gate="A" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="0402S" package="0402S">
<connects>
<connect gate="A" pin="1" pad="1"/>
<connect gate="A" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="0504" package="0504">
<connects>
<connect gate="A" pin="1" pad="1"/>
<connect gate="A" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="0603" package="0603">
<connects>
<connect gate="A" pin="1" pad="1"/>
<connect gate="A" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="0805" package="0805">
<connects>
<connect gate="A" pin="1" pad="1"/>
<connect gate="A" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="01005" package="01005">
<connects>
<connect gate="A" pin="1" pad="1"/>
<connect gate="A" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="1206" package="1206">
<connects>
<connect gate="A" pin="1" pad="1"/>
<connect gate="A" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="1210" package="1210">
<connects>
<connect gate="A" pin="1" pad="1"/>
<connect gate="A" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="1806" package="1806">
<connects>
<connect gate="A" pin="1" pad="1"/>
<connect gate="A" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="1812" package="1812">
<connects>
<connect gate="A" pin="1" pad="1"/>
<connect gate="A" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="1825" package="1825">
<connects>
<connect gate="A" pin="1" pad="1"/>
<connect gate="A" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="2010" package="2010">
<connects>
<connect gate="A" pin="1" pad="1"/>
<connect gate="A" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="2512" package="2512">
<connects>
<connect gate="A" pin="1" pad="1"/>
<connect gate="A" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="3616" package="3616">
<connects>
<connect gate="A" pin="1" pad="1"/>
<connect gate="A" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="4022" package="4022">
<connects>
<connect gate="A" pin="1" pad="1"/>
<connect gate="A" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="" package="DUALRESISTOR">
<connects>
<connect gate="A" pin="1" pad="1"/>
<connect gate="A" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="C" prefix="C" uservalue="yes">
<description>&lt;B&gt;CAPACITOR&lt;/B&gt;, European symbol</description>
<gates>
<gate name="A" symbol="C-EU" x="0" y="0"/>
</gates>
<devices>
<device name="0201" package="0201">
<connects>
<connect gate="A" pin="1" pad="1"/>
<connect gate="A" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="0402" package="0402">
<connects>
<connect gate="A" pin="1" pad="1"/>
<connect gate="A" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="0402S" package="0402S">
<connects>
<connect gate="A" pin="1" pad="1"/>
<connect gate="A" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="0504" package="0504">
<connects>
<connect gate="A" pin="1" pad="1"/>
<connect gate="A" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="0603" package="0603">
<connects>
<connect gate="A" pin="1" pad="1"/>
<connect gate="A" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="0805" package="0805">
<connects>
<connect gate="A" pin="1" pad="1"/>
<connect gate="A" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="01005" package="01005">
<connects>
<connect gate="A" pin="1" pad="1"/>
<connect gate="A" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="1206" package="1206">
<connects>
<connect gate="A" pin="1" pad="1"/>
<connect gate="A" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="1210" package="1210">
<connects>
<connect gate="A" pin="1" pad="1"/>
<connect gate="A" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="1806" package="1806">
<connects>
<connect gate="A" pin="1" pad="1"/>
<connect gate="A" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="1812" package="1812">
<connects>
<connect gate="A" pin="1" pad="1"/>
<connect gate="A" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="1825" package="1825">
<connects>
<connect gate="A" pin="1" pad="1"/>
<connect gate="A" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="2010" package="2010">
<connects>
<connect gate="A" pin="1" pad="1"/>
<connect gate="A" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="2512" package="2512">
<connects>
<connect gate="A" pin="1" pad="1"/>
<connect gate="A" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="3616" package="3616">
<connects>
<connect gate="A" pin="1" pad="1"/>
<connect gate="A" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="4022" package="4022">
<connects>
<connect gate="A" pin="1" pad="1"/>
<connect gate="A" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="A" package="TANT_A">
<connects>
<connect gate="A" pin="1" pad="-"/>
<connect gate="A" pin="2" pad="+"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
</devicesets>
</library>
<library name="alvie">
<packages>
<package name="SOIC14">
<wire x1="-4.11" y1="1.794" x2="4.11" y2="1.794" width="0.1524" layer="21"/>
<wire x1="4.11" y1="1.794" x2="4.11" y2="-1.294" width="0.1524" layer="21"/>
<wire x1="4.11" y1="-1.294" x2="4.11" y2="-1.794" width="0.1524" layer="21"/>
<wire x1="4.11" y1="-1.794" x2="-4.11" y2="-1.794" width="0.1524" layer="21"/>
<wire x1="-4.11" y1="-1.794" x2="-4.11" y2="-1.294" width="0.1524" layer="21"/>
<wire x1="-4.11" y1="-1.294" x2="-4.11" y2="1.794" width="0.1524" layer="21"/>
<wire x1="-4.11" y1="-1.294" x2="4.11" y2="-1.294" width="0.1524" layer="21"/>
<smd name="14" x="-3.81" y="2.7" dx="0.6" dy="1.5" layer="1"/>
<smd name="13" x="-2.54" y="2.7" dx="0.6" dy="1.5" layer="1"/>
<smd name="12" x="-1.27" y="2.7" dx="0.6" dy="1.5" layer="1"/>
<smd name="11" x="0" y="2.7" dx="0.6" dy="1.5" layer="1"/>
<smd name="10" x="1.27" y="2.7" dx="0.6" dy="1.5" layer="1"/>
<smd name="9" x="2.54" y="2.7" dx="0.6" dy="1.5" layer="1"/>
<smd name="8" x="3.81" y="2.7" dx="0.6" dy="1.5" layer="1"/>
<smd name="7" x="3.81" y="-2.7" dx="0.6" dy="1.5" layer="1"/>
<smd name="6" x="2.54" y="-2.7" dx="0.6" dy="1.5" layer="1"/>
<smd name="5" x="1.27" y="-2.7" dx="0.6" dy="1.5" layer="1"/>
<smd name="4" x="0" y="-2.7" dx="0.6" dy="1.5" layer="1"/>
<smd name="3" x="-1.27" y="-2.7" dx="0.6" dy="1.5" layer="1"/>
<smd name="2" x="-2.54" y="-2.7" dx="0.6" dy="1.5" layer="1"/>
<smd name="1" x="-3.81" y="-2.7" dx="0.6" dy="1.5" layer="1"/>
<text x="-5" y="-4" size="1.778" layer="25" font="vector" rot="R90">&gt;NAME</text>
<rectangle x1="-0.24" y1="1.794" x2="0.24" y2="2.96" layer="51"/>
<rectangle x1="-4.05" y1="1.794" x2="-3.57" y2="2.96" layer="51"/>
<rectangle x1="-2.78" y1="1.794" x2="-2.3" y2="2.96" layer="51"/>
<rectangle x1="-1.51" y1="1.794" x2="-1.03" y2="2.96" layer="51"/>
<rectangle x1="1.03" y1="1.794" x2="1.51" y2="2.96" layer="51"/>
<rectangle x1="2.3" y1="1.794" x2="2.78" y2="2.96" layer="51"/>
<rectangle x1="3.57" y1="1.794" x2="4.05" y2="2.96" layer="51"/>
<rectangle x1="-0.24" y1="-2.96" x2="0.24" y2="-1.794" layer="51" rot="R180"/>
<rectangle x1="3.57" y1="-2.96" x2="4.05" y2="-1.794" layer="51" rot="R180"/>
<rectangle x1="2.3" y1="-2.96" x2="2.78" y2="-1.794" layer="51" rot="R180"/>
<rectangle x1="1.03" y1="-2.96" x2="1.51" y2="-1.794" layer="51" rot="R180"/>
<rectangle x1="-1.51" y1="-2.96" x2="-1.03" y2="-1.794" layer="51" rot="R180"/>
<rectangle x1="-2.78" y1="-2.96" x2="-2.3" y2="-1.794" layer="51" rot="R180"/>
<rectangle x1="-4.05" y1="-2.96" x2="-3.57" y2="-1.794" layer="51" rot="R180"/>
</package>
<package name="SOT23-5L">
<wire x1="1.422" y1="0.81" x2="1.422" y2="-0.81" width="0.1524" layer="21"/>
<wire x1="1.422" y1="-0.81" x2="-1.422" y2="-0.81" width="0.1524" layer="51"/>
<wire x1="-1.422" y1="-0.81" x2="-1.422" y2="0.81" width="0.1524" layer="21"/>
<wire x1="-1.422" y1="0.81" x2="1.422" y2="0.81" width="0.1524" layer="51"/>
<wire x1="-0.522" y1="0.81" x2="0.522" y2="0.81" width="0.1524" layer="21"/>
<wire x1="-0.428" y1="-0.81" x2="-0.522" y2="-0.81" width="0.1524" layer="21"/>
<wire x1="0.522" y1="-0.81" x2="0.428" y2="-0.81" width="0.1524" layer="21"/>
<wire x1="-1.328" y1="-0.81" x2="-1.422" y2="-0.81" width="0.1524" layer="21"/>
<wire x1="1.422" y1="-0.81" x2="1.328" y2="-0.81" width="0.1524" layer="21"/>
<wire x1="1.328" y1="0.81" x2="1.422" y2="0.81" width="0.1524" layer="21"/>
<wire x1="-1.422" y1="0.81" x2="-1.328" y2="0.81" width="0.1524" layer="21"/>
<smd name="1" x="-0.95" y="-1.3" dx="0.55" dy="1.2" layer="1"/>
<smd name="2" x="0" y="-1.3" dx="0.55" dy="1.2" layer="1"/>
<smd name="3" x="0.95" y="-1.3" dx="0.55" dy="1.2" layer="1"/>
<smd name="4" x="0.95" y="1.3" dx="0.55" dy="1.2" layer="1"/>
<smd name="5" x="-0.95" y="1.3" dx="0.55" dy="1.2" layer="1"/>
<text x="-1.905" y="1.905" size="1.27" layer="25">&gt;NAME</text>
<text x="-1.905" y="-3.429" size="1.27" layer="27">&gt;VALUE</text>
<rectangle x1="-1.2" y1="-1.5" x2="-0.7" y2="-0.85" layer="51"/>
<rectangle x1="-0.25" y1="-1.5" x2="0.25" y2="-0.85" layer="51"/>
<rectangle x1="0.7" y1="-1.5" x2="1.2" y2="-0.85" layer="51"/>
<rectangle x1="0.7" y1="0.85" x2="1.2" y2="1.5" layer="51"/>
<rectangle x1="-1.2" y1="0.85" x2="-0.7" y2="1.5" layer="51"/>
</package>
<package name="TFT-22-NOSD">
<wire x1="-33.6" y1="-20.05" x2="-27.6" y2="-20.05" width="0.127" layer="21"/>
<wire x1="-27.6" y1="-20.05" x2="27.6" y2="-20.05" width="0.127" layer="21"/>
<wire x1="27.6" y1="-20.05" x2="33.6" y2="-20.05" width="0.127" layer="21"/>
<wire x1="33.6" y1="-20.05" x2="33.6" y2="20.05" width="0.127" layer="21"/>
<wire x1="33.6" y1="20.05" x2="27.6" y2="20.05" width="0.127" layer="21"/>
<wire x1="27.6" y1="20.05" x2="-27.6" y2="20.05" width="0.127" layer="21"/>
<wire x1="-27.6" y1="20.05" x2="-33.6" y2="20.05" width="0.127" layer="21"/>
<wire x1="-33.6" y1="20.05" x2="-33.6" y2="-20.05" width="0.127" layer="21"/>
<hole x="-30.6" y="-17.05" drill="3.2"/>
<hole x="-30.6" y="17.05" drill="3.2"/>
<hole x="30.6" y="17.05" drill="3.2"/>
<hole x="30.6" y="-17.05" drill="3.2"/>
<pad name="P$1" x="-31.6" y="-10.16" drill="0.8" shape="octagon"/>
<pad name="P$2" x="-31.6" y="-7.62" drill="0.8" shape="octagon"/>
<pad name="P$3" x="-31.6" y="-5.08" drill="0.8" shape="octagon"/>
<pad name="P$4" x="-31.6" y="-2.54" drill="0.8" shape="octagon"/>
<pad name="P$5" x="-31.6" y="0" drill="0.8" shape="octagon"/>
<pad name="P$6" x="-31.6" y="2.54" drill="0.8" shape="octagon"/>
<pad name="P$7" x="-31.6" y="5.08" drill="0.8" shape="octagon"/>
<pad name="P$8" x="-31.6" y="7.62" drill="0.8" shape="octagon"/>
<pad name="P$9" x="-31.6" y="10.16" drill="0.8" shape="octagon"/>
<wire x1="-27.6" y1="-20.05" x2="-27.6" y2="20.05" width="0.127" layer="21"/>
<wire x1="27.6" y1="20.05" x2="27.6" y2="-20.05" width="0.127" layer="21"/>
</package>
<package name="SOIC20S">
<wire x1="-6.4" y1="-3.9" x2="6.4" y2="-3.9" width="0.2032" layer="21"/>
<wire x1="6.4" y1="-3.9" x2="6.4" y2="3.7" width="0.2032" layer="21"/>
<wire x1="6.4" y1="3.7" x2="-6.4" y2="3.7" width="0.2032" layer="21"/>
<wire x1="-6.4" y1="3.7" x2="-6.4" y2="-3.9" width="0.2032" layer="21"/>
<circle x="-5.5999" y="-2.8999" radius="0.5097" width="0.2032" layer="21"/>
<smd name="1" x="-5.715" y="-5" dx="0.6" dy="1.5" layer="1"/>
<smd name="2" x="-4.445" y="-5" dx="0.6" dy="1.5" layer="1"/>
<smd name="3" x="-3.175" y="-5" dx="0.6" dy="1.5" layer="1"/>
<smd name="4" x="-1.905" y="-5" dx="0.6" dy="1.5" layer="1"/>
<smd name="5" x="-0.635" y="-5" dx="0.6" dy="1.5" layer="1"/>
<smd name="6" x="0.635" y="-5" dx="0.6" dy="1.5" layer="1"/>
<smd name="7" x="1.905" y="-5" dx="0.6" dy="1.5" layer="1"/>
<smd name="8" x="3.175" y="-5" dx="0.6" dy="1.5" layer="1"/>
<smd name="9" x="4.445" y="-5" dx="0.6" dy="1.5" layer="1"/>
<smd name="10" x="5.715" y="-5" dx="0.6" dy="1.5" layer="1"/>
<smd name="11" x="5.715" y="5" dx="0.6" dy="1.5" layer="1"/>
<smd name="12" x="4.445" y="5" dx="0.6" dy="1.5" layer="1"/>
<smd name="13" x="3.175" y="5" dx="0.6" dy="1.5" layer="1"/>
<smd name="14" x="1.905" y="5" dx="0.6" dy="1.5" layer="1"/>
<smd name="15" x="0.635" y="5" dx="0.6" dy="1.5" layer="1"/>
<smd name="16" x="-0.635" y="5" dx="0.6" dy="1.5" layer="1"/>
<smd name="17" x="-1.905" y="5" dx="0.6" dy="1.5" layer="1"/>
<smd name="18" x="-3.175" y="5" dx="0.6" dy="1.5" layer="1"/>
<smd name="19" x="-4.445" y="5" dx="0.6" dy="1.5" layer="1"/>
<smd name="20" x="-5.715" y="5" dx="0.6" dy="1.5" layer="1"/>
<text x="-6.985" y="-3.81" size="1.27" layer="25" rot="R90">&gt;NAME</text>
<text x="-4.445" y="-1.27" size="1.27" layer="27">&gt;VALUE</text>
<rectangle x1="-5.9649" y1="-5.3" x2="-5.4651" y2="-3.7" layer="51"/>
<rectangle x1="-4.6949" y1="-5.3" x2="-4.1951" y2="-3.7" layer="51"/>
<rectangle x1="-3.4249" y1="-5.3" x2="-2.9251" y2="-3.7" layer="51"/>
<rectangle x1="-2.1549" y1="-5.3" x2="-1.6551" y2="-3.7" layer="51"/>
<rectangle x1="-0.8849" y1="-5.3" x2="-0.3851" y2="-3.7" layer="51"/>
<rectangle x1="0.3851" y1="-5.3" x2="0.8849" y2="-3.7" layer="51"/>
<rectangle x1="1.6551" y1="-5.3" x2="2.1549" y2="-3.7" layer="51"/>
<rectangle x1="2.9251" y1="-5.3" x2="3.4249" y2="-3.7" layer="51"/>
<rectangle x1="4.1951" y1="-5.3" x2="4.6949" y2="-3.7" layer="51"/>
<rectangle x1="5.4651" y1="-5.3" x2="5.9649" y2="-3.7" layer="51"/>
<rectangle x1="5.4651" y1="3.7" x2="5.9649" y2="5.3" layer="51"/>
<rectangle x1="4.1951" y1="3.7" x2="4.6949" y2="5.3" layer="51"/>
<rectangle x1="2.9251" y1="3.7" x2="3.4249" y2="5.3" layer="51"/>
<rectangle x1="1.6551" y1="3.7" x2="2.1549" y2="5.3" layer="51"/>
<rectangle x1="0.3851" y1="3.7" x2="0.8849" y2="5.3" layer="51"/>
<rectangle x1="-0.8849" y1="3.7" x2="-0.3851" y2="5.3" layer="51"/>
<rectangle x1="-2.1549" y1="3.7" x2="-1.6551" y2="5.3" layer="51"/>
<rectangle x1="-3.4249" y1="3.7" x2="-2.9251" y2="5.3" layer="51"/>
<rectangle x1="-4.6949" y1="3.7" x2="-4.1951" y2="5.3" layer="51"/>
<rectangle x1="-5.9649" y1="3.7" x2="-5.4651" y2="5.3" layer="51"/>
</package>
<package name="PIN-1X3">
<wire x1="-3.175" y1="1.27" x2="-1.905" y2="1.27" width="0.1524" layer="21"/>
<wire x1="-1.905" y1="1.27" x2="-1.27" y2="0.635" width="0.1524" layer="21"/>
<wire x1="-1.27" y1="-0.635" x2="-1.905" y2="-1.27" width="0.1524" layer="21"/>
<wire x1="-1.27" y1="0.635" x2="-0.635" y2="1.27" width="0.1524" layer="21"/>
<wire x1="-0.635" y1="1.27" x2="0.635" y2="1.27" width="0.1524" layer="21"/>
<wire x1="0.635" y1="1.27" x2="1.27" y2="0.635" width="0.1524" layer="21"/>
<wire x1="1.27" y1="-0.635" x2="0.635" y2="-1.27" width="0.1524" layer="21"/>
<wire x1="0.635" y1="-1.27" x2="-0.635" y2="-1.27" width="0.1524" layer="21"/>
<wire x1="-0.635" y1="-1.27" x2="-1.27" y2="-0.635" width="0.1524" layer="21"/>
<wire x1="-3.81" y1="0.635" x2="-3.81" y2="-0.635" width="0.1524" layer="21"/>
<wire x1="-3.175" y1="1.27" x2="-3.81" y2="0.635" width="0.1524" layer="21"/>
<wire x1="-3.81" y1="-0.635" x2="-3.175" y2="-1.27" width="0.1524" layer="21"/>
<wire x1="-1.905" y1="-1.27" x2="-3.175" y2="-1.27" width="0.1524" layer="21"/>
<wire x1="1.27" y1="0.635" x2="1.905" y2="1.27" width="0.1524" layer="21"/>
<wire x1="1.905" y1="1.27" x2="3.175" y2="1.27" width="0.1524" layer="21"/>
<wire x1="3.175" y1="1.27" x2="3.81" y2="0.635" width="0.1524" layer="21"/>
<wire x1="3.81" y1="-0.635" x2="3.175" y2="-1.27" width="0.1524" layer="21"/>
<wire x1="3.175" y1="-1.27" x2="1.905" y2="-1.27" width="0.1524" layer="21"/>
<wire x1="1.905" y1="-1.27" x2="1.27" y2="-0.635" width="0.1524" layer="21"/>
<wire x1="3.81" y1="0.635" x2="3.81" y2="-0.635" width="0.1524" layer="21"/>
<pad name="1" x="-2.54" y="0" drill="1.016" rot="R90"/>
<pad name="2" x="0" y="0" drill="1.016" rot="R90"/>
<pad name="3" x="2.54" y="0" drill="1.016" rot="R90"/>
<text x="-3.81" y="1.651" size="1.27" layer="25" ratio="10">&gt;NAME</text>
<text x="-3.81" y="-2.159" size="1.27" layer="27" ratio="10">&gt;VALUE</text>
<rectangle x1="-0.254" y1="-0.254" x2="0.254" y2="0.254" layer="51"/>
<rectangle x1="-2.794" y1="-0.254" x2="-2.286" y2="0.254" layer="51"/>
<rectangle x1="2.286" y1="-0.254" x2="2.794" y2="0.254" layer="51"/>
</package>
<package name="PIN-1X4">
<wire x1="-4.445" y1="1.27" x2="-3.175" y2="1.27" width="0.1524" layer="21"/>
<wire x1="-3.175" y1="1.27" x2="-2.54" y2="0.635" width="0.1524" layer="21"/>
<wire x1="-2.54" y1="-0.635" x2="-3.175" y2="-1.27" width="0.1524" layer="21"/>
<wire x1="-2.54" y1="0.635" x2="-1.905" y2="1.27" width="0.1524" layer="21"/>
<wire x1="-1.905" y1="1.27" x2="-0.635" y2="1.27" width="0.1524" layer="21"/>
<wire x1="-0.635" y1="1.27" x2="0" y2="0.635" width="0.1524" layer="21"/>
<wire x1="0" y1="-0.635" x2="-0.635" y2="-1.27" width="0.1524" layer="21"/>
<wire x1="-0.635" y1="-1.27" x2="-1.905" y2="-1.27" width="0.1524" layer="21"/>
<wire x1="-1.905" y1="-1.27" x2="-2.54" y2="-0.635" width="0.1524" layer="21"/>
<wire x1="-5.08" y1="0.635" x2="-5.08" y2="-0.635" width="0.1524" layer="21"/>
<wire x1="-4.445" y1="1.27" x2="-5.08" y2="0.635" width="0.1524" layer="21"/>
<wire x1="-5.08" y1="-0.635" x2="-4.445" y2="-1.27" width="0.1524" layer="21"/>
<wire x1="-3.175" y1="-1.27" x2="-4.445" y2="-1.27" width="0.1524" layer="21"/>
<wire x1="0" y1="0.635" x2="0.635" y2="1.27" width="0.1524" layer="21"/>
<wire x1="0.635" y1="1.27" x2="1.905" y2="1.27" width="0.1524" layer="21"/>
<wire x1="1.905" y1="1.27" x2="2.54" y2="0.635" width="0.1524" layer="21"/>
<wire x1="2.54" y1="-0.635" x2="1.905" y2="-1.27" width="0.1524" layer="21"/>
<wire x1="1.905" y1="-1.27" x2="0.635" y2="-1.27" width="0.1524" layer="21"/>
<wire x1="0.635" y1="-1.27" x2="0" y2="-0.635" width="0.1524" layer="21"/>
<wire x1="3.175" y1="1.27" x2="4.445" y2="1.27" width="0.1524" layer="21"/>
<wire x1="4.445" y1="1.27" x2="5.08" y2="0.635" width="0.1524" layer="21"/>
<wire x1="5.08" y1="0.635" x2="5.08" y2="-0.635" width="0.1524" layer="21"/>
<wire x1="5.08" y1="-0.635" x2="4.445" y2="-1.27" width="0.1524" layer="21"/>
<wire x1="3.175" y1="1.27" x2="2.54" y2="0.635" width="0.1524" layer="21"/>
<wire x1="2.54" y1="-0.635" x2="3.175" y2="-1.27" width="0.1524" layer="21"/>
<wire x1="4.445" y1="-1.27" x2="3.175" y2="-1.27" width="0.1524" layer="21"/>
<pad name="1" x="-3.81" y="0" drill="1.016" rot="R90"/>
<pad name="2" x="-1.27" y="0" drill="1.016" rot="R90"/>
<pad name="3" x="1.27" y="0" drill="1.016" rot="R90"/>
<pad name="4" x="3.81" y="0" drill="1.016" rot="R90"/>
<text x="-5.08" y="1.651" size="1.27" layer="25" ratio="10">&gt;NAME</text>
<text x="0.635" y="1.651" size="1.27" layer="27" ratio="10">&gt;VALUE</text>
<rectangle x1="-1.524" y1="-0.254" x2="-1.016" y2="0.254" layer="51"/>
<rectangle x1="-4.064" y1="-0.254" x2="-3.556" y2="0.254" layer="51"/>
<rectangle x1="1.016" y1="-0.254" x2="1.524" y2="0.254" layer="51"/>
<rectangle x1="3.556" y1="-0.254" x2="4.064" y2="0.254" layer="51"/>
</package>
</packages>
<symbols>
<symbol name="ATTINYX14">
<wire x1="-17.78" y1="15.24" x2="17.78" y2="15.24" width="0.254" layer="94"/>
<wire x1="17.78" y1="15.24" x2="17.78" y2="-12.7" width="0.254" layer="94"/>
<wire x1="17.78" y1="-12.7" x2="-17.78" y2="-12.7" width="0.254" layer="94"/>
<wire x1="-17.78" y1="-12.7" x2="-17.78" y2="15.24" width="0.254" layer="94"/>
<pin name="VDD" x="0" y="20.32" visible="pad" length="middle" direction="pwr" rot="R270"/>
<pin name="GND" x="0" y="-17.78" visible="pad" length="middle" direction="pwr" rot="R90"/>
<pin name="PA0/NRESET/UPDI" x="-22.86" y="10.16" length="middle"/>
<pin name="PA1" x="-22.86" y="7.62" length="middle"/>
<pin name="PA2" x="-22.86" y="5.08" length="middle"/>
<pin name="PA3/EXTCLK" x="-22.86" y="2.54" length="middle"/>
<pin name="PA4" x="-22.86" y="0" length="middle"/>
<pin name="PA5" x="-22.86" y="-2.54" length="middle"/>
<pin name="PA6" x="-22.86" y="-5.08" length="middle"/>
<pin name="PA7" x="-22.86" y="-7.62" length="middle"/>
<pin name="PB0" x="22.86" y="10.16" length="middle" rot="R180"/>
<pin name="PB1" x="22.86" y="7.62" length="middle" rot="R180"/>
<pin name="PB2/TOSC2" x="22.86" y="5.08" length="middle" rot="R180"/>
<pin name="PB3/TOSC1" x="22.86" y="2.54" length="middle" rot="R180"/>
<text x="0" y="12.7" size="1.27" layer="95" align="bottom-center">VDD</text>
<text x="0" y="-10.16" size="1.27" layer="95" align="top-center">GND</text>
<text x="-17.78" y="16.51" size="1.27" layer="95">&gt;NAME</text>
<text x="-17.78" y="-15.24" size="1.27" layer="96">&gt;VALUE</text>
</symbol>
<symbol name="MIC5504">
<wire x1="-7.62" y1="10.16" x2="-7.62" y2="-7.62" width="0.254" layer="94"/>
<wire x1="-7.62" y1="-7.62" x2="7.62" y2="-7.62" width="0.254" layer="94"/>
<wire x1="7.62" y1="-7.62" x2="7.62" y2="10.16" width="0.254" layer="94"/>
<wire x1="7.62" y1="10.16" x2="-7.62" y2="10.16" width="0.254" layer="94"/>
<pin name="VIN" x="-12.7" y="7.62" length="middle" direction="pwr"/>
<pin name="VOUT" x="12.7" y="7.62" length="middle" direction="pwr" rot="R180"/>
<pin name="GND" x="0" y="-12.7" length="middle" direction="pwr" rot="R90"/>
<pin name="EN" x="-12.7" y="2.54" length="middle" direction="in"/>
<text x="0" y="5.08" size="1.778" layer="94" align="center">MIC550x</text>
</symbol>
<symbol name="TFT-22-NOSD">
<wire x1="-15.24" y1="15.24" x2="15.24" y2="15.24" width="0.254" layer="94"/>
<wire x1="15.24" y1="15.24" x2="15.24" y2="-12.7" width="0.254" layer="94"/>
<wire x1="15.24" y1="-12.7" x2="-15.24" y2="-12.7" width="0.254" layer="94"/>
<wire x1="-15.24" y1="-12.7" x2="-15.24" y2="15.24" width="0.254" layer="94"/>
<pin name="VCC" x="-20.32" y="12.7" length="middle" direction="pwr"/>
<pin name="GND" x="-20.32" y="-10.16" length="middle" direction="pwr"/>
<pin name="CS" x="-20.32" y="10.16" length="middle" direction="in"/>
<pin name="RESET" x="-20.32" y="7.62" length="middle" direction="in"/>
<pin name="DC" x="-20.32" y="5.08" length="middle" direction="in"/>
<pin name="MOSI" x="-20.32" y="2.54" length="middle" direction="in"/>
<pin name="SCK" x="-20.32" y="0" length="middle" direction="in"/>
<pin name="LED" x="-20.32" y="-2.54" length="middle" direction="in"/>
<pin name="MISO" x="-20.32" y="-5.08" length="middle"/>
<text x="-15.24" y="16.51" size="1.27" layer="95">&gt;NAME</text>
<text x="-15.24" y="-15.24" size="1.27" layer="95">&gt;VALUE</text>
</symbol>
<symbol name="ATTINYX16">
<wire x1="-15.24" y1="17.78" x2="20.32" y2="17.78" width="0.254" layer="94"/>
<wire x1="20.32" y1="17.78" x2="20.32" y2="-15.24" width="0.254" layer="94"/>
<wire x1="20.32" y1="-15.24" x2="-15.24" y2="-15.24" width="0.254" layer="94"/>
<wire x1="-15.24" y1="-15.24" x2="-15.24" y2="17.78" width="0.254" layer="94"/>
<pin name="VDD" x="2.54" y="22.86" visible="pad" length="middle" direction="pwr" rot="R270"/>
<pin name="GND" x="2.54" y="-20.32" visible="pad" length="middle" direction="pwr" rot="R90"/>
<pin name="PA0/NRESET/UPDI" x="-20.32" y="12.7" length="middle"/>
<pin name="PA1" x="-20.32" y="10.16" length="middle"/>
<pin name="PA2" x="-20.32" y="7.62" length="middle"/>
<pin name="PA3/EXTCLK" x="-20.32" y="5.08" length="middle"/>
<pin name="PA4" x="-20.32" y="2.54" length="middle"/>
<pin name="PA5" x="-20.32" y="0" length="middle"/>
<pin name="PA6" x="-20.32" y="-2.54" length="middle"/>
<pin name="PA7" x="-20.32" y="-5.08" length="middle"/>
<pin name="PB0" x="25.4" y="12.7" length="middle" rot="R180"/>
<pin name="PB1" x="25.4" y="10.16" length="middle" rot="R180"/>
<pin name="PB2/TOSC2" x="25.4" y="7.62" length="middle" rot="R180"/>
<pin name="PB3/TOSC1" x="25.4" y="5.08" length="middle" rot="R180"/>
<text x="2.54" y="15.24" size="1.27" layer="95" align="bottom-center">VDD</text>
<text x="2.54" y="-12.7" size="1.27" layer="95" align="top-center">GND</text>
<text x="-15.24" y="19.05" size="1.27" layer="95">&gt;NAME</text>
<text x="-15.24" y="-17.78" size="1.27" layer="96">&gt;VALUE</text>
<pin name="PB4" x="25.4" y="2.54" length="middle" rot="R180"/>
<pin name="PB5" x="25.4" y="0" length="middle" rot="R180"/>
<pin name="PC0" x="25.4" y="-2.54" length="middle" rot="R180"/>
<pin name="PC1" x="25.4" y="-5.08" length="middle" rot="R180"/>
<pin name="PC2" x="25.4" y="-7.62" length="middle" rot="R180"/>
<pin name="PC3" x="25.4" y="-10.16" length="middle" rot="R180"/>
</symbol>
<symbol name="PIN-1X3">
<wire x1="3.81" y1="-5.08" x2="-1.27" y2="-5.08" width="0.4064" layer="94"/>
<wire x1="1.27" y1="2.54" x2="2.54" y2="2.54" width="0.6096" layer="94"/>
<wire x1="1.27" y1="0" x2="2.54" y2="0" width="0.6096" layer="94"/>
<wire x1="1.27" y1="-2.54" x2="2.54" y2="-2.54" width="0.6096" layer="94"/>
<wire x1="-1.27" y1="5.08" x2="-1.27" y2="-5.08" width="0.4064" layer="94"/>
<wire x1="3.81" y1="-5.08" x2="3.81" y2="5.08" width="0.4064" layer="94"/>
<wire x1="-1.27" y1="5.08" x2="3.81" y2="5.08" width="0.4064" layer="94"/>
<text x="-1.27" y="-7.62" size="1.778" layer="96">&gt;VALUE</text>
<text x="-1.27" y="5.842" size="1.778" layer="95">&gt;NAME</text>
<pin name="1" x="7.62" y="-2.54" visible="pad" length="middle" direction="pas" swaplevel="1" rot="R180"/>
<pin name="2" x="7.62" y="0" visible="pad" length="middle" direction="pas" swaplevel="1" rot="R180"/>
<pin name="3" x="7.62" y="2.54" visible="pad" length="middle" direction="pas" swaplevel="1" rot="R180"/>
</symbol>
<symbol name="PIN-1X4">
<wire x1="2.54" y1="-6.35" x2="-2.54" y2="-6.35" width="0.4064" layer="94"/>
<wire x1="0" y1="1.27" x2="1.27" y2="1.27" width="0.6096" layer="94"/>
<wire x1="0" y1="-1.27" x2="1.27" y2="-1.27" width="0.6096" layer="94"/>
<wire x1="0" y1="-3.81" x2="1.27" y2="-3.81" width="0.6096" layer="94"/>
<wire x1="-2.54" y1="6.35" x2="-2.54" y2="-6.35" width="0.4064" layer="94"/>
<wire x1="2.54" y1="-6.35" x2="2.54" y2="6.35" width="0.4064" layer="94"/>
<wire x1="-2.54" y1="6.35" x2="2.54" y2="6.35" width="0.4064" layer="94"/>
<wire x1="0" y1="3.81" x2="1.27" y2="3.81" width="0.6096" layer="94"/>
<text x="-2.54" y="-8.89" size="1.778" layer="96">&gt;VALUE</text>
<text x="-2.54" y="7.112" size="1.778" layer="95">&gt;NAME</text>
<pin name="1" x="6.35" y="-3.81" visible="pad" length="middle" direction="pas" swaplevel="1" rot="R180"/>
<pin name="2" x="6.35" y="-1.27" visible="pad" length="middle" direction="pas" swaplevel="1" rot="R180"/>
<pin name="3" x="6.35" y="1.27" visible="pad" length="middle" direction="pas" swaplevel="1" rot="R180"/>
<pin name="4" x="6.35" y="3.81" visible="pad" length="middle" direction="pas" swaplevel="1" rot="R180"/>
</symbol>
</symbols>
<devicesets>
<deviceset name="ATTINYX14" prefix="U">
<gates>
<gate name="G$1" symbol="ATTINYX14" x="0" y="-2.54"/>
</gates>
<devices>
<device name="" package="SOIC14">
<connects>
<connect gate="G$1" pin="GND" pad="14"/>
<connect gate="G$1" pin="PA0/NRESET/UPDI" pad="10"/>
<connect gate="G$1" pin="PA1" pad="11"/>
<connect gate="G$1" pin="PA2" pad="12"/>
<connect gate="G$1" pin="PA3/EXTCLK" pad="13"/>
<connect gate="G$1" pin="PA4" pad="2"/>
<connect gate="G$1" pin="PA5" pad="3"/>
<connect gate="G$1" pin="PA6" pad="4"/>
<connect gate="G$1" pin="PA7" pad="5"/>
<connect gate="G$1" pin="PB0" pad="9"/>
<connect gate="G$1" pin="PB1" pad="8"/>
<connect gate="G$1" pin="PB2/TOSC2" pad="7"/>
<connect gate="G$1" pin="PB3/TOSC1" pad="6"/>
<connect gate="G$1" pin="VDD" pad="1"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="MIC550X">
<gates>
<gate name="G$1" symbol="MIC5504" x="0" y="0"/>
</gates>
<devices>
<device name="" package="SOT23-5L">
<connects>
<connect gate="G$1" pin="EN" pad="3"/>
<connect gate="G$1" pin="GND" pad="2"/>
<connect gate="G$1" pin="VIN" pad="1"/>
<connect gate="G$1" pin="VOUT" pad="5"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="TFT-22-NOSD">
<gates>
<gate name="G$1" symbol="TFT-22-NOSD" x="-30.48" y="0"/>
</gates>
<devices>
<device name="" package="TFT-22-NOSD">
<connects>
<connect gate="G$1" pin="CS" pad="P$7"/>
<connect gate="G$1" pin="DC" pad="P$5"/>
<connect gate="G$1" pin="GND" pad="P$8"/>
<connect gate="G$1" pin="LED" pad="P$2"/>
<connect gate="G$1" pin="MISO" pad="P$1"/>
<connect gate="G$1" pin="MOSI" pad="P$4"/>
<connect gate="G$1" pin="RESET" pad="P$6"/>
<connect gate="G$1" pin="SCK" pad="P$3"/>
<connect gate="G$1" pin="VCC" pad="P$9"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="ATTINYX16" prefix="IC">
<gates>
<gate name="G$1" symbol="ATTINYX16" x="0" y="0"/>
</gates>
<devices>
<device name="S" package="SOIC20S">
<connects>
<connect gate="G$1" pin="GND" pad="20"/>
<connect gate="G$1" pin="PA0/NRESET/UPDI" pad="16"/>
<connect gate="G$1" pin="PA1" pad="17"/>
<connect gate="G$1" pin="PA2" pad="18"/>
<connect gate="G$1" pin="PA3/EXTCLK" pad="19"/>
<connect gate="G$1" pin="PA4" pad="2"/>
<connect gate="G$1" pin="PA5" pad="3"/>
<connect gate="G$1" pin="PA6" pad="4"/>
<connect gate="G$1" pin="PA7" pad="5"/>
<connect gate="G$1" pin="PB0" pad="11"/>
<connect gate="G$1" pin="PB1" pad="10"/>
<connect gate="G$1" pin="PB2/TOSC2" pad="9"/>
<connect gate="G$1" pin="PB3/TOSC1" pad="8"/>
<connect gate="G$1" pin="PB4" pad="7"/>
<connect gate="G$1" pin="PB5" pad="6"/>
<connect gate="G$1" pin="PC0" pad="12"/>
<connect gate="G$1" pin="PC1" pad="13"/>
<connect gate="G$1" pin="PC2" pad="14"/>
<connect gate="G$1" pin="PC3" pad="15"/>
<connect gate="G$1" pin="VDD" pad="1"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="PIN-1X3" prefix="JP">
<gates>
<gate name="G$1" symbol="PIN-1X3" x="0" y="0"/>
</gates>
<devices>
<device name="" package="PIN-1X3">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
<connect gate="G$1" pin="3" pad="3"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="PIN-1X4">
<gates>
<gate name="G$1" symbol="PIN-1X4" x="0" y="0"/>
</gates>
<devices>
<device name="" package="PIN-1X4">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
<connect gate="G$1" pin="3" pad="3"/>
<connect gate="G$1" pin="4" pad="4"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
</devicesets>
</library>
<library name="transistor-small-signal">
<description>&lt;b&gt;Small Signal Transistors&lt;/b&gt;&lt;p&gt;
Packages from :&lt;br&gt;
www.infineon.com; &lt;br&gt;
www.semiconductors.com;&lt;br&gt;
www.irf.com&lt;p&gt;
&lt;author&gt;Created by librarian@cadsoft.de&lt;/author&gt;</description>
<packages>
<package name="SOT23">
<description>&lt;b&gt;SOT-23&lt;/b&gt;</description>
<wire x1="1.4224" y1="0.6604" x2="1.4224" y2="-0.6604" width="0.1524" layer="51"/>
<wire x1="1.4224" y1="-0.6604" x2="-1.4224" y2="-0.6604" width="0.1524" layer="51"/>
<wire x1="-1.4224" y1="-0.6604" x2="-1.4224" y2="0.6604" width="0.1524" layer="51"/>
<wire x1="-1.4224" y1="0.6604" x2="1.4224" y2="0.6604" width="0.1524" layer="51"/>
<smd name="3" x="0" y="1.1" dx="1" dy="1.4" layer="1"/>
<smd name="2" x="0.95" y="-1.1" dx="1" dy="1.4" layer="1"/>
<smd name="1" x="-0.95" y="-1.1" dx="1" dy="1.4" layer="1"/>
<text x="-1.905" y="1.905" size="1.27" layer="25">&gt;NAME</text>
<text x="-1.905" y="-3.175" size="1.27" layer="27">&gt;VALUE</text>
<rectangle x1="-0.2286" y1="0.7112" x2="0.2286" y2="1.2954" layer="51"/>
<rectangle x1="0.7112" y1="-1.2954" x2="1.1684" y2="-0.7112" layer="51"/>
<rectangle x1="-1.1684" y1="-1.2954" x2="-0.7112" y2="-0.7112" layer="51"/>
</package>
</packages>
<symbols>
<symbol name="N-MOS">
<wire x1="-1.27" y1="0" x2="-0.254" y2="0.381" width="0.1524" layer="94"/>
<wire x1="-0.254" y1="0.381" x2="-0.254" y2="-0.381" width="0.1524" layer="94"/>
<wire x1="-0.254" y1="-0.381" x2="-1.27" y2="0" width="0.1524" layer="94"/>
<wire x1="-1.27" y1="0" x2="-1.016" y2="0" width="0.1524" layer="94"/>
<wire x1="-1.016" y1="0" x2="-0.889" y2="0" width="0.1524" layer="94"/>
<wire x1="-0.889" y1="0" x2="0" y2="0" width="0.1524" layer="94"/>
<wire x1="0" y1="0" x2="0" y2="-2.032" width="0.1524" layer="94"/>
<wire x1="0" y1="-2.032" x2="0" y2="-2.794" width="0.1524" layer="94"/>
<wire x1="-1.524" y1="0" x2="-1.27" y2="0" width="0.1524" layer="94"/>
<wire x1="-2.54" y1="2.54" x2="-2.54" y2="-2.54" width="0.254" layer="94"/>
<wire x1="0" y1="3.048" x2="1.27" y2="3.048" width="0.1524" layer="94"/>
<wire x1="1.27" y1="3.048" x2="1.27" y2="0.762" width="0.1524" layer="94"/>
<wire x1="1.27" y1="0.762" x2="1.27" y2="0.508" width="0.1524" layer="94"/>
<wire x1="1.27" y1="0.508" x2="1.27" y2="-2.794" width="0.1524" layer="94"/>
<wire x1="1.27" y1="-2.794" x2="0" y2="-2.794" width="0.1524" layer="94"/>
<wire x1="1.778" y1="0" x2="0.762" y2="0" width="0.1524" layer="94"/>
<wire x1="0.762" y1="0" x2="1.27" y2="0.762" width="0.1524" layer="94"/>
<wire x1="1.27" y1="0.762" x2="1.778" y2="0" width="0.1524" layer="94"/>
<wire x1="1.778" y1="0.762" x2="0.762" y2="0.762" width="0.1524" layer="94"/>
<wire x1="-1.524" y1="2.032" x2="0" y2="2.032" width="0.1524" layer="94"/>
<wire x1="0" y1="2.032" x2="0" y2="2.54" width="0.1524" layer="94"/>
<wire x1="-1.524" y1="-2.032" x2="0" y2="-2.032" width="0.1524" layer="94"/>
<wire x1="-1.016" y1="0" x2="-0.381" y2="-0.254" width="0.254" layer="94"/>
<wire x1="-0.381" y1="-0.254" x2="-0.381" y2="0.254" width="0.254" layer="94"/>
<wire x1="-0.381" y1="0.254" x2="-0.889" y2="0" width="0.254" layer="94"/>
<wire x1="1.27" y1="0.508" x2="1.016" y2="0.127" width="0.254" layer="94"/>
<wire x1="1.016" y1="0.127" x2="1.524" y2="0.127" width="0.254" layer="94"/>
<wire x1="1.524" y1="0.127" x2="1.27" y2="0.508" width="0.254" layer="94"/>
<circle x="0" y="-2.794" radius="0.3592" width="0" layer="94"/>
<circle x="0" y="-2.032" radius="0.3592" width="0" layer="94"/>
<circle x="0" y="3.048" radius="0.3592" width="0" layer="94"/>
<text x="2.54" y="0" size="1.778" layer="95">&gt;NAME</text>
<text x="2.54" y="-2.54" size="1.778" layer="96">&gt;VALUE</text>
<rectangle x1="-2.032" y1="1.27" x2="-1.524" y2="2.54" layer="94"/>
<rectangle x1="-2.032" y1="-2.54" x2="-1.524" y2="-1.27" layer="94"/>
<rectangle x1="-2.032" y1="-0.762" x2="-1.524" y2="0.762" layer="94"/>
<pin name="G" x="-5.08" y="-2.54" visible="off" length="short" direction="pas"/>
<pin name="D" x="0" y="5.08" visible="off" length="short" direction="pas" rot="R270"/>
<pin name="S" x="0" y="-5.08" visible="off" length="short" direction="pas" rot="R90"/>
</symbol>
</symbols>
<devicesets>
<deviceset name="BSS123" prefix="Q">
<description>&lt;b&gt;N-CHANNEL MOS FET&lt;/b&gt;</description>
<gates>
<gate name="G$1" symbol="N-MOS" x="0" y="0"/>
</gates>
<devices>
<device name="" package="SOT23">
<connects>
<connect gate="G$1" pin="D" pad="3"/>
<connect gate="G$1" pin="G" pad="1"/>
<connect gate="G$1" pin="S" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
</devicesets>
</library>
<library name="supply1">
<description>&lt;b&gt;Supply Symbols&lt;/b&gt;&lt;p&gt;
 GND, VCC, 0V, +5V, -5V, etc.&lt;p&gt;
 Please keep in mind, that these devices are necessary for the
 automatic wiring of the supply signals.&lt;p&gt;
 The pin name defined in the symbol is identical to the net which is to be wired automatically.&lt;p&gt;
 In this library the device names are the same as the pin names of the symbols, therefore the correct signal names appear next to the supply symbols in the schematic.&lt;p&gt;
 &lt;author&gt;Created by librarian@cadsoft.de&lt;/author&gt;</description>
<packages>
</packages>
<symbols>
<symbol name="GND">
<wire x1="-1.905" y1="0" x2="1.905" y2="0" width="0.254" layer="94"/>
<text x="-2.54" y="-2.54" size="1.778" layer="96">&gt;VALUE</text>
<pin name="GND" x="0" y="2.54" visible="off" length="short" direction="sup" rot="R270"/>
</symbol>
</symbols>
<devicesets>
<deviceset name="GND" prefix="GND">
<description>&lt;b&gt;SUPPLY SYMBOL&lt;/b&gt;</description>
<gates>
<gate name="1" symbol="GND" x="0" y="0"/>
</gates>
<devices>
<device name="">
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
</devicesets>
</library>
<library name="switch-alps">
<description>ALPS Switch from Markus Faust &amp;lt;mfaust@htwm.de&amp;gt;&lt;p&gt;
Source: EC12E.scr from eagle.support.ger on news.cadsoft.de at 08.10.2007</description>
<packages>
<package name="ALPS_EC12E_SW">
<description>ALPS rotary encoder&lt;br&gt;
EC12E series with push-on switch</description>
<wire x1="-6.2" y1="-6.6" x2="6.2" y2="-6.6" width="0.127" layer="21"/>
<wire x1="6.2" y1="-6.6" x2="6.2" y2="6.6" width="0.127" layer="21"/>
<wire x1="6.2" y1="6.6" x2="-6.2" y2="6.6" width="0.127" layer="21"/>
<wire x1="-6.2" y1="6.6" x2="-6.2" y2="-6.6" width="0.127" layer="21"/>
<wire x1="-2.6" y1="1.5" x2="2.6" y2="1.5" width="0.127" layer="21"/>
<circle x="0" y="0" radius="3" width="0.127" layer="21"/>
<pad name="A" x="-2.5" y="-7.5" drill="1" shape="square"/>
<pad name="C" x="0" y="-7.5" drill="1" shape="square"/>
<pad name="B" x="2.5" y="-7.5" drill="1" shape="square"/>
<pad name="D" x="-2.5" y="7" drill="1" shape="square"/>
<pad name="E" x="2.5" y="7" drill="1" shape="square"/>
<pad name="GND1" x="-6.1" y="0" drill="2.2" shape="square"/>
<pad name="GND2" x="6.1" y="0" drill="2.2" shape="square"/>
<text x="-2.5" y="8.5" size="1.27" layer="25">&gt;NAME</text>
<text x="3.5" y="-9" size="1.27" layer="27" rot="R180">&gt;VALUE</text>
</package>
</packages>
<symbols>
<symbol name="ENCODER">
<wire x1="-2.54" y1="5.08" x2="-2.54" y2="4.1275" width="0.3048" layer="94"/>
<wire x1="-2.54" y1="4.1275" x2="-0.9525" y2="1.905" width="0.3048" layer="94"/>
<wire x1="-1.905" y1="1.905" x2="-2.54" y2="1.905" width="0.3048" layer="94"/>
<wire x1="-2.54" y1="1.905" x2="-2.54" y2="0" width="0.3048" layer="94"/>
<wire x1="2.54" y1="5.08" x2="2.54" y2="4.1275" width="0.3048" layer="94"/>
<wire x1="2.54" y1="4.1275" x2="4.1275" y2="1.905" width="0.3048" layer="94"/>
<wire x1="3.175" y1="1.905" x2="2.54" y2="1.905" width="0.3048" layer="94"/>
<wire x1="2.54" y1="1.905" x2="2.54" y2="0" width="0.3048" layer="94"/>
<wire x1="-2.54" y1="0" x2="2.54" y2="0" width="0.1524" layer="94"/>
<circle x="0" y="0" radius="0.5679" width="0" layer="94"/>
<text x="-3.81" y="0" size="1.27" layer="95" rot="R90">&gt;PART</text>
<text x="6.35" y="0" size="1.27" layer="96" rot="R90">&gt;VALUE</text>
<pin name="C" x="0" y="-2.54" visible="off" length="short" direction="pas" rot="R90"/>
<pin name="A" x="-2.54" y="7.62" visible="off" length="short" direction="pas" rot="R270"/>
<pin name="B" x="2.54" y="7.62" visible="off" length="short" direction="pas" rot="R270"/>
</symbol>
<symbol name="TASTER">
<wire x1="-2.54" y1="0" x2="-1.5875" y2="0" width="0.3048" layer="94"/>
<wire x1="-1.5875" y1="0" x2="0.635" y2="1.5875" width="0.3048" layer="94"/>
<wire x1="0.635" y1="0.635" x2="0.635" y2="0" width="0.3048" layer="94"/>
<wire x1="0.635" y1="0" x2="2.54" y2="0" width="0.3048" layer="94"/>
<wire x1="0" y1="1.27" x2="0" y2="3.81" width="0.127" layer="94" style="shortdash"/>
<wire x1="-0.635" y1="3.81" x2="0.635" y2="3.81" width="0.127" layer="94"/>
<wire x1="-0.635" y1="3.81" x2="-0.635" y2="3.4925" width="0.127" layer="94"/>
<wire x1="0.635" y1="3.81" x2="0.635" y2="3.4925" width="0.127" layer="94"/>
<text x="-5.08" y="5.08" size="1.27" layer="95">&gt;PART</text>
<text x="-5.08" y="-2.54" size="1.27" layer="96">&gt;VALUE</text>
<pin name="1" x="5.08" y="0" visible="off" length="short" direction="pas" swaplevel="1" rot="R180"/>
<pin name="2" x="-5.08" y="0" visible="off" length="short" direction="pas" swaplevel="1"/>
</symbol>
<symbol name="GEHAEUSEANSCHLUSS">
<wire x1="0" y1="0" x2="0.9525" y2="0" width="0.254" layer="94"/>
<wire x1="1.5875" y1="0" x2="2.2225" y2="0" width="0.254" layer="94"/>
<wire x1="2.8575" y1="0" x2="3.4925" y2="0" width="0.254" layer="94"/>
<wire x1="4.1275" y1="0" x2="5.08" y2="0" width="0.254" layer="94"/>
<pin name="G" x="0" y="0" visible="pad" length="point" direction="pas"/>
</symbol>
</symbols>
<devicesets>
<deviceset name="EC12E_SW" prefix="SW">
<description>ALPS rotary Encoder EC12E series with switch</description>
<gates>
<gate name="G$1" symbol="ENCODER" x="-10.16" y="-2.54" addlevel="always"/>
<gate name="G$2" symbol="TASTER" x="10.16" y="5.08" addlevel="always"/>
<gate name="G$3" symbol="GEHAEUSEANSCHLUSS" x="10.16" y="-5.08" addlevel="request"/>
<gate name="G$4" symbol="GEHAEUSEANSCHLUSS" x="10.16" y="-7.62" addlevel="request"/>
</gates>
<devices>
<device name="" package="ALPS_EC12E_SW">
<connects>
<connect gate="G$1" pin="A" pad="A"/>
<connect gate="G$1" pin="B" pad="B"/>
<connect gate="G$1" pin="C" pad="C"/>
<connect gate="G$2" pin="1" pad="D"/>
<connect gate="G$2" pin="2" pad="E"/>
<connect gate="G$3" pin="G" pad="GND1"/>
<connect gate="G$4" pin="G" pad="GND2"/>
</connects>
<technologies>
<technology name="">
<attribute name="MF" value="" constant="no"/>
<attribute name="MPN" value="EC12E2424407" constant="no"/>
<attribute name="OC_FARNELL" value="1520813" constant="no"/>
<attribute name="OC_NEWARK" value="74M1068" constant="no"/>
</technology>
</technologies>
</device>
</devices>
</deviceset>
</devicesets>
</library>
<library name="con-jst2">
<description>&lt;b&gt;J.S.T. Connectors&lt;/b&gt;&lt;p&gt;
J.S.T Mfg Co.,Ltd.&lt;p&gt;
http://www.jst-mfg.com&lt;p&gt;
&lt;author&gt;Created by librarian@cadsoft.de&lt;/author&gt;</description>
<packages>
<package name="SM10B-SRSS-TB">
<description>&lt;b&gt;Shrouded Header, side entry type&lt;/b&gt;</description>
<wire x1="-5.873" y1="-1.473" x2="-5.873" y2="2.523" width="0.254" layer="51"/>
<wire x1="-5.873" y1="2.523" x2="5.873" y2="2.523" width="0.254" layer="51"/>
<wire x1="5.873" y1="2.523" x2="5.873" y2="-1.473" width="0.254" layer="51"/>
<wire x1="5.873" y1="-1.473" x2="-5.873" y2="-1.473" width="0.254" layer="51"/>
<wire x1="-5.873" y1="0.5" x2="-5.873" y2="2.523" width="0.254" layer="21"/>
<wire x1="-5.873" y1="2.523" x2="-5.5" y2="2.523" width="0.254" layer="21"/>
<wire x1="5.873" y1="0.5" x2="5.873" y2="2.523" width="0.254" layer="21"/>
<wire x1="5.873" y1="2.523" x2="5.5" y2="2.523" width="0.254" layer="21"/>
<wire x1="-4.5" y1="-1.473" x2="4.5" y2="-1.473" width="0.254" layer="21"/>
<smd name="1" x="-4.5" y="2.975" dx="0.6" dy="1.55" layer="1"/>
<smd name="2" x="-3.5" y="2.975" dx="0.6" dy="1.55" layer="1"/>
<smd name="3" x="-2.5" y="2.975" dx="0.6" dy="1.55" layer="1"/>
<smd name="4" x="-1.5" y="2.975" dx="0.6" dy="1.55" layer="1"/>
<smd name="5" x="-0.5" y="2.975" dx="0.6" dy="1.55" layer="1"/>
<smd name="6" x="0.5" y="2.975" dx="0.6" dy="1.55" layer="1"/>
<smd name="7" x="1.5" y="2.975" dx="0.6" dy="1.55" layer="1"/>
<smd name="8" x="2.5" y="2.975" dx="0.6" dy="1.55" layer="1"/>
<smd name="9" x="3.5" y="2.975" dx="0.6" dy="1.55" layer="1"/>
<smd name="10" x="4.5" y="2.975" dx="0.6" dy="1.55" layer="1"/>
<smd name="FIT@1" x="-5.8" y="-0.9" dx="1.2" dy="1.8" layer="1"/>
<smd name="FIT@2" x="5.8" y="-0.9" dx="1.2" dy="1.8" layer="1"/>
<text x="-6.985" y="-1.905" size="1.27" layer="25" rot="R90">&gt;NAME</text>
<text x="8.255" y="-1.905" size="1.27" layer="27" rot="R90">&gt;VALUE</text>
</package>
<package name="BM10B-SRSS-TB">
<description>&lt;b&gt;Shrouded Header, top entry type&lt;/b&gt;</description>
<wire x1="-5.873" y1="1.473" x2="-5.873" y2="-1.427" width="0.254" layer="51"/>
<wire x1="-5.873" y1="-1.427" x2="5.873" y2="-1.427" width="0.254" layer="51"/>
<wire x1="5.873" y1="-1.427" x2="5.873" y2="1.473" width="0.254" layer="51"/>
<wire x1="5.873" y1="1.473" x2="-5.873" y2="1.473" width="0.254" layer="51"/>
<wire x1="-5.873" y1="-0.5" x2="-5.873" y2="-1.427" width="0.254" layer="21"/>
<wire x1="-5.873" y1="-1.427" x2="-5.5" y2="-1.427" width="0.254" layer="21"/>
<wire x1="5.873" y1="-0.5" x2="5.873" y2="-1.427" width="0.254" layer="21"/>
<wire x1="5.873" y1="-1.427" x2="5.5" y2="-1.427" width="0.254" layer="21"/>
<wire x1="-4.5" y1="1.473" x2="4.5" y2="1.473" width="0.254" layer="21"/>
<smd name="10" x="-4.5" y="-1.625" dx="0.6" dy="1.55" layer="1"/>
<smd name="9" x="-3.5" y="-1.625" dx="0.6" dy="1.55" layer="1"/>
<smd name="8" x="-2.5" y="-1.625" dx="0.6" dy="1.55" layer="1"/>
<smd name="7" x="-1.5" y="-1.625" dx="0.6" dy="1.55" layer="1"/>
<smd name="6" x="-0.5" y="-1.625" dx="0.6" dy="1.55" layer="1"/>
<smd name="5" x="0.5" y="-1.625" dx="0.6" dy="1.55" layer="1"/>
<smd name="4" x="1.5" y="-1.625" dx="0.6" dy="1.55" layer="1"/>
<smd name="3" x="2.5" y="-1.625" dx="0.6" dy="1.55" layer="1"/>
<smd name="2" x="3.5" y="-1.625" dx="0.6" dy="1.55" layer="1"/>
<smd name="1" x="4.5" y="-1.625" dx="0.6" dy="1.55" layer="1"/>
<smd name="FIT@1" x="5.8" y="0.9" dx="1.2" dy="1.8" layer="1"/>
<smd name="FIT@2" x="-5.8" y="0.9" dx="1.2" dy="1.8" layer="1"/>
<text x="-8.255" y="1.905" size="1.27" layer="25" rot="R270">&gt;NAME</text>
<text x="6.985" y="1.905" size="1.27" layer="27" rot="R270">&gt;VALUE</text>
</package>
</packages>
<symbols>
<symbol name="KV">
<wire x1="1.27" y1="0" x2="0" y2="0" width="0.6096" layer="94"/>
<text x="2.54" y="-0.762" size="1.524" layer="95">&gt;NAME</text>
<text x="-0.762" y="1.397" size="1.524" layer="96">&gt;VALUE</text>
<pin name="S" x="-2.54" y="0" visible="off" length="short" direction="pas"/>
</symbol>
<symbol name="K">
<wire x1="1.27" y1="0" x2="0" y2="0" width="0.6096" layer="94"/>
<text x="2.54" y="-0.762" size="1.524" layer="95">&gt;NAME</text>
<pin name="S" x="-2.54" y="0" visible="off" length="short" direction="pas"/>
</symbol>
</symbols>
<devicesets>
<deviceset name="?M10B-SRSS-TB" prefix="X">
<description>&lt;b&gt;Disconnectable Crimp style connector, 1.0mm pitch&lt;/b&gt;&lt;br&gt;10 contacts</description>
<gates>
<gate name="-1" symbol="KV" x="0" y="12.7" addlevel="always" swaplevel="1"/>
<gate name="-2" symbol="K" x="0" y="10.16" addlevel="always" swaplevel="1"/>
<gate name="-3" symbol="K" x="0" y="7.62" addlevel="always" swaplevel="1"/>
<gate name="-4" symbol="K" x="0" y="5.08" addlevel="always" swaplevel="1"/>
<gate name="-5" symbol="K" x="0" y="2.54" addlevel="always" swaplevel="1"/>
<gate name="-6" symbol="K" x="0" y="0" addlevel="always" swaplevel="1"/>
<gate name="-7" symbol="K" x="0" y="-2.54" addlevel="always" swaplevel="1"/>
<gate name="-8" symbol="K" x="0" y="-5.08" addlevel="always" swaplevel="1"/>
<gate name="-9" symbol="K" x="0" y="-7.62" addlevel="always" swaplevel="1"/>
<gate name="-10" symbol="K" x="0" y="-10.16" addlevel="always" swaplevel="1"/>
</gates>
<devices>
<device name="S" package="SM10B-SRSS-TB">
<connects>
<connect gate="-1" pin="S" pad="1"/>
<connect gate="-10" pin="S" pad="10"/>
<connect gate="-2" pin="S" pad="2"/>
<connect gate="-3" pin="S" pad="3"/>
<connect gate="-4" pin="S" pad="4"/>
<connect gate="-5" pin="S" pad="5"/>
<connect gate="-6" pin="S" pad="6"/>
<connect gate="-7" pin="S" pad="7"/>
<connect gate="-8" pin="S" pad="8"/>
<connect gate="-9" pin="S" pad="9"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="B" package="BM10B-SRSS-TB">
<connects>
<connect gate="-1" pin="S" pad="1"/>
<connect gate="-10" pin="S" pad="10"/>
<connect gate="-2" pin="S" pad="2"/>
<connect gate="-3" pin="S" pad="3"/>
<connect gate="-4" pin="S" pad="4"/>
<connect gate="-5" pin="S" pad="5"/>
<connect gate="-6" pin="S" pad="6"/>
<connect gate="-7" pin="S" pad="7"/>
<connect gate="-8" pin="S" pad="8"/>
<connect gate="-9" pin="S" pad="9"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
</devicesets>
</library>
<library name="dp_devices">
<description>Dangerous Prototypes Standard PCB sizes
http://dangerousprototypes.com</description>
<packages>
<package name="LED-3MM">
<wire x1="1.5748" y1="-1.27" x2="1.5748" y2="1.27" width="0.254" layer="51"/>
<wire x1="-1.524" y1="0" x2="-1.1708" y2="0.9756" width="0.1524" layer="51" curve="-39.80361"/>
<wire x1="-1.524" y1="0" x2="-1.1391" y2="-1.0125" width="0.1524" layer="51" curve="41.633208"/>
<wire x1="1.1571" y1="0.9918" x2="1.524" y2="0" width="0.1524" layer="51" curve="-40.601165"/>
<wire x1="1.1708" y1="-0.9756" x2="1.524" y2="0" width="0.1524" layer="51" curve="39.80361"/>
<wire x1="0" y1="1.524" x2="1.2401" y2="0.8858" width="0.1524" layer="21" curve="-54.461337"/>
<wire x1="-1.2192" y1="0.9144" x2="0" y2="1.524" width="0.1524" layer="21" curve="-53.130102"/>
<wire x1="0" y1="-1.524" x2="1.203" y2="-0.9356" width="0.1524" layer="21" curve="52.126876"/>
<wire x1="-1.203" y1="-0.9356" x2="0" y2="-1.524" width="0.1524" layer="21" curve="52.126876"/>
<wire x1="-0.635" y1="0" x2="0" y2="0.635" width="0.1524" layer="51" curve="-90"/>
<wire x1="-1.016" y1="0" x2="0" y2="1.016" width="0.1524" layer="51" curve="-90"/>
<wire x1="0" y1="-0.635" x2="0.635" y2="0" width="0.1524" layer="51" curve="90"/>
<wire x1="0" y1="-1.016" x2="1.016" y2="0" width="0.1524" layer="51" curve="90"/>
<wire x1="0" y1="2.032" x2="1.561" y2="1.3009" width="0.254" layer="21" curve="-50.193108"/>
<wire x1="-1.7929" y1="0.9562" x2="0" y2="2.032" width="0.254" layer="21" curve="-61.926949"/>
<wire x1="0" y1="-2.032" x2="1.5512" y2="-1.3126" width="0.254" layer="21" curve="49.763022"/>
<wire x1="-1.7643" y1="-1.0082" x2="0" y2="-2.032" width="0.254" layer="21" curve="60.255215"/>
<wire x1="-2.032" y1="0" x2="-1.7891" y2="0.9634" width="0.254" layer="51" curve="-28.301701"/>
<wire x1="-2.032" y1="0" x2="-1.7306" y2="-1.065" width="0.254" layer="51" curve="31.60822"/>
<pad name="A" x="-1.27" y="0" drill="0.8128" shape="octagon"/>
<pad name="K" x="1.27" y="0" drill="0.8128" shape="octagon"/>
<text x="1.905" y="0.381" size="1.27" layer="25" font="vector" ratio="10">&gt;NAME</text>
<text x="1.905" y="-1.651" size="1.27" layer="27" font="vector" ratio="10">&gt;VALUE</text>
</package>
<package name="LED-5MM">
<wire x1="2.54" y1="-1.905" x2="2.54" y2="1.905" width="0.2032" layer="21"/>
<wire x1="2.54" y1="-1.905" x2="2.54" y2="1.905" width="0.254" layer="21" curve="-286.260205"/>
<wire x1="-1.143" y1="0" x2="0" y2="1.143" width="0.1524" layer="51" curve="-90"/>
<wire x1="0" y1="-1.143" x2="1.143" y2="0" width="0.1524" layer="51" curve="90"/>
<wire x1="-1.651" y1="0" x2="0" y2="1.651" width="0.1524" layer="51" curve="-90"/>
<wire x1="0" y1="-1.651" x2="1.651" y2="0" width="0.1524" layer="51" curve="90"/>
<wire x1="-2.159" y1="0" x2="0" y2="2.159" width="0.1524" layer="51" curve="-90"/>
<wire x1="0" y1="-2.159" x2="2.159" y2="0" width="0.1524" layer="51" curve="90"/>
<circle x="0" y="0" radius="2.54" width="0.1524" layer="21"/>
<pad name="A" x="-1.27" y="0" drill="0.8128" shape="octagon"/>
<pad name="K" x="1.27" y="0" drill="0.8128" shape="octagon"/>
<text x="3.175" y="0.5334" size="1.27" layer="25" ratio="10">&gt;NAME</text>
<text x="3.2004" y="-1.8034" size="1.27" layer="27" ratio="10">&gt;VALUE</text>
</package>
<package name="LED-805">
<wire x1="-0.35" y1="0.925" x2="0.35" y2="0.925" width="0.1016" layer="51" curve="162.394521"/>
<wire x1="-0.35" y1="-0.925" x2="0.35" y2="-0.925" width="0.1016" layer="51" curve="-162.394521"/>
<wire x1="0.575" y1="0.525" x2="0.575" y2="-0.525" width="0.1016" layer="51"/>
<wire x1="-0.575" y1="-0.5" x2="-0.575" y2="0.925" width="0.1016" layer="51"/>
<circle x="-0.45" y="0.85" radius="0.103" width="0.1016" layer="51"/>
<smd name="C" x="0" y="1.05" dx="1.2" dy="1.2" layer="1"/>
<smd name="A" x="0" y="-1.05" dx="1.2" dy="1.2" layer="1"/>
<text x="-1.27" y="-1.27" size="1.27" layer="25" ratio="10" rot="R90">&gt;NAME</text>
<text x="2.54" y="-1.27" size="1.27" layer="27" ratio="10" rot="R90">&gt;VALUE</text>
<rectangle x1="0.3" y1="0.5" x2="0.625" y2="1" layer="51"/>
<rectangle x1="-0.325" y1="0.5" x2="-0.175" y2="0.75" layer="51"/>
<rectangle x1="0.175" y1="0.5" x2="0.325" y2="0.75" layer="51"/>
<rectangle x1="-0.2" y1="0.5" x2="0.2" y2="0.675" layer="51"/>
<rectangle x1="0.3" y1="-1" x2="0.625" y2="-0.5" layer="51"/>
<rectangle x1="-0.625" y1="-1" x2="-0.3" y2="-0.5" layer="51"/>
<rectangle x1="0.175" y1="-0.75" x2="0.325" y2="-0.5" layer="51"/>
<rectangle x1="-0.325" y1="-0.75" x2="-0.175" y2="-0.5" layer="51"/>
<rectangle x1="-0.2" y1="-0.675" x2="0.2" y2="-0.5" layer="51"/>
<rectangle x1="-0.1" y1="0" x2="0.1" y2="0.2" layer="21"/>
<rectangle x1="-0.6" y1="0.5" x2="-0.3" y2="0.8" layer="51"/>
<rectangle x1="-0.625" y1="0.925" x2="-0.3" y2="1" layer="51"/>
</package>
<package name="LED-5MM-90DEG">
<wire x1="-2.55" y1="2.5" x2="2.65" y2="2.5" width="0.1524" layer="21"/>
<wire x1="-2.55" y1="-2.5" x2="2.65" y2="-2.5" width="0.1524" layer="21"/>
<wire x1="2.65" y1="-2.5" x2="3.65" y2="-2.5" width="0.1524" layer="21"/>
<wire x1="2.65" y1="2.9" x2="2.65" y2="-2.5" width="0.1524" layer="21"/>
<wire x1="3.65" y1="2.9" x2="3.65" y2="-2.5" width="0.1524" layer="21"/>
<wire x1="2.65" y1="2.9" x2="3.65" y2="2.9" width="0.1524" layer="21"/>
<wire x1="-2.55" y1="2.5" x2="-2.55" y2="-2.5" width="0.1524" layer="21" curve="180"/>
<pad name="A" x="5.1" y="1.25" drill="0.8128" shape="octagon"/>
<pad name="K" x="5.1" y="-1.25" drill="0.8128" shape="octagon"/>
<text x="-1.778" y="3.302" size="1.27" layer="25" font="vector" ratio="10">&gt;NAME</text>
<text x="-4.5" y="-0.5" size="1.27" layer="27" font="vector" ratio="10">&gt;VALUE</text>
<text x="4.572" y="-3.556" size="1.016" layer="21" ratio="10">K</text>
<rectangle x1="3.65" y1="-1.5" x2="4.35" y2="-1" layer="21"/>
<rectangle x1="3.65" y1="1" x2="4.35" y2="1.5" layer="21"/>
</package>
<package name="LED-603">
<wire x1="-0.3" y1="0.8" x2="0.3" y2="0.8" width="0.1016" layer="51" curve="170.055574"/>
<wire x1="-0.275" y1="-0.825" x2="0.275" y2="-0.825" width="0.0508" layer="51" curve="-180"/>
<wire x1="-0.4" y1="0.375" x2="-0.4" y2="-0.35" width="0.1016" layer="51"/>
<wire x1="0.4" y1="0.35" x2="0.4" y2="-0.35" width="0.1016" layer="51"/>
<circle x="-0.35" y="0.625" radius="0.075" width="0.0508" layer="51"/>
<smd name="C" x="0" y="0.75" dx="0.8" dy="0.8" layer="1"/>
<smd name="A" x="0" y="-0.75" dx="0.8" dy="0.8" layer="1"/>
<text x="-0.635" y="-1.27" size="1.27" layer="25" ratio="10" rot="R90">&gt;NAME</text>
<text x="1.905" y="-1.27" size="1.27" layer="27" ratio="10" rot="R90">&gt;VALUE</text>
<rectangle x1="-0.45" y1="0.7" x2="-0.25" y2="0.85" layer="51"/>
<rectangle x1="-0.275" y1="0.55" x2="-0.225" y2="0.6" layer="51"/>
<rectangle x1="-0.45" y1="0.35" x2="-0.4" y2="0.725" layer="51"/>
<rectangle x1="0.25" y1="0.55" x2="0.45" y2="0.85" layer="51"/>
<rectangle x1="-0.45" y1="0.35" x2="0.45" y2="0.575" layer="51"/>
<rectangle x1="-0.45" y1="-0.85" x2="-0.25" y2="-0.35" layer="51"/>
<rectangle x1="0.25" y1="-0.85" x2="0.45" y2="-0.35" layer="51"/>
<rectangle x1="-0.275" y1="-0.575" x2="0.275" y2="-0.35" layer="51"/>
<rectangle x1="-0.275" y1="-0.65" x2="-0.175" y2="-0.55" layer="51"/>
<rectangle x1="0.175" y1="-0.65" x2="0.275" y2="-0.55" layer="51"/>
<rectangle x1="-0.125" y1="0" x2="0.125" y2="0.25" layer="21"/>
</package>
</packages>
<symbols>
<symbol name="LED">
<wire x1="1.27" y1="0" x2="0" y2="-2.54" width="0.254" layer="94"/>
<wire x1="0" y1="-2.54" x2="-1.27" y2="0" width="0.254" layer="94"/>
<wire x1="1.27" y1="-2.54" x2="0" y2="-2.54" width="0.254" layer="94"/>
<wire x1="0" y1="-2.54" x2="-1.27" y2="-2.54" width="0.254" layer="94"/>
<wire x1="1.27" y1="0" x2="0" y2="0" width="0.254" layer="94"/>
<wire x1="0" y1="0" x2="-1.27" y2="0" width="0.254" layer="94"/>
<wire x1="0" y1="0" x2="0" y2="-2.54" width="0.1524" layer="94"/>
<wire x1="-2.032" y1="-0.762" x2="-3.429" y2="-2.159" width="0.1524" layer="94"/>
<wire x1="-1.905" y1="-1.905" x2="-3.302" y2="-3.302" width="0.1524" layer="94"/>
<text x="3.556" y="-4.572" size="1.778" layer="95" rot="R90">&gt;NAME</text>
<text x="5.715" y="-4.572" size="1.778" layer="96" rot="R90">&gt;VALUE</text>
<pin name="C" x="0" y="-5.08" visible="off" length="short" direction="pas" rot="R90"/>
<pin name="A" x="0" y="2.54" visible="off" length="short" direction="pas" rot="R270"/>
<polygon width="0.1524" layer="94">
<vertex x="-3.429" y="-2.159"/>
<vertex x="-3.048" y="-1.27"/>
<vertex x="-2.54" y="-1.778"/>
</polygon>
<polygon width="0.1524" layer="94">
<vertex x="-3.302" y="-3.302"/>
<vertex x="-2.921" y="-2.413"/>
<vertex x="-2.413" y="-2.921"/>
</polygon>
</symbol>
</symbols>
<devicesets>
<deviceset name="LED" prefix="LED" uservalue="yes">
<description>Light Emitting Diode</description>
<gates>
<gate name="LED" symbol="LED" x="0" y="0"/>
</gates>
<devices>
<device name="-3MM" package="LED-3MM">
<connects>
<connect gate="LED" pin="A" pad="A"/>
<connect gate="LED" pin="C" pad="K"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="-5MM" package="LED-5MM">
<connects>
<connect gate="LED" pin="A" pad="A"/>
<connect gate="LED" pin="C" pad="K"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="-0805" package="LED-805">
<connects>
<connect gate="LED" pin="A" pad="A"/>
<connect gate="LED" pin="C" pad="C"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="-5MM-90DEG" package="LED-5MM-90DEG">
<connects>
<connect gate="LED" pin="A" pad="A"/>
<connect gate="LED" pin="C" pad="K"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="-0603" package="LED-603">
<connects>
<connect gate="LED" pin="A" pad="A"/>
<connect gate="LED" pin="C" pad="C"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
</devicesets>
</library>
<library name="diy-modules">
<description>&lt;b&gt;DIY Modules for Arduino, Raspberry Pi, CubieBoard etc.&lt;/b&gt;
&lt;br&gt;&lt;br&gt;
The library contains a list of symbols and footprints for popular, cheap and easy-to-use electronic modules.&lt;br&gt;
The modules are intend to work with microprocessor-based platforms such as &lt;a href="http://arduino.cc"&gt;Arduino&lt;/a&gt;, &lt;a href="http://raspberrypi.org/"&gt;Raspberry Pi&lt;/a&gt;, &lt;a href="http://cubieboard.org/"&gt;CubieBoard&lt;/a&gt;, &lt;a href="http://beagleboard.org/"&gt;BeagleBone&lt;/a&gt; and many others. There are many manufacturers of the modules in the world. Almost all of them can be bought on &lt;a href="ebay.com"&gt;ebay.com&lt;/a&gt;.&lt;br&gt;
&lt;br&gt;
By using this library, you can design a PCB for devices created with usage of modules. Even if you do not need to create PCB design, you can also use the library to quickly document your work by drawing schematics of devices built by you.&lt;br&gt;
&lt;br&gt;
The latest version, examples, photos and much more can be found at: &lt;b&gt;&lt;a href="http://diymodules.org/eagle"&gt;diymodules.org/eagle&lt;/a&gt;&lt;/b&gt;&lt;br&gt;&lt;br&gt;
Comments, suggestions and bug reports please send to: &lt;b&gt;&lt;a href="mailto:eagle@diymodules.org"&gt;eagle@diymodules.org&lt;/b&gt;&lt;/a&gt;&lt;br&gt;&lt;br&gt;
&lt;i&gt;Version: 1.8.0 (2017-Jul-02)&lt;/i&gt;&lt;br&gt;
&lt;i&gt;Created by: Miroslaw Brudnowski&lt;/i&gt;&lt;br&gt;&lt;br&gt;
&lt;i&gt;Released under the Creative Commons Attribution 4.0 International License: &lt;a href="http://creativecommons.org/licenses/by/4.0"&gt;http://creativecommons.org/licenses/by/4.0&lt;/a&gt;&lt;/i&gt;
&lt;br&gt;&lt;br&gt;
&lt;center&gt;
&lt;a href="http://diymodules.org/eagle"&gt;&lt;img src="http://www.diymodules.org/img/diymodules-lbr-image.php?v=1.8.0" alt="DIYmodules.org"&gt;&lt;/a&gt;
&lt;/center&gt;</description>
<packages>
<package name="DISPLAY-OLED-128X64">
<description>&lt;b&gt;128x64 Dot Matrix OLED Module&lt;/b&gt;&lt;br /&gt;
Variant with SPI interface</description>
<wire x1="-13.716" y1="13.97" x2="13.716" y2="13.97" width="0.127" layer="21"/>
<wire x1="13.716" y1="13.97" x2="13.716" y2="-13.97" width="0.127" layer="21"/>
<wire x1="13.716" y1="-13.97" x2="-13.716" y2="-13.97" width="0.127" layer="21"/>
<wire x1="-13.716" y1="-13.97" x2="-13.716" y2="13.97" width="0.127" layer="21"/>
<pad name="1" x="-7.62" y="12.7" drill="1" shape="square"/>
<pad name="2" x="-5.08" y="12.7" drill="1"/>
<pad name="3" x="-2.54" y="12.7" drill="1"/>
<pad name="4" x="0" y="12.7" drill="1"/>
<pad name="5" x="2.54" y="12.7" drill="1"/>
<pad name="6" x="5.08" y="12.7" drill="1"/>
<pad name="7" x="7.62" y="12.7" drill="1"/>
<wire x1="-8.89" y1="13.335" x2="-8.255" y2="13.97" width="0.127" layer="21"/>
<wire x1="-6.985" y1="13.97" x2="-6.35" y2="13.335" width="0.127" layer="21"/>
<wire x1="-6.35" y1="13.335" x2="-5.715" y2="13.97" width="0.127" layer="21"/>
<wire x1="-4.445" y1="13.97" x2="-3.81" y2="13.335" width="0.127" layer="21"/>
<wire x1="-3.81" y1="13.335" x2="-3.175" y2="13.97" width="0.127" layer="21"/>
<wire x1="-1.905" y1="13.97" x2="-1.27" y2="13.335" width="0.127" layer="21"/>
<wire x1="-1.27" y1="13.335" x2="-0.635" y2="13.97" width="0.127" layer="21"/>
<wire x1="0.635" y1="13.97" x2="1.27" y2="13.335" width="0.127" layer="21"/>
<wire x1="1.27" y1="13.335" x2="1.905" y2="13.97" width="0.127" layer="21"/>
<wire x1="3.175" y1="13.97" x2="3.81" y2="13.335" width="0.127" layer="21"/>
<wire x1="3.81" y1="13.335" x2="4.445" y2="13.97" width="0.127" layer="21"/>
<wire x1="5.715" y1="13.97" x2="6.35" y2="13.335" width="0.127" layer="21"/>
<wire x1="6.35" y1="13.335" x2="6.985" y2="13.97" width="0.127" layer="21"/>
<wire x1="8.255" y1="13.97" x2="8.89" y2="13.335" width="0.127" layer="21"/>
<wire x1="8.89" y1="12.065" x2="8.255" y2="11.43" width="0.127" layer="21"/>
<wire x1="8.255" y1="11.43" x2="6.985" y2="11.43" width="0.127" layer="21"/>
<wire x1="6.985" y1="11.43" x2="6.35" y2="12.065" width="0.127" layer="21"/>
<wire x1="6.35" y1="12.065" x2="5.715" y2="11.43" width="0.127" layer="21"/>
<wire x1="5.715" y1="11.43" x2="4.445" y2="11.43" width="0.127" layer="21"/>
<wire x1="4.445" y1="11.43" x2="3.81" y2="12.065" width="0.127" layer="21"/>
<wire x1="3.81" y1="12.065" x2="3.175" y2="11.43" width="0.127" layer="21"/>
<wire x1="3.175" y1="11.43" x2="1.905" y2="11.43" width="0.127" layer="21"/>
<wire x1="1.905" y1="11.43" x2="1.27" y2="12.065" width="0.127" layer="21"/>
<wire x1="1.27" y1="12.065" x2="0.635" y2="11.43" width="0.127" layer="21"/>
<wire x1="0.635" y1="11.43" x2="-0.635" y2="11.43" width="0.127" layer="21"/>
<wire x1="-0.635" y1="11.43" x2="-1.27" y2="12.065" width="0.127" layer="21"/>
<wire x1="-1.27" y1="12.065" x2="-1.905" y2="11.43" width="0.127" layer="21"/>
<wire x1="-1.905" y1="11.43" x2="-3.175" y2="11.43" width="0.127" layer="21"/>
<wire x1="-3.175" y1="11.43" x2="-3.81" y2="12.065" width="0.127" layer="21"/>
<wire x1="-3.81" y1="12.065" x2="-4.445" y2="11.43" width="0.127" layer="21"/>
<wire x1="-4.445" y1="11.43" x2="-5.715" y2="11.43" width="0.127" layer="21"/>
<wire x1="-5.715" y1="11.43" x2="-6.35" y2="12.065" width="0.127" layer="21"/>
<wire x1="-6.35" y1="12.065" x2="-6.985" y2="11.43" width="0.127" layer="21"/>
<wire x1="-6.985" y1="11.43" x2="-8.255" y2="11.43" width="0.127" layer="21"/>
<wire x1="-8.255" y1="11.43" x2="-8.89" y2="12.065" width="0.127" layer="21"/>
<wire x1="-8.89" y1="12.065" x2="-8.89" y2="13.335" width="0.127" layer="21"/>
<hole x="-11.684" y="11.938" drill="2"/>
<wire x1="-11.43" y1="8.128" x2="11.43" y2="8.128" width="0.127" layer="21"/>
<wire x1="11.43" y1="8.128" x2="11.43" y2="-5.588" width="0.127" layer="21"/>
<wire x1="11.43" y1="-5.588" x2="-11.43" y2="-5.588" width="0.127" layer="21"/>
<wire x1="-11.43" y1="-5.588" x2="-11.43" y2="8.128" width="0.127" layer="21"/>
<text x="0" y="15.24" size="1.27" layer="25" align="bottom-center">&gt;NAME</text>
<text x="0" y="-15.24" size="1.27" layer="27" align="top-center">&gt;VALUE</text>
<hole x="11.684" y="11.938" drill="2"/>
<hole x="11.684" y="-11.938" drill="2"/>
<hole x="-11.684" y="-11.938" drill="2"/>
<wire x1="8.89" y1="13.335" x2="8.89" y2="12.065" width="0.127" layer="21"/>
</package>
</packages>
<symbols>
<symbol name="DISPLAY-OLED-128X64">
<description>&lt;b&gt;128x64 Dot Matrix OLED Module&lt;/b&gt;</description>
<wire x1="-15.24" y1="15.24" x2="15.24" y2="15.24" width="0.254" layer="94"/>
<wire x1="15.24" y1="15.24" x2="15.24" y2="-15.24" width="0.254" layer="94"/>
<wire x1="15.24" y1="-15.24" x2="-15.24" y2="-15.24" width="0.254" layer="94"/>
<wire x1="-15.24" y1="-15.24" x2="-15.24" y2="15.24" width="0.254" layer="94"/>
<pin name="GND" x="-7.62" y="20.32" length="middle" direction="pwr" rot="R270"/>
<pin name="VCC" x="-5.08" y="20.32" length="middle" direction="pwr" rot="R270"/>
<pin name="D0" x="-2.54" y="20.32" length="middle" rot="R270"/>
<pin name="D1" x="0" y="20.32" length="middle" rot="R270"/>
<pin name="RES" x="2.54" y="20.32" length="middle" rot="R270"/>
<pin name="DC" x="5.08" y="20.32" length="middle" rot="R270"/>
<pin name="CS" x="7.62" y="20.32" length="middle" rot="R270"/>
<wire x1="-12.7" y1="7.62" x2="12.7" y2="7.62" width="0.254" layer="94"/>
<wire x1="12.7" y1="7.62" x2="12.7" y2="-7.62" width="0.254" layer="94"/>
<wire x1="12.7" y1="-7.62" x2="-12.7" y2="-7.62" width="0.254" layer="94"/>
<wire x1="-12.7" y1="-7.62" x2="-12.7" y2="7.62" width="0.254" layer="94"/>
<rectangle x1="-11.684" y1="5.08" x2="-10.16" y2="6.604" layer="94"/>
<rectangle x1="-9.144" y1="5.08" x2="-7.62" y2="6.604" layer="94"/>
<rectangle x1="-11.684" y1="2.54" x2="-10.16" y2="4.064" layer="94"/>
<text x="17.78" y="12.7" size="1.778" layer="95">&gt;NAME</text>
<text x="17.78" y="10.16" size="1.778" layer="96">&gt;VALUE</text>
<rectangle x1="-6.604" y1="5.08" x2="-5.08" y2="6.604" layer="94"/>
<rectangle x1="-11.684" y1="0" x2="-10.16" y2="1.524" layer="94"/>
<rectangle x1="-9.144" y1="2.54" x2="-7.62" y2="4.064" layer="94"/>
<text x="10.16" y="-5.08" size="1.778" layer="94" align="bottom-right">128x64</text>
<text x="10.16" y="-2.54" size="1.778" layer="94" align="bottom-right">OLED</text>
</symbol>
</symbols>
<devicesets>
<deviceset name="DISPLAY-OLED-128X64">
<description>&lt;b&gt;128x64 Dot Matrix OLED Module&lt;/b&gt; based on &lt;b&gt;SSD1306&lt;/b&gt; chip&lt;br /&gt;
Variant with &lt;b&gt;SPI interface&lt;/b&gt;
&lt;p&gt;More details available here:&lt;br /&gt;
&lt;a href="http://www.instructables.com/id/How-to-use-OLED-display-arduino-module/"&gt;http://www.instructables.com/id/How-to-use-OLED-display-arduino-module/&lt;/a&gt;&lt;/p&gt;
&lt;p&gt;&lt;b&gt;SSD1306&lt;/b&gt; datasheet:&lt;br&gt;
&lt;a href="https://cdn-shop.adafruit.com/datasheets/SSD1306.pdf"&gt;https://cdn-shop.adafruit.com/datasheets/SSD1306.pdf&lt;/a&gt;&lt;/p&gt;
&lt;p&gt;&lt;b&gt;&lt;a href="http://www.ebay.com/sch/oled+display+128x64"&gt;Click here to find device on ebay.com&lt;/a&gt;&lt;/b&gt;&lt;br /&gt;
&lt;b&gt;Note:&lt;/b&gt; There are two variants: I2C and SPI. Search for the proper version.&lt;/p&gt;

&lt;p&gt;&lt;img alt="photo" src="http://www.diymodules.org/img/device-photo.php?name=DISPLAY-OLED-128X64"&gt;&lt;/p&gt;</description>
<gates>
<gate name="G$1" symbol="DISPLAY-OLED-128X64" x="0" y="0"/>
</gates>
<devices>
<device name="" package="DISPLAY-OLED-128X64">
<connects>
<connect gate="G$1" pin="CS" pad="7"/>
<connect gate="G$1" pin="D0" pad="3"/>
<connect gate="G$1" pin="D1" pad="4"/>
<connect gate="G$1" pin="DC" pad="6"/>
<connect gate="G$1" pin="GND" pad="1"/>
<connect gate="G$1" pin="RES" pad="5"/>
<connect gate="G$1" pin="VCC" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
</devicesets>
</library>
<library name="EAGLE_BoosterPack_Library">
<packages>
<package name="TACTILE-PTH">
<description>&lt;b&gt;OMRON SWITCH&lt;/b&gt;</description>
<wire x1="3.048" y1="1.016" x2="3.048" y2="2.54" width="0.2032" layer="51"/>
<wire x1="3.048" y1="2.54" x2="2.54" y2="3.048" width="0.2032" layer="51"/>
<wire x1="2.54" y1="-3.048" x2="3.048" y2="-2.54" width="0.2032" layer="51"/>
<wire x1="3.048" y1="-2.54" x2="3.048" y2="-1.016" width="0.2032" layer="51"/>
<wire x1="-2.54" y1="3.048" x2="-3.048" y2="2.54" width="0.2032" layer="51"/>
<wire x1="-3.048" y1="2.54" x2="-3.048" y2="1.016" width="0.2032" layer="51"/>
<wire x1="-2.54" y1="-3.048" x2="-3.048" y2="-2.54" width="0.2032" layer="51"/>
<wire x1="-3.048" y1="-2.54" x2="-3.048" y2="-1.016" width="0.2032" layer="51"/>
<wire x1="2.54" y1="-3.048" x2="2.159" y2="-3.048" width="0.2032" layer="51"/>
<wire x1="-2.54" y1="-3.048" x2="-2.159" y2="-3.048" width="0.2032" layer="51"/>
<wire x1="-2.54" y1="3.048" x2="-2.159" y2="3.048" width="0.2032" layer="51"/>
<wire x1="2.54" y1="3.048" x2="2.159" y2="3.048" width="0.2032" layer="51"/>
<wire x1="2.159" y1="3.048" x2="-2.159" y2="3.048" width="0.2032" layer="21"/>
<wire x1="-2.159" y1="-3.048" x2="2.159" y2="-3.048" width="0.2032" layer="21"/>
<wire x1="3.048" y1="0.998" x2="3.048" y2="-1.016" width="0.2032" layer="21"/>
<wire x1="-3.048" y1="1.028" x2="-3.048" y2="-1.016" width="0.2032" layer="21"/>
<wire x1="-2.54" y1="1.27" x2="-2.54" y2="0.508" width="0.2032" layer="51"/>
<wire x1="-2.54" y1="-0.508" x2="-2.54" y2="-1.27" width="0.2032" layer="51"/>
<wire x1="-2.54" y1="0.508" x2="-2.159" y2="-0.381" width="0.2032" layer="51"/>
<circle x="0" y="0" radius="1.778" width="0.2032" layer="21"/>
<pad name="1" x="-3.2512" y="2.2606" drill="1.016" diameter="1.8796"/>
<pad name="2" x="3.2512" y="2.2606" drill="1.016" diameter="1.8796"/>
<pad name="3" x="-3.2512" y="-2.2606" drill="1.016" diameter="1.8796"/>
<pad name="4" x="3.2512" y="-2.2606" drill="1.016" diameter="1.8796"/>
<text x="-2.54" y="3.81" size="1.27" layer="25" ratio="10">&gt;NAME</text>
</package>
<package name="TACTILE_SWITCH_SMD">
<wire x1="-1.54" y1="-2.54" x2="-2.54" y2="-1.54" width="0.2032" layer="51"/>
<wire x1="-2.54" y1="-1.24" x2="-2.54" y2="1.27" width="0.2032" layer="21"/>
<wire x1="-2.54" y1="1.54" x2="-1.54" y2="2.54" width="0.2032" layer="51"/>
<wire x1="-1.54" y1="2.54" x2="1.54" y2="2.54" width="0.2032" layer="21"/>
<wire x1="1.54" y1="2.54" x2="2.54" y2="1.54" width="0.2032" layer="51"/>
<wire x1="2.54" y1="1.24" x2="2.54" y2="-1.24" width="0.2032" layer="21"/>
<wire x1="2.54" y1="-1.54" x2="1.54" y2="-2.54" width="0.2032" layer="51"/>
<wire x1="1.54" y1="-2.54" x2="-1.54" y2="-2.54" width="0.2032" layer="21"/>
<wire x1="1.905" y1="1.27" x2="1.905" y2="0.445" width="0.127" layer="51"/>
<wire x1="1.905" y1="0.445" x2="2.16" y2="-0.01" width="0.127" layer="51"/>
<wire x1="1.905" y1="-0.23" x2="1.905" y2="-1.115" width="0.127" layer="51"/>
<circle x="0" y="0" radius="1.27" width="0.2032" layer="21"/>
<smd name="1" x="-2.54" y="1.905" dx="0.762" dy="1.524" layer="1" rot="R90"/>
<smd name="2" x="2.54" y="1.905" dx="0.762" dy="1.524" layer="1" rot="R90"/>
<smd name="3" x="-2.54" y="-1.905" dx="0.762" dy="1.524" layer="1" rot="R90"/>
<smd name="4" x="2.54" y="-1.905" dx="0.762" dy="1.524" layer="1" rot="R90"/>
<text x="-0.889" y="1.778" size="0.4064" layer="25">&gt;NAME</text>
<text x="-0.889" y="-2.032" size="0.4064" layer="27">&gt;Value</text>
</package>
<package name="TACTILE_SWITCH_SMD-2">
<wire x1="1.905" y1="1.27" x2="1.905" y2="0.445" width="0.127" layer="51"/>
<wire x1="1.905" y1="0.445" x2="2.16" y2="-0.01" width="0.127" layer="51"/>
<wire x1="1.905" y1="-0.23" x2="1.905" y2="-1.115" width="0.127" layer="51"/>
<wire x1="-2.25" y1="2.25" x2="2.25" y2="2.25" width="0.127" layer="51"/>
<wire x1="2.25" y1="2.25" x2="2.25" y2="-2.25" width="0.127" layer="51"/>
<wire x1="2.25" y1="-2.25" x2="-2.25" y2="-2.25" width="0.127" layer="51"/>
<wire x1="-2.25" y1="-2.25" x2="-2.25" y2="2.25" width="0.127" layer="51"/>
<wire x1="-2.2" y1="0.8" x2="-2.2" y2="-0.8" width="0.2032" layer="21"/>
<wire x1="1.3" y1="2.2" x2="-1.3" y2="2.2" width="0.2032" layer="21"/>
<wire x1="2.2" y1="-0.8" x2="2.2" y2="0.8" width="0.2032" layer="21"/>
<wire x1="-1.3" y1="-2.2" x2="1.3" y2="-2.2" width="0.2032" layer="21"/>
<wire x1="2.2" y1="0.8" x2="1.8" y2="0.8" width="0.2032" layer="21"/>
<wire x1="2.2" y1="-0.8" x2="1.8" y2="-0.8" width="0.2032" layer="21"/>
<wire x1="-1.8" y1="0.8" x2="-2.2" y2="0.8" width="0.2032" layer="21"/>
<wire x1="-1.8" y1="-0.8" x2="-2.2" y2="-0.8" width="0.2032" layer="21"/>
<circle x="0" y="0" radius="1.27" width="0.2032" layer="21"/>
<smd name="1" x="2.225" y="1.75" dx="1.1" dy="0.7" layer="1" rot="R90"/>
<smd name="2" x="2.225" y="-1.75" dx="1.1" dy="0.7" layer="1" rot="R90"/>
<smd name="3" x="-2.225" y="-1.75" dx="1.1" dy="0.7" layer="1" rot="R90"/>
<smd name="4" x="-2.225" y="1.75" dx="1.1" dy="0.7" layer="1" rot="R90"/>
<text x="-0.889" y="1.778" size="0.4064" layer="25">&gt;NAME</text>
<text x="-0.889" y="-2.032" size="0.4064" layer="27">&gt;Value</text>
</package>
<package name="TACTILE-PTH-12MM">
<wire x1="5" y1="-1.3" x2="5" y2="-0.7" width="0.2032" layer="51"/>
<wire x1="5" y1="-0.7" x2="4.5" y2="-0.2" width="0.2032" layer="51"/>
<wire x1="5" y1="0.2" x2="5" y2="1" width="0.2032" layer="51"/>
<wire x1="-6" y1="4" x2="-6" y2="5" width="0.2032" layer="21"/>
<wire x1="-5" y1="6" x2="5" y2="6" width="0.2032" layer="21"/>
<wire x1="6" y1="5" x2="6" y2="4" width="0.2032" layer="21"/>
<wire x1="6" y1="1" x2="6" y2="-1" width="0.2032" layer="21"/>
<wire x1="6" y1="-4" x2="6" y2="-5" width="0.2032" layer="21"/>
<wire x1="5" y1="-6" x2="-5" y2="-6" width="0.2032" layer="21"/>
<wire x1="-6" y1="-5" x2="-6" y2="-4" width="0.2032" layer="21"/>
<wire x1="-6" y1="-1" x2="-6" y2="1" width="0.2032" layer="21"/>
<wire x1="-6" y1="5" x2="-5" y2="6" width="0.2032" layer="21" curve="-90"/>
<wire x1="5" y1="6" x2="6" y2="5" width="0.2032" layer="21" curve="-90"/>
<wire x1="6" y1="-5" x2="5" y2="-6" width="0.2032" layer="21" curve="-90"/>
<wire x1="-5" y1="-6" x2="-6" y2="-5" width="0.2032" layer="21" curve="-90"/>
<circle x="0" y="0" radius="3.5" width="0.2032" layer="21"/>
<circle x="-4.5" y="4.5" radius="0.3" width="0.7" layer="21"/>
<circle x="4.5" y="4.5" radius="0.3" width="0.7" layer="21"/>
<circle x="4.5" y="-4.5" radius="0.3" width="0.7" layer="21"/>
<circle x="-4.5" y="-4.5" radius="0.3" width="0.7" layer="21"/>
<pad name="4" x="-6.25" y="2.5" drill="1.2" diameter="2.159"/>
<pad name="2" x="-6.25" y="-2.5" drill="1.2" diameter="2.159"/>
<pad name="1" x="6.25" y="-2.5" drill="1.2" diameter="2.159"/>
<pad name="3" x="6.25" y="2.5" drill="1.2" diameter="2.159"/>
</package>
<package name="TACTILE-SWITCH-1101NE">
<description>Sparkfun SKU# COM-08229</description>
<wire x1="-3" y1="1.1" x2="-3" y2="-1.1" width="0.127" layer="51"/>
<wire x1="3" y1="1.1" x2="3" y2="-1.1" width="0.127" layer="51"/>
<wire x1="-2.75" y1="1.75" x2="-3" y2="1.5" width="0.127" layer="21" curve="90"/>
<wire x1="-2.75" y1="1.75" x2="2.75" y2="1.75" width="0.127" layer="21"/>
<wire x1="2.75" y1="1.75" x2="3" y2="1.5" width="0.127" layer="21" curve="-90"/>
<wire x1="3" y1="-1.5" x2="2.65" y2="-1.75" width="0.127" layer="21" curve="-90"/>
<wire x1="2.65" y1="-1.75" x2="-2.75" y2="-1.75" width="0.127" layer="21"/>
<wire x1="-3" y1="-1.5" x2="-2.75" y2="-1.75" width="0.127" layer="21" curve="90"/>
<wire x1="-3" y1="-1.5" x2="-3" y2="-1.1" width="0.127" layer="21"/>
<wire x1="-3" y1="1.1" x2="-3" y2="1.5" width="0.127" layer="21"/>
<wire x1="3" y1="1.1" x2="3" y2="1.5" width="0.127" layer="21"/>
<wire x1="3" y1="-1.5" x2="3" y2="-1.1" width="0.127" layer="21"/>
<wire x1="-1.5" y1="0.75" x2="1.5" y2="0.75" width="0.127" layer="21"/>
<wire x1="1.5" y1="-0.75" x2="-1.5" y2="-0.75" width="0.127" layer="21"/>
<wire x1="-1.5" y1="-0.75" x2="-1.5" y2="0.75" width="0.127" layer="21"/>
<wire x1="1.5" y1="-0.75" x2="1.5" y2="0.75" width="0.127" layer="21"/>
<wire x1="-2" y1="0" x2="-1" y2="0" width="0.127" layer="51"/>
<wire x1="-1" y1="0" x2="0.1" y2="0.5" width="0.127" layer="51"/>
<wire x1="0.3" y1="0" x2="2" y2="0" width="0.127" layer="51"/>
<smd name="P$1" x="-3.15" y="0" dx="2.3" dy="1.6" layer="1" rot="R180"/>
<smd name="P$2" x="3.15" y="0" dx="2.3" dy="1.6" layer="1" rot="R180"/>
<text x="-3" y="2" size="0.762" layer="25">&gt;NAME</text>
<text x="-3" y="-2.7" size="0.762" layer="27">&gt;VALUE</text>
</package>
<package name="REED_SWITCH_PTH">
<wire x1="-6.985" y1="-0.635" x2="6.985" y2="-0.635" width="0.127" layer="21"/>
<wire x1="-6.985" y1="-0.635" x2="-6.985" y2="0" width="0.127" layer="21"/>
<wire x1="-6.985" y1="0" x2="-6.985" y2="0.635" width="0.127" layer="21"/>
<wire x1="-6.985" y1="0.635" x2="6.985" y2="0.635" width="0.127" layer="21"/>
<wire x1="6.985" y1="0.635" x2="6.985" y2="0" width="0.127" layer="21"/>
<wire x1="6.985" y1="0" x2="6.985" y2="-0.635" width="0.127" layer="21"/>
<wire x1="-6.985" y1="0" x2="-7.62" y2="0" width="0.127" layer="21"/>
<wire x1="6.985" y1="0" x2="7.62" y2="0" width="0.127" layer="21"/>
<pad name="P$1" x="-8.89" y="0" drill="1.016" diameter="1.8796"/>
<pad name="P$2" x="8.89" y="0" drill="1.016" diameter="1.8796"/>
</package>
<package name="TACTILE_SWITCH_TALL">
<wire x1="-3" y1="-3" x2="3" y2="-3" width="0.254" layer="21"/>
<wire x1="3" y1="-3" x2="3" y2="3" width="0.254" layer="21"/>
<wire x1="3" y1="3" x2="-3" y2="3" width="0.254" layer="21"/>
<wire x1="-3" y1="3" x2="-3" y2="-3" width="0.254" layer="21"/>
<circle x="0" y="0" radius="1.75" width="0.254" layer="21"/>
<smd name="A1" x="-3.975" y="-2.25" dx="1.3" dy="1.55" layer="1" rot="R90"/>
<smd name="A2" x="3.975" y="-2.25" dx="1.3" dy="1.55" layer="1" rot="R90"/>
<smd name="B1" x="-3.975" y="2.25" dx="1.3" dy="1.55" layer="1" rot="R90"/>
<smd name="B2" x="3.975" y="2.25" dx="1.3" dy="1.55" layer="1" rot="R90"/>
</package>
<package name="REED_SWITCH_PLASTIC">
<wire x1="-7.5" y1="-1.65" x2="7.5" y2="-1.65" width="0.127" layer="21"/>
<wire x1="-7.5" y1="-1.65" x2="-7.5" y2="0" width="0.127" layer="21"/>
<wire x1="-7.5" y1="0" x2="-7.5" y2="1.65" width="0.127" layer="21"/>
<wire x1="-7.5" y1="1.65" x2="7.5" y2="1.65" width="0.127" layer="21"/>
<wire x1="7.5" y1="1.65" x2="7.5" y2="0" width="0.127" layer="21"/>
<wire x1="7.5" y1="0" x2="7.5" y2="-1.65" width="0.127" layer="21"/>
<wire x1="-7.5" y1="0" x2="-7.72" y2="0" width="0.127" layer="21"/>
<wire x1="7.5" y1="0" x2="7.72" y2="0" width="0.127" layer="21"/>
<pad name="P$1" x="-8.89" y="0" drill="1.016" diameter="1.8796"/>
<pad name="P$2" x="8.89" y="0" drill="1.016" diameter="1.8796"/>
</package>
<package name="TACTILE-PTH-SIDEEZ">
<wire x1="1.5" y1="-3.8" x2="-1.5" y2="-3.8" width="0.2032" layer="51"/>
<wire x1="-3.65" y1="-2" x2="-3.65" y2="3.5" width="0.2032" layer="51"/>
<wire x1="-3.65" y1="3.5" x2="-3" y2="3.5" width="0.2032" layer="51"/>
<wire x1="3" y1="3.5" x2="3.65" y2="3.5" width="0.2032" layer="51"/>
<wire x1="3.65" y1="3.5" x2="3.65" y2="-2" width="0.2032" layer="51"/>
<wire x1="-3" y1="2" x2="3" y2="2" width="0.2032" layer="51"/>
<wire x1="-3" y1="2" x2="-3" y2="3.5" width="0.2032" layer="51"/>
<wire x1="3" y1="2" x2="3" y2="3.5" width="0.2032" layer="51"/>
<wire x1="-3.65" y1="-2" x2="-1.5" y2="-2" width="0.2032" layer="51"/>
<wire x1="-1.5" y1="-2" x2="1.5" y2="-2" width="0.2032" layer="51"/>
<wire x1="1.5" y1="-2" x2="3.65" y2="-2" width="0.2032" layer="51"/>
<wire x1="1.5" y1="-2" x2="1.5" y2="-3.8" width="0.2032" layer="51"/>
<wire x1="-1.5" y1="-2" x2="-1.5" y2="-3.8" width="0.2032" layer="51"/>
<wire x1="-3.65" y1="1" x2="-3.65" y2="-2" width="0.2032" layer="21"/>
<wire x1="-3.65" y1="-2" x2="3.65" y2="-2" width="0.2032" layer="21"/>
<wire x1="3.65" y1="-2" x2="3.65" y2="1" width="0.2032" layer="21"/>
<wire x1="2" y1="2" x2="-2" y2="2" width="0.2032" layer="21"/>
<pad name="ANCHOR1" x="-3.5" y="2.5" drill="1.2" diameter="2.2" stop="no"/>
<pad name="ANCHOR2" x="3.5" y="2.5" drill="1.2" diameter="2.2" stop="no"/>
<pad name="1" x="-2.5" y="0" drill="0.8" diameter="1.7" stop="no"/>
<pad name="2" x="2.5" y="0" drill="0.8" diameter="1.7" stop="no"/>
<text x="-2.54" y="3.81" size="1.27" layer="25" ratio="10">&gt;NAME</text>
<circle x="2.5" y="0" radius="0.4445" width="0" layer="29"/>
<circle x="-2.5" y="0" radius="0.4445" width="0" layer="29"/>
<circle x="-3.5" y="2.5" radius="0.635" width="0" layer="29"/>
<circle x="3.5" y="2.5" radius="0.635" width="0" layer="29"/>
<circle x="-3.5" y="2.5" radius="1.143" width="0" layer="30"/>
<circle x="2.5" y="0" radius="0.889" width="0" layer="30"/>
<circle x="-2.5" y="0" radius="0.889" width="0" layer="30"/>
<circle x="3.5" y="2.5" radius="1.143" width="0" layer="30"/>
</package>
<package name="TACTILE_SWITCH_SMD-3">
<wire x1="-2.04" y1="-0.44" x2="-2.04" y2="0.47" width="0.2032" layer="21"/>
<wire x1="-1.04" y1="1.14" x2="1.04" y2="1.14" width="0.2032" layer="21"/>
<circle x="0" y="0" radius="0.8" width="0.15" layer="21"/>
<smd name="1" x="-1.8" y="1.1" dx="0.8" dy="1" layer="1" rot="R90"/>
<smd name="2" x="1.8" y="1.1" dx="0.8" dy="1" layer="1" rot="R90"/>
<smd name="3" x="-1.8" y="-1.1" dx="0.8" dy="1" layer="1" rot="R90"/>
<smd name="4" x="1.8" y="-1.1" dx="0.8" dy="1" layer="1" rot="R90"/>
<wire x1="2.06" y1="-0.44" x2="2.06" y2="0.47" width="0.2032" layer="21"/>
<wire x1="-1.04" y1="-1.16" x2="1.04" y2="-1.16" width="0.2032" layer="21"/>
</package>
<package name="TACTILE-SMD-12MM">
<wire x1="5" y1="-1.3" x2="5" y2="-0.7" width="0.2032" layer="51"/>
<wire x1="5" y1="-0.7" x2="4.5" y2="-0.2" width="0.2032" layer="51"/>
<wire x1="5" y1="0.2" x2="5" y2="1" width="0.2032" layer="51"/>
<wire x1="-6" y1="4" x2="-6" y2="5" width="0.2032" layer="21"/>
<wire x1="-5" y1="6" x2="5" y2="6" width="0.2032" layer="21"/>
<wire x1="6" y1="5" x2="6" y2="4" width="0.2032" layer="21"/>
<wire x1="6" y1="1" x2="6" y2="-1" width="0.2032" layer="21"/>
<wire x1="6" y1="-4" x2="6" y2="-5" width="0.2032" layer="21"/>
<wire x1="5" y1="-6" x2="-5" y2="-6" width="0.2032" layer="21"/>
<wire x1="-6" y1="-5" x2="-6" y2="-4" width="0.2032" layer="21"/>
<wire x1="-6" y1="-1" x2="-6" y2="1" width="0.2032" layer="21"/>
<circle x="0" y="0" radius="3.5" width="0.2032" layer="21"/>
<circle x="-4.5" y="4.5" radius="0.3" width="0.7" layer="21"/>
<circle x="4.5" y="4.5" radius="0.3" width="0.7" layer="21"/>
<circle x="4.5" y="-4.5" radius="0.3" width="0.7" layer="21"/>
<circle x="-4.5" y="-4.5" radius="0.3" width="0.7" layer="21"/>
<smd name="4" x="-6.975" y="2.5" dx="1.6" dy="1.55" layer="1"/>
<smd name="2" x="-6.975" y="-2.5" dx="1.6" dy="1.55" layer="1"/>
<smd name="1" x="6.975" y="-2.5" dx="1.6" dy="1.55" layer="1"/>
<smd name="3" x="6.975" y="2.5" dx="1.6" dy="1.55" layer="1"/>
<wire x1="-6" y1="-5" x2="-5" y2="-6" width="0.2032" layer="21"/>
<wire x1="6" y1="-5" x2="5" y2="-6" width="0.2032" layer="21"/>
<wire x1="6" y1="5" x2="5" y2="6" width="0.2032" layer="21"/>
<wire x1="-5" y1="6" x2="-6" y2="5" width="0.2032" layer="21"/>
</package>
<package name="TACTILE-PTH-EZ">
<wire x1="3.048" y1="1.016" x2="3.048" y2="2.54" width="0.2032" layer="51"/>
<wire x1="3.048" y1="2.54" x2="2.54" y2="3.048" width="0.2032" layer="51"/>
<wire x1="2.54" y1="-3.048" x2="3.048" y2="-2.54" width="0.2032" layer="51"/>
<wire x1="3.048" y1="-2.54" x2="3.048" y2="-1.016" width="0.2032" layer="51"/>
<wire x1="-2.54" y1="3.048" x2="-3.048" y2="2.54" width="0.2032" layer="51"/>
<wire x1="-3.048" y1="2.54" x2="-3.048" y2="1.016" width="0.2032" layer="51"/>
<wire x1="-2.54" y1="-3.048" x2="-3.048" y2="-2.54" width="0.2032" layer="51"/>
<wire x1="-3.048" y1="-2.54" x2="-3.048" y2="-1.016" width="0.2032" layer="51"/>
<wire x1="2.54" y1="-3.048" x2="2.159" y2="-3.048" width="0.2032" layer="51"/>
<wire x1="-2.54" y1="-3.048" x2="-2.159" y2="-3.048" width="0.2032" layer="51"/>
<wire x1="-2.54" y1="3.048" x2="-2.159" y2="3.048" width="0.2032" layer="51"/>
<wire x1="2.54" y1="3.048" x2="2.159" y2="3.048" width="0.2032" layer="51"/>
<wire x1="2.159" y1="3.048" x2="-2.159" y2="3.048" width="0.2032" layer="21"/>
<wire x1="-2.159" y1="-3.048" x2="2.159" y2="-3.048" width="0.2032" layer="21"/>
<wire x1="3.048" y1="0.998" x2="3.048" y2="-1.016" width="0.2032" layer="21"/>
<wire x1="-3.048" y1="1.028" x2="-3.048" y2="-1.016" width="0.2032" layer="21"/>
<wire x1="-2.54" y1="1.27" x2="-2.54" y2="0.508" width="0.2032" layer="51"/>
<wire x1="-2.54" y1="-0.508" x2="-2.54" y2="-1.27" width="0.2032" layer="51"/>
<wire x1="-2.54" y1="0.508" x2="-2.159" y2="-0.381" width="0.2032" layer="51"/>
<circle x="0" y="0" radius="1.778" width="0.2032" layer="21"/>
<pad name="1" x="-3.2512" y="2.2606" drill="1.016" diameter="1.8796" stop="no"/>
<pad name="2" x="3.2512" y="2.2606" drill="1.016" diameter="1.8796" stop="no"/>
<pad name="3" x="-3.2512" y="-2.2606" drill="1.016" diameter="1.8796" stop="no"/>
<pad name="4" x="3.2512" y="-2.2606" drill="1.016" diameter="1.8796" stop="no"/>
<text x="-2.54" y="3.81" size="1.27" layer="25" ratio="10">&gt;NAME</text>
<polygon width="0.127" layer="30">
<vertex x="-3.2664" y="3.142"/>
<vertex x="-3.2589" y="3.1445" curve="89.986886"/>
<vertex x="-4.1326" y="2.286"/>
<vertex x="-4.1351" y="2.2657" curve="90.00652"/>
<vertex x="-3.2563" y="1.392"/>
<vertex x="-3.2487" y="1.3869" curve="90.006616"/>
<vertex x="-2.3826" y="2.2403"/>
<vertex x="-2.3775" y="2.2683" curve="89.98711"/>
</polygon>
<polygon width="0.127" layer="29">
<vertex x="-3.2462" y="2.7026"/>
<vertex x="-3.2589" y="2.7051" curve="90.026544"/>
<vertex x="-3.6881" y="2.2733"/>
<vertex x="-3.6881" y="2.2632" curve="89.974074"/>
<vertex x="-3.2562" y="1.8213"/>
<vertex x="-3.2259" y="1.8186" curve="90.051271"/>
<vertex x="-2.8093" y="2.2658"/>
<vertex x="-2.8093" y="2.2606" curve="90.012964"/>
</polygon>
<polygon width="0.127" layer="30">
<vertex x="3.2411" y="3.1395"/>
<vertex x="3.2486" y="3.142" curve="89.986886"/>
<vertex x="2.3749" y="2.2835"/>
<vertex x="2.3724" y="2.2632" curve="90.00652"/>
<vertex x="3.2512" y="1.3895"/>
<vertex x="3.2588" y="1.3844" curve="90.006616"/>
<vertex x="4.1249" y="2.2378"/>
<vertex x="4.13" y="2.2658" curve="89.98711"/>
</polygon>
<polygon width="0.127" layer="29">
<vertex x="3.2613" y="2.7001"/>
<vertex x="3.2486" y="2.7026" curve="90.026544"/>
<vertex x="2.8194" y="2.2708"/>
<vertex x="2.8194" y="2.2607" curve="89.974074"/>
<vertex x="3.2513" y="1.8188"/>
<vertex x="3.2816" y="1.8161" curve="90.051271"/>
<vertex x="3.6982" y="2.2633"/>
<vertex x="3.6982" y="2.2581" curve="90.012964"/>
</polygon>
<polygon width="0.127" layer="30">
<vertex x="-3.2613" y="-1.3868"/>
<vertex x="-3.2538" y="-1.3843" curve="89.986886"/>
<vertex x="-4.1275" y="-2.2428"/>
<vertex x="-4.13" y="-2.2631" curve="90.00652"/>
<vertex x="-3.2512" y="-3.1368"/>
<vertex x="-3.2436" y="-3.1419" curve="90.006616"/>
<vertex x="-2.3775" y="-2.2885"/>
<vertex x="-2.3724" y="-2.2605" curve="89.98711"/>
</polygon>
<polygon width="0.127" layer="29">
<vertex x="-3.2411" y="-1.8262"/>
<vertex x="-3.2538" y="-1.8237" curve="90.026544"/>
<vertex x="-3.683" y="-2.2555"/>
<vertex x="-3.683" y="-2.2656" curve="89.974074"/>
<vertex x="-3.2511" y="-2.7075"/>
<vertex x="-3.2208" y="-2.7102" curve="90.051271"/>
<vertex x="-2.8042" y="-2.263"/>
<vertex x="-2.8042" y="-2.2682" curve="90.012964"/>
</polygon>
<polygon width="0.127" layer="30">
<vertex x="3.2411" y="-1.3843"/>
<vertex x="3.2486" y="-1.3818" curve="89.986886"/>
<vertex x="2.3749" y="-2.2403"/>
<vertex x="2.3724" y="-2.2606" curve="90.00652"/>
<vertex x="3.2512" y="-3.1343"/>
<vertex x="3.2588" y="-3.1394" curve="90.006616"/>
<vertex x="4.1249" y="-2.286"/>
<vertex x="4.13" y="-2.258" curve="89.98711"/>
</polygon>
<polygon width="0.127" layer="29">
<vertex x="3.2613" y="-1.8237"/>
<vertex x="3.2486" y="-1.8212" curve="90.026544"/>
<vertex x="2.8194" y="-2.253"/>
<vertex x="2.8194" y="-2.2631" curve="89.974074"/>
<vertex x="3.2513" y="-2.705"/>
<vertex x="3.2816" y="-2.7077" curve="90.051271"/>
<vertex x="3.6982" y="-2.2605"/>
<vertex x="3.6982" y="-2.2657" curve="90.012964"/>
</polygon>
</package>
</packages>
<symbols>
<symbol name="SWITCH-MOMENTARY-2">
<wire x1="1.905" y1="0" x2="2.54" y2="0" width="0.254" layer="94"/>
<wire x1="-2.54" y1="0" x2="1.905" y2="1.27" width="0.254" layer="94"/>
<circle x="-2.54" y="0" radius="0.127" width="0.4064" layer="94"/>
<circle x="2.54" y="0" radius="0.127" width="0.4064" layer="94"/>
<text x="-2.54" y="2.54" size="1.778" layer="95">&gt;NAME</text>
<text x="-2.54" y="-2.54" size="1.778" layer="96">&gt;VALUE</text>
<pin name="1" x="-5.08" y="0" visible="off" length="short" direction="pas" swaplevel="2"/>
<pin name="2" x="5.08" y="0" visible="off" length="short" direction="pas" swaplevel="1" rot="R180"/>
</symbol>
</symbols>
<devicesets>
<deviceset name="SWITCHES_" prefix="S">
<description>Various NO switches- pushbuttons, reed, etc</description>
<gates>
<gate name="G$1" symbol="SWITCH-MOMENTARY-2" x="0" y="0"/>
</gates>
<devices>
<device name="PTH" package="TACTILE-PTH">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="3"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="SMD" package="TACTILE_SWITCH_SMD">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="3"/>
</connects>
<technologies>
<technology name="">
<attribute name="PROD_ID" value="SWCH-08247" constant="no"/>
</technology>
</technologies>
</device>
<device name="SMD-2" package="TACTILE_SWITCH_SMD-2">
<connects>
<connect gate="G$1" pin="1" pad="2"/>
<connect gate="G$1" pin="2" pad="1"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="12MM" package="TACTILE-PTH-12MM">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="3"/>
</connects>
<technologies>
<technology name="">
<attribute name="PROD_ID" value="SWCH-09185" constant="no"/>
</technology>
</technologies>
</device>
<device name="-SMD-1101NE" package="TACTILE-SWITCH-1101NE">
<connects>
<connect gate="G$1" pin="1" pad="P$1"/>
<connect gate="G$1" pin="2" pad="P$2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="PTH_REED" package="REED_SWITCH_PTH">
<connects>
<connect gate="G$1" pin="1" pad="P$1"/>
<connect gate="G$1" pin="2" pad="P$2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="SMD-4" package="TACTILE_SWITCH_TALL">
<connects>
<connect gate="G$1" pin="1" pad="A2"/>
<connect gate="G$1" pin="2" pad="B2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="PTH_REED2" package="REED_SWITCH_PLASTIC">
<connects>
<connect gate="G$1" pin="1" pad="P$1"/>
<connect gate="G$1" pin="2" pad="P$2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="SIDE_EZ" package="TACTILE-PTH-SIDEEZ">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="SMD-3" package="TACTILE_SWITCH_SMD-3">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="4"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="SMD-12MM" package="TACTILE-SMD-12MM">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="3"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="PTH_EZ" package="TACTILE-PTH-EZ">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="3"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
</devicesets>
</library>
<library name="buzzer">
<description>&lt;b&gt;Speakers and Buzzers&lt;/b&gt;&lt;p&gt;
&lt;ul&gt;Distributors:
&lt;li&gt;Buerklin
&lt;li&gt;Spoerle
&lt;li&gt;Schukat
&lt;/ul&gt;
&lt;author&gt;Created by librarian@cadsoft.de&lt;/author&gt;</description>
<packages>
<package name="F/CM12P">
<description>&lt;b&gt;BUZZER&lt;/b&gt;</description>
<wire x1="3.175" y1="3.048" x2="4.445" y2="3.048" width="0.254" layer="21"/>
<wire x1="3.81" y1="3.683" x2="3.81" y2="2.413" width="0.254" layer="21"/>
<circle x="0" y="0" radius="6.985" width="0.1524" layer="21"/>
<circle x="0" y="0" radius="1.27" width="0.1524" layer="21"/>
<circle x="3.81" y="3.048" radius="1.27" width="0.1524" layer="21"/>
<pad name="-" x="-3.81" y="0" drill="1.016" diameter="2.54" shape="octagon"/>
<pad name="+" x="3.81" y="0" drill="1.016" diameter="2.54" shape="octagon"/>
<text x="2.54" y="6.985" size="1.27" layer="25" ratio="10">&gt;NAME</text>
<text x="-4.445" y="-3.81" size="1.27" layer="27" ratio="10">&gt;VALUE</text>
</package>
</packages>
<symbols>
<symbol name="B2P">
<wire x1="-1.27" y1="3.175" x2="0" y2="3.175" width="0.1524" layer="94"/>
<wire x1="0" y1="3.175" x2="0" y2="3.81" width="0.1524" layer="94"/>
<wire x1="0" y1="3.175" x2="0" y2="2.54" width="0.1524" layer="94"/>
<wire x1="0.635" y1="4.445" x2="0.635" y2="1.905" width="0.1524" layer="94"/>
<wire x1="0.635" y1="1.905" x2="1.905" y2="1.905" width="0.1524" layer="94"/>
<wire x1="1.905" y1="1.905" x2="1.905" y2="4.445" width="0.1524" layer="94"/>
<wire x1="1.905" y1="4.445" x2="0.635" y2="4.445" width="0.1524" layer="94"/>
<wire x1="2.54" y1="3.81" x2="2.54" y2="3.175" width="0.1524" layer="94"/>
<wire x1="2.54" y1="3.175" x2="3.81" y2="3.175" width="0.1524" layer="94"/>
<wire x1="2.54" y1="3.175" x2="2.54" y2="2.54" width="0.1524" layer="94"/>
<wire x1="2.54" y1="-2.54" x2="2.54" y2="1.27" width="0.1524" layer="94"/>
<wire x1="0" y1="-2.54" x2="0" y2="1.27" width="0.1524" layer="94"/>
<wire x1="-2.54" y1="1.27" x2="0" y2="1.27" width="0.254" layer="94"/>
<wire x1="5.08" y1="1.27" x2="5.08" y2="5.08" width="0.254" layer="94"/>
<wire x1="5.08" y1="5.08" x2="5.715" y2="5.08" width="0.254" layer="94"/>
<wire x1="5.715" y1="5.08" x2="5.715" y2="5.715" width="0.254" layer="94"/>
<wire x1="5.715" y1="5.715" x2="-3.175" y2="5.715" width="0.254" layer="94"/>
<wire x1="-3.175" y1="5.715" x2="-3.175" y2="5.08" width="0.254" layer="94"/>
<wire x1="-3.175" y1="5.08" x2="-2.54" y2="5.08" width="0.254" layer="94"/>
<wire x1="-2.54" y1="5.08" x2="-2.54" y2="1.27" width="0.254" layer="94"/>
<wire x1="-2.54" y1="5.08" x2="5.08" y2="5.08" width="0.254" layer="94"/>
<wire x1="2.54" y1="1.27" x2="5.08" y2="1.27" width="0.254" layer="94"/>
<wire x1="0" y1="1.27" x2="2.54" y2="1.27" width="0.254" layer="94"/>
<text x="-2.54" y="6.35" size="1.778" layer="95">&gt;NAME</text>
<text x="6.35" y="0" size="1.778" layer="96">&gt;VALUE</text>
<pin name="2" x="5.08" y="-2.54" visible="pad" length="short" direction="pas" rot="R180"/>
<pin name="1" x="-2.54" y="-2.54" visible="pad" length="short" direction="pas"/>
</symbol>
</symbols>
<devicesets>
<deviceset name="F/CM12P" prefix="SG">
<description>&lt;b&gt;BUZZER&lt;/b&gt;&lt;p&gt; Source: Buerklin</description>
<gates>
<gate name="G$1" symbol="B2P" x="0" y="0"/>
</gates>
<devices>
<device name="" package="F/CM12P">
<connects>
<connect gate="G$1" pin="1" pad="+"/>
<connect gate="G$1" pin="2" pad="-"/>
</connects>
<technologies>
<technology name="">
<attribute name="MF" value="" constant="no"/>
<attribute name="MPN" value="" constant="no"/>
<attribute name="OC_FARNELL" value="unknown" constant="no"/>
<attribute name="OC_NEWARK" value="unknown" constant="no"/>
</technology>
</technologies>
</device>
</devices>
</deviceset>
</devicesets>
</library>
<library name="jos-ph">
<packages>
<package name="APA102_5050">
<description>APA102 RGB LED</description>
<wire x1="2.5" y1="-2.5" x2="-2.5" y2="-2.5" width="0.127" layer="21"/>
<wire x1="-2.5" y1="-2.5" x2="-2.5" y2="1.5" width="0.127" layer="21"/>
<wire x1="-2.5" y1="1.5" x2="-2.5" y2="2.5" width="0.127" layer="21"/>
<wire x1="-2.5" y1="2.5" x2="-1.5" y2="2.5" width="0.127" layer="21"/>
<wire x1="-1.5" y1="2.5" x2="2.5" y2="2.5" width="0.127" layer="21"/>
<wire x1="2.5" y1="2.5" x2="2.5" y2="-2.5" width="0.127" layer="21"/>
<wire x1="-1.5" y1="2.5" x2="-2.5" y2="1.5" width="0.127" layer="21"/>
<circle x="-2.934" y="2.688" radius="0.1414" width="0.127" layer="21"/>
<circle x="0" y="0" radius="2.1" width="0.127" layer="21"/>
<smd name="1" x="2" y="-1.7" dx="2" dy="1.1" layer="1" rot="R180"/>
<smd name="2" x="2" y="0" dx="2" dy="1.1" layer="1" rot="R180"/>
<smd name="3" x="2" y="1.7" dx="2" dy="1.1" layer="1" rot="R180"/>
<smd name="4" x="-2" y="1.7" dx="2" dy="1.1" layer="1" rot="R180"/>
<smd name="5" x="-2" y="0" dx="2" dy="1.1" layer="1" rot="R180"/>
<smd name="6" x="-2" y="-1.7" dx="2" dy="1.1" layer="1" rot="R180"/>
<text x="0" y="-3" size="1.27" layer="25" font="vector" rot="R180" align="bottom-center">&gt;NAME</text>
</package>
<package name="APA102_2020">
<wire x1="0" y1="0" x2="2" y2="0" width="0.05" layer="21"/>
<wire x1="2" y1="0" x2="2" y2="2" width="0.05" layer="21"/>
<wire x1="2" y1="2" x2="0" y2="2" width="0.05" layer="21"/>
<wire x1="0" y1="2" x2="0" y2="0" width="0.05" layer="21"/>
<smd name="D_IN" x="0.25" y="1.825" dx="0.6" dy="0.35" layer="1"/>
<smd name="D_OUT" x="1.75" y="1.825" dx="0.6" dy="0.35" layer="1"/>
<smd name="GND" x="0.25" y="0.175" dx="0.6" dy="0.35" layer="1"/>
<smd name="V_CC" x="1.75" y="0.175" dx="0.6" dy="0.35" layer="1"/>
<smd name="C_IN" x="0.25" y="1" dx="0.6" dy="0.5" layer="1"/>
<smd name="CL_OUT" x="1.75" y="1" dx="0.6" dy="0.5" layer="1"/>
<text x="-0.05" y="1.825" size="0.5" layer="25" align="center-right">D_IN</text>
<text x="-0.05" y="1" size="0.5" layer="25" align="center-right">CL_IN</text>
<text x="-0.05" y="0.15" size="0.5" layer="25" align="center-right">GND</text>
<text x="2.05" y="1.775" size="0.5" layer="25" align="center-left">D_OUT</text>
<text x="2.05" y="1" size="0.5" layer="25" align="center-left">CL_OUT</text>
<text x="2.1" y="0.2" size="0.5" layer="25" align="center-left">5V</text>
<polygon width="0.05" layer="21" spacing="0.01" pour="hatch">
<vertex x="0.1" y="2.1"/>
<vertex x="-0.1" y="1.9"/>
<vertex x="-0.1" y="2.1"/>
</polygon>
</package>
</packages>
<symbols>
<symbol name="APA102">
<description>APA102 RGB LED</description>
<wire x1="-10.16" y1="7.62" x2="-10.16" y2="-2.54" width="0.254" layer="94"/>
<wire x1="-10.16" y1="-2.54" x2="7.62" y2="-2.54" width="0.254" layer="94"/>
<wire x1="7.62" y1="-2.54" x2="7.62" y2="7.62" width="0.254" layer="94"/>
<wire x1="7.62" y1="7.62" x2="-10.16" y2="7.62" width="0.254" layer="94"/>
<pin name="DI" x="-12.7" y="5.08" visible="pin" length="short"/>
<pin name="CI" x="-12.7" y="0" visible="pin" length="short"/>
<pin name="GND" x="-2.54" y="-5.08" visible="pin" length="short" rot="R90"/>
<pin name="VCC" x="0" y="10.16" visible="pin" length="short" rot="R270"/>
<pin name="CO" x="10.16" y="0" visible="pin" length="short" rot="R180"/>
<pin name="DO" x="10.16" y="5.08" visible="pin" length="short" rot="R180"/>
<text x="-10.16" y="8.128" size="1.143" layer="94">APA102</text>
</symbol>
</symbols>
<devicesets>
<deviceset name="APA102">
<description>APA102C 5050 RGB LED

--
Adafruit DotStar Single LED Pixel
Product Page: https://www.adafruit.com/products/2343
Datasheet: https://cdn-shop.adafruit.com/product-files/2343/APA102C.pdf</description>
<gates>
<gate name="G$1" symbol="APA102" x="-5.08" y="0"/>
</gates>
<devices>
<device name="5050" package="APA102_5050">
<connects>
<connect gate="G$1" pin="CI" pad="5"/>
<connect gate="G$1" pin="CO" pad="2"/>
<connect gate="G$1" pin="DI" pad="4"/>
<connect gate="G$1" pin="DO" pad="3"/>
<connect gate="G$1" pin="GND" pad="6"/>
<connect gate="G$1" pin="VCC" pad="1"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="2020" package="APA102_2020">
<connects>
<connect gate="G$1" pin="CI" pad="C_IN"/>
<connect gate="G$1" pin="CO" pad="CL_OUT"/>
<connect gate="G$1" pin="DI" pad="D_IN"/>
<connect gate="G$1" pin="DO" pad="D_OUT"/>
<connect gate="G$1" pin="GND" pad="GND"/>
<connect gate="G$1" pin="VCC" pad="V_CC"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
</devicesets>
</library>
<library name="74xx-little-us">
<description>&lt;b&gt;Single and Dual Gates Family, US symbols&lt;/b&gt;&lt;p&gt;
Little logic devices from Texas Instruments&lt;br&gt;
TinyLogic(R) from FAIRCHILD Semiconductor TM
&lt;p&gt;
&lt;author&gt;Created by evgeni@eniks.com&lt;/author&gt;&lt;br&gt;
&lt;author&gt;Extended by librarian@cadsoft.de&lt;/author&gt;</description>
<packages>
<package name="SOT23-5">
<description>&lt;b&gt;Small Outline Transistor&lt;/b&gt;&lt;p&gt;
SOT753 - Philips Semiconductors&lt;br&gt;
Source: http://www.semiconductors.philips.com/acrobat_download/datasheets/74HC_HCT1G66_3.pdf</description>
<wire x1="0" y1="-1.29" x2="0" y2="-1.3" width="0.01" layer="21"/>
<wire x1="1.42" y1="0.8" x2="1.42" y2="-0.8" width="0.127" layer="21"/>
<wire x1="1.42" y1="-0.8" x2="-1.42" y2="-0.8" width="0.127" layer="51"/>
<wire x1="-1.42" y1="-0.8" x2="-1.42" y2="0.8" width="0.127" layer="21"/>
<wire x1="-1.42" y1="0.8" x2="1.42" y2="0.8" width="0.127" layer="51"/>
<wire x1="-1.27" y1="0.65" x2="1.28" y2="0.65" width="0.075" layer="21"/>
<wire x1="1.28" y1="0.65" x2="1.28" y2="-0.66" width="0.075" layer="21"/>
<wire x1="1.28" y1="-0.66" x2="-1.27" y2="-0.66" width="0.075" layer="21"/>
<wire x1="-1.27" y1="-0.66" x2="-1.27" y2="0.65" width="0.075" layer="21"/>
<wire x1="-1.75" y1="2.25" x2="1.75" y2="2.25" width="0.254" layer="39"/>
<wire x1="1.75" y1="2.25" x2="1.75" y2="-2.25" width="0.254" layer="39"/>
<wire x1="1.75" y1="-2.25" x2="-1.75" y2="-2.25" width="0.254" layer="39"/>
<wire x1="-1.75" y1="-2.25" x2="-1.75" y2="2.25" width="0.254" layer="39"/>
<smd name="1" x="-0.95" y="-1.29" dx="0.69" dy="0.99" layer="1"/>
<smd name="2" x="0" y="-1.29" dx="0.69" dy="0.99" layer="1"/>
<smd name="3" x="0.95" y="-1.29" dx="0.69" dy="0.99" layer="1"/>
<smd name="4" x="0.95" y="1.3" dx="0.69" dy="0.99" layer="1"/>
<smd name="5" x="-0.95" y="1.3" dx="0.69" dy="0.99" layer="1"/>
<text x="-2.54" y="2.54" size="1.27" layer="25">&gt;NAME</text>
<text x="-2.54" y="-3.81" size="1.27" layer="27">&gt;VALUE</text>
<rectangle x1="-1.11" y1="0.68" x2="-0.78" y2="1.43" layer="51"/>
<rectangle x1="0.79" y1="0.67" x2="1.12" y2="1.42" layer="51"/>
<rectangle x1="-1.11" y1="-1.42" x2="-0.78" y2="-0.67" layer="51"/>
<rectangle x1="-0.16" y1="-1.42" x2="0.17" y2="-0.67" layer="51"/>
<rectangle x1="0.79" y1="-1.42" x2="1.12" y2="-0.67" layer="51"/>
</package>
<package name="SC70-5">
<description>&lt;b&gt;SMT SC70-5&lt;/b&gt;&lt;p&gt;
SOT353 - Philips Semiconductors&lt;br&gt;
Source: http://www.semiconductors.philips.com/acrobat_download/datasheets/74HC_HCT1G66_3.pdf</description>
<wire x1="1" y1="0.55" x2="-1" y2="0.55" width="0.127" layer="51"/>
<wire x1="-1" y1="0.55" x2="-1" y2="-0.55" width="0.127" layer="21"/>
<wire x1="-1" y1="-0.55" x2="1" y2="-0.55" width="0.127" layer="51"/>
<wire x1="1" y1="-0.55" x2="1" y2="0.55" width="0.127" layer="21"/>
<smd name="1" x="-0.65" y="-0.85" dx="0.4" dy="0.7" layer="1"/>
<smd name="2" x="0" y="-0.85" dx="0.4" dy="0.7" layer="1"/>
<smd name="3" x="0.65" y="-0.85" dx="0.4" dy="0.7" layer="1"/>
<smd name="4" x="0.65" y="0.85" dx="0.4" dy="0.7" layer="1"/>
<smd name="5" x="-0.65" y="0.85" dx="0.4" dy="0.7" layer="1"/>
<text x="-2.54" y="1.27" size="1.27" layer="25">&gt;NAME</text>
<text x="-2.54" y="-2.54" size="1.27" layer="27">&gt;VALUE</text>
<rectangle x1="-0.125" y1="-1.05" x2="0.125" y2="-0.6" layer="51"/>
<rectangle x1="-0.775" y1="-1.05" x2="-0.525" y2="-0.6" layer="51"/>
<rectangle x1="0.525" y1="-1.05" x2="0.775" y2="-0.6" layer="51"/>
<rectangle x1="-0.775" y1="0.6" x2="-0.525" y2="1.05" layer="51"/>
<rectangle x1="0.525" y1="0.6" x2="0.775" y2="1.05" layer="51"/>
</package>
</packages>
<symbols>
<symbol name="7408">
<wire x1="-7.62" y1="5.08" x2="-7.62" y2="-5.08" width="0.4064" layer="94"/>
<wire x1="-7.62" y1="-5.08" x2="2.54" y2="-5.08" width="0.4064" layer="94"/>
<wire x1="2.54" y1="5.08" x2="2.54" y2="-5.08" width="0.4064" layer="94" curve="-180"/>
<wire x1="2.54" y1="5.08" x2="-7.62" y2="5.08" width="0.4064" layer="94"/>
<text x="-7.62" y="5.715" size="1.778" layer="95">&gt;NAME</text>
<text x="-7.62" y="-7.62" size="1.778" layer="96">&gt;VALUE</text>
<pin name="I0" x="-12.7" y="2.54" visible="pad" length="middle" direction="in" swaplevel="1"/>
<pin name="I1" x="-12.7" y="-2.54" visible="pad" length="middle" direction="in" swaplevel="1"/>
<pin name="O" x="12.7" y="0" visible="pad" length="middle" direction="out" rot="R180"/>
</symbol>
<symbol name="PWRN">
<text x="-0.635" y="-0.635" size="1.778" layer="95">&gt;NAME</text>
<text x="1.905" y="-6.35" size="1.27" layer="95" rot="R90">GND</text>
<text x="1.905" y="2.54" size="1.27" layer="95" rot="R90">VCC</text>
<pin name="GND" x="0" y="-7.62" visible="pad" length="middle" direction="pwr" rot="R90"/>
<pin name="VCC" x="0" y="7.62" visible="pad" length="middle" direction="pwr" rot="R270"/>
</symbol>
</symbols>
<devicesets>
<deviceset name="74*1G08" prefix="IC">
<description>2-input &lt;b&gt;AND&lt;/b&gt; gate</description>
<gates>
<gate name="A" symbol="7408" x="20.32" y="0"/>
<gate name="P" symbol="PWRN" x="2.54" y="-7.62" addlevel="request"/>
</gates>
<devices>
<device name="DBV" package="SOT23-5">
<connects>
<connect gate="A" pin="I0" pad="1"/>
<connect gate="A" pin="I1" pad="2"/>
<connect gate="A" pin="O" pad="4"/>
<connect gate="P" pin="GND" pad="3"/>
<connect gate="P" pin="VCC" pad="5"/>
</connects>
<technologies>
<technology name="AHC"/>
<technology name="AHCT"/>
<technology name="AUC"/>
<technology name="LVC"/>
</technologies>
</device>
<device name="DCK" package="SC70-5">
<connects>
<connect gate="A" pin="I0" pad="1"/>
<connect gate="A" pin="I1" pad="2"/>
<connect gate="A" pin="O" pad="4"/>
<connect gate="P" pin="GND" pad="3"/>
<connect gate="P" pin="VCC" pad="5"/>
</connects>
<technologies>
<technology name="AHC"/>
<technology name="AHCT"/>
<technology name="AUC"/>
<technology name="LVC"/>
</technologies>
</device>
</devices>
</deviceset>
</devicesets>
</library>
</libraries>
<attributes>
</attributes>
<variantdefs>
</variantdefs>
<classes>
<class number="0" name="default" width="0" drill="0">
</class>
<class number="1" name="power" width="0.4064" drill="0">
</class>
</classes>
<parts>
<part name="FRAME1" library="GadgetFactory" deviceset="FRAME-A4L-LOC" device=""/>
<part name="U1" library="alvie" deviceset="ATTINYX14" device=""/>
<part name="R2" library="GadgetFactory" deviceset="R" device="0603" value="100K/DNP"/>
<part name="GND2" library="supply1" deviceset="GND" device=""/>
<part name="GND3" library="supply1" deviceset="GND" device=""/>
<part name="SW1" library="switch-alps" deviceset="EC12E_SW" device=""/>
<part name="SW2" library="switch-alps" deviceset="EC12E_SW" device=""/>
<part name="SW3" library="switch-alps" deviceset="EC12E_SW" device=""/>
<part name="VR1" library="alvie" deviceset="MIC550X" device=""/>
<part name="GND4" library="supply1" deviceset="GND" device=""/>
<part name="C2" library="GadgetFactory" deviceset="C" device="0603" value="100nF"/>
<part name="X1" library="con-jst2" deviceset="?M10B-SRSS-TB" device="B" value="BM10B-SRSS-TB"/>
<part name="LED1" library="dp_devices" deviceset="LED" device="-5MM"/>
<part name="LED2" library="dp_devices" deviceset="LED" device="-5MM"/>
<part name="LED3" library="dp_devices" deviceset="LED" device="-5MM"/>
<part name="R1" library="GadgetFactory" deviceset="R" device="0603" value="150"/>
<part name="R3" library="GadgetFactory" deviceset="R" device="0603" value="1K/DNP"/>
<part name="R4" library="GadgetFactory" deviceset="R" device="0603" value="1K/DNP"/>
<part name="R6" library="GadgetFactory" deviceset="R" device="0603" value="10K"/>
<part name="R7" library="GadgetFactory" deviceset="R" device="0603" value="10K"/>
<part name="R8" library="GadgetFactory" deviceset="R" device="0603" value="10K"/>
<part name="R9" library="GadgetFactory" deviceset="R" device="0603" value="10K"/>
<part name="U$2" library="diy-modules" deviceset="DISPLAY-OLED-128X64" device=""/>
<part name="FRAME2" library="GadgetFactory" deviceset="FRAME-A4L-LOC" device=""/>
<part name="U$4" library="diy-modules" deviceset="DISPLAY-OLED-128X64" device=""/>
<part name="U$3" library="alvie" deviceset="TFT-22-NOSD" device=""/>
<part name="S1" library="EAGLE_BoosterPack_Library" deviceset="SWITCHES_" device="PTH_EZ"/>
<part name="JP1" library="alvie" deviceset="PIN-1X4" device=""/>
<part name="GND1" library="supply1" deviceset="GND" device=""/>
<part name="C1" library="GadgetFactory" deviceset="C" device="1210" value="22uF"/>
<part name="SG1" library="buzzer" deviceset="F/CM12P" device=""/>
<part name="R10" library="GadgetFactory" deviceset="R" device="0603" value="300R"/>
<part name="S2" library="EAGLE_BoosterPack_Library" deviceset="SWITCHES_" device="PTH_EZ"/>
<part name="S3" library="EAGLE_BoosterPack_Library" deviceset="SWITCHES_" device="PTH_EZ"/>
<part name="SW4" library="switch-alps" deviceset="EC12E_SW" device=""/>
<part name="S4" library="EAGLE_BoosterPack_Library" deviceset="SWITCHES_" device="PTH_EZ"/>
<part name="LD1" library="jos-ph" deviceset="APA102" device="5050"/>
<part name="LD2" library="jos-ph" deviceset="APA102" device="5050"/>
<part name="LD3" library="jos-ph" deviceset="APA102" device="5050"/>
<part name="R5" library="GadgetFactory" deviceset="R" device="0603" value="150"/>
<part name="R11" library="GadgetFactory" deviceset="R" device="0603" value="150"/>
<part name="Q1" library="transistor-small-signal" deviceset="BSS123" device=""/>
<part name="GND5" library="supply1" deviceset="GND" device=""/>
<part name="R12" library="GadgetFactory" deviceset="R" device="0603" value="100R"/>
<part name="R13" library="GadgetFactory" deviceset="R" device="0603" value="100R"/>
<part name="IC1" library="74xx-little-us" deviceset="74*1G08" device="DCK" technology="AHC"/>
<part name="Q2" library="transistor-small-signal" deviceset="BSS123" device=""/>
<part name="R14" library="GadgetFactory" deviceset="R" device="0603" value="150"/>
<part name="R15" library="GadgetFactory" deviceset="R" device="0603" value="0R/DNP"/>
<part name="GND6" library="supply1" deviceset="GND" device=""/>
<part name="GND7" library="supply1" deviceset="GND" device=""/>
<part name="C3" library="GadgetFactory" deviceset="C" device="0603" value="100nF"/>
<part name="IC2" library="alvie" deviceset="ATTINYX16" device="S"/>
<part name="C4" library="GadgetFactory" deviceset="C" device="0603" value="100nF"/>
<part name="GND8" library="supply1" deviceset="GND" device=""/>
<part name="GND9" library="supply1" deviceset="GND" device=""/>
<part name="GND10" library="supply1" deviceset="GND" device=""/>
<part name="GND11" library="supply1" deviceset="GND" device=""/>
<part name="GND12" library="supply1" deviceset="GND" device=""/>
<part name="GND13" library="supply1" deviceset="GND" device=""/>
<part name="R16" library="GadgetFactory" deviceset="R" device="0603" value="0R/DNP"/>
<part name="JP2" library="alvie" deviceset="PIN-1X3" device=""/>
<part name="GND14" library="supply1" deviceset="GND" device=""/>
<part name="R17" library="GadgetFactory" deviceset="R" device="0603" value="0R/DNP"/>
</parts>
<sheets>
<sheet>
<plain>
<text x="139.7" y="147.32" size="1.778" layer="91">ALPS SKHHDTA010</text>
<text x="15.24" y="162.56" size="1.778" layer="91">PA1: MOSI</text>
<text x="15.24" y="160.02" size="1.778" layer="91">PA3: SCK</text>
<text x="15.24" y="157.48" size="1.778" layer="91">PA4: SS</text>
<text x="15.24" y="154.94" size="1.778" layer="91">PA2: MISO</text>
</plain>
<instances>
<instance part="FRAME1" gate="A" x="0" y="0"/>
<instance part="U1" gate="G$1" x="104.14" y="101.6"/>
<instance part="R2" gate="A" x="63.5" y="152.4" rot="R90"/>
<instance part="GND2" gate="1" x="119.38" y="121.92"/>
<instance part="GND3" gate="1" x="104.14" y="71.12"/>
<instance part="SW1" gate="G$1" x="180.34" y="144.78"/>
<instance part="SW1" gate="G$2" x="190.5" y="147.32" rot="R270"/>
<instance part="SW2" gate="G$1" x="180.34" y="121.92"/>
<instance part="SW2" gate="G$2" x="190.5" y="124.46" rot="R270"/>
<instance part="SW3" gate="G$1" x="180.34" y="99.06"/>
<instance part="SW3" gate="G$2" x="190.5" y="101.6" rot="R270"/>
<instance part="C2" gate="A" x="111.76" y="132.08"/>
<instance part="X1" gate="-1" x="236.22" y="66.04"/>
<instance part="X1" gate="-2" x="236.22" y="63.5"/>
<instance part="X1" gate="-3" x="236.22" y="60.96"/>
<instance part="X1" gate="-4" x="236.22" y="58.42"/>
<instance part="X1" gate="-5" x="236.22" y="55.88"/>
<instance part="X1" gate="-6" x="236.22" y="53.34"/>
<instance part="X1" gate="-7" x="236.22" y="50.8"/>
<instance part="X1" gate="-8" x="236.22" y="48.26"/>
<instance part="X1" gate="-9" x="236.22" y="45.72"/>
<instance part="X1" gate="-10" x="236.22" y="43.18"/>
<instance part="LED1" gate="LED" x="218.44" y="139.7"/>
<instance part="LED2" gate="LED" x="226.06" y="139.7"/>
<instance part="LED3" gate="LED" x="233.68" y="139.7"/>
<instance part="R1" gate="A" x="218.44" y="149.86" rot="R90"/>
<instance part="R3" gate="A" x="129.54" y="119.38" rot="R90"/>
<instance part="R4" gate="A" x="137.16" y="119.38" rot="R90"/>
<instance part="R6" gate="A" x="60.96" y="116.84" rot="R90"/>
<instance part="R7" gate="A" x="53.34" y="116.84" rot="R90"/>
<instance part="R8" gate="A" x="45.72" y="116.84" rot="R90"/>
<instance part="R9" gate="A" x="38.1" y="116.84" rot="R90"/>
<instance part="S1" gate="G$1" x="170.18" y="147.32" rot="R90"/>
<instance part="JP1" gate="G$1" x="89.154" y="156.21" rot="R180"/>
<instance part="GND1" gate="1" x="81.28" y="144.78"/>
<instance part="S2" gate="G$1" x="170.18" y="124.46" rot="R90"/>
<instance part="S3" gate="G$1" x="170.18" y="101.6" rot="R90"/>
<instance part="SW4" gate="G$1" x="213.36" y="86.36"/>
<instance part="SW4" gate="G$2" x="223.52" y="88.9" rot="R270"/>
<instance part="S4" gate="G$1" x="203.2" y="88.9" rot="R90"/>
<instance part="R5" gate="A" x="226.06" y="149.86" rot="R90"/>
<instance part="R11" gate="A" x="233.68" y="149.86" rot="R90"/>
<instance part="IC1" gate="A" x="119.38" y="43.18"/>
<instance part="IC1" gate="P" x="121.92" y="22.86"/>
<instance part="Q2" gate="G$1" x="157.48" y="45.72"/>
<instance part="R14" gate="A" x="144.78" y="43.18" rot="R180"/>
<instance part="R15" gate="A" x="144.78" y="53.34" rot="R180"/>
<instance part="GND6" gate="1" x="157.48" y="33.02"/>
<instance part="GND7" gate="1" x="121.92" y="7.62"/>
<instance part="C3" gate="A" x="134.62" y="22.86" rot="R90"/>
<instance part="IC2" gate="G$1" x="50.8" y="50.8"/>
<instance part="C4" gate="A" x="63.5" y="81.28" rot="R90"/>
<instance part="GND8" gate="1" x="53.34" y="25.4"/>
<instance part="GND9" gate="1" x="63.5" y="73.66"/>
<instance part="R16" gate="A" x="208.28" y="43.18" rot="R180"/>
<instance part="JP2" gate="G$1" x="139.7" y="162.56" rot="R180"/>
<instance part="GND14" gate="1" x="129.54" y="154.94"/>
</instances>
<busses>
</busses>
<nets>
<net name="SDA" class="0">
<segment>
<pinref part="U1" gate="G$1" pin="PB1"/>
<wire x1="127" y1="109.22" x2="137.16" y2="109.22" width="0.1524" layer="91"/>
<label x="139.7" y="109.22" size="1.778" layer="95"/>
<pinref part="R4" gate="A" pin="1"/>
<wire x1="137.16" y1="109.22" x2="144.78" y2="109.22" width="0.1524" layer="91"/>
<wire x1="137.16" y1="114.3" x2="137.16" y2="109.22" width="0.1524" layer="91"/>
<junction x="137.16" y="109.22"/>
</segment>
<segment>
<pinref part="X1" gate="-3" pin="S"/>
<wire x1="233.68" y1="60.96" x2="213.36" y2="60.96" width="0.1524" layer="91"/>
<label x="213.36" y="60.96" size="1.778" layer="95"/>
</segment>
<segment>
<pinref part="IC2" gate="G$1" pin="PB1"/>
<wire x1="76.2" y1="60.96" x2="88.9" y2="60.96" width="0.1524" layer="91"/>
<label x="76.2" y="60.96" size="1.778" layer="95"/>
</segment>
</net>
<net name="SCL" class="0">
<segment>
<pinref part="U1" gate="G$1" pin="PB0"/>
<wire x1="127" y1="111.76" x2="129.54" y2="111.76" width="0.1524" layer="91"/>
<label x="139.7" y="111.76" size="1.778" layer="95"/>
<pinref part="R3" gate="A" pin="1"/>
<wire x1="129.54" y1="111.76" x2="144.78" y2="111.76" width="0.1524" layer="91"/>
<wire x1="129.54" y1="114.3" x2="129.54" y2="111.76" width="0.1524" layer="91"/>
<junction x="129.54" y="111.76"/>
</segment>
<segment>
<pinref part="X1" gate="-4" pin="S"/>
<wire x1="233.68" y1="58.42" x2="213.36" y2="58.42" width="0.1524" layer="91"/>
<label x="213.36" y="58.42" size="1.778" layer="95"/>
</segment>
<segment>
<pinref part="IC2" gate="G$1" pin="PB0"/>
<wire x1="76.2" y1="63.5" x2="88.9" y2="63.5" width="0.1524" layer="91"/>
<label x="76.2" y="63.5" size="1.778" layer="95"/>
</segment>
</net>
<net name="VDDUC" class="1">
<segment>
<pinref part="U1" gate="G$1" pin="VDD"/>
<wire x1="104.14" y1="121.92" x2="104.14" y2="132.08" width="0.1524" layer="91"/>
<wire x1="104.14" y1="132.08" x2="73.66" y2="132.08" width="0.1524" layer="91"/>
<junction x="104.14" y="132.08"/>
<label x="83.82" y="132.08" size="1.778" layer="95"/>
<pinref part="C2" gate="A" pin="1"/>
<wire x1="109.22" y1="132.08" x2="104.14" y2="132.08" width="0.1524" layer="91"/>
<wire x1="104.14" y1="132.08" x2="104.14" y2="137.16" width="0.1524" layer="91"/>
<wire x1="104.14" y1="137.16" x2="134.62" y2="137.16" width="0.1524" layer="91"/>
<pinref part="R3" gate="A" pin="2"/>
<wire x1="129.54" y1="124.46" x2="129.54" y2="132.08" width="0.1524" layer="91"/>
<wire x1="129.54" y1="132.08" x2="134.62" y2="132.08" width="0.1524" layer="91"/>
<pinref part="R4" gate="A" pin="2"/>
<wire x1="134.62" y1="132.08" x2="137.16" y2="132.08" width="0.1524" layer="91"/>
<wire x1="137.16" y1="132.08" x2="137.16" y2="124.46" width="0.1524" layer="91"/>
<wire x1="134.62" y1="137.16" x2="134.62" y2="132.08" width="0.1524" layer="91"/>
<junction x="134.62" y="132.08"/>
</segment>
<segment>
<pinref part="R6" gate="A" pin="2"/>
<wire x1="60.96" y1="121.92" x2="60.96" y2="132.08" width="0.1524" layer="91"/>
<wire x1="60.96" y1="132.08" x2="53.34" y2="132.08" width="0.1524" layer="91"/>
<label x="35.56" y="132.08" size="1.778" layer="95"/>
<pinref part="R7" gate="A" pin="2"/>
<wire x1="53.34" y1="132.08" x2="45.72" y2="132.08" width="0.1524" layer="91"/>
<wire x1="45.72" y1="132.08" x2="38.1" y2="132.08" width="0.1524" layer="91"/>
<wire x1="38.1" y1="132.08" x2="35.56" y2="132.08" width="0.1524" layer="91"/>
<wire x1="53.34" y1="121.92" x2="53.34" y2="132.08" width="0.1524" layer="91"/>
<pinref part="R8" gate="A" pin="2"/>
<wire x1="45.72" y1="121.92" x2="45.72" y2="132.08" width="0.1524" layer="91"/>
<pinref part="R9" gate="A" pin="2"/>
<wire x1="38.1" y1="121.92" x2="38.1" y2="132.08" width="0.1524" layer="91"/>
<junction x="38.1" y="132.08"/>
<junction x="45.72" y="132.08"/>
<junction x="53.34" y="132.08"/>
</segment>
<segment>
<pinref part="JP1" gate="G$1" pin="1"/>
<wire x1="82.804" y1="160.02" x2="78.74" y2="160.02" width="0.1524" layer="91"/>
<wire x1="78.74" y1="160.02" x2="78.74" y2="162.56" width="0.1524" layer="91"/>
<wire x1="78.74" y1="162.56" x2="60.96" y2="162.56" width="0.1524" layer="91"/>
<label x="60.96" y="162.56" size="1.778" layer="95"/>
</segment>
<segment>
<pinref part="R11" gate="A" pin="2"/>
<wire x1="233.68" y1="154.94" x2="233.68" y2="157.48" width="0.1524" layer="91"/>
<wire x1="233.68" y1="157.48" x2="226.06" y2="157.48" width="0.1524" layer="91"/>
<pinref part="R1" gate="A" pin="2"/>
<wire x1="226.06" y1="157.48" x2="218.44" y2="157.48" width="0.1524" layer="91"/>
<wire x1="218.44" y1="157.48" x2="205.74" y2="157.48" width="0.1524" layer="91"/>
<wire x1="218.44" y1="154.94" x2="218.44" y2="157.48" width="0.1524" layer="91"/>
<pinref part="R5" gate="A" pin="2"/>
<wire x1="226.06" y1="154.94" x2="226.06" y2="157.48" width="0.1524" layer="91"/>
<junction x="218.44" y="157.48"/>
<junction x="226.06" y="157.48"/>
<label x="205.74" y="157.48" size="1.778" layer="95"/>
</segment>
<segment>
<pinref part="IC1" gate="P" pin="VCC"/>
<wire x1="121.92" y1="33.02" x2="121.92" y2="30.48" width="0.1524" layer="91"/>
<wire x1="121.92" y1="33.02" x2="134.62" y2="33.02" width="0.1524" layer="91"/>
<label x="137.16" y="33.02" size="1.778" layer="95"/>
<wire x1="134.62" y1="33.02" x2="139.7" y2="33.02" width="0.1524" layer="91"/>
<wire x1="134.62" y1="25.4" x2="134.62" y2="33.02" width="0.1524" layer="91"/>
<junction x="134.62" y="33.02"/>
<pinref part="C3" gate="A" pin="2"/>
</segment>
<segment>
<pinref part="IC2" gate="G$1" pin="VDD"/>
<wire x1="53.34" y1="73.66" x2="53.34" y2="86.36" width="0.1524" layer="91"/>
<wire x1="53.34" y1="86.36" x2="63.5" y2="86.36" width="0.1524" layer="91"/>
<pinref part="C4" gate="A" pin="2"/>
<wire x1="63.5" y1="86.36" x2="66.04" y2="86.36" width="0.1524" layer="91"/>
<wire x1="63.5" y1="83.82" x2="63.5" y2="86.36" width="0.1524" layer="91"/>
<junction x="63.5" y="86.36"/>
<label x="66.04" y="86.36" size="1.778" layer="95"/>
</segment>
</net>
<net name="UPDI" class="0">
<segment>
<pinref part="U1" gate="G$1" pin="PA0/NRESET/UPDI"/>
<wire x1="81.28" y1="111.76" x2="71.12" y2="111.76" width="0.1524" layer="91"/>
<wire x1="71.12" y1="154.94" x2="71.12" y2="144.78" width="0.1524" layer="91"/>
<pinref part="JP1" gate="G$1" pin="3"/>
<wire x1="71.12" y1="144.78" x2="71.12" y2="111.76" width="0.1524" layer="91"/>
<wire x1="71.12" y1="154.94" x2="82.804" y2="154.94" width="0.1524" layer="91"/>
<pinref part="R2" gate="A" pin="1"/>
<wire x1="63.5" y1="147.32" x2="63.5" y2="144.78" width="0.1524" layer="91"/>
<wire x1="63.5" y1="144.78" x2="71.12" y2="144.78" width="0.1524" layer="91"/>
<junction x="71.12" y="144.78"/>
<label x="71.12" y="111.76" size="1.778" layer="95"/>
</segment>
<segment>
<pinref part="IC2" gate="G$1" pin="PA0/NRESET/UPDI"/>
<wire x1="30.48" y1="63.5" x2="20.32" y2="63.5" width="0.1524" layer="91"/>
<label x="20.32" y="63.5" size="1.778" layer="95"/>
</segment>
<segment>
<pinref part="R16" gate="A" pin="2"/>
<wire x1="203.2" y1="43.18" x2="190.5" y2="43.18" width="0.1524" layer="91"/>
<label x="190.5" y="43.18" size="1.778" layer="95"/>
</segment>
</net>
<net name="GND" class="1">
<segment>
<pinref part="GND2" gate="1" pin="GND"/>
<pinref part="C2" gate="A" pin="2"/>
<wire x1="114.3" y1="132.08" x2="119.38" y2="132.08" width="0.1524" layer="91"/>
<wire x1="119.38" y1="132.08" x2="119.38" y2="124.46" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="U1" gate="G$1" pin="GND"/>
<pinref part="GND3" gate="1" pin="GND"/>
<wire x1="104.14" y1="83.82" x2="104.14" y2="73.66" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="JP1" gate="G$1" pin="4"/>
<wire x1="82.804" y1="152.4" x2="81.28" y2="152.4" width="0.1524" layer="91"/>
<wire x1="81.28" y1="152.4" x2="81.28" y2="147.32" width="0.1524" layer="91"/>
<pinref part="GND1" gate="1" pin="GND"/>
</segment>
<segment>
<pinref part="X1" gate="-2" pin="S"/>
<wire x1="233.68" y1="63.5" x2="213.36" y2="63.5" width="0.1524" layer="91"/>
<label x="213.36" y="63.5" size="1.778" layer="95"/>
</segment>
<segment>
<pinref part="Q2" gate="G$1" pin="S"/>
<wire x1="157.48" y1="40.64" x2="157.48" y2="35.56" width="0.1524" layer="91"/>
<pinref part="GND6" gate="1" pin="GND"/>
</segment>
<segment>
<pinref part="IC1" gate="P" pin="GND"/>
<pinref part="GND7" gate="1" pin="GND"/>
<wire x1="121.92" y1="15.24" x2="121.92" y2="12.7" width="0.1524" layer="91"/>
<pinref part="C3" gate="A" pin="1"/>
<wire x1="121.92" y1="12.7" x2="121.92" y2="10.16" width="0.1524" layer="91"/>
<wire x1="134.62" y1="20.32" x2="134.62" y2="12.7" width="0.1524" layer="91"/>
<wire x1="134.62" y1="12.7" x2="121.92" y2="12.7" width="0.1524" layer="91"/>
<junction x="121.92" y="12.7"/>
</segment>
<segment>
<pinref part="IC2" gate="G$1" pin="GND"/>
<pinref part="GND8" gate="1" pin="GND"/>
<wire x1="53.34" y1="30.48" x2="53.34" y2="27.94" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="C4" gate="A" pin="1"/>
<pinref part="GND9" gate="1" pin="GND"/>
<wire x1="63.5" y1="78.74" x2="63.5" y2="76.2" width="0.1524" layer="91"/>
</segment>
<segment>
<wire x1="132.08" y1="160.02" x2="129.54" y2="160.02" width="0.1524" layer="91"/>
<wire x1="129.54" y1="160.02" x2="129.54" y2="157.48" width="0.1524" layer="91"/>
<pinref part="JP2" gate="G$1" pin="3"/>
<pinref part="GND14" gate="1" pin="GND"/>
</segment>
</net>
<net name="N$4" class="0">
<segment>
<wire x1="73.66" y1="160.02" x2="63.5" y2="160.02" width="0.1524" layer="91"/>
<pinref part="R2" gate="A" pin="2"/>
<wire x1="63.5" y1="160.02" x2="63.5" y2="157.48" width="0.1524" layer="91"/>
<wire x1="73.66" y1="160.02" x2="73.66" y2="157.48" width="0.1524" layer="91"/>
<pinref part="JP1" gate="G$1" pin="2"/>
<wire x1="73.66" y1="157.48" x2="82.804" y2="157.48" width="0.1524" layer="91"/>
</segment>
</net>
<net name="+5V" class="1">
<segment>
<pinref part="X1" gate="-1" pin="S"/>
<wire x1="233.68" y1="66.04" x2="213.36" y2="66.04" width="0.1524" layer="91"/>
<label x="213.36" y="66.04" size="1.778" layer="95"/>
</segment>
<segment>
<pinref part="JP2" gate="G$1" pin="1"/>
<wire x1="132.08" y1="165.1" x2="119.38" y2="165.1" width="0.1524" layer="91"/>
<label x="119.38" y="165.1" size="1.778" layer="95"/>
</segment>
</net>
<net name="LED_ANODE" class="0">
<segment>
<pinref part="LED3" gate="LED" pin="A"/>
<pinref part="R11" gate="A" pin="1"/>
<wire x1="233.68" y1="144.78" x2="233.68" y2="142.24" width="0.1524" layer="91"/>
</segment>
</net>
<net name="A" class="0">
<segment>
<pinref part="SW3" gate="G$1" pin="C"/>
<wire x1="180.34" y1="96.52" x2="180.34" y2="93.98" width="0.1524" layer="91"/>
<wire x1="180.34" y1="93.98" x2="190.5" y2="93.98" width="0.1524" layer="91"/>
<pinref part="SW3" gate="G$2" pin="1"/>
<wire x1="190.5" y1="93.98" x2="190.5" y2="96.52" width="0.1524" layer="91"/>
<label x="182.88" y="93.98" size="1.778" layer="95"/>
<wire x1="180.34" y1="93.98" x2="170.18" y2="93.98" width="0.1524" layer="91"/>
<pinref part="S3" gate="G$1" pin="1"/>
<wire x1="170.18" y1="93.98" x2="170.18" y2="96.52" width="0.1524" layer="91"/>
<junction x="180.34" y="93.98"/>
</segment>
<segment>
<pinref part="U1" gate="G$1" pin="PA5"/>
<wire x1="81.28" y1="99.06" x2="73.66" y2="99.06" width="0.1524" layer="91"/>
<label x="73.66" y="99.06" size="1.778" layer="95"/>
</segment>
<segment>
<pinref part="IC2" gate="G$1" pin="PA5"/>
<wire x1="30.48" y1="50.8" x2="20.32" y2="50.8" width="0.1524" layer="91"/>
<label x="20.32" y="50.8" size="1.778" layer="95"/>
</segment>
<segment>
<pinref part="IC1" gate="A" pin="I1"/>
<wire x1="106.68" y1="40.64" x2="101.6" y2="40.64" width="0.1524" layer="91"/>
<label x="101.6" y="40.64" size="1.778" layer="95"/>
</segment>
</net>
<net name="B" class="0">
<segment>
<pinref part="SW2" gate="G$1" pin="C"/>
<wire x1="180.34" y1="119.38" x2="180.34" y2="116.84" width="0.1524" layer="91"/>
<wire x1="180.34" y1="116.84" x2="190.5" y2="116.84" width="0.1524" layer="91"/>
<pinref part="SW2" gate="G$2" pin="1"/>
<wire x1="190.5" y1="116.84" x2="190.5" y2="119.38" width="0.1524" layer="91"/>
<label x="182.88" y="116.84" size="1.778" layer="95"/>
<wire x1="180.34" y1="116.84" x2="170.18" y2="116.84" width="0.1524" layer="91"/>
<pinref part="S2" gate="G$1" pin="1"/>
<wire x1="170.18" y1="116.84" x2="170.18" y2="119.38" width="0.1524" layer="91"/>
<junction x="180.34" y="116.84"/>
</segment>
<segment>
<pinref part="U1" gate="G$1" pin="PA6"/>
<wire x1="81.28" y1="96.52" x2="73.66" y2="96.52" width="0.1524" layer="91"/>
<label x="73.66" y="96.52" size="1.778" layer="95"/>
</segment>
<segment>
<pinref part="IC2" gate="G$1" pin="PA6"/>
<wire x1="30.48" y1="48.26" x2="20.32" y2="48.26" width="0.1524" layer="91"/>
<label x="20.32" y="48.26" size="1.778" layer="95"/>
</segment>
<segment>
<pinref part="IC1" gate="A" pin="I0"/>
<wire x1="106.68" y1="45.72" x2="101.6" y2="45.72" width="0.1524" layer="91"/>
<label x="101.6" y="45.72" size="1.778" layer="95"/>
</segment>
</net>
<net name="C" class="0">
<segment>
<pinref part="SW1" gate="G$2" pin="1"/>
<wire x1="190.5" y1="142.24" x2="190.5" y2="139.7" width="0.1524" layer="91"/>
<wire x1="190.5" y1="139.7" x2="180.34" y2="139.7" width="0.1524" layer="91"/>
<pinref part="SW1" gate="G$1" pin="C"/>
<wire x1="180.34" y1="139.7" x2="180.34" y2="142.24" width="0.1524" layer="91"/>
<label x="182.88" y="139.7" size="1.778" layer="95"/>
<pinref part="S1" gate="G$1" pin="1"/>
<wire x1="170.18" y1="142.24" x2="170.18" y2="139.7" width="0.1524" layer="91"/>
<wire x1="170.18" y1="139.7" x2="180.34" y2="139.7" width="0.1524" layer="91"/>
<junction x="180.34" y="139.7"/>
</segment>
<segment>
<pinref part="U1" gate="G$1" pin="PA7"/>
<wire x1="81.28" y1="93.98" x2="73.66" y2="93.98" width="0.1524" layer="91"/>
<label x="73.66" y="93.98" size="1.778" layer="95"/>
</segment>
<segment>
<pinref part="IC2" gate="G$1" pin="PA7"/>
<wire x1="30.48" y1="45.72" x2="20.32" y2="45.72" width="0.1524" layer="91"/>
<label x="20.32" y="45.72" size="1.778" layer="95"/>
</segment>
</net>
<net name="D" class="0">
<segment>
<pinref part="SW4" gate="G$1" pin="C"/>
<wire x1="213.36" y1="83.82" x2="213.36" y2="81.28" width="0.1524" layer="91"/>
<wire x1="213.36" y1="81.28" x2="223.52" y2="81.28" width="0.1524" layer="91"/>
<pinref part="SW4" gate="G$2" pin="1"/>
<wire x1="223.52" y1="81.28" x2="223.52" y2="83.82" width="0.1524" layer="91"/>
<label x="215.9" y="81.28" size="1.778" layer="95"/>
<wire x1="213.36" y1="81.28" x2="203.2" y2="81.28" width="0.1524" layer="91"/>
<pinref part="S4" gate="G$1" pin="1"/>
<wire x1="203.2" y1="81.28" x2="203.2" y2="83.82" width="0.1524" layer="91"/>
<junction x="213.36" y="81.28"/>
</segment>
<segment>
<pinref part="U1" gate="G$1" pin="PB2/TOSC2"/>
<wire x1="127" y1="106.68" x2="137.16" y2="106.68" width="0.1524" layer="91"/>
<label x="129.54" y="106.68" size="1.778" layer="95"/>
</segment>
<segment>
<pinref part="IC2" gate="G$1" pin="PB2/TOSC2"/>
<wire x1="76.2" y1="58.42" x2="88.9" y2="58.42" width="0.1524" layer="91"/>
<label x="76.2" y="58.42" size="1.778" layer="95"/>
</segment>
</net>
<net name="DB" class="0">
<segment>
<pinref part="U1" gate="G$1" pin="PA2"/>
<wire x1="81.28" y1="106.68" x2="53.34" y2="106.68" width="0.1524" layer="91"/>
<label x="63.5" y="106.68" size="1.778" layer="95"/>
<pinref part="R7" gate="A" pin="1"/>
<wire x1="53.34" y1="106.68" x2="53.34" y2="111.76" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="SW1" gate="G$1" pin="B"/>
<wire x1="182.88" y1="152.4" x2="182.88" y2="154.94" width="0.1524" layer="91"/>
<label x="182.88" y="152.4" size="1.778" layer="95" rot="R90"/>
</segment>
<segment>
<pinref part="SW2" gate="G$1" pin="B"/>
<wire x1="182.88" y1="129.54" x2="182.88" y2="132.08" width="0.1524" layer="91"/>
<label x="182.88" y="129.54" size="1.778" layer="95" rot="R90"/>
</segment>
<segment>
<pinref part="SW3" gate="G$1" pin="B"/>
<wire x1="182.88" y1="106.68" x2="182.88" y2="109.22" width="0.1524" layer="91"/>
<label x="182.88" y="106.68" size="1.778" layer="95" rot="R90"/>
</segment>
<segment>
<pinref part="SW4" gate="G$1" pin="B"/>
<wire x1="215.9" y1="93.98" x2="215.9" y2="96.52" width="0.1524" layer="91"/>
<label x="215.9" y="93.98" size="1.778" layer="95" rot="R90"/>
</segment>
<segment>
<pinref part="IC2" gate="G$1" pin="PA2"/>
<wire x1="30.48" y1="58.42" x2="20.32" y2="58.42" width="0.1524" layer="91"/>
<label x="20.32" y="58.42" size="1.778" layer="95"/>
</segment>
</net>
<net name="DC" class="0">
<segment>
<wire x1="55.88" y1="104.14" x2="45.72" y2="104.14" width="0.1524" layer="91"/>
<label x="53.34" y="104.14" size="1.778" layer="95"/>
<pinref part="R8" gate="A" pin="1"/>
<wire x1="45.72" y1="104.14" x2="45.72" y2="111.76" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="SW1" gate="G$2" pin="2"/>
<wire x1="190.5" y1="152.4" x2="190.5" y2="154.94" width="0.1524" layer="91"/>
<label x="190.5" y="152.4" size="1.778" layer="95" rot="R90"/>
</segment>
<segment>
<pinref part="SW2" gate="G$2" pin="2"/>
<wire x1="190.5" y1="129.54" x2="190.5" y2="132.08" width="0.1524" layer="91"/>
<label x="190.5" y="129.54" size="1.778" layer="95" rot="R90"/>
</segment>
<segment>
<pinref part="SW3" gate="G$2" pin="2"/>
<wire x1="190.5" y1="106.68" x2="190.5" y2="109.22" width="0.1524" layer="91"/>
<label x="190.5" y="106.68" size="1.778" layer="95" rot="R90"/>
</segment>
<segment>
<pinref part="SW4" gate="G$2" pin="2"/>
<wire x1="223.52" y1="93.98" x2="223.52" y2="96.52" width="0.1524" layer="91"/>
<label x="223.52" y="93.98" size="1.778" layer="95" rot="R90"/>
</segment>
<segment>
<pinref part="U1" gate="G$1" pin="PB3/TOSC1"/>
<wire x1="127" y1="104.14" x2="137.16" y2="104.14" width="0.1524" layer="91"/>
<label x="129.54" y="104.14" size="1.778" layer="95"/>
</segment>
<segment>
<pinref part="IC2" gate="G$1" pin="PC0"/>
<wire x1="76.2" y1="48.26" x2="88.9" y2="48.26" width="0.1524" layer="91"/>
<label x="76.2" y="48.26" size="1.778" layer="95"/>
</segment>
</net>
<net name="DD" class="0">
<segment>
<pinref part="U1" gate="G$1" pin="PA4"/>
<wire x1="81.28" y1="101.6" x2="38.1" y2="101.6" width="0.1524" layer="91"/>
<label x="63.5" y="101.6" size="1.778" layer="95"/>
<pinref part="R9" gate="A" pin="1"/>
<wire x1="38.1" y1="101.6" x2="38.1" y2="111.76" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="S1" gate="G$1" pin="2"/>
<wire x1="170.18" y1="152.4" x2="170.18" y2="154.94" width="0.1524" layer="91"/>
<label x="170.18" y="152.4" size="1.778" layer="95" rot="R90"/>
</segment>
<segment>
<pinref part="S2" gate="G$1" pin="2"/>
<wire x1="170.18" y1="129.54" x2="170.18" y2="132.08" width="0.1524" layer="91"/>
<label x="170.18" y="129.54" size="1.778" layer="95" rot="R90"/>
</segment>
<segment>
<pinref part="S3" gate="G$1" pin="2"/>
<wire x1="170.18" y1="106.68" x2="170.18" y2="109.22" width="0.1524" layer="91"/>
<label x="170.18" y="106.68" size="1.778" layer="95" rot="R90"/>
</segment>
<segment>
<pinref part="S4" gate="G$1" pin="2"/>
<wire x1="203.2" y1="93.98" x2="203.2" y2="96.52" width="0.1524" layer="91"/>
<label x="203.2" y="93.98" size="1.778" layer="95" rot="R90"/>
</segment>
<segment>
<pinref part="IC2" gate="G$1" pin="PA4"/>
<wire x1="30.48" y1="53.34" x2="20.32" y2="53.34" width="0.1524" layer="91"/>
<label x="20.32" y="53.34" size="1.778" layer="95"/>
</segment>
</net>
<net name="CS0" class="0">
<segment>
<pinref part="X1" gate="-5" pin="S"/>
<wire x1="233.68" y1="55.88" x2="213.36" y2="55.88" width="0.1524" layer="91"/>
<label x="213.36" y="55.88" size="1.778" layer="95"/>
</segment>
</net>
<net name="CS1" class="0">
<segment>
<pinref part="X1" gate="-6" pin="S"/>
<wire x1="233.68" y1="53.34" x2="213.36" y2="53.34" width="0.1524" layer="91"/>
<label x="213.36" y="53.34" size="1.778" layer="95"/>
</segment>
</net>
<net name="OLED_DC" class="0">
<segment>
<pinref part="X1" gate="-7" pin="S"/>
<wire x1="233.68" y1="50.8" x2="213.36" y2="50.8" width="0.1524" layer="91"/>
<label x="213.36" y="50.8" size="1.778" layer="95"/>
</segment>
</net>
<net name="MOSI" class="0">
<segment>
<pinref part="X1" gate="-8" pin="S"/>
<wire x1="233.68" y1="48.26" x2="213.36" y2="48.26" width="0.1524" layer="91"/>
<label x="213.36" y="48.26" size="1.778" layer="95"/>
</segment>
</net>
<net name="SCK" class="0">
<segment>
<pinref part="X1" gate="-9" pin="S"/>
<wire x1="233.68" y1="45.72" x2="213.36" y2="45.72" width="0.1524" layer="91"/>
<label x="213.36" y="45.72" size="1.778" layer="95"/>
</segment>
</net>
<net name="N$8" class="0">
<segment>
<pinref part="LED1" gate="LED" pin="A"/>
<pinref part="R1" gate="A" pin="1"/>
<wire x1="218.44" y1="142.24" x2="218.44" y2="144.78" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$9" class="0">
<segment>
<pinref part="LED2" gate="LED" pin="A"/>
<pinref part="R5" gate="A" pin="1"/>
<wire x1="226.06" y1="144.78" x2="226.06" y2="142.24" width="0.1524" layer="91"/>
</segment>
</net>
<net name="LEDA" class="1">
<segment>
<pinref part="LED1" gate="LED" pin="C"/>
<wire x1="218.44" y1="134.62" x2="218.44" y2="127" width="0.1524" layer="91"/>
<label x="218.44" y="127" size="1.778" layer="95" rot="R90"/>
</segment>
<segment>
<pinref part="IC2" gate="G$1" pin="PB4"/>
<wire x1="76.2" y1="53.34" x2="88.9" y2="53.34" width="0.1524" layer="91"/>
<label x="76.2" y="53.34" size="1.778" layer="95"/>
</segment>
</net>
<net name="LEDB" class="1">
<segment>
<pinref part="LED2" gate="LED" pin="C"/>
<wire x1="226.06" y1="134.62" x2="226.06" y2="127" width="0.1524" layer="91"/>
<label x="226.06" y="127" size="1.778" layer="95" rot="R90"/>
</segment>
<segment>
<pinref part="IC2" gate="G$1" pin="PB5"/>
<wire x1="76.2" y1="50.8" x2="88.9" y2="50.8" width="0.1524" layer="91"/>
<label x="76.2" y="50.8" size="1.778" layer="95"/>
</segment>
</net>
<net name="LEDC" class="1">
<segment>
<pinref part="LED3" gate="LED" pin="C"/>
<wire x1="233.68" y1="134.62" x2="233.68" y2="127" width="0.1524" layer="91"/>
<label x="233.68" y="127" size="1.778" layer="95" rot="R90"/>
</segment>
<segment>
<pinref part="IC2" gate="G$1" pin="PB3/TOSC1"/>
<wire x1="76.2" y1="55.88" x2="88.9" y2="55.88" width="0.1524" layer="91"/>
<label x="76.2" y="55.88" size="1.778" layer="95"/>
</segment>
</net>
<net name="LED_SCK" class="0">
<segment>
<pinref part="U1" gate="G$1" pin="PA3/EXTCLK"/>
<wire x1="81.28" y1="104.14" x2="68.58" y2="104.14" width="0.1524" layer="91"/>
<label x="68.58" y="104.14" size="1.778" layer="95"/>
</segment>
<segment>
<pinref part="IC2" gate="G$1" pin="PA3/EXTCLK"/>
<wire x1="30.48" y1="55.88" x2="20.32" y2="55.88" width="0.1524" layer="91"/>
<label x="20.32" y="55.88" size="1.778" layer="95"/>
</segment>
</net>
<net name="DA/LED_DATA" class="0">
<segment>
<pinref part="U1" gate="G$1" pin="PA1"/>
<wire x1="81.28" y1="109.22" x2="60.96" y2="109.22" width="0.1524" layer="91"/>
<label x="63.5" y="109.22" size="1.778" layer="95"/>
<pinref part="R6" gate="A" pin="1"/>
<wire x1="60.96" y1="111.76" x2="60.96" y2="109.22" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="IC2" gate="G$1" pin="PA1"/>
<wire x1="30.48" y1="60.96" x2="20.32" y2="60.96" width="0.1524" layer="91"/>
<label x="20.32" y="60.96" size="1.778" layer="95"/>
</segment>
<segment>
<pinref part="SW1" gate="G$1" pin="A"/>
<wire x1="177.8" y1="152.4" x2="177.8" y2="154.94" width="0.1524" layer="91"/>
<label x="177.8" y="152.4" size="1.778" layer="95" rot="R90"/>
</segment>
<segment>
<pinref part="SW2" gate="G$1" pin="A"/>
<wire x1="177.8" y1="129.54" x2="177.8" y2="132.08" width="0.1524" layer="91"/>
<label x="177.8" y="129.54" size="1.778" layer="95" rot="R90"/>
</segment>
<segment>
<pinref part="SW3" gate="G$1" pin="A"/>
<wire x1="177.8" y1="106.68" x2="177.8" y2="109.22" width="0.1524" layer="91"/>
<label x="177.8" y="106.68" size="1.778" layer="95" rot="R90"/>
</segment>
<segment>
<pinref part="SW4" gate="G$1" pin="A"/>
<wire x1="210.82" y1="93.98" x2="210.82" y2="96.52" width="0.1524" layer="91"/>
<label x="210.82" y="93.98" size="1.778" layer="95" rot="R90"/>
</segment>
</net>
<net name="PROG" class="0">
<segment>
<pinref part="X1" gate="-10" pin="S"/>
<wire x1="233.68" y1="43.18" x2="213.36" y2="43.18" width="0.1524" layer="91"/>
<label x="213.36" y="43.18" size="1.778" layer="95"/>
<pinref part="R16" gate="A" pin="1"/>
</segment>
</net>
<net name="N$10" class="0">
<segment>
<pinref part="R14" gate="A" pin="2"/>
<pinref part="IC1" gate="A" pin="O"/>
<wire x1="139.7" y1="43.18" x2="137.16" y2="43.18" width="0.1524" layer="91"/>
<pinref part="R15" gate="A" pin="2"/>
<wire x1="137.16" y1="43.18" x2="132.08" y2="43.18" width="0.1524" layer="91"/>
<wire x1="139.7" y1="53.34" x2="137.16" y2="53.34" width="0.1524" layer="91"/>
<wire x1="137.16" y1="53.34" x2="137.16" y2="43.18" width="0.1524" layer="91"/>
<junction x="137.16" y="43.18"/>
</segment>
</net>
<net name="N$11" class="0">
<segment>
<pinref part="R14" gate="A" pin="1"/>
<pinref part="Q2" gate="G$1" pin="G"/>
<wire x1="149.86" y1="43.18" x2="152.4" y2="43.18" width="0.1524" layer="91"/>
</segment>
</net>
<net name="OLED_RST" class="0">
<segment>
<pinref part="R15" gate="A" pin="1"/>
<wire x1="149.86" y1="53.34" x2="157.48" y2="53.34" width="0.1524" layer="91"/>
<pinref part="Q2" gate="G$1" pin="D"/>
<wire x1="157.48" y1="53.34" x2="175.26" y2="53.34" width="0.1524" layer="91"/>
<wire x1="157.48" y1="50.8" x2="157.48" y2="53.34" width="0.1524" layer="91"/>
<junction x="157.48" y="53.34"/>
<label x="162.56" y="53.34" size="1.778" layer="95"/>
</segment>
<segment>
<pinref part="IC2" gate="G$1" pin="PC2"/>
<wire x1="76.2" y1="43.18" x2="88.9" y2="43.18" width="0.1524" layer="91"/>
<label x="76.2" y="43.18" size="1.778" layer="95"/>
</segment>
</net>
<net name="BUZ_ON" class="0">
<segment>
<pinref part="IC2" gate="G$1" pin="PC3"/>
<wire x1="76.2" y1="40.64" x2="88.9" y2="40.64" width="0.1524" layer="91"/>
<label x="76.2" y="40.64" size="1.778" layer="95"/>
</segment>
</net>
<net name="SPARE1" class="0">
<segment>
<pinref part="IC2" gate="G$1" pin="PC1"/>
<wire x1="76.2" y1="45.72" x2="88.9" y2="45.72" width="0.1524" layer="91"/>
<label x="76.2" y="45.72" size="1.778" layer="95"/>
</segment>
<segment>
<wire x1="132.08" y1="162.56" x2="119.38" y2="162.56" width="0.1524" layer="91"/>
<label x="119.38" y="162.56" size="1.778" layer="95"/>
<pinref part="JP2" gate="G$1" pin="2"/>
</segment>
</net>
</nets>
</sheet>
<sheet>
<plain>
</plain>
<instances>
<instance part="U$2" gate="G$1" x="43.18" y="33.02"/>
<instance part="FRAME2" gate="A" x="0" y="0"/>
<instance part="U$4" gate="G$1" x="109.22" y="33.02"/>
<instance part="U$3" gate="G$1" x="104.14" y="106.68"/>
<instance part="LD1" gate="G$1" x="127" y="142.24"/>
<instance part="LD2" gate="G$1" x="154.94" y="142.24"/>
<instance part="LD3" gate="G$1" x="182.88" y="142.24"/>
<instance part="SG1" gate="G$1" x="238.76" y="101.6" rot="R270"/>
<instance part="R10" gate="A" x="236.22" y="86.36" rot="R90"/>
<instance part="Q1" gate="G$1" x="236.22" y="68.58"/>
<instance part="GND5" gate="1" x="236.22" y="55.88"/>
<instance part="R12" gate="A" x="228.6" y="101.6" rot="R90"/>
<instance part="R13" gate="A" x="213.36" y="66.04" rot="R180"/>
<instance part="VR1" gate="G$1" x="40.64" y="147.32"/>
<instance part="GND4" gate="1" x="40.64" y="124.46"/>
<instance part="C1" gate="A" x="55.88" y="147.32" rot="R90"/>
<instance part="GND10" gate="1" x="81.28" y="88.9"/>
<instance part="GND11" gate="1" x="124.46" y="132.08"/>
<instance part="GND12" gate="1" x="152.4" y="132.08"/>
<instance part="GND13" gate="1" x="180.34" y="132.08"/>
<instance part="R17" gate="A" x="40.64" y="162.56" rot="R180"/>
</instances>
<busses>
</busses>
<nets>
<net name="SCK" class="0">
<segment>
<pinref part="U$2" gate="G$1" pin="D0"/>
<wire x1="40.64" y1="53.34" x2="40.64" y2="60.96" width="0.1524" layer="91"/>
<label x="40.64" y="55.88" size="1.778" layer="95" rot="R90"/>
</segment>
<segment>
<wire x1="83.82" y1="106.68" x2="68.58" y2="106.68" width="0.1524" layer="91"/>
<label x="68.58" y="106.68" size="1.778" layer="95"/>
<pinref part="U$3" gate="G$1" pin="SCK"/>
</segment>
<segment>
<pinref part="U$4" gate="G$1" pin="D0"/>
<wire x1="106.68" y1="53.34" x2="106.68" y2="60.96" width="0.1524" layer="91"/>
<label x="106.68" y="55.88" size="1.778" layer="95" rot="R90"/>
</segment>
</net>
<net name="MOSI" class="0">
<segment>
<pinref part="U$2" gate="G$1" pin="D1"/>
<wire x1="43.18" y1="53.34" x2="43.18" y2="60.96" width="0.1524" layer="91"/>
<label x="43.18" y="55.88" size="1.778" layer="95" rot="R90"/>
</segment>
<segment>
<wire x1="83.82" y1="109.22" x2="68.58" y2="109.22" width="0.1524" layer="91"/>
<label x="68.58" y="109.22" size="1.778" layer="95"/>
<pinref part="U$3" gate="G$1" pin="MOSI"/>
</segment>
<segment>
<pinref part="U$4" gate="G$1" pin="D1"/>
<wire x1="109.22" y1="53.34" x2="109.22" y2="60.96" width="0.1524" layer="91"/>
<label x="109.22" y="55.88" size="1.778" layer="95" rot="R90"/>
</segment>
</net>
<net name="OLED_RST" class="0">
<segment>
<pinref part="U$2" gate="G$1" pin="RES"/>
<wire x1="45.72" y1="53.34" x2="45.72" y2="60.96" width="0.1524" layer="91"/>
<label x="45.72" y="55.88" size="1.778" layer="95" rot="R90"/>
</segment>
<segment>
<wire x1="83.82" y1="114.3" x2="68.58" y2="114.3" width="0.1524" layer="91"/>
<label x="68.58" y="114.3" size="1.778" layer="95"/>
<pinref part="U$3" gate="G$1" pin="RESET"/>
</segment>
<segment>
<pinref part="U$4" gate="G$1" pin="RES"/>
<wire x1="111.76" y1="53.34" x2="111.76" y2="60.96" width="0.1524" layer="91"/>
<label x="111.76" y="55.88" size="1.778" layer="95" rot="R90"/>
</segment>
</net>
<net name="CS0" class="0">
<segment>
<pinref part="U$2" gate="G$1" pin="CS"/>
<wire x1="50.8" y1="53.34" x2="50.8" y2="60.96" width="0.1524" layer="91"/>
<label x="50.8" y="55.88" size="1.778" layer="95" rot="R90"/>
</segment>
<segment>
<wire x1="83.82" y1="116.84" x2="68.58" y2="116.84" width="0.1524" layer="91"/>
<label x="68.58" y="116.84" size="1.778" layer="95"/>
<pinref part="U$3" gate="G$1" pin="CS"/>
</segment>
</net>
<net name="+5V" class="1">
<segment>
<pinref part="U$2" gate="G$1" pin="VCC"/>
<wire x1="38.1" y1="53.34" x2="38.1" y2="60.96" width="0.1524" layer="91"/>
<label x="38.1" y="55.88" size="1.778" layer="95" rot="R90"/>
</segment>
<segment>
<wire x1="83.82" y1="119.38" x2="68.58" y2="119.38" width="0.1524" layer="91"/>
<label x="68.58" y="119.38" size="1.778" layer="95"/>
<pinref part="U$3" gate="G$1" pin="VCC"/>
</segment>
<segment>
<wire x1="83.82" y1="104.14" x2="68.58" y2="104.14" width="0.1524" layer="91"/>
<label x="68.58" y="104.14" size="1.778" layer="95"/>
<pinref part="U$3" gate="G$1" pin="LED"/>
</segment>
<segment>
<pinref part="U$4" gate="G$1" pin="VCC"/>
<wire x1="104.14" y1="53.34" x2="104.14" y2="60.96" width="0.1524" layer="91"/>
<label x="104.14" y="55.88" size="1.778" layer="95" rot="R90"/>
</segment>
<segment>
<pinref part="VR1" gate="G$1" pin="EN"/>
<wire x1="27.94" y1="149.86" x2="25.4" y2="149.86" width="0.1524" layer="91"/>
<wire x1="25.4" y1="149.86" x2="25.4" y2="154.94" width="0.1524" layer="91"/>
<pinref part="VR1" gate="G$1" pin="VIN"/>
<wire x1="27.94" y1="154.94" x2="25.4" y2="154.94" width="0.1524" layer="91"/>
<junction x="25.4" y="154.94"/>
<wire x1="25.4" y1="154.94" x2="12.7" y2="154.94" width="0.1524" layer="91"/>
<label x="12.7" y="154.94" size="1.778" layer="95"/>
<wire x1="35.56" y1="162.56" x2="25.4" y2="162.56" width="0.1524" layer="91"/>
<wire x1="25.4" y1="162.56" x2="25.4" y2="154.94" width="0.1524" layer="91"/>
<pinref part="R17" gate="A" pin="2"/>
</segment>
<segment>
<pinref part="LD1" gate="G$1" pin="VCC"/>
<wire x1="127" y1="152.4" x2="127" y2="154.94" width="0.1524" layer="91"/>
<wire x1="127" y1="154.94" x2="116.84" y2="154.94" width="0.1524" layer="91"/>
<label x="116.84" y="154.94" size="1.778" layer="95"/>
<pinref part="LD3" gate="G$1" pin="VCC"/>
<wire x1="127" y1="154.94" x2="154.94" y2="154.94" width="0.1524" layer="91"/>
<wire x1="154.94" y1="154.94" x2="182.88" y2="154.94" width="0.1524" layer="91"/>
<wire x1="182.88" y1="154.94" x2="182.88" y2="152.4" width="0.1524" layer="91"/>
<pinref part="LD2" gate="G$1" pin="VCC"/>
<wire x1="154.94" y1="154.94" x2="154.94" y2="152.4" width="0.1524" layer="91"/>
<junction x="127" y="154.94"/>
<junction x="154.94" y="154.94"/>
</segment>
</net>
<net name="GND" class="1">
<segment>
<pinref part="U$2" gate="G$1" pin="GND"/>
<wire x1="35.56" y1="53.34" x2="35.56" y2="60.96" width="0.1524" layer="91"/>
<label x="35.56" y="55.88" size="1.778" layer="95" rot="R90"/>
</segment>
<segment>
<wire x1="83.82" y1="96.52" x2="81.28" y2="96.52" width="0.1524" layer="91"/>
<wire x1="81.28" y1="96.52" x2="81.28" y2="91.44" width="0.1524" layer="91"/>
<label x="81.28" y="91.44" size="1.778" layer="95" rot="R90"/>
<pinref part="U$3" gate="G$1" pin="GND"/>
<pinref part="GND10" gate="1" pin="GND"/>
</segment>
<segment>
<pinref part="U$4" gate="G$1" pin="GND"/>
<wire x1="101.6" y1="53.34" x2="101.6" y2="60.96" width="0.1524" layer="91"/>
<label x="101.6" y="55.88" size="1.778" layer="95" rot="R90"/>
</segment>
<segment>
<pinref part="Q1" gate="G$1" pin="S"/>
<wire x1="236.22" y1="63.5" x2="236.22" y2="58.42" width="0.1524" layer="91"/>
<pinref part="GND5" gate="1" pin="GND"/>
</segment>
<segment>
<pinref part="VR1" gate="G$1" pin="GND"/>
<wire x1="40.64" y1="134.62" x2="40.64" y2="132.08" width="0.1524" layer="91"/>
<pinref part="GND4" gate="1" pin="GND"/>
<pinref part="C1" gate="A" pin="1"/>
<wire x1="40.64" y1="132.08" x2="40.64" y2="127" width="0.1524" layer="91"/>
<wire x1="55.88" y1="144.78" x2="55.88" y2="132.08" width="0.1524" layer="91"/>
<wire x1="55.88" y1="132.08" x2="40.64" y2="132.08" width="0.1524" layer="91"/>
<junction x="40.64" y="132.08"/>
</segment>
<segment>
<pinref part="LD1" gate="G$1" pin="GND"/>
<wire x1="124.46" y1="137.16" x2="124.46" y2="134.62" width="0.1524" layer="91"/>
<pinref part="GND11" gate="1" pin="GND"/>
</segment>
<segment>
<pinref part="LD2" gate="G$1" pin="GND"/>
<pinref part="GND12" gate="1" pin="GND"/>
<wire x1="152.4" y1="137.16" x2="152.4" y2="134.62" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="LD3" gate="G$1" pin="GND"/>
<pinref part="GND13" gate="1" pin="GND"/>
<wire x1="180.34" y1="137.16" x2="180.34" y2="134.62" width="0.1524" layer="91"/>
</segment>
</net>
<net name="OLED_DC" class="0">
<segment>
<pinref part="U$2" gate="G$1" pin="DC"/>
<wire x1="48.26" y1="53.34" x2="48.26" y2="60.96" width="0.1524" layer="91"/>
<label x="48.26" y="55.88" size="1.778" layer="95" rot="R90"/>
</segment>
<segment>
<wire x1="83.82" y1="111.76" x2="68.58" y2="111.76" width="0.1524" layer="91"/>
<label x="68.58" y="111.76" size="1.778" layer="95"/>
<pinref part="U$3" gate="G$1" pin="DC"/>
</segment>
<segment>
<pinref part="U$4" gate="G$1" pin="DC"/>
<wire x1="114.3" y1="53.34" x2="114.3" y2="60.96" width="0.1524" layer="91"/>
<label x="114.3" y="55.88" size="1.778" layer="95" rot="R90"/>
</segment>
</net>
<net name="CS1" class="0">
<segment>
<pinref part="U$4" gate="G$1" pin="CS"/>
<wire x1="116.84" y1="53.34" x2="116.84" y2="60.96" width="0.1524" layer="91"/>
<label x="116.84" y="55.88" size="1.778" layer="95" rot="R90"/>
</segment>
</net>
<net name="N$1" class="0">
<segment>
<pinref part="LD1" gate="G$1" pin="DO"/>
<pinref part="LD2" gate="G$1" pin="DI"/>
<wire x1="137.16" y1="147.32" x2="142.24" y2="147.32" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$5" class="0">
<segment>
<pinref part="LD1" gate="G$1" pin="CO"/>
<pinref part="LD2" gate="G$1" pin="CI"/>
<wire x1="137.16" y1="142.24" x2="142.24" y2="142.24" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$6" class="0">
<segment>
<pinref part="LD2" gate="G$1" pin="DO"/>
<pinref part="LD3" gate="G$1" pin="DI"/>
<wire x1="165.1" y1="147.32" x2="170.18" y2="147.32" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$7" class="0">
<segment>
<pinref part="LD2" gate="G$1" pin="CO"/>
<pinref part="LD3" gate="G$1" pin="CI"/>
<wire x1="165.1" y1="142.24" x2="170.18" y2="142.24" width="0.1524" layer="91"/>
</segment>
</net>
<net name="VDDUC" class="1">
<segment>
<pinref part="SG1" gate="G$1" pin="1"/>
<wire x1="236.22" y1="109.22" x2="236.22" y2="104.14" width="0.1524" layer="91"/>
<wire x1="236.22" y1="109.22" x2="246.38" y2="109.22" width="0.1524" layer="91"/>
<label x="238.76" y="109.22" size="1.778" layer="95"/>
<pinref part="R12" gate="A" pin="2"/>
<wire x1="228.6" y1="106.68" x2="228.6" y2="109.22" width="0.1524" layer="91"/>
<wire x1="228.6" y1="109.22" x2="236.22" y2="109.22" width="0.1524" layer="91"/>
<junction x="236.22" y="109.22"/>
</segment>
<segment>
<pinref part="VR1" gate="G$1" pin="VOUT"/>
<wire x1="53.34" y1="154.94" x2="55.88" y2="154.94" width="0.1524" layer="91"/>
<label x="55.88" y="154.94" size="1.778" layer="95"/>
<wire x1="55.88" y1="154.94" x2="68.58" y2="154.94" width="0.1524" layer="91"/>
<wire x1="45.72" y1="162.56" x2="55.88" y2="162.56" width="0.1524" layer="91"/>
<wire x1="55.88" y1="162.56" x2="55.88" y2="154.94" width="0.1524" layer="91"/>
<junction x="55.88" y="154.94"/>
<pinref part="C1" gate="A" pin="2"/>
<wire x1="55.88" y1="149.86" x2="55.88" y2="154.94" width="0.1524" layer="91"/>
<pinref part="R17" gate="A" pin="1"/>
</segment>
</net>
<net name="N$3" class="0">
<segment>
<pinref part="SG1" gate="G$1" pin="2"/>
<pinref part="R10" gate="A" pin="2"/>
<wire x1="236.22" y1="96.52" x2="236.22" y2="93.98" width="0.1524" layer="91"/>
<pinref part="R12" gate="A" pin="1"/>
<wire x1="236.22" y1="93.98" x2="236.22" y2="91.44" width="0.1524" layer="91"/>
<wire x1="228.6" y1="96.52" x2="228.6" y2="93.98" width="0.1524" layer="91"/>
<wire x1="228.6" y1="93.98" x2="236.22" y2="93.98" width="0.1524" layer="91"/>
<junction x="236.22" y="93.98"/>
</segment>
</net>
<net name="BUZ" class="0">
<segment>
<wire x1="236.22" y1="81.28" x2="236.22" y2="73.66" width="0.1524" layer="91"/>
<label x="236.22" y="76.2" size="1.778" layer="95" rot="R90"/>
<pinref part="R10" gate="A" pin="1"/>
<pinref part="Q1" gate="G$1" pin="D"/>
</segment>
</net>
<net name="BUZ_ON" class="0">
<segment>
<pinref part="Q1" gate="G$1" pin="G"/>
<pinref part="R13" gate="A" pin="1"/>
<wire x1="231.14" y1="66.04" x2="218.44" y2="66.04" width="0.1524" layer="91"/>
<label x="220.98" y="66.04" size="1.778" layer="95"/>
</segment>
</net>
<net name="UPDI" class="0">
<segment>
<pinref part="R13" gate="A" pin="2"/>
<wire x1="208.28" y1="66.04" x2="203.2" y2="66.04" width="0.1524" layer="91"/>
<label x="203.2" y="66.04" size="1.778" layer="95"/>
</segment>
</net>
<net name="DA/LED_DATA" class="0">
<segment>
<pinref part="LD1" gate="G$1" pin="DI"/>
<wire x1="114.3" y1="147.32" x2="91.44" y2="147.32" width="0.1524" layer="91"/>
<label x="91.44" y="147.32" size="1.778" layer="95"/>
</segment>
</net>
<net name="LED_SCK" class="0">
<segment>
<pinref part="LD1" gate="G$1" pin="CI"/>
<wire x1="114.3" y1="142.24" x2="91.44" y2="142.24" width="0.1524" layer="91"/>
<label x="91.44" y="142.24" size="1.778" layer="95"/>
</segment>
</net>
</nets>
</sheet>
</sheets>
<errors>
<approved hash="104,1,104.14,121.92,U1,VDD,VDDUC,,,"/>
<approved hash="104,2,38.1,53.34,U$2,VCC,+5V,,,"/>
<approved hash="104,2,104.14,53.34,U$4,VCC,+5V,,,"/>
<approved hash="104,2,83.82,119.38,U$3,VCC,+5V,,,"/>
</errors>
</schematic>
</drawing>
</eagle>
