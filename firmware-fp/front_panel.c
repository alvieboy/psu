/*
  ******************************************************************************
  * (C) 2018 Alvaro Lopes <alvieboy@alvie.com>
  *
  * Redistribution and use in source and binary forms, with or without modification,
  * are permitted provided that the following conditions are met:
  *   1. Redistributions of source code must retain the above copyright notice,
  *      this list of conditions and the following disclaimer.
  *   2. Redistributions in binary form must reproduce the above copyright notice,
  *      this list of conditions and the following disclaimer in the documentation
  *      and/or other materials provided with the distribution.
  *   3. Neither the name of Alvaro Lopes nor the names of contributors
  *      may be used to endorse or promote products derived from this software
  *      without specific prior written permission.
  *
  * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
  * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
  * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
  * DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
  * FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
  * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
  * SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
  * CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
  * OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
  * OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
  *
  ******************************************************************************
  */

#include <avr/io.h>
#include <avr/interrupt.h>
#include <util/delay.h>
#include <util/atomic.h>
#include <avr/eeprom.h>

/* Pin definitions */

#define PIN_DA_PORT PORTA
#define PIN_DA_PIN  1
#define PIN_DB_PIN  2
#define PIN_DC_PIN  3
#define PIN_DD_PIN  4

#define PIN_SA_PIN  5
#define PIN_SB_PIN  6
#define PIN_SC_PIN  7

// PORTB
#define PIN_SD_PIN 2
#define PIN_LED_PIN 3

#define I2C_ADDRESS 0x06

#define SET_BIT( where, index )                do { where |= ( 1 << (index) ); } while (0)
#define CLR_BIT( where, index )                do { where &= ~( 1 << (index) ); } while (0)

/*
 I2C register mapping.

 +-----------+-----------------------------+
 | Index     | Description                 |
 +-----------+-----------------------------+
 | 0         | All Button states           |
 | 1         | Rotary 0 position           |
 | 2         | Rotary 1 position           |
 | 3         | Rotary 2 position           |
 | 4         | Rotary 3 position           |
 | 5         | Button 0 sequence           |
 | 6         | Button 1 sequence           |
 | 7         | Button 2 sequence           |
 | 8         | Button 3 sequence           |
 | 9         | Button 4 sequence           |
 | 10        | Button 5 sequence           |
 | 11        | Button 6 sequence           |
 | 12        | Button 7 sequence           |
 | 13        | Rotary 0 sequence           |
 | 14        | Rotary 1 sequence           |
 | 15        | Rotary 2 sequence           |
 | 16        | Rotary 3 sequence           |
 | 17        | Indicator status            |
 +-----------+-----------------------------+
 */

#define I2C_REGS_MAX 18
#define I2C_NOREG 0xFF

#define BUTTON_STATE i2c_regs[0]
#define ROTARY_POSITION(x) i2c_regs[(x)+1]
#define BUTTON_SEQUENCE(x) i2c_regs[(x)+5]
#define ROTARY_SEQUENCE(x) i2c_regs[(x)+13]
#define INDICATOR_REG_INDEX 17
#define INDICATOR_OUT i2c_regs[INDICATOR_REG_INDEX]

#define I2C_STATUS GPIOR0
#define I2C_STATUS_WRITETRANS 0

#define MAX_BUTTONS 8
#define MAX_ROTARY 4

#define DEBOUNCE_THRESHOLD_HIGH 7
#define DEBOUNCE_THRESHOLD_LOW  -8

#define ROTARY_ACCEL_MAX      (12<<8)
#define ROTARY_ACCEL_INC      (25*8)
#define ROTARY_ACCEL_DEC      (2)


static int8_t button_cnt[MAX_BUTTONS];

static uint8_t i2c_current_reg=0; // CHANGE
static uint8_t i2c_regs[I2C_REGS_MAX];
static uint8_t current_scan = 0;
static uint8_t button_val = 0;
static uint8_t rotary_val = 0;
static volatile uint8_t scan_seq = 0;
static uint8_t button_input_tmp;
static uint8_t rotary_input_tmp;
static uint8_t prev_a, prev_b;


static void gpio__configure()
{
    VPORTA.DIR= (1<<PIN_SA_PIN)
        |   ( 1<<PIN_SB_PIN )
        |   ( 1<<PIN_SC_PIN );

    VPORTB.DIR= (1<<PIN_SD_PIN) |
        ( 1<<PIN_LED_PIN);
}

static void twi__configure()
{
    TWI0.SADDR = I2C_ADDRESS;
    TWI0.SADDRMASK = 0xFF;
    TWI0.SCTRLA=
        1 << TWI_DIEN_bp |                  /* 1: enable data interrupt vector call             */
        1 << TWI_APIEN_bp |                 /* 1: enable address/stop interrupt vector call     */
        1 << TWI_PIEN_bp |                  /* 1: set address interrupt flag on stop condition  */
        0 << TWI_PMEN_bp |                  /* 0: only respond to single slave address          */
        1 << TWI_SMEN_bp |                  /* 1: clear data interrupt flag on DATA access      */
        1 << TWI_ENABLE_bp;                 /* 1: enable TWI slave                              */

}

static void clk__configure()
{
    // 16/20Mhz oscillator, no clkout
    _PROTECTED_WRITE(CLKCTRL_MCLKCTRLA, 0x00 );
    // Set CLK_MAIN to 10Mhz
    _PROTECTED_WRITE(CLKCTRL_MCLKCTRLB,
                     (0x00<<CLKCTRL_PDIV_gp) |
                     (1<<CLKCTRL_PEN_bp));
    // Lock configuration
    _PROTECTED_WRITE(CLKCTRL_MCLKLOCK,
                     (1<<CLKCTRL_LOCKEN_bp));

}

static uint8_t eeprom_read(uint8_t index)
{
    if (index<EEPROM_SIZE) {
        return eeprom_read_byte((const uint8_t*)(int)index);
    }
    return 0;
}

static void eeprom_write(uint8_t index, uint8_t val)
{
    if (index<EEPROM_SIZE) {
        return eeprom_update_byte((uint8_t*)(int)index, val);
    }
}

static void i2c_write_register(uint8_t reg, uint8_t val)
{
    if (reg==INDICATOR_REG_INDEX) {
        INDICATOR_OUT=val;
    }
    // check NVM - TODO
    if (reg>=32 && reg<(32+EEPROM_SIZE))  {
        eeprom_write(reg, val);
    }
}

static uint8_t i2c_read_register(uint8_t reg)
{
    if ( reg < I2C_REGS_MAX ) {
        return i2c_regs[reg];
    }
    if (reg==31) {
        return 0xde;
    }
    if ((reg>=32) && reg<(32+EEPROM_SIZE)) {
        return eeprom_read(reg-32);
    }
    return 0x00;
}

void __attribute__((interrupt)) TWI0_TWIS_vect()
{
    uint8_t status = TWI0.SSTATUS;
    uint8_t val;

    if ( ~status & TWI_APIF_bm ) {
        if ( status & TWI_DIR_bm ) {
            TWI0.SDATA = i2c_read_register(i2c_current_reg);

            if (i2c_current_reg!=I2C_NOREG)
                i2c_current_reg++;

            TWI0.SCTRLB = TWI_SCMD_RESPONSE_gc;         /* transmit data or zeros */
        }
        else {
            /* on data interrupt at master write */
            val = TWI0.SDATA;
            //INDICATOR_OUT = 0xFF;
            if (!( I2C_STATUS & (1<<I2C_STATUS_WRITETRANS)) ) {
                i2c_current_reg = val;
            } else {
                i2c_write_register(i2c_current_reg, val);
                if (i2c_current_reg!=I2C_NOREG)
                    i2c_current_reg++;
            }
            SET_BIT( I2C_STATUS, I2C_STATUS_WRITETRANS );
            //TWI0.SCTRLB = TWI_ACKACT_ACK_gc |
            //    TWI_SCMD_RESPONSE_gc;               /* ACK and continue receiving */
        }
    }
    else if ( status & TWI_AP_bm ) {
        /* on address match interrupt */
        TWI0.SCTRLB = TWI_SCMD_RESPONSE_gc;         /* ACK and start sending/receiving */
    }
    else {
        /* on stop condition interrupt */
        CLR_BIT( I2C_STATUS, I2C_STATUS_WRITETRANS );
        TWI0.SCTRLB = TWI_SCMD_COMPTRANS_gc;            /* wait for new transaction */
    }

}

static void update_indicator(uint8_t index)
{
    if (INDICATOR_OUT & (1<<index)) {
        PORTB.OUTCLR = (1<<PIN_LED_PIN); // on
    } else {
        PORTB.OUTSET = (1<<PIN_LED_PIN); // on
    }
}

// NOTE: to be called with interrupts disabled.
static void add_inputs(uint8_t button_input, uint8_t rotary_input)
{
  //  i2c_regs[5] = button_input;
    button_val = ~button_input; // Inverted inputs
    rotary_val = rotary_input;
    scan_seq++;
}

void __attribute__((interrupt)) TCA0_OVF_vect()
{
    // Scan DA to DD
    uint8_t pa_in = PORTA.IN;
    uint8_t scan_value = (pa_in>>1) & 0xF; // DA-DD

    // DA-DB are not to be debounced.
    // DC-DD are normal switches
    rotary_input_tmp <<= 2;
    rotary_input_tmp |= (scan_value&0x3);

    scan_value>>=2;

    button_input_tmp <<= 2;
    button_input_tmp |= (scan_value&0x3);

    // Update scan
    switch (current_scan)
    {
    case 0:
        // Clear SA, turn on SB
        PORTA.OUTCLR = (1<<PIN_SA_PIN);
        update_indicator(1);
        PORTA.OUTSET = (1<<PIN_SB_PIN);
        break;
    case 1:
        // Clear SB, turn on SC
        PORTA.OUTCLR = (1<<PIN_SB_PIN);
        update_indicator(2);
        PORTA.OUTSET = (1<<PIN_SC_PIN);
        break;
    case 2:
        // Clear SC, turn on SD
        PORTA.OUTCLR = (1<<PIN_SC_PIN);
        update_indicator(3);
        PORTB.OUTSET = (1<<PIN_SD_PIN);
        break;
    case 3:
        // Clear SD, turn on SA
        PORTB.OUTCLR = (1<<PIN_SD_PIN);
        update_indicator(0);
        PORTA.OUTSET = (1<<PIN_SA_PIN);
        ATOMIC_BLOCK(ATOMIC_FORCEON) {
            add_inputs( button_input_tmp, rotary_input_tmp); // Process inputs at end of scan
        }
        button_input_tmp = 0;
        rotary_input_tmp = 0;
        break;
    }
    current_scan++;
    current_scan &= 0x3;

    // Clear interrupt
    TCA0.SINGLE.INTFLAGS=(1<<TCA_SINGLE_OVF_bp);
}

// Interrupt should work at 8KHz.

static void timer__configure()
{
    // Stop timer.
    TCA0.SINGLE.CTRLA = 0;
    // Configure

    TCA0.SINGLE.CTRLB = 0;
    TCA0.SINGLE.CTRLC = 0;
    TCA0.SINGLE.CTRLD = 0;
    // ~8Khz
    TCA0.SINGLE.PERL = 155;
    TCA0.SINGLE.PERH = 0;
    TCA0.SINGLE.PERBUF = 155;
    TCA0.SINGLE.CNT = 0;
    TCA0.SINGLE.INTCTRL = 1;

    // Prescaler 8: fTCA=1250000Hz
    //
    TCA0.SINGLE.CTRLA =
        TCA_SINGLE_CLKSEL_DIV8_gc |
        (0x1<<TCA_SINGLE_ENABLE_bp);

}

static void process_single_button(uint8_t index, uint8_t val)
{
    uint8_t current_state = !!(BUTTON_STATE & (1<<index));
    if (val != current_state) {
        if (val==0) {
            if (button_cnt[index] > DEBOUNCE_THRESHOLD_LOW) {
                button_cnt[index]--;
            } else {
                ATOMIC_BLOCK(ATOMIC_FORCEON) {
                    CLR_BIT(BUTTON_STATE,index);
                    BUTTON_SEQUENCE(index)++;
                }
                //d->callback(s);
            }
        } else {
            if (button_cnt[index] < DEBOUNCE_THRESHOLD_HIGH) {
                button_cnt[index]++;
            } else {
                ATOMIC_BLOCK(ATOMIC_FORCEON) {
                    SET_BIT(BUTTON_STATE,index);
                    BUTTON_SEQUENCE(index)++;
                }
                //d->callback(s);
            }
        }
    }
}

static int16_t rotary_accel[MAX_ROTARY];

static void process_single_rotary(uint8_t index, uint8_t val)
{
    uint8_t a = val & 1;
    uint8_t b = (val>>1) & 1;
    uint8_t delta;

    uint8_t rot_a = (prev_a>>index)&1;
    uint8_t rot_b = (prev_b>>index)&1;

    rotary_accel[index] -= ROTARY_ACCEL_DEC;

    if (rotary_accel[index]<0)
        rotary_accel[index] = 0;

    if (a != rot_a) {
        if (a) {
            SET_BIT(prev_a, index);
        } else {
            CLR_BIT(prev_a, index);
        }
        if (b != rot_b) {
            if (a==0) {
                // Rotary moved.
                rotary_accel[index] += ROTARY_ACCEL_INC;
                if (rotary_accel[index] > ROTARY_ACCEL_MAX)
                    rotary_accel[index] = ROTARY_ACCEL_MAX;

                delta = 1 + (rotary_accel[index] >> 8);

                ATOMIC_BLOCK(ATOMIC_FORCEON) {
                    ROTARY_SEQUENCE(index)++;
                    if (rot_b) {
                        ROTARY_POSITION(index)+=delta;
                    } else {
                        ROTARY_POSITION(index)-=delta;
                    }
                }
            }
        }
        if (b) {
            SET_BIT(prev_b, index);
        } else {
            CLR_BIT(prev_b, index);
        }
    }
}



static void process_buttons(uint8_t val)
{
    uint8_t index;
    for (index=0;index<MAX_BUTTONS;index++) {
        process_single_button(index, val&1);
        val>>=1;
    }
}

static void process_rotary(uint8_t val)
{
    uint8_t index;
    for (index=0;index<MAX_ROTARY;index++) {
        process_single_rotary(index, val&3);
        val>>=2;
    }
}

static void process_inputs(uint8_t button_val, uint8_t rotary_val)
{
    process_buttons(button_val);
    process_rotary(rotary_val);
}

int __attribute__((noreturn)) main()
{
    clk__configure();
    gpio__configure();

    I2C_STATUS = 0;

    twi__configure();

    // Start scan
    INDICATOR_OUT = 0;
    // Clear indicator
    update_indicator(0);
    // Start scanning
    PORTA.OUTSET = (1<<PIN_SA_PIN);

    timer__configure();
    sei();

    uint8_t prev_scan_seq = 0;

    while (1) {
        if (scan_seq!=prev_scan_seq) {
            prev_scan_seq = scan_seq;
            process_inputs(button_val, rotary_val);
        }
        // Sleep until next interrupt. TODO.
    }
}
