/*
  ******************************************************************************
  * (C) 2018 Alvaro Lopes <alvieboy@alvie.com>
  *
  * Based on work Copyright (C) Tilen Majerle, 2015
  *
  * Redistribution and use in source and binary forms, with or without modification,
  * are permitted provided that the following conditions are met:
  *   1. Redistributions of source code must retain the above copyright notice,
  *      this list of conditions and the following disclaimer.
  *   2. Redistributions in binary form must reproduce the above copyright notice,
  *      this list of conditions and the following disclaimer in the documentation
  *      and/or other materials provided with the distribution.
  *   3. Neither the name of Alvaro Lopes nor the names of contributors
  *      may be used to endorse or promote products derived from this software
  *      without specific prior written permission.
  *
  * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
  * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
  * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
  * DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
  * FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
  * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
  * SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
  * CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
  * OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
  * OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
  *
  ******************************************************************************
  */

#ifndef __ONEWIRE_H__
#define __ONEWIRE_H__

#include "defs.h"
#include <inttypes.h>

#define ONEWIRE_CMD_RSCRATCHPAD			0xBE
#define ONEWIRE_CMD_WSCRATCHPAD			0x4E
#define ONEWIRE_CMD_CPYSCRATCHPAD		0x48
#define ONEWIRE_CMD_RECEEPROM			0xB8
#define ONEWIRE_CMD_RPWRSUPPLY			0xB4
#define ONEWIRE_CMD_SEARCHROM			0xF0
#define ONEWIRE_CMD_READROM			0x33
#define ONEWIRE_CMD_MATCHROM			0x55
#define ONEWIRE_CMD_SKIPROM	                0xCC

#define ONEWIRE_BIT_WIDTH        60
#define ONEWIRE_ONE_PULSE_WIDTH  6
#define ONEWIRE_ZERO_PULSE_WIDTH 60
#define ONEWIRE_RECOVERY_DELAY   10
#define ONEWIRE_SAMPLE_DELAY     9

#define ONEWIRE_RESET_PULSE_WIDTH 480
#define ONEWIRE_RESET_WIDTH       (ONEWIRE_RESET_PULSE_WIDTH*2)
#define ONEWIRE_RESET_SAMPLEDLY   70


typedef int onewire_result_t;

#define ONEWIRE_OK 0
#define ONEWIRE_NO_MORE_DEVICES 1
#define ONEWIRE_ERR_NO_DEVICES -1

#define ONEWIRE_ISERR(x) ((x)<0)


void onewire__init(void);
void onewire__resetsearch(void);
onewire_result_t onewire__search(uint8_t command);
void onewire__select(const uint8_t *addr);
int onewire__reset(void);
void onewire__writebyte(uint8_t byte);
uint8_t onewire__readbit(void);
uint8_t onewire__readbyte(void);
const uint8_t *onewire__getrom(void);
uint8_t onewire__crc8(const uint8_t *addr, unsigned len);

onewire_result_t onewire__first(void);
onewire_result_t onewire__next(void);


#endif
