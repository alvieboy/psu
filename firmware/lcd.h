/*
  ******************************************************************************
  * (C) 2018 Alvaro Lopes <alvieboy@alvie.com>
  *
  * Redistribution and use in source and binary forms, with or without modification,
  * are permitted provided that the following conditions are met:
  *   1. Redistributions of source code must retain the above copyright notice,
  *      this list of conditions and the following disclaimer.
  *   2. Redistributions in binary form must reproduce the above copyright notice,
  *      this list of conditions and the following disclaimer in the documentation
  *      and/or other materials provided with the distribution.
  *   3. Neither the name of Alvaro Lopes nor the names of contributors
  *      may be used to endorse or promote products derived from this software
  *      without specific prior written permission.
  *
  * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
  * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
  * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
  * DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
  * FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
  * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
  * SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
  * CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
  * OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
  * OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
  *
  ******************************************************************************
  */

#ifndef __LCD_H__
#define __LCD_H__

#include <inttypes.h>
#include "font.h"

#ifdef __cplusplus
extern "C" {
#endif

#define LCD_WIDTH  320
#define LCD_HEIGHT 240

void lcd__init(void);
void lcd__clear(uint16_t color);
void lcd__setreset(unsigned);
void lcd__drawString(const font_t*font, int x, int y, const char *ch, uint16_t fg, uint16_t bg);
void lcd__drawFilledRect(int x, int y, int w, int h, uint16_t color);
void lcd__drawRect(int x, int y, int w, int h, uint16_t color);
void lcd__drawChar8(const font_t *font, int x, int y, uint8_t ch, uint16_t fg, uint16_t bg);
void lcd__drawChar16(const font_t *font, int x, int y, uint8_t ch, uint16_t fg, uint16_t bg);
void lcd__drawChar24(const font_t *font, int x, int y, uint8_t ch, uint16_t fg, uint16_t bg);
void lcd__drawChar32(const font_t *font, int x, int y, uint8_t ch, uint16_t fg, uint16_t bg);

static inline void lcd__drawChar(const font_t *font, int x, int y, uint8_t ch, uint16_t fg, uint16_t bg)
{
    if (font->w > 24) {
        lcd__drawChar32(font, x, y, ch, fg, bg);
    } else if (font->w > 16) {
        lcd__drawChar24(font, x, y, ch, fg, bg);
    } else if (font->w >8 ) {
        lcd__drawChar16(font, x, y, ch, fg, bg);
    } else {
        lcd__drawChar8(font, x, y, ch, fg, bg);
    }
}


#define COLOR_WHITE 0xFFFF
#define COLOR_BLACK 0x0000

#define COLOR_RGB(r,g,b) ( (b>>3) | (g>>2) << 5 | (r>>3) << 11 )

#define COLOR_RED     COLOR_RGB(0xFF,0x00,0x00)
#define COLOR_CYAN    COLOR_RGB(0x80,0x80,0xFF)
#define COLOR_GREEN   COLOR_RGB(0x00,0xFF,0x00)
#define COLOR_YELLOW  COLOR_RGB(0xFF,0xFF,0x00)
#define COLOR_GRAY    COLOR_RGB(0x7F,0x7F,0x7F)
#define COLOR_DARKGREEN    COLOR_RGB(0x00,0x7F,0x00)

#define VC_DOTWIDTH 14

#ifdef __cplusplus
}
#endif

#endif
