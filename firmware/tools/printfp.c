/*
  ******************************************************************************
  * (C) 2018 Alvaro Lopes <alvieboy@alvie.com>
  *
  * Redistribution and use in source and binary forms, with or without modification,
  * are permitted provided that the following conditions are met:
  *   1. Redistributions of source code must retain the above copyright notice,
  *      this list of conditions and the following disclaimer.
  *   2. Redistributions in binary form must reproduce the above copyright notice,
  *      this list of conditions and the following disclaimer in the documentation
  *      and/or other materials provided with the distribution.
  *   3. Neither the name of Alvaro Lopes nor the names of contributors
  *      may be used to endorse or promote products derived from this software
  *      without specific prior written permission.
  *
  * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
  * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
  * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
  * DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
  * FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
  * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
  * SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
  * CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
  * OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
  * OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
  *
  ******************************************************************************
  */

#include <stdio.h>
#include <inttypes.h>

#define FRACBITS 26

#define FLOAT2FP(x) ((uint32_t)((x)*(1<<FRACBITS)))

void printfp(uint32_t fp)
{
    uint8_t chars[32];
    int32_t i, n;

    // Round up
    fp += 0x00008312;

    // Convert integer part
    n = fp >> FRACBITS;
    i=0;
    do {
        chars[i++] = '0' + n % 10;
        n /= 10;
    } while (n!=0);
    i--;

    for (; i >= 0; i--)
        putc(chars[i],stdout);

    putc('.',stdout);

    // Convert fractional part
    for (i = 0; i < 3; i++) {
        fp &= (1<<FRACBITS)-1;
        fp *= 10;
        putc((fp >> FRACBITS) + '0',stdout);
    }

}


int main()
{
    uint32_t val = 1967;
    uint32_t mul = 0x7D323;
    uint32_t offset = 0;

    int32_t calc = (val*mul)+offset;

    double fp = calc;
    fp/=(1<<26);
    printf("%lf \n", fp);
    printf("%08x \n", (((int32_t)-12)<<FRACBITS)/3169);
    printfp(calc); putc('\n',stdout);
    printf("%08x\n", FLOAT2FP(0.0005));
    printfp( FLOAT2FP(1.0005001) ); putc('\n',stdout);
}
