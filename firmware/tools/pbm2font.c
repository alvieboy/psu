/*
  ******************************************************************************
  * (C) 2018 Alvaro Lopes <alvieboy@alvie.com>
  *
  * Redistribution and use in source and binary forms, with or without modification,
  * are permitted provided that the following conditions are met:
  *   1. Redistributions of source code must retain the above copyright notice,
  *      this list of conditions and the following disclaimer.
  *   2. Redistributions in binary form must reproduce the above copyright notice,
  *      this list of conditions and the following disclaimer in the documentation
  *      and/or other materials provided with the distribution.
  *   3. Neither the name of Alvaro Lopes nor the names of contributors
  *      may be used to endorse or promote products derived from this software
  *      without specific prior written permission.
  *
  * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
  * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
  * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
  * DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
  * FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
  * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
  * SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
  * CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
  * OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
  * OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
  *
  ******************************************************************************
  */

#include <stdio.h>
#include <stdlib.h>

int main(int argc, char **argv)
{
    unsigned char line[128];
    if (argc<4)
        return -1;
    int width = atoi(argv[2]);
    int height = atoi(argv[3]);

    if (width%8) {
        printf("non-byte char boundaries not supported\n");
        return -1;
    }

    FILE *in = fopen(argv[1],"r");

    fgets(line, sizeof(line), in);
    printf("> %s\n", line);
    fgets(line, sizeof(line), in);
    printf("> %s\n", line);
    fgets(line, sizeof(line), in);
    printf("> %s\n", line);
    unsigned w, h;
    sscanf(line, "%u %u", &w, &h);
    if (w%8) {
        printf("non-byte image boundaries not supported\n");
        return -1;
    }
    printf("// Image size: %u %u\n", w,h);
    unsigned total = (w/8)*h;
    unsigned numchars = w/width;
    printf("// bytes %u Num chars: %u\n", total, numchars);

    unsigned char *image = (unsigned char*)malloc(total);
    unsigned stride = w/8;

    int r = fread(image, total, 1, in);
    if (r<=0)
        return -1;


    int nc;
    int y,x;

    for (nc=0;nc<numchars;nc++) {
        for (y=0;y<height;y++) {
            for (x=0;x<width/8;x++) {
                printf(",0x%02X", (unsigned)image[((width/8)*nc)+  x+ (y*stride) ]);
            }
        }
        printf(", // End of char \n");
    }
}
