#ifndef __QADC_H__
#define __QADC_H__

#include <QObject>
#include <QGridLayout>
#include <QTimer>
#include <QDoubleSpinBox>

#define PSU_VOLTAGE_SENSE  0
#define P5V_CURRENT_SENSE  1
#define P5V_SENSE          2
#define PSU_CURRENT_SENSE  3
#define VPOS_SENSE         4
#define VNEG_SENSE         5
#define TEMP_SENSE         6
#define VREF_SENSE         7



extern "C" void adc__inject(uint16_t values[16]);

class ADC: public QObject
{
    Q_OBJECT
    public:
#if 0
    void inject() {
        adc__inject(values);
    }
#endif
    static ADC*create(QWidget*parent) {
        adc = new ADC(parent);
        return adc;
    }

    QLayout *getLayout() { return glayout; }
    static ADC*get() {return adc;}

    void update( uint16_t values[16] );


    QDoubleSpinBox *createVoltageInput(QWidget*parent, double min, double max, double def);
    uint16_t toDigital(double vcc, double voltage);

private:
    ADC(QWidget*);
public:

    void start()
    {
        QObject::connect(&timer, SIGNAL(timeout()),
                         this, SLOT(timerExpired()));

        timer.setSingleShot(true);
        //timer.start(1);
    }
signals:
    void ADCComplete(void);
public slots:
    // This runs on UI thread
    void timerExpired();
    void VPSUWiperChanged(quint8);
    void IPSUWiperChanged(quint8);
    void VPOSWiperChanged(quint8);
    void VNEGWiperChanged(quint8);

    void onPSUVoltageCheckStateChanged(bool);
    void onPSUCurrentCheckStateChanged(int);
    void onP5VVoltageCheckStateChanged(int);
    void onP5VCurrentCheckStateChanged(int);
    void onVPOSCheckStateChanged(int);
    void onVNEGCheckStateChanged(int);

private:
    QTimer timer;
    static ADC *adc;
    QDoubleSpinBox *vcc;
    QDoubleSpinBox *sensors[7];
    bool manual[7];


    QGridLayout *glayout;
};

#endif
