#ifndef __I2CDEVICE_H__
#define __I2CDEVICE_H__

#include <inttypes.h>
#include <QObject>

class I2CDevice: public QObject
{
public:
    virtual int transmit(const uint8_t *pData, uint16_t Size)=0;
    virtual int receive(uint8_t *pData, uint16_t Size)=0;
};

#endif
