#include <inttypes.h>
#include "FrontPanel.h"
#include <QDebug>
#include <string.h>

#define ROTARY_INDEX_TOP_LEFT 3
#define ROTARY_INDEX_TOP_RIGHT 2
#define ROTARY_INDEX_BOTTOM_LEFT 0
#define ROTARY_INDEX_BOTTOM_RIGHT 1

#define BUTTON_ROTARY_TOP_LEFT 6
#define BUTTON_ROTARY_TOP_RIGHT 4
#define BUTTON_ROTARY_BOTTOM_LEFT 0
#define BUTTON_ROTARY_BOTTOM_RIGHT 2

#define BUTTON_LEFT 1
#define BUTTON_RIGHT 3
#define BUTTON_CENTER_LEFT 5
#define BUTTON_CENTER_RIGHT 7

#define OUTPUT_LED_P5V     2
#define OUTPUT_LED_VADJ    1
#define OUTPUT_LED_PSU     0
#define OUTPUT_BUZZER      3


FrontPanel::FrontPanel()
{
    memset(regs,0,sizeof(regs));
}

FrontPanel::~FrontPanel()
{
}

void FrontPanel::updateIndicators(uint8_t newvalue)
{
    uint8_t change = regs[17] ^ newvalue;

    if (change&(1<<OUTPUT_LED_P5V)) {
        qDebug()<<"5V LED changed";
        emit LedP5VChanged( (newvalue & (1<<OUTPUT_LED_P5V)) != 0 );
    }
    if (change&(1<<OUTPUT_LED_VADJ)) {
        qDebug()<<"VADJ LED changed";
        emit LedVADJChanged( (newvalue & (1<<OUTPUT_LED_VADJ)) != 0 );
    }
    if (change&(1<<OUTPUT_LED_PSU)) {
        qDebug()<<"PSU LED changed";
        emit LedPSUChanged( (newvalue & (1<<OUTPUT_LED_PSU)) != 0 );
    }
    if (change&(1<<OUTPUT_BUZZER)) {
        qDebug()<<"Buzzer changed";
        emit BuzzerChanged( (newvalue & (1<<OUTPUT_BUZZER)) != 0 );
    }

    regs[17] = newvalue;

}

int FrontPanel::transmit(const uint8_t *pData, uint16_t Size)
{
    ptr = pData[0];
//    qDebug()<<"FrontPanel: PTR "<<ptr;
    pData++;
    Size--;
    if (Size) {
        while (Size) {
            if (ptr<sizeof(regs)) {
                qDebug()<<"Updating register "<<ptr<<(unsigned)*pData;
                if (ptr==17) {
                    updateIndicators(*pData);
                } else {
                    regs[ptr] = *pData++;
                }
                ptr++;
            }
            Size--;
        }
    }
    return 0;
}

int FrontPanel::receive(uint8_t *pData, uint16_t Size)
{
//    qDebug()<<"FrontPanel: RX size"<<Size;
    while (Size--) {
        *pData++ = regs[ptr];
        if (ptr<sizeof(regs))
            ptr++;
    }
    return 0;
}

void FrontPanel::updateRotary(unsigned index, double v)
{
    unsigned val = v;
    if (val>255)
        val=255;
    regs[index+1] = val;
    regs[index+13]++;
}


void FrontPanel::onRotaryTopLeftChanged(double v)
{
    updateRotary(ROTARY_INDEX_TOP_LEFT,v);
}

void FrontPanel::onRotaryTopLeftButtonChanged(bool v)
{
}

void FrontPanel::onRotaryTopRightChanged(double v)
{
    updateRotary(ROTARY_INDEX_TOP_RIGHT,v);
}

void FrontPanel::onRotaryTopRightButtonChanged(bool v)
{
    
}

void FrontPanel::onRotaryBottomLeftChanged(double v)
{
    updateRotary(ROTARY_INDEX_BOTTOM_LEFT,v);
}

void FrontPanel::onRotaryBottomLeftButtonChanged(bool v)
{
}

void FrontPanel::onRotaryBottomRightChanged(double v)
{
    updateRotary(ROTARY_INDEX_BOTTOM_RIGHT,v);
}

void FrontPanel::onRotaryBottomRightButtonChanged(bool v)
{
}

void FrontPanel::setButton(unsigned button, bool val)
{
    if (val) {
        regs[0] = regs[0] | (0x1<<button);
    } else {
        regs[0] = regs[0] & ~(0x1<<button);
    }
    regs[5+button]++;
    qDebug()<<"Button "<<button<<(val?"set":"cleared");
}

void FrontPanel::onButtonP5VChanged(bool v)
{
    setButton(BUTTON_LEFT,v);
}

void FrontPanel::onButtonVADJChanged(bool v)
{
    setButton(BUTTON_CENTER_LEFT,v);
}

void FrontPanel::onButtonExtraChanged(bool v)
{
    setButton(BUTTON_CENTER_RIGHT,v);
}

void FrontPanel::onButtonPSUChanged(bool v)
{
    setButton(BUTTON_RIGHT,v);

}

