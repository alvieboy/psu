TEMPLATE = app
TARGET = psu

QT = core gui widgets 
CONFIG+=qwt
                         	
SOURCES += psu-qt.cpp MainWindow.cpp RenderArea.cpp QLedIndicator.cpp \
	I2CBus.cpp \
        EEPROM_24C02.cpp \
        MCP45HV.cpp \
        SPIBus.cpp \
        ADC.cpp \
        LCD.cpp \
        PSUThread.cpp \
        FrontPanel.cpp \
	../fixed.c \
        ../lcd.c \
        ../spi.c \
        ../font.c \
        ../i2c.c \
        ../calibration.c \
        ../input.c \
        ../output.c \
        ../gpio.c \
        ../temp.c \
        ../onewire.c \
        ../store.c \
        ../eeprom.c \
        ../ds18b20.c \
        ../panel.c \
        ../screen.c \
        ../psucontrol.c \
        ../screen_main.c \
        ../screen_sensors.c \
        ../screen_calibration.c \
        ../vsnprintf.c \
        ../conversions.c \
        layer/adc.c \
        layer/delay.c \
        layer/crc.c \
        layer/setup.c \
        layer/uart.c \
        layer/usbd.c \
        layer/stm32f1xx_hal_gpio.c \
        layer/stm32f1xx_hal_spi.c \
        layer/stm32f1xx_hal_i2c.c \
        ../main.c 
        


HEADERS += MainWindow.h RenderArea.h QLedIndicator.h PSUThread.h I2CBus.h FrontPanel.h ADC.h MCP45HV.h

INCLUDEPATH+=.. layer/

DEFINES+=TICK_FREQUENCY=100
