#include "RenderArea.h"
#include <QPainter>
#include <QDebug>

#define ZOOMIDX 1
#define ZOOMMUL (1<<ZOOMIDX)

// Temporario...
//#define PIXELHACK 1


#define DISPLAY_WIDTH 320
#define DISPLAY_HEIGHT 240

RenderArea::RenderArea(QWidget *parent): QWidget(parent)
{
//    setBackgroundRole(QPalette::Base);
//    setAutoFillBackground(true);
    image = new QImage(DISPLAY_WIDTH*ZOOMMUL, DISPLAY_HEIGHT*ZOOMMUL, QImage::Format_RGB32 );
}

QSize RenderArea::minimumSizeHint() const
{
    return QSize(DISPLAY_WIDTH*ZOOMMUL, DISPLAY_HEIGHT*ZOOMMUL);
}

QSize RenderArea::sizeHint() const
{
    return QSize(DISPLAY_WIDTH*ZOOMMUL, DISPLAY_HEIGHT*ZOOMMUL);
}

void RenderArea::paintEvent(QPaintEvent *event)
{
#ifdef PIXELHACK
    QRect r(1,0,1+DISPLAY_WIDTH*ZOOMMUL,DISPLAY_HEIGHT*ZOOMMUL);
    QRect sr(0,0,DISPLAY_WIDTH*ZOOMMUL,DISPLAY_HEIGHT*ZOOMMUL);
    QPainter painter(this);
    painter.drawImage(r,*image,sr);
#else
    QRect sr(0,0,DISPLAY_WIDTH*ZOOMMUL,DISPLAY_HEIGHT*ZOOMMUL);
    QPainter painter(this);
    painter.drawImage(sr,*image);
#endif
}

void RenderArea::drawHLine(int x, int y, int width, uint32_t color)
{
    while (width--) {
        drawPixel(x,y,color);
        x++;
    }
}

void RenderArea::drawVLine(int x, int y, int height, uint32_t color)
{
    while (height--) {
        drawPixel(x,y,color);
        y++;
    }
}

void RenderArea::drawImage(int x, int y, QImage *i)
{
    QSize s = i->size();
    x*=ZOOMMUL;
    y*=ZOOMMUL;

    QRect source(0,0,s.width(),s.height());
    QRect dest(x,y,s.width()*ZOOMMUL,s.height()*ZOOMMUL);

    QPainter painter(image);
    painter.drawImage(dest, *i, source);
}

void RenderArea::startFrame()
{
    image->fill(0x0);
}
void RenderArea::finishFrame()
{
    repaint();
}

void RenderArea::drawPixel(int x, int y, uint32_t color)
{
    int sx, sy;
    unsigned dx;

    if (x<0 || x>DISPLAY_WIDTH)
        return;

    if (y<0 || y>DISPLAY_HEIGHT)
        return;

    y*=ZOOMMUL;
    for (sy=0; sy<ZOOMMUL; sy++) {
        dx=x*ZOOMMUL;
        for (sx=0;sx<ZOOMMUL;sx++) {
            image->setPixel(dx, y, color);
            dx++;
        }
        y++;
    }
}

