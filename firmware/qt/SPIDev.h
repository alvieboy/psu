#ifndef __SPIDEV_H__
#define __SPIDEV_H__

#include <inttypes.h>

class SPIDev
{
public:
    virtual void txrx(const uint8_t *txdata, uint8_t *rxdata, unsigned len);
    virtual void selected();
    virtual void deselected();
};


#endif
