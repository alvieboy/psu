#ifndef SPIDEVICE_H__
#define SPIDEVICE_H__

#include <inttypes.h>

class SPIDevice
{
public:
    virtual void select(bool) = 0;
    virtual void txrx(uint8_t mode, const uint8_t *tx, uint8_t *rx, unsigned len) = 0;
};

#endif
