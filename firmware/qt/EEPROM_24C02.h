#ifndef __EEPROM_24C02_H__
#define __EEPROM_24C02_H__

#include "I2CDevice.h"

class EEPROM_24C02: public I2CDevice
{
public:
    EEPROM_24C02(const char *filename);
    virtual ~EEPROM_24C02();
    virtual int transmit(const uint8_t *pData, uint16_t Size);
    virtual int receive(uint8_t *pData, uint16_t Size);
private:
    void saveFile();
    void readFile();
    unsigned ptr;
    int fd;
    uint8_t data[256];
};

#endif
