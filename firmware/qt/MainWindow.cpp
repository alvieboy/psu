#include "MainWindow.h"
#include "RenderArea.h"
#include "QVBoxLayout"
#include "QHBoxLayout"
#include "qwt_knob.h"
#include <QPushButton>
#include "SPIBus.h"


MainWindow::MainWindow(QWidget *parent): QMainWindow(parent, Qt::Window)
{
    renderarea = new RenderArea(this);
    //setCentralWidget(renderarea);


    QWidget * centralWidget = new QWidget(this);
    verticalLayout = new QVBoxLayout(centralWidget);

    verticalLayout->setSpacing(6);
    verticalLayout->setContentsMargins(11, 11, 11, 11);
//    verticalLayout->setObjectName(QString::fromUtf8("verticalLayout"));

    QHBoxLayout *horizLayout = new QHBoxLayout(centralWidget);
    QHBoxLayout *indLayout = new QHBoxLayout(centralWidget);


    QVBoxLayout *leftvt = new QVBoxLayout(centralWidget);
    QVBoxLayout *rightvt = new QVBoxLayout(centralWidget);

    top_left = new QwtKnob(this);
    top_left->setTotalSteps(255);
    top_left->setLowerBound(0);
    top_left->setUpperBound(255);
    top_left->setWrapping(true);
    top_left->setTotalAngle(360);
    QObject::connect(top_left, SIGNAL(valueChanged(double)), this, SLOT(onRotaryTopLeftChanged(double)) );

    top_right = new QwtKnob(this);
    top_right->setTotalSteps(255);
    top_right->setLowerBound(0);
    top_right->setUpperBound(255);
    top_right->setWrapping(true);
    top_right->setTotalAngle(360);
    QObject::connect(top_right, SIGNAL(valueChanged(double)), this, SLOT(onRotaryTopRightChanged(double)) );

    bottom_left = new QwtKnob(this);
    bottom_left->setTotalSteps(255);
    bottom_left->setLowerBound(0);
    bottom_left->setUpperBound(255);
    bottom_left->setWrapping(true);
    bottom_left->setTotalAngle(360);
    QObject::connect(bottom_left, SIGNAL(valueChanged(double)), this, SLOT(onRotaryBottomLeftChanged(double)) );

    bottom_right = new QwtKnob(this);
    bottom_right->setTotalSteps(255);
    bottom_right->setLowerBound(0);
    bottom_right->setUpperBound(255);
    bottom_right->setWrapping(true);
    bottom_right->setTotalAngle(360);
    QObject::connect(bottom_right, SIGNAL(valueChanged(double)), this, SLOT(onRotaryBottomRightChanged(double)) );

    leftvt->addWidget( top_left );
    leftvt->addWidget( bottom_left );

    rightvt->addWidget( top_right );
    rightvt->addWidget( bottom_right );

    horizLayout->addLayout(leftvt);
    horizLayout->addWidget(renderarea);
    horizLayout->addLayout(rightvt);

    leds[0] = new QLedIndicator(this);
    indLayout->addWidget( leds[0] );
    QPushButton *push = new QPushButton(this);
    //push->setCheckable(true);
    QObject::connect(push, SIGNAL(pressed()), this, SLOT(onButton1Pressed()) );
    QObject::connect(push, SIGNAL(released()), this, SLOT(onButton1Released()) );
    indLayout->addWidget(push);


    leds[1] = new QLedIndicator(this);
    indLayout->addWidget( leds[1] );
    push = new QPushButton(this);
    QObject::connect(push, SIGNAL(pressed()), this, SLOT(onButton2Pressed()) );
    QObject::connect(push, SIGNAL(released()), this, SLOT(onButton2Released()) );
    indLayout->addWidget(push);

    push = new QPushButton(this);
    QObject::connect(push, SIGNAL(pressed()), this, SLOT(onButton3Pressed()) );
    QObject::connect(push, SIGNAL(released()), this, SLOT(onButton3Released()) );
    indLayout->addWidget(push);

    leds[2] = new QLedIndicator(this);
    indLayout->addWidget( leds[2] );
    push = new QPushButton(this);
    QObject::connect(push, SIGNAL(pressed()), this, SLOT(onButton4Pressed()) );
    QObject::connect(push, SIGNAL(released()), this, SLOT(onButton4Released()) );
    indLayout->addWidget(push);

    verticalLayout->addLayout(horizLayout);
    verticalLayout->addLayout(indLayout);


    setCentralWidget(centralWidget);

    QObject::connect(&timer, SIGNAL(timeout()),
                     this, SLOT(timerExpired()));

    timer.setSingleShot(false);
    timer.start(20);

    //retranslateUi(this);
    show();
}

void MainWindow::appendLayout(QLayout*l)
{
    verticalLayout->addLayout(l);
}


void MainWindow::timerExpired()
{
    renderarea->finishFrame();
}
void MainWindow::onRotaryTopLeftChanged(double v)
{        
    emit rotaryTopLeftChanged(v);
}
void MainWindow::onRotaryTopLeftButtonChanged(bool v)
{
    emit rotaryTopLeftButtonChanged(v);
}
void MainWindow::onRotaryTopRightChanged(double v)
{
    emit rotaryTopRightChanged(v);
}
void MainWindow::onRotaryTopRightButtonChanged(bool v)
{
    emit rotaryTopRightButtonChanged(v);
}
void MainWindow::onRotaryBottomLeftChanged(double v)
{
    emit rotaryBottomLeftChanged(v);
}
void MainWindow::onRotaryBottomLeftButtonChanged(bool v)
{
    emit rotaryBottomLeftButtonChanged(v);
}
void MainWindow::onRotaryBottomRightChanged(double v)
{
    emit rotaryBottomRightChanged(v);
}
void MainWindow::onRotaryBottomRightButtonChanged(bool v)
{
    emit rotaryBottomRightButtonChanged(v);
}

void MainWindow::onButton1Pressed()
{
    emit button1Changed(true);
}
void MainWindow::onButton1Released()
{
    emit button1Changed(false);
}
void MainWindow::onButton2Pressed()
{
    emit button2Changed(true);
}
void MainWindow::onButton2Released()
{
    emit button2Changed(false);
}

void MainWindow::onButton3Pressed()
{
    emit button3Changed(true);
}
void MainWindow::onButton3Released()
{
    emit button3Changed(false);
}

void MainWindow::onButton4Pressed()
{
    emit button4Changed(true);
}
void MainWindow::onButton4Released()
{
    emit button4Changed(false);
}

void MainWindow::onLed1Changed(bool v)
{
    qDebug()<<"Led 1 is now "<<v;
    leds[0]->setChecked(v);
}
void MainWindow::onLed2Changed(bool v)
{
    qDebug()<<"Led 2 is now "<<v;
    leds[1]->setChecked(v);
}
void MainWindow::onLed3Changed(bool v)
{
    qDebug()<<"Led 3 is now "<<v;
    leds[2]->setChecked(v);

}
void MainWindow::onBuzzerChanged(bool)
{
    // Ignore
}


MainWindow::~MainWindow()
{

}

