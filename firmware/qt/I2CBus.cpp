#include "I2CBus.h"
#include <QDebug>

I2CBus::I2CBus(I2C_TypeDef *i2c_inst): i2c(i2c_inst)
{
    hal_i2c__registerhandler(i2c,
                             &wrap_transmit,
                             &wrap_receive,
                             this);
}

int I2CBus::wrap_transmit(void *data, uint16_t DevAddress, const uint8_t *pData, uint16_t Size)
{
    return static_cast<I2CBus*>(data)->transmit(DevAddress,pData,Size);
}
int I2CBus::wrap_receive(void *data, uint16_t DevAddress, uint8_t *pData, uint16_t Size)
{
    return static_cast<I2CBus*>(data)->receive(DevAddress,pData,Size);
}

int I2CBus::transmit(uint16_t DevAddress, const uint8_t *pData, uint16_t Size)
{
    DevAddress &= 0xFE;
    //qDebug()<<"I2CBUS: TX to address "<<DevAddress;

    if (devices.find(DevAddress)!=devices.end()) {
        return devices[DevAddress]->transmit(pData,Size);
    }
    return -1;
}

int I2CBus::receive(uint16_t DevAddress, uint8_t *pData, uint16_t Size)
{
    DevAddress &= 0xFE;
    if (devices.find(DevAddress)!=devices.end()) {
        return devices[DevAddress]->receive(pData,Size);
    }
    return -1;
}

void I2CBus::attach(uint16_t address, I2CDevice*dev)
{
    devices[address] = dev;
}
