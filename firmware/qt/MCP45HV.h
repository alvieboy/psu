#ifndef __MCP45HV_H__
#define __MCP45HV_H__

#include "I2CDevice.h"

class MCP45HV: public I2CDevice
{
    Q_OBJECT
public:
    MCP45HV();
    virtual ~MCP45HV();
    virtual int transmit(const uint8_t *pData, uint16_t Size);
    virtual int receive(uint8_t *pData, uint16_t Size);
signals:
    void changed(quint8);
private:
};

#endif
