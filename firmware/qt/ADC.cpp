#include "ADC.h"
#include <QLabel>
#include <QSpinBox>
#include <QDoubleSpinBox>
#include <math.h>
#include <QDebug>
#include <QCheckBox>
ADC *ADC::adc;

void ADC::timerExpired()
{
//    printf("ADC complete\n");
    emit ADCComplete();
}

QDoubleSpinBox *ADC::createVoltageInput(QWidget*parent, double min, double max, double def)
{
    QDoubleSpinBox *s = new QDoubleSpinBox(parent);
    s->setMaximum(max);
    s->setMinimum(min);
    s->setValue(def);
    s->setDecimals(3);
    s->setSingleStep(0.05);
    s->setSuffix("V");
    return s;
}

ADC::ADC(QWidget*parent)
{
    glayout = new QGridLayout(parent);
    glayout->addWidget(new QLabel("Internal VCC:"), 0, 0);
    vcc = new QDoubleSpinBox(parent);
    vcc->setMaximum(3.6);
    vcc->setMinimum(2.0);
    vcc->setValue(3.3);
    vcc->setDecimals(2);
    vcc->setSingleStep(0.01);
    vcc->setSuffix("V");

    // Row 0
    glayout->addWidget(vcc, 0, 1);

    glayout->addWidget(new QLabel("Temp sensor:"), 0, 2);
    sensors[TEMP_SENSE] = createVoltageInput(parent, 0.0, 3.3, 0.09);
    glayout->addWidget(sensors[TEMP_SENSE], 0, 3);

    // Row 1
    glayout->addWidget(new QLabel("PSU voltage:"), 1, 0);
    sensors[PSU_VOLTAGE_SENSE] = createVoltageInput(parent, 0.0, 3.3, 1.00);
    glayout->addWidget(sensors[PSU_VOLTAGE_SENSE], 1, 1);
    sensors[PSU_VOLTAGE_SENSE]->setEnabled(false);
    QCheckBox *check = new QCheckBox("Manual", parent);

    QObject::connect(check, SIGNAL(toggled(bool)), this, SLOT(onPSUVoltageCheckStateChanged(bool)));

    glayout->addWidget( check, 1, 2);


    glayout->addWidget(new QLabel("PSU current:"), 1, 3);
    sensors[PSU_CURRENT_SENSE] = createVoltageInput(parent, 0.0, 3.3, 0.00);
    glayout->addWidget(sensors[PSU_CURRENT_SENSE], 1, 4);
    sensors[PSU_CURRENT_SENSE]->setEnabled(false);
    check = new QCheckBox("Manual", parent);
    QObject::connect(check, SIGNAL(stateChanged(int)), this, SLOT(onPSUCurrentCheckStateChanged(int)));
    glayout->addWidget( check, 1, 5);


    // Row 2
    glayout->addWidget(new QLabel("5V voltage:"), 2, 0);
    sensors[P5V_SENSE] = createVoltageInput(parent, 0.0, 3.3, 3.3/2.0);
    glayout->addWidget(sensors[P5V_SENSE], 2, 1);
    sensors[P5V_SENSE]->setEnabled(false);
    check = new QCheckBox("Manual", parent);
    QObject::connect(check, SIGNAL(stateChanged(int)), this, SLOT(onP5VVoltageCheckStateChanged(int)));
    glayout->addWidget( check, 2, 2);

    glayout->addWidget(new QLabel("5V current:"), 2, 3);;
    sensors[P5V_CURRENT_SENSE] = createVoltageInput(parent, 0.0, 3.3, 0.0);
    glayout->addWidget(sensors[P5V_CURRENT_SENSE], 2, 4);
    sensors[P5V_CURRENT_SENSE]->setEnabled(false);
    check = new QCheckBox("Manual", parent);
    QObject::connect(check, SIGNAL(stateChanged(int)), this, SLOT(onP5VCurrentCheckStateChanged(int)));
    glayout->addWidget( check, 2, 5);

    // Row 3
    glayout->addWidget(new QLabel("POS voltage:"), 3, 0);
    sensors[VPOS_SENSE] = createVoltageInput(parent, 0.0, 3.3, 3.3/2.0);
    glayout->addWidget(sensors[VPOS_SENSE], 3, 1);
    sensors[VPOS_SENSE]->setEnabled(false);
    check = new QCheckBox("Manual", parent);
    QObject::connect(check, SIGNAL(stateChanged(int)), this, SLOT(onVPOSCheckStateChanged(int)));
    glayout->addWidget( check, 3, 2);

    glayout->addWidget(new QLabel("NEG voltage:"), 3, 3);
    sensors[VNEG_SENSE] = createVoltageInput(parent, 0.0, 3.3, 3.3/2.0);
    glayout->addWidget(sensors[VNEG_SENSE], 3, 4);
    sensors[VNEG_SENSE]->setEnabled(false);
    QObject::connect(check, SIGNAL(stateChanged(int)), this, SLOT(onVNEGCheckStateChanged(int)));
    check = new QCheckBox("Manual", parent);
    glayout->addWidget( check, 3, 5);

}


void ADC::onPSUVoltageCheckStateChanged(bool v)
{
    qDebug()<<"State changed"<<v;
    sensors[PSU_VOLTAGE_SENSE]->setEnabled( v );
}
void ADC::onPSUCurrentCheckStateChanged(int v)
{
}
void ADC::onP5VVoltageCheckStateChanged(int v)
{
}
void ADC::onP5VCurrentCheckStateChanged(int v)
{
}
void ADC::onVPOSCheckStateChanged(int v)
{
}
void ADC::onVNEGCheckStateChanged(int v)
{
}

uint16_t ADC::toDigital(double vcc, double voltage)
{
    double v = (4096.0/vcc)*voltage;
    unsigned vu = round(v);
    if (vu>4096)
        vu=4096;
    return vu;
}

void ADC::update(uint16_t values[16])
{
    double v = vcc->value();
    unsigned i;
    for (i=0;i<7;i++) {
        if (sensors[i]) {
            values[i] = toDigital(v, sensors[i]->value());
        } else {
            values[i] = 0;
        }
    }
    values[7] = toDigital(v, 1.2);
#if 0
    qDebug()<<"ADC values: ";
    for (i=0;i<7;i++) {
        qDebug()<<i<<": "<<(unsigned)values[i];
    }
#endif
    // Compute
}

extern "C" void adc__start(void)
{
    uint16_t values[16] = {0};
    ADC::get()->update(values);//get()->start();
    adc__inject(values);
}


void ADC::VPSUWiperChanged(quint8 v)
{
    double vadc = (double)(255-v);
    vadc *= 0.01385;
    vadc += 0.185;
    qDebug()<<"VPSU wiper"<<v<<vadc;
    sensors[PSU_VOLTAGE_SENSE]->setValue( vadc );

}

void ADC::IPSUWiperChanged(quint8 v)
{
    qDebug()<<"IPSU wiper"<<v;
}

void ADC::VPOSWiperChanged(quint8 v)
{
    qDebug()<<"VPOS wiper"<<v;
}
void ADC::VNEGWiperChanged(quint8 v)
{
    qDebug()<<"VNEG wiper"<<v;

}
