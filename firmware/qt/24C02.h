#ifndef __EEPROM_24C02_H__
#define __EEPROM_24C02_H__

class EEPROM_24C02: public I2CDevice
{
public:
    virtual int transmit(const uint8_t *pData, uint16_t Size)=0;
    virtual int receive(uint8_t *pData, uint16_t Size)=0;
};

#endif
