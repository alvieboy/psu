#ifndef __FRONTPANEL_H__
#define __FRONTPANEL_H__

#include "I2CDevice.h"

class FrontPanel: public I2CDevice
{
public:
    FrontPanel();
    virtual ~FrontPanel();
    virtual int transmit(const uint8_t *pData, uint16_t Size);
    virtual int receive(uint8_t *pData, uint16_t Size);
private:
    uint8_t regs[18];
};

#endif
