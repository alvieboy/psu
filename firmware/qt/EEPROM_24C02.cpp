#include <inttypes.h>
#include <unistd.h>
#include <stdlib.h>
#include <fcntl.h>

#include "EEPROM_24C02.h"
#include <QDebug>


EEPROM_24C02::EEPROM_24C02(const char *filename)
{
    fd = open(filename, O_RDWR|O_CREAT|O_TRUNC, 0666);
    if (fd<0) {
        qDebug()<<"WARNING: cannot open file";
    }
    readFile();
}

EEPROM_24C02::~EEPROM_24C02()
{
    if (fd>=0)
        close(fd);
}

int EEPROM_24C02::transmit(const uint8_t *pData, uint16_t Size)
{
    qDebug()<<"24C02: TX size"<<Size;
    ptr = pData[0];
    pData++;
    Size--;
    if (Size) {
        while (Size) {
            if (ptr<sizeof(data)) {
                data[ptr++] = *pData++;
            }
            Size--;
        }
        saveFile();
    }
    return 0;
}

int EEPROM_24C02::receive(uint8_t *pData, uint16_t Size)
{
    qDebug()<<"24C02: RX size"<<Size;
    while (Size--) {
        *pData++ = data[ptr];
        if (ptr<512)
            ptr++;
    }
    return 0;
}


void EEPROM_24C02::saveFile()
{
    if (fd>=0) {
        lseek(fd,0,SEEK_SET);
        write(fd, data, sizeof(data));
    }
}
void EEPROM_24C02::readFile()
{
    if (fd>=0) {
        ftruncate(fd, sizeof(data));
        lseek(fd,0,SEEK_SET);
        read(fd, data, sizeof(data));
    }
}

