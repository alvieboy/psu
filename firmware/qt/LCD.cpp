#include "LCD.h"
#include <QDebug>
#include "dev_gpio.h"
#include <QThread>
void LCD::select(bool s)
{
    //qDebug()<<"LCD SELECT "<<s;
    selected = s;
}

void LCD::txrx(uint8_t mode, const uint8_t *tx, uint8_t *rx, unsigned len)
{
    if (!selected)
        return;
    if (dc==0) {
        //qDebug()<<"LCD Command len "<<len<<" dc "<<dc<<"mode"<<mode;
        command = tx[0];
        offset=0;
    } else {
        // Data
//        qDebug()<<"LCD Data len "<<len<<" offset "<<offset<<"mode"<<mode<<"command"<<command;
        switch(command) {
        case 0x2A:
  //          qDebug()<<"Column data";
            regdata(col, offset, tx, len);
            ccol = getStartCol();
            showLimits();
            break;
        case 0x2B:
    //        qDebug()<<"Page data";
            regdata(page, offset, tx, len);
            cpage = getStartPage();
            showLimits();
            break;
        case 0x2C:
      //      qDebug()<<"Pixel data";
            putPixel(((unsigned)tx[0]<<8) + tx[1]);
            break;
        }
        offset++;
    }
}

void LCD::showLimits()
{
#if 0
    qDebug()<<QThread::currentThreadId()<<"Limits: page "<<getStartPage()<<" to "<<getEndPage();
    qDebug()<<"         col "<<getStartCol()<<" to "<<getEndCol();
#endif

}

void LCD::putPixel(uint16_t color)
{
//    qDebug()<<QThread::currentThreadId()<<"PUT PIXEL col" << ccol <<"page"<<cpage<<"color"<<color;

    if (cpage>getEndPage()) {
        return;
    }
#define COLOR_RGB(r,g,b) ( (b>>3) | (g>>2) << 5 | (r>>3) << 11 )
    unsigned r,g,b;
    // 565
    b = (color & 0x1F) << 3;
    g = ((color>>5) & 0x3F) << 2;
    r = ((color>>11) & 0x1F) << 3;

    drawer->drawPixel(ccol, cpage, b + (g<<8) + (r<<16));
    ccol++;
    if (ccol>getEndCol()) {
        ccol = getStartCol();
        cpage++;
    }
}


template<typename T>
void LCD::regdata( T&t, unsigned off, const uint8_t*data, unsigned len)
{
    while (len && off < sizeof(t)) {
      //  qDebug()<<"Off "<<off<<"data "<<*data;
        t[off] = *data++;
        len--;
        off++;
    }
}


void LCD::attachDC(GPIO_TypeDef *d, unsigned pin)
{
    dev_gpio__registerhandler(d,
                              pin,
                              wrap_dc_changed,
                              wrap_dc_read,
                              this);

}

void LCD::wrap_dc_changed(void*data, uint8_t val)
{
    static_cast<LCD*>(data)->dc_changed(val);
}

uint8_t LCD::wrap_dc_read(void*data)
{
    return 0;
}

void LCD::dc_changed(uint8_t v)
{
    dc = !!v;
}
