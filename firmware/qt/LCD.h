#ifndef __LCD_H__
#define __LCD_H__

#include <cstddef>

#include "SPIDevice.h"
#include "stm32f1xx_hal_gpio.h"
#include "ScreenDrawer.h"

class LCD: public SPIDevice
{
public:
    LCD() { drawer=NULL;}
    void attachDC(GPIO_TypeDef *d, unsigned pin);
    void attachDrawer(ScreenDrawer*d) { drawer=d; }
    void select(bool);
    void txrx(uint8_t mode, const uint8_t *tx, uint8_t *rx, unsigned len);

    static void wrap_dc_changed(void*, uint8_t);
    static uint8_t wrap_dc_read(void*);
    void dc_changed(uint8_t);
    uint8_t dc;
    bool selected;
    ScreenDrawer *drawer;
    uint8_t command;
    uint16_t offset;
    uint8_t cmdbuffer[8];
    uint8_t col[4];
    uint8_t page[4];
    uint16_t ccol, cpage;

    unsigned getStartCol() const { return ((unsigned)col[0]<<8) + (unsigned)col[1]; }
    unsigned getEndCol() const   { return ((unsigned)col[2]<<8) + (unsigned)col[3]; }

    unsigned getStartPage() const { return ((unsigned)page[0]<<8) + (unsigned)page[1]; }
    unsigned getEndPage() const   { return ((unsigned)page[2]<<8) + (unsigned)page[3]; }

    template<typename T>
        void regdata( T&, unsigned off, const uint8_t*data, unsigned len);
    void showLimits();
    void putPixel(uint16_t);
};

#endif
