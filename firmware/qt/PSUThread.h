#ifndef __PSUTHREAD_H__
#define __PSUTHREAD_H__

#include <QThread>
#include <QTimer>
#include <QMutex>
#include "ADC.h"
#include <QDebug>

extern "C"  {
    void psu_main(void);
}

extern "C" void tick__handler(void);

class PSUThread : public QThread
{
    Q_OBJECT
private:
    PSUThread()
    {
        ticks = 0;
    }
public:
    static PSUThread *create() {
        thr = new PSUThread();
        return thr;
    }
    static PSUThread *get() { return thr; }
    static PSUThread *thr;
    void run() override {
        //2QObject::connect(adc,SIGNAL(ADCComplete()), this, SLOT(ADCComplete()));

        QObject::connect(&timer, SIGNAL(timeout()),
                         this, SLOT(timerExpired()));

        timer.setSingleShot(false);
        timer.start(10);

        psu_main();
    }
public slots:
    void timerExpired()
    {
        mutex.lock();
        ticks++;
        mutex.unlock();
    }
    void process_events()
    {
        bool t = false;
        mutex.lock();
        if (ticks) {
            ticks--;
            t=true;
        }
        mutex.unlock();
        if (t)
            tick__handler();
    }
private:
    QTimer timer;
    QMutex mutex;
    volatile unsigned ticks;
};

#endif
