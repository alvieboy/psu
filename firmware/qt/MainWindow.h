#include <QMainWindow>
#include <QImage>
#include <QLabel>
#include <QVBoxLayout>
#include "RenderArea.h"
#include "qwt_knob.h"
#include "qledindicator.h"
#include <QTimer>
class MainWindow: public QMainWindow
{
    Q_OBJECT
public:
    explicit MainWindow(QWidget *parent = 0);
    virtual ~MainWindow();
    ScreenDrawer *getDrawer()  { return renderarea; }
    void appendLayout(QLayout*);
public slots:
    void timerExpired();
    void onLed1Changed(bool);
    void onLed2Changed(bool);
    void onLed3Changed(bool);
    void onBuzzerChanged(bool);
    void onRotaryTopLeftChanged(double);
    void onRotaryTopLeftButtonChanged(bool);
    void onRotaryTopRightChanged(double);
    void onRotaryTopRightButtonChanged(bool);
    void onRotaryBottomLeftChanged(double);
    void onRotaryBottomLeftButtonChanged(bool);
    void onRotaryBottomRightChanged(double);
    void onRotaryBottomRightButtonChanged(bool);
    void onButton1Pressed();
    void onButton1Released();
    void onButton2Pressed();
    void onButton2Released();
    void onButton3Pressed();
    void onButton3Released();
    void onButton4Pressed();
    void onButton4Released();

signals:
    void rotaryTopLeftChanged(double);
    void rotaryTopLeftButtonChanged(bool);
    void rotaryTopRightChanged(double);
    void rotaryTopRightButtonChanged(bool);
    void rotaryBottomLeftChanged(double);
    void rotaryBottomLeftButtonChanged(bool);
    void rotaryBottomRightChanged(double);
    void rotaryBottomRightButtonChanged(bool);
    void button1Changed(bool);
    void button2Changed(bool);
    void button3Changed(bool);
    void button4Changed(bool);



protected:
    RenderArea *renderarea;
    QwtKnob *top_left;
    QwtKnob *top_right;
    QwtKnob *bottom_left;
    QwtKnob *bottom_right;
    QLedIndicator *leds[3];
    QVBoxLayout *verticalLayout;
    QTimer timer;
};
