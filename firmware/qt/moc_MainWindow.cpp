/****************************************************************************
** Meta object code from reading C++ file 'MainWindow.h'
**
** Created by: The Qt Meta Object Compiler version 63 (Qt 4.8.7)
**
** WARNING! All changes made in this file will be lost!
*****************************************************************************/

#include "MainWindow.h"
#if !defined(Q_MOC_OUTPUT_REVISION)
#error "The header file 'MainWindow.h' doesn't include <QObject>."
#elif Q_MOC_OUTPUT_REVISION != 63
#error "This file was generated using the moc from 4.8.7. It"
#error "cannot be used with the include files from this version of Qt."
#error "(The moc has changed too much.)"
#endif

QT_BEGIN_MOC_NAMESPACE
static const uint qt_meta_data_MainWindow[] = {

 // content:
       6,       // revision
       0,       // classname
       0,    0, // classinfo
      33,   14, // methods
       0,    0, // properties
       0,    0, // enums/sets
       0,    0, // constructors
       0,       // flags
      12,       // signalCount

 // signals: signature, parameters, type, tag, flags
      12,   11,   11,   11, 0x05,
      41,   11,   11,   11, 0x05,
      74,   11,   11,   11, 0x05,
     104,   11,   11,   11, 0x05,
     138,   11,   11,   11, 0x05,
     170,   11,   11,   11, 0x05,
     206,   11,   11,   11, 0x05,
     239,   11,   11,   11, 0x05,
     276,   11,   11,   11, 0x05,
     297,   11,   11,   11, 0x05,
     318,   11,   11,   11, 0x05,
     339,   11,   11,   11, 0x05,

 // slots: signature, parameters, type, tag, flags
     360,   11,   11,   11, 0x0a,
     375,   11,   11,   11, 0x0a,
     395,   11,   11,   11, 0x0a,
     415,   11,   11,   11, 0x0a,
     435,   11,   11,   11, 0x0a,
     457,   11,   11,   11, 0x0a,
     488,   11,   11,   11, 0x0a,
     523,   11,   11,   11, 0x0a,
     555,   11,   11,   11, 0x0a,
     591,   11,   11,   11, 0x0a,
     625,   11,   11,   11, 0x0a,
     663,   11,   11,   11, 0x0a,
     698,   11,   11,   11, 0x0a,
     737,   11,   11,   11, 0x0a,
     756,   11,   11,   11, 0x0a,
     776,   11,   11,   11, 0x0a,
     795,   11,   11,   11, 0x0a,
     815,   11,   11,   11, 0x0a,
     834,   11,   11,   11, 0x0a,
     854,   11,   11,   11, 0x0a,
     873,   11,   11,   11, 0x0a,

       0        // eod
};

static const char qt_meta_stringdata_MainWindow[] = {
    "MainWindow\0\0rotaryTopLeftChanged(double)\0"
    "rotaryTopLeftButtonChanged(bool)\0"
    "rotaryTopRightChanged(double)\0"
    "rotaryTopRightButtonChanged(bool)\0"
    "rotaryBottomLeftChanged(double)\0"
    "rotaryBottomLeftButtonChanged(bool)\0"
    "rotaryBottomRightChanged(double)\0"
    "rotaryBottomRightButtonChanged(bool)\0"
    "button1Changed(bool)\0button2Changed(bool)\0"
    "button3Changed(bool)\0button4Changed(bool)\0"
    "timerExpired()\0onLed1Changed(bool)\0"
    "onLed2Changed(bool)\0onLed3Changed(bool)\0"
    "onBuzzerChanged(bool)\0"
    "onRotaryTopLeftChanged(double)\0"
    "onRotaryTopLeftButtonChanged(bool)\0"
    "onRotaryTopRightChanged(double)\0"
    "onRotaryTopRightButtonChanged(bool)\0"
    "onRotaryBottomLeftChanged(double)\0"
    "onRotaryBottomLeftButtonChanged(bool)\0"
    "onRotaryBottomRightChanged(double)\0"
    "onRotaryBottomRightButtonChanged(bool)\0"
    "onButton1Pressed()\0onButton1Released()\0"
    "onButton2Pressed()\0onButton2Released()\0"
    "onButton3Pressed()\0onButton3Released()\0"
    "onButton4Pressed()\0onButton4Released()\0"
};

void MainWindow::qt_static_metacall(QObject *_o, QMetaObject::Call _c, int _id, void **_a)
{
    if (_c == QMetaObject::InvokeMetaMethod) {
        Q_ASSERT(staticMetaObject.cast(_o));
        MainWindow *_t = static_cast<MainWindow *>(_o);
        switch (_id) {
        case 0: _t->rotaryTopLeftChanged((*reinterpret_cast< double(*)>(_a[1]))); break;
        case 1: _t->rotaryTopLeftButtonChanged((*reinterpret_cast< bool(*)>(_a[1]))); break;
        case 2: _t->rotaryTopRightChanged((*reinterpret_cast< double(*)>(_a[1]))); break;
        case 3: _t->rotaryTopRightButtonChanged((*reinterpret_cast< bool(*)>(_a[1]))); break;
        case 4: _t->rotaryBottomLeftChanged((*reinterpret_cast< double(*)>(_a[1]))); break;
        case 5: _t->rotaryBottomLeftButtonChanged((*reinterpret_cast< bool(*)>(_a[1]))); break;
        case 6: _t->rotaryBottomRightChanged((*reinterpret_cast< double(*)>(_a[1]))); break;
        case 7: _t->rotaryBottomRightButtonChanged((*reinterpret_cast< bool(*)>(_a[1]))); break;
        case 8: _t->button1Changed((*reinterpret_cast< bool(*)>(_a[1]))); break;
        case 9: _t->button2Changed((*reinterpret_cast< bool(*)>(_a[1]))); break;
        case 10: _t->button3Changed((*reinterpret_cast< bool(*)>(_a[1]))); break;
        case 11: _t->button4Changed((*reinterpret_cast< bool(*)>(_a[1]))); break;
        case 12: _t->timerExpired(); break;
        case 13: _t->onLed1Changed((*reinterpret_cast< bool(*)>(_a[1]))); break;
        case 14: _t->onLed2Changed((*reinterpret_cast< bool(*)>(_a[1]))); break;
        case 15: _t->onLed3Changed((*reinterpret_cast< bool(*)>(_a[1]))); break;
        case 16: _t->onBuzzerChanged((*reinterpret_cast< bool(*)>(_a[1]))); break;
        case 17: _t->onRotaryTopLeftChanged((*reinterpret_cast< double(*)>(_a[1]))); break;
        case 18: _t->onRotaryTopLeftButtonChanged((*reinterpret_cast< bool(*)>(_a[1]))); break;
        case 19: _t->onRotaryTopRightChanged((*reinterpret_cast< double(*)>(_a[1]))); break;
        case 20: _t->onRotaryTopRightButtonChanged((*reinterpret_cast< bool(*)>(_a[1]))); break;
        case 21: _t->onRotaryBottomLeftChanged((*reinterpret_cast< double(*)>(_a[1]))); break;
        case 22: _t->onRotaryBottomLeftButtonChanged((*reinterpret_cast< bool(*)>(_a[1]))); break;
        case 23: _t->onRotaryBottomRightChanged((*reinterpret_cast< double(*)>(_a[1]))); break;
        case 24: _t->onRotaryBottomRightButtonChanged((*reinterpret_cast< bool(*)>(_a[1]))); break;
        case 25: _t->onButton1Pressed(); break;
        case 26: _t->onButton1Released(); break;
        case 27: _t->onButton2Pressed(); break;
        case 28: _t->onButton2Released(); break;
        case 29: _t->onButton3Pressed(); break;
        case 30: _t->onButton3Released(); break;
        case 31: _t->onButton4Pressed(); break;
        case 32: _t->onButton4Released(); break;
        default: ;
        }
    }
}

const QMetaObjectExtraData MainWindow::staticMetaObjectExtraData = {
    0,  qt_static_metacall 
};

const QMetaObject MainWindow::staticMetaObject = {
    { &QMainWindow::staticMetaObject, qt_meta_stringdata_MainWindow,
      qt_meta_data_MainWindow, &staticMetaObjectExtraData }
};

#ifdef Q_NO_DATA_RELOCATION
const QMetaObject &MainWindow::getStaticMetaObject() { return staticMetaObject; }
#endif //Q_NO_DATA_RELOCATION

const QMetaObject *MainWindow::metaObject() const
{
    return QObject::d_ptr->metaObject ? QObject::d_ptr->metaObject : &staticMetaObject;
}

void *MainWindow::qt_metacast(const char *_clname)
{
    if (!_clname) return 0;
    if (!strcmp(_clname, qt_meta_stringdata_MainWindow))
        return static_cast<void*>(const_cast< MainWindow*>(this));
    return QMainWindow::qt_metacast(_clname);
}

int MainWindow::qt_metacall(QMetaObject::Call _c, int _id, void **_a)
{
    _id = QMainWindow::qt_metacall(_c, _id, _a);
    if (_id < 0)
        return _id;
    if (_c == QMetaObject::InvokeMetaMethod) {
        if (_id < 33)
            qt_static_metacall(this, _c, _id, _a);
        _id -= 33;
    }
    return _id;
}

// SIGNAL 0
void MainWindow::rotaryTopLeftChanged(double _t1)
{
    void *_a[] = { 0, const_cast<void*>(reinterpret_cast<const void*>(&_t1)) };
    QMetaObject::activate(this, &staticMetaObject, 0, _a);
}

// SIGNAL 1
void MainWindow::rotaryTopLeftButtonChanged(bool _t1)
{
    void *_a[] = { 0, const_cast<void*>(reinterpret_cast<const void*>(&_t1)) };
    QMetaObject::activate(this, &staticMetaObject, 1, _a);
}

// SIGNAL 2
void MainWindow::rotaryTopRightChanged(double _t1)
{
    void *_a[] = { 0, const_cast<void*>(reinterpret_cast<const void*>(&_t1)) };
    QMetaObject::activate(this, &staticMetaObject, 2, _a);
}

// SIGNAL 3
void MainWindow::rotaryTopRightButtonChanged(bool _t1)
{
    void *_a[] = { 0, const_cast<void*>(reinterpret_cast<const void*>(&_t1)) };
    QMetaObject::activate(this, &staticMetaObject, 3, _a);
}

// SIGNAL 4
void MainWindow::rotaryBottomLeftChanged(double _t1)
{
    void *_a[] = { 0, const_cast<void*>(reinterpret_cast<const void*>(&_t1)) };
    QMetaObject::activate(this, &staticMetaObject, 4, _a);
}

// SIGNAL 5
void MainWindow::rotaryBottomLeftButtonChanged(bool _t1)
{
    void *_a[] = { 0, const_cast<void*>(reinterpret_cast<const void*>(&_t1)) };
    QMetaObject::activate(this, &staticMetaObject, 5, _a);
}

// SIGNAL 6
void MainWindow::rotaryBottomRightChanged(double _t1)
{
    void *_a[] = { 0, const_cast<void*>(reinterpret_cast<const void*>(&_t1)) };
    QMetaObject::activate(this, &staticMetaObject, 6, _a);
}

// SIGNAL 7
void MainWindow::rotaryBottomRightButtonChanged(bool _t1)
{
    void *_a[] = { 0, const_cast<void*>(reinterpret_cast<const void*>(&_t1)) };
    QMetaObject::activate(this, &staticMetaObject, 7, _a);
}

// SIGNAL 8
void MainWindow::button1Changed(bool _t1)
{
    void *_a[] = { 0, const_cast<void*>(reinterpret_cast<const void*>(&_t1)) };
    QMetaObject::activate(this, &staticMetaObject, 8, _a);
}

// SIGNAL 9
void MainWindow::button2Changed(bool _t1)
{
    void *_a[] = { 0, const_cast<void*>(reinterpret_cast<const void*>(&_t1)) };
    QMetaObject::activate(this, &staticMetaObject, 9, _a);
}

// SIGNAL 10
void MainWindow::button3Changed(bool _t1)
{
    void *_a[] = { 0, const_cast<void*>(reinterpret_cast<const void*>(&_t1)) };
    QMetaObject::activate(this, &staticMetaObject, 10, _a);
}

// SIGNAL 11
void MainWindow::button4Changed(bool _t1)
{
    void *_a[] = { 0, const_cast<void*>(reinterpret_cast<const void*>(&_t1)) };
    QMetaObject::activate(this, &staticMetaObject, 11, _a);
}
QT_END_MOC_NAMESPACE
