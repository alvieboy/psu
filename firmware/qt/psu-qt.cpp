#include <QApplication>
#include "MainWindow.h"
#include "PSUThread.h"
#include "I2CBus.h"
#include "SPIBus.h"
#include "EEPROM_24C02.h"
#include "LCD.h"
#include "FrontPanel.h"
#include "MCP45HV.h"

int main(int argc, char **argv)
{
    QApplication app (argc, argv);
    MainWindow win;

    ADC *adc = ADC::create(&win);
    win.appendLayout(adc->getLayout());
    PSUThread *thr = PSUThread::create();
    FrontPanel *fp = new FrontPanel();

    I2CBus i2cbus(I2C1);
    i2cbus.attach( 0xA0, new EEPROM_24C02("eeprom.dat") );

    MCP45HV *pot = new MCP45HV();
    i2cbus.attach( 0x78, pot);
    QObject::connect(pot, SIGNAL(changed(quint8)), adc, SLOT(VNEGWiperChanged(quint8)) );
    pot = new MCP45HV();
    i2cbus.attach( 0x7A, pot);
    QObject::connect(pot, SIGNAL(changed(quint8)), adc, SLOT(VPOSWiperChanged(quint8)) );
    pot = new MCP45HV();
    i2cbus.attach( 0x7C, pot);
    QObject::connect(pot, SIGNAL(changed(quint8)), adc, SLOT(VPSUWiperChanged(quint8)) );

    pot = new MCP45HV();
    i2cbus.attach( 0x7E, pot);
    QObject::connect(pot, SIGNAL(changed(quint8)), adc, SLOT(IPSUWiperChanged(quint8)) );


    i2cbus.attach( 0x06, fp);

    SPIBus spibus(SPI1);

    LCD *lcd = new LCD;

    spibus.attach(lcd,
                  GPIOA,
                  15);

    lcd->attachDC(GPIOB, 8);
                              /*
    void rotaryTopLeftChanged(double);
    void rotaryTopLeftButtonChanged(bool);
    void rotaryTopRightChanged(double);
    void rotaryTopRightButtonChanged(bool);
    void rotaryBottomLeftChanged(double);
    void rotaryBottomLeftButtonChanged(bool);
    void rotaryBottomRightChanged(double);
    void rotaryBottomRightButtonChanged(bool);
    void button1Changed(bool);
    void button2Changed(bool);
    void button3Changed(bool);
    void button4Changed(bool);
    */
    QObject::connect(&win, SIGNAL(rotaryTopLeftChanged(double)), fp, SLOT(onRotaryTopLeftChanged(double)));
    QObject::connect(&win, SIGNAL(rotaryTopRightChanged(double)), fp, SLOT(onRotaryTopRightChanged(double)));
    QObject::connect(&win, SIGNAL(rotaryBottomLeftChanged(double)), fp, SLOT(onRotaryBottomLeftChanged(double)));
    QObject::connect(&win, SIGNAL(rotaryBottomRightChanged(double)), fp, SLOT(onRotaryBottomRightChanged(double)));

    QObject::connect(&win, SIGNAL(button1Changed(bool)), fp, SLOT(onButtonP5VChanged(bool)));
    QObject::connect(&win, SIGNAL(button2Changed(bool)), fp, SLOT(onButtonVADJChanged(bool)));
    QObject::connect(&win, SIGNAL(button3Changed(bool)), fp, SLOT(onButtonExtraChanged(bool)));
    QObject::connect(&win, SIGNAL(button4Changed(bool)), fp, SLOT(onButtonPSUChanged(bool)));

    QObject::connect(fp, SIGNAL(LedP5VChanged(bool)), &win, SLOT(onLed1Changed(bool)));
    QObject::connect(fp, SIGNAL(LedVADJChanged(bool)), &win, SLOT(onLed2Changed(bool)));
    QObject::connect(fp, SIGNAL(LedPSUChanged(bool)), &win, SLOT(onLed3Changed(bool)));
    QObject::connect(fp, SIGNAL(BuzzerChanged(bool)), &win, SLOT(onBuzzerChanged(bool)));

    win.show();

    lcd->attachDrawer( win.getDrawer());

    thr->start();

    return app.exec();
}