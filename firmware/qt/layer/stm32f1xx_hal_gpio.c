#include "stm32f1xx_hal_gpio.h"
#include "dev_gpio.h"

typedef struct {
    gpio_write_cb_t write;
    gpio_read_cb_t read;
    void *data;
} gpiohandler_t;

typedef struct GPIO_TypeDef {
    gpiohandler_t handlers[16];
} GPIO_TypeDef;


void HAL_GPIO_WritePin(GPIO_TypeDef *port, uint16_t pin, uint8_t val)
{
    unsigned i = 0;

    gpiohandler_t *handlers = port->handlers;

    for (i=0;i<16; i++) {
        if ( pin & (1<<i)) {
            if (handlers[i].write) {
                port->handlers[i].write(port->handlers[i].data, val);
            }
        }
    }
}


int HAL_GPIO_ReadPin(GPIO_TypeDef*port,uint16_t pin)
{
    gpiohandler_t *handlers = port->handlers;
    unsigned i;
    for (i=0;i<16; i++) {
        if ( pin & (1<<i)) {
            if (handlers[i].read) {
                return port->handlers[i].read(port->handlers[i].data);
            }
        }
    }
    return -1;
}

void HAL_GPIO_Init(GPIO_TypeDef*port, GPIO_InitTypeDef*init)
{
}

void dev_gpio__registerhandler(GPIO_TypeDef*port,
                               unsigned pin,
                               gpio_write_cb_t write,
                               gpio_read_cb_t read,
                               void *data)
{
    port->handlers[pin].write = write;
    port->handlers[pin].read = read;
    port->handlers[pin].data = data;
}

static GPIO_TypeDef GPIOA_inst;
static GPIO_TypeDef GPIOB_inst;
static GPIO_TypeDef GPIOC_inst;

GPIO_TypeDef *GPIOA = &GPIOA_inst;
GPIO_TypeDef *GPIOB = &GPIOB_inst;
GPIO_TypeDef *GPIOC = &GPIOC_inst;

void hal_gpio__init(void)
{

}
