#ifndef __HAL_GPIO_H__
#define __HAL_GPIO_H__

#include <inttypes.h>

#ifdef __cplusplus
extern "C" {
#endif

typedef struct GPIO_TypeDef GPIO_TypeDef;

typedef struct
{
  uint32_t Pin;       /*!< Specifies the GPIO pins to be configured.
                           This parameter can be any value of @ref GPIO_pins_define */

  uint32_t Mode;      /*!< Specifies the operating mode for the selected pins.
                           This parameter can be a value of @ref GPIO_mode_define */
                           
  uint32_t Pull;      /*!< Specifies the Pull-up or Pull-Down activation for the selected pins.
                           This parameter can be a value of @ref GPIO_pull_define */
                           
  uint32_t Speed;     /*!< Specifies the speed for the selected pins.
                           This parameter can be a value of @ref GPIO_speed_define */
}GPIO_InitTypeDef;


extern GPIO_TypeDef  *GPIOA;
extern GPIO_TypeDef  *GPIOB;
extern GPIO_TypeDef  *GPIOC;

static const uint16_t GPIO_PIN_0 = 1<<0;
static const uint16_t GPIO_PIN_6 = 1<<6;
static const uint16_t GPIO_PIN_7 = 1<<7;
static const uint16_t GPIO_PIN_8 = 1<<8;
static const uint16_t GPIO_PIN_11 = 1<<11;
static const uint16_t GPIO_PIN_13 = 1<<13;
static const uint16_t GPIO_PIN_14 = 1<<14;
static const uint16_t GPIO_PIN_15 = 1<<15;


void HAL_GPIO_WritePin(GPIO_TypeDef*port,uint16_t pin,uint8_t val);

int HAL_GPIO_ReadPin(GPIO_TypeDef*port,uint16_t pin);

void HAL_GPIO_Init(GPIO_TypeDef*, GPIO_InitTypeDef*);


#define  GPIO_MODE_INPUT                        ((uint32_t)0x00000000)   /*!< Input Floating Mode                   */
#define  GPIO_MODE_OUTPUT_PP                    ((uint32_t)0x00000001)   /*!< Output Push Pull Mode                 */
#define  GPIO_MODE_OUTPUT_OD                    ((uint32_t)0x00000011)   /*!< Output Open Drain Mode                */
#define  GPIO_MODE_AF_PP                        ((uint32_t)0x00000002)   /*!< Alternate Function Push Pull Mode     */
#define  GPIO_MODE_AF_OD                        ((uint32_t)0x00000012)   /*!< Alternate Function Open Drain Mode    */
#define  GPIO_MODE_AF_INPUT                     GPIO_MODE_INPUT          /*!< Alternate Function Input Mode         */

#define  GPIO_MODE_ANALOG                       ((uint32_t)0x00000003)   /*!< Analog Mode  */
    
#define  GPIO_MODE_IT_RISING                    ((uint32_t)0x10110000)   /*!< External Interrupt Mode with Rising edge trigger detection          */
#define  GPIO_MODE_IT_FALLING                   ((uint32_t)0x10210000)   /*!< External Interrupt Mode with Falling edge trigger detection         */
#define  GPIO_MODE_IT_RISING_FALLING            ((uint32_t)0x10310000)   /*!< External Interrupt Mode with Rising/Falling edge trigger detection  */
 
#define  GPIO_MODE_EVT_RISING                   ((uint32_t)0x10120000)   /*!< External Event Mode with Rising edge trigger detection               */
#define  GPIO_MODE_EVT_FALLING                  ((uint32_t)0x10220000)   /*!< External Event Mode with Falling edge trigger detection              */
#define  GPIO_MODE_EVT_RISING_FALLING           ((uint32_t)0x10320000)   /*!< External Event Mode with Rising/Falling edge trigger detection       */
 
#define  GPIO_SPEED_FREQ_LOW              0
#define  GPIO_SPEED_FREQ_MEDIUM           0
#define  GPIO_SPEED_FREQ_HIGH             0

/**
  * @}
  */


 /** @defgroup GPIO_pull_define GPIO pull define
   * @brief GPIO Pull-Up or Pull-Down Activation
   * @{
   */  
#define  GPIO_NOPULL        ((uint32_t)0x00000000)   /*!< No Pull-up or Pull-down activation  */
#define  GPIO_PULLUP        ((uint32_t)0x00000001)   /*!< Pull-up activation                  */
#define  GPIO_PULLDOWN      ((uint32_t)0x00000002)   /*!< Pull-down activation                */

#ifdef __cplusplus
}
#endif

#endif
