#include "stm32f1xx_hal_spi.h"
#include "dev_spi.h"

typedef struct SPI_TypeDef {
    void (*txrx)(void *, uint8_t mode, const uint8_t *tx, uint8_t *rx, unsigned len);
    void *data;
    uint8_t mode;
} SPI_TypeDef;


HAL_StatusTypeDef HAL_SPI_Init(SPI_HandleTypeDef *hspi)
{
    hspi->Instance->mode = hspi->Init.DataSize;
    //printf("SPI MODE %d\n", mode);
    return HAL_OK;
}

HAL_StatusTypeDef HAL_SPI_DeInit (SPI_HandleTypeDef *hspi)
{
    return HAL_OK;
}

HAL_StatusTypeDef HAL_SPI_Transmit(SPI_HandleTypeDef *hspi, uint8_t *pData, uint16_t Size, uint32_t Timeout)
{
    hspi->Instance->txrx(
                         hspi->Instance->data,
                         hspi->Instance->mode,
                         pData,
                         0,
                         Size
                        );

    return HAL_OK;
}

HAL_StatusTypeDef HAL_SPI_Receive(SPI_HandleTypeDef *hspi, uint8_t *pData, uint16_t Size, uint32_t Timeout)
{
    hspi->Instance->txrx(
                         hspi->Instance->data,
                         hspi->Instance->mode,
                         0,
                         pData,
                         Size
                        );
    return HAL_OK;
}

HAL_StatusTypeDef HAL_SPI_TransmitReceive(SPI_HandleTypeDef *hspi, uint8_t *pTxData, uint8_t *pRxData, uint16_t Size, uint32_t Timeout)
{
    hspi->Instance->txrx(
                         hspi->Instance->data,
                         hspi->Instance->mode,
                         pTxData,
                         pRxData,
                         Size
                        );
    return HAL_OK;
}

static SPI_TypeDef SPI1_inst;

SPI_TypeDef *SPI1=&SPI1_inst;



void dev_spi__registerhandler(SPI_TypeDef *h,
                              void (*txrx)(void *, uint8_t mode, const uint8_t *tx, uint8_t *rx, unsigned len),
                              void *data)
{
    h->data = data;
    h->txrx = txrx;
}

