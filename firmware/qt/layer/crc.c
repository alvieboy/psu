#include "crc.h"

void crc__init(void)
{
}

void crc__enable(void)
{
}

void crc__disable(void)
{
}

crc_t crc__calculate(const uint32_t *data, uint32_t len_words)
{
}

crc_t crc__accumulate(const uint32_t *data, uint32_t len_words)
{
}
