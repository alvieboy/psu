#ifndef __HAL_H__
#define __HAL_H__

#include "defs.h"
#include <unistd.h>

#include <inttypes.h>

typedef int32_t HAL_StatusTypeDef;

#define HAL_OK 0

static inline void SystemCoreClockUpdate(void)
{
}

static inline void HAL_Init(void)
{
}

static inline void HAL_Delay(uint32_t cycles)
{
    usleep(cycles*1000);

}


#endif
