#ifndef __HAL_I2C_H__
#define __HAL_I2C_H__

#include <inttypes.h>
#include "stm32f1xx_hal.h"

#ifdef __cplusplus
extern "C" {
#endif



typedef struct I2C_TypeDef I2C_TypeDef;

extern I2C_TypeDef *I2C1;

typedef struct
{
  uint32_t ClockSpeed;       /*!< Specifies the clock frequency.
                                  This parameter must be set to a value lower than 400kHz */

  uint32_t DutyCycle;        /*!< Specifies the I2C fast mode duty cycle.
                                  This parameter can be a value of @ref I2C_duty_cycle_in_fast_mode */

  uint32_t OwnAddress1;      /*!< Specifies the first device own address.
                                  This parameter can be a 7-bit or 10-bit address. */

  uint32_t AddressingMode;   /*!< Specifies if 7-bit or 10-bit addressing mode is selected.
                                  This parameter can be a value of @ref I2C_addressing_mode */

  uint32_t DualAddressMode;  /*!< Specifies if dual addressing mode is selected.
                                  This parameter can be a value of @ref I2C_dual_addressing_mode */

  uint32_t OwnAddress2;      /*!< Specifies the second device own address if dual addressing mode is selected
                                  This parameter can be a 7-bit address. */

  uint32_t GeneralCallMode;  /*!< Specifies if general call mode is selected.
                                  This parameter can be a value of @ref I2C_general_call_addressing_mode */

  uint32_t NoStretchMode;    /*!< Specifies if nostretch mode is selected.
                                  This parameter can be a value of @ref I2C_nostretch_mode */

}I2C_InitTypeDef;

typedef struct
{
  I2C_TypeDef                *Instance;  /*!< I2C registers base address     */

  I2C_InitTypeDef            Init;       /*!< I2C communication parameters   */

}I2C_HandleTypeDef;

#define HAL_I2C_ERROR_NONE      ((uint32_t)0x00)    /*!< No error             */
#define HAL_I2C_ERROR_BERR      ((uint32_t)0x01)    /*!< BERR error           */
#define HAL_I2C_ERROR_ARLO      ((uint32_t)0x02)    /*!< ARLO error           */
#define HAL_I2C_ERROR_AF        ((uint32_t)0x04)    /*!< AF error             */
#define HAL_I2C_ERROR_OVR       ((uint32_t)0x08)    /*!< OVR error            */
#define HAL_I2C_ERROR_DMA       ((uint32_t)0x10)    /*!< DMA transfer error   */
#define HAL_I2C_ERROR_TIMEOUT   ((uint32_t)0x20)     /*!< Timeout error        */

/** 
  * @}
  */



/** @defgroup I2C_duty_cycle_in_fast_mode I2C Duty Cycle
  * @{
  */
#define I2C_DUTYCYCLE_2                 ((uint32_t)0x00000000)
#define I2C_DUTYCYCLE_16_9              I2C_CCR_DUTY
/**
  * @}
  */

/** @defgroup I2C_addressing_mode I2C addressing mode
  * @{
  */
#define I2C_ADDRESSINGMODE_7BIT         ((uint32_t)0x00004000)
#define I2C_ADDRESSINGMODE_10BIT        (I2C_OAR1_ADDMODE | ((uint32_t)0x00004000))
/**
  * @}
  */

/** @defgroup I2C_dual_addressing_mode I2C dual addressing mode
  * @{
  */
#define I2C_DUALADDRESS_DISABLE         ((uint32_t)0x00000000)
#define I2C_DUALADDRESS_ENABLE          I2C_OAR2_ENDUAL
/**
  * @}
  */

/** @defgroup I2C_general_call_addressing_mode I2C general call addressing mode
  * @{
  */
#define I2C_GENERALCALL_DISABLE         ((uint32_t)0x00000000)
#define I2C_GENERALCALL_ENABLE          I2C_CR1_ENGC
/**
  * @}
  */

/** @defgroup I2C_nostretch_mode I2C nostretch mode
  * @{
  */
#define I2C_NOSTRETCH_DISABLE           ((uint32_t)0x00000000)
#define I2C_NOSTRETCH_ENABLE            I2C_CR1_NOSTRETCH
/**
  * @}
  */

/** @defgroup I2C_Memory_Address_Size I2C Memory Address Size
  * @{
  */
#define I2C_MEMADD_SIZE_8BIT            ((uint32_t)0x00000001)
#define I2C_MEMADD_SIZE_16BIT           ((uint32_t)0x00000010)
/**
  * @}
  */

/** @defgroup I2C_Interrupt_configuration_definition I2C Interrupt configuration definition
  * @{
  */
#define I2C_IT_BUF                      I2C_CR2_ITBUFEN
#define I2C_IT_EVT                      I2C_CR2_ITEVTEN
#define I2C_IT_ERR                      I2C_CR2_ITERREN
/**
  * @}
  */

/** @defgroup I2C_Flag_definition I2C Flag definition
  * @brief I2C Interrupt definition
  *           - 0001XXXX  : Flag control mask for SR1 Register
  *           - 0010XXXX  : Flag control mask for SR2 Register
  * @{
  */
#define I2C_FLAG_SMBALERT               ((uint32_t)0x00018000)
#define I2C_FLAG_TIMEOUT                ((uint32_t)0x00014000)
#define I2C_FLAG_PECERR                 ((uint32_t)0x00011000)
#define I2C_FLAG_OVR                    ((uint32_t)0x00010800)
#define I2C_FLAG_AF                     ((uint32_t)0x00010400)
#define I2C_FLAG_ARLO                   ((uint32_t)0x00010200)
#define I2C_FLAG_BERR                   ((uint32_t)0x00010100)
#define I2C_FLAG_TXE                    ((uint32_t)0x00010080)
#define I2C_FLAG_RXNE                   ((uint32_t)0x00010040)
#define I2C_FLAG_STOPF                  ((uint32_t)0x00010010)
#define I2C_FLAG_ADD10                  ((uint32_t)0x00010008)
#define I2C_FLAG_BTF                    ((uint32_t)0x00010004)
#define I2C_FLAG_ADDR                   ((uint32_t)0x00010002)
#define I2C_FLAG_SB                     ((uint32_t)0x00010001)
#define I2C_FLAG_DUALF                  ((uint32_t)0x00100080)
#define I2C_FLAG_SMBHOST                ((uint32_t)0x00100040)
#define I2C_FLAG_SMBDEFAULT             ((uint32_t)0x00100020)
#define I2C_FLAG_GENCALL                ((uint32_t)0x00100010)
#define I2C_FLAG_TRA                    ((uint32_t)0x00100004)
#define I2C_FLAG_BUSY                   ((uint32_t)0x00100002)
#define I2C_FLAG_MSL                    ((uint32_t)0x00100001)
#define I2C_FLAG_MASK                   ((uint32_t)0x0000FFFF)

#define __HAL_RCC_I2C1_CLK_ENABLE()
#define __HAL_RCC_I2C1_CLK_DISABLE()

HAL_StatusTypeDef HAL_I2C_Init(I2C_HandleTypeDef *hi2c);
HAL_StatusTypeDef HAL_I2C_DeInit (I2C_HandleTypeDef *hi2c);
void HAL_I2C_MspInit(I2C_HandleTypeDef *hi2c);
void HAL_I2C_MspDeInit(I2C_HandleTypeDef *hi2c);

/**
  * @}
  */ 

/** @addtogroup I2C_Exported_Functions_Group2 Input and Output operation functions
  * @{
  */
   
/* IO operation functions  ****************************************************/

 /******* Blocking mode: Polling */
HAL_StatusTypeDef HAL_I2C_Master_Transmit(I2C_HandleTypeDef *hi2c, uint16_t DevAddress, uint8_t *pData, uint16_t Size, uint32_t Timeout);
HAL_StatusTypeDef HAL_I2C_Master_Receive(I2C_HandleTypeDef *hi2c, uint16_t DevAddress, uint8_t *pData, uint16_t Size, uint32_t Timeout);
HAL_StatusTypeDef HAL_I2C_Slave_Transmit(I2C_HandleTypeDef *hi2c, uint8_t *pData, uint16_t Size, uint32_t Timeout);
HAL_StatusTypeDef HAL_I2C_Slave_Receive(I2C_HandleTypeDef *hi2c, uint8_t *pData, uint16_t Size, uint32_t Timeout);
HAL_StatusTypeDef HAL_I2C_Mem_Write(I2C_HandleTypeDef *hi2c, uint16_t DevAddress, uint16_t MemAddress, uint16_t MemAddSize, uint8_t *pData, uint16_t Size, uint32_t Timeout);
HAL_StatusTypeDef HAL_I2C_Mem_Read(I2C_HandleTypeDef *hi2c, uint16_t DevAddress, uint16_t MemAddress, uint16_t MemAddSize, uint8_t *pData, uint16_t Size, uint32_t Timeout);
HAL_StatusTypeDef HAL_I2C_IsDeviceReady(I2C_HandleTypeDef *hi2c, uint16_t DevAddress, uint32_t Trials, uint32_t Timeout);

#ifdef __cplusplus
}
#endif

#endif
