#include "uart.h"
#include <stdio.h>
#include <stdarg.h>

void uart__init(void)
{
}

void outbyte(int c)
{
    putc(c, stderr);
    fflush(stderr);
}

void outstring(const char *str)
{
    fprintf(stderr,"%s", str);
    fflush(stderr);
}

int inbyte(unsigned char *c)
{
    return -1;
}


static void printnibble(unsigned int c)
{
    c&=0xf;
    if (c>9)
        outbyte(c+'a'-10);
    else
        outbyte(c+'0');
}

void printhexbyte(unsigned int c)
{
    printnibble(c>>4);
    printnibble(c);
}


void printhex(unsigned int c)
{
    printhexbyte(c>>24);
    printhexbyte(c>>16);
    printhexbyte(c>>8);
    printhexbyte(c);
}

void printhex16(unsigned short c)
{
    printhexbyte(c>>8);
    printhexbyte(c);
}

static char printf_line[128];

void uart__printf(const char *fmt,...)
{
    va_list ap;
    va_start(ap, fmt);
    int len = __vsnprintf(printf_line, 128, fmt, ap);
    if (len>0) {
        uart__puts(printf_line);
    }
    va_end(ap);
}

void uart__puts(const char *str)
{
    outstring(str);
}

