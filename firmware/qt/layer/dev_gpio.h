#ifndef __DEVGPIO_H__
#define __DEVGPIO_H__

#include <inttypes.h>

#ifdef __cplusplus
extern "C" {
#endif

typedef void (*gpio_write_cb_t)(void*,uint8_t);
typedef uint8_t (*gpio_read_cb_t)(void*);

void dev_gpio__registerhandler(GPIO_TypeDef*port,
                               unsigned pin,
                               gpio_write_cb_t write,
                               gpio_read_cb_t read,
                               void *data);
#ifdef __cplusplus
}
#endif

#endif
