#include "stm32f1xx_hal_i2c.h"

typedef struct I2C_TypeDef {
    int (*transmit)(void *, uint16_t DevAddress, const uint8_t *pData, uint16_t Size);
    int (*receive)(void *, uint16_t DevAddress, uint8_t *pData, uint16_t Size);
    void *data;
} I2C_TypeDef;


HAL_StatusTypeDef HAL_I2C_Init(I2C_HandleTypeDef *hi2c)
{
}

HAL_StatusTypeDef HAL_I2C_DeInit (I2C_HandleTypeDef *hi2c)
{
}

/**
  * @}
  */ 

/** @addtogroup I2C_Exported_Functions_Group2 Input and Output operation functions
  * @{
  */
   
/* IO operation functions  ****************************************************/

 /******* Blocking mode: Polling */
HAL_StatusTypeDef HAL_I2C_Master_Transmit(I2C_HandleTypeDef *hi2c, uint16_t DevAddress, uint8_t *pData, uint16_t Size, uint32_t Timeout)
{
    if (hi2c->Instance->transmit) {
        return hi2c->Instance->transmit(hi2c->Instance->data, DevAddress,pData, Size);
    }
}
HAL_StatusTypeDef HAL_I2C_Master_Receive(I2C_HandleTypeDef *hi2c, uint16_t DevAddress, uint8_t *pData, uint16_t Size, uint32_t Timeout)
{
    if (hi2c->Instance->receive) {
        return hi2c->Instance->receive(hi2c->Instance->data, DevAddress,pData, Size);
    }
    return -1;
}

static I2C_TypeDef I2C_inst;
I2C_TypeDef *I2C1 = &I2C_inst;

void hal_i2c__registerhandler(I2C_TypeDef *h,
                              int (*transmit)(void *, uint16_t DevAddress, const uint8_t *pData, uint16_t Size),
                              int (*receive)(void *, uint16_t DevAddress, uint8_t *pData, uint16_t Size),
                              void*data)
{
    h->data = data;
    h->receive = receive;
    h->transmit = transmit;
}

