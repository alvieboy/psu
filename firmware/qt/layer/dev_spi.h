#ifndef __DEV_SPI__H__
#define __DEV_SPI__H__

#ifdef __cplusplus
extern "C"  {
#endif

void dev_spi__registerhandler(SPI_TypeDef *spi,
                              void (*txrx)(void *, uint8_t mode, const uint8_t *tx, uint8_t *rx, unsigned len),
                              void *data
                             );

#ifdef __cplusplus
}
#endif

#endif
