#ifndef __DEV_I2C_H__
#define __DEV_I2C_H__

#ifdef __cplusplus
extern "C" {
#endif

typedef struct I2C_TypeDef I2C_TypeDef;

void hal_i2c__registerhandler(I2C_TypeDef *h,
                              int (*transmit)(void *, uint16_t DevAddress, const uint8_t *pData, uint16_t Size),
                              int (*receive)(void *, uint16_t DevAddress, uint8_t *pData, uint16_t Size),
                              void*data);

#ifdef __cplusplus
}
#endif

#endif
