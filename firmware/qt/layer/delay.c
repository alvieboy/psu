#include "delay.h"
#include "defs.h"
#include <unistd.h>

void delay__init(void)
{
}

static uint16_t delay__read()
{
    return 0;
}

void delay__us(uint16_t us PARAM_UNUSED)
{
    usleep(1);
}
