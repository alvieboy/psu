#ifndef __HAL_SPI_H__
#define __HAL_SPI_H__

#include <inttypes.h>
#include "stm32f1xx_hal.h"

#ifdef __cplusplus
extern "C" {
#endif


typedef struct SPI_TypeDef SPI_TypeDef;

extern SPI_TypeDef *SPI1;

typedef struct
{
  uint32_t Mode;               /*!< Specifies the SPI operating mode.
                                    This parameter can be a value of @ref SPI_mode */

  uint32_t Direction;          /*!< Specifies the SPI Directional mode state.
                                    This parameter can be a value of @ref SPI_Direction_mode */

  uint32_t DataSize;           /*!< Specifies the SPI data size.
                                    This parameter can be a value of @ref SPI_data_size */

  uint32_t CLKPolarity;        /*!< Specifies the serial clock steady state.
                                    This parameter can be a value of @ref SPI_Clock_Polarity */

  uint32_t CLKPhase;           /*!< Specifies the clock active edge for the bit capture.
                                    This parameter can be a value of @ref SPI_Clock_Phase */

  uint32_t NSS;                /*!< Specifies whether the NSS signal is managed by
                                    hardware (NSS pin) or by software using the SSI bit.
                                    This parameter can be a value of @ref SPI_Slave_Select_management */

  uint32_t BaudRatePrescaler;  /*!< Specifies the Baud Rate prescaler value which will be
                                    used to configure the transmit and receive SCK clock.
                                    This parameter can be a value of @ref SPI_BaudRate_Prescaler
                                    @note The communication clock is derived from the master
                                    clock. The slave clock does not need to be set */

  uint32_t FirstBit;           /*!< Specifies whether data transfers start from MSB or LSB bit.
                                    This parameter can be a value of @ref SPI_MSB_LSB_transmission */

  uint32_t TIMode;             /*!< Specifies if the TI mode is enabled or not.
                                    This parameter can be a value of @ref SPI_TI_mode */

  uint32_t CRCCalculation;     /*!< Specifies if the CRC calculation is enabled or not.
                                    This parameter can be a value of @ref SPI_CRC_Calculation */

  uint32_t CRCPolynomial;      /*!< Specifies the polynomial used for the CRC calculation.
                                    This parameter must be a number between Min_Data = 0 and Max_Data = 65535 */

}SPI_InitTypeDef;


typedef struct __SPI_HandleTypeDef
{
  SPI_TypeDef                *Instance;    /*!< SPI registers base address */

  SPI_InitTypeDef            Init;         /*!< SPI communication parameters */
}
SPI_HandleTypeDef;


#define HAL_SPI_ERROR_NONE      ((uint32_t)0x00)    /*!< No error             */
#define HAL_SPI_ERROR_MODF      ((uint32_t)0x01)    /*!< MODF error           */
#define HAL_SPI_ERROR_CRC       ((uint32_t)0x02)    /*!< CRC error            */
#define HAL_SPI_ERROR_OVR       ((uint32_t)0x04)    /*!< OVR error            */
#define HAL_SPI_ERROR_DMA       ((uint32_t)0x08)    /*!< DMA transfer error   */
#define HAL_SPI_ERROR_FLAG      ((uint32_t)0x10)    /*!< Flag: RXNE,TXE, BSY  */
/**
  * @}
  */




/** @defgroup SPI_mode SPI mode
  * @{
  */
#define SPI_MODE_SLAVE                  ((uint32_t)0x00000000)
#define SPI_MODE_MASTER                 1

/**
  * @}
  */

/** @defgroup SPI_Direction_mode SPI Direction mode
  * @{
  */
#define SPI_DIRECTION_2LINES            ((uint32_t)0x00000000)
#define SPI_DIRECTION_2LINES_RXONLY     0
#define SPI_DIRECTION_1LINE             0

/**
  * @}
  */

/** @defgroup SPI_data_size SPI data size
  * @{
  */
#define SPI_DATASIZE_8BIT               0
#define SPI_DATASIZE_16BIT              1

/**
  * @}
  */ 

/** @defgroup SPI_Clock_Polarity SPI Clock Polarity
  * @{
  */
#define SPI_POLARITY_LOW                ((uint32_t)0x00000000)
#define SPI_POLARITY_HIGH               1

/**
  * @}
  */

/** @defgroup SPI_Clock_Phase SPI Clock Phase
  * @{
  */
#define SPI_PHASE_1EDGE                 ((uint32_t)0x00000000)
#define SPI_PHASE_2EDGE                 0

/**
  * @}
  */

/** @defgroup SPI_Slave_Select_management SPI Slave Select management
  * @{
  */
#define SPI_NSS_SOFT                    0
#define SPI_NSS_HARD_INPUT              ((uint32_t)0x00000000)
#define SPI_NSS_HARD_OUTPUT             0

/**
  * @}
  */ 

/** @defgroup SPI_BaudRate_Prescaler SPI BaudRate Prescaler
  * @{
  */
#define SPI_BAUDRATEPRESCALER_2         ((uint32_t)0x00000000)
#define SPI_BAUDRATEPRESCALER_4         0
#define SPI_BAUDRATEPRESCALER_8         0
#define SPI_BAUDRATEPRESCALER_16        0
#define SPI_BAUDRATEPRESCALER_32        0
#define SPI_BAUDRATEPRESCALER_64        0
#define SPI_BAUDRATEPRESCALER_128       0
#define SPI_BAUDRATEPRESCALER_256       0

/**
  * @}
  */ 

/** @defgroup SPI_MSB_LSB_transmission SPI MSB LSB transmission
  * @{
  */
#define SPI_FIRSTBIT_MSB                ((uint32_t)0x00000000)
#define SPI_FIRSTBIT_LSB                0

/**
  * @}
  */

/** @defgroup SPI_TI_mode SPI TI mode disable
  * @brief  SPI TI Mode not supported for STM32F1xx family 
  * @{
  */
#define SPI_TIMODE_DISABLE             ((uint32_t)0x00000000)

/**
  * @}
  */
  
/** @defgroup SPI_CRC_Calculation SPI CRC Calculation
  * @{
  */
#define SPI_CRCCALCULATION_DISABLE      ((uint32_t)0x00000000)
#define SPI_CRCCALCULATION_ENABLE       0



#define __HAL_SPI_ENABLE(x)

HAL_StatusTypeDef HAL_SPI_Init(SPI_HandleTypeDef *hspi);
HAL_StatusTypeDef HAL_SPI_DeInit (SPI_HandleTypeDef *hspi);
void HAL_SPI_MspInit(SPI_HandleTypeDef *hspi);
void HAL_SPI_MspDeInit(SPI_HandleTypeDef *hspi);
/**
  * @}
  */

/* I/O operation functions  *****************************************************/
/** @addtogroup SPI_Exported_Functions_Group2
  * @{
  */
HAL_StatusTypeDef HAL_SPI_Transmit(SPI_HandleTypeDef *hspi, uint8_t *pData, uint16_t Size, uint32_t Timeout);
HAL_StatusTypeDef HAL_SPI_Receive(SPI_HandleTypeDef *hspi, uint8_t *pData, uint16_t Size, uint32_t Timeout);
HAL_StatusTypeDef HAL_SPI_TransmitReceive(SPI_HandleTypeDef *hspi, uint8_t *pTxData, uint8_t *pRxData, uint16_t Size, uint32_t Timeout);



#ifdef __cplusplus
}
#endif

#endif
