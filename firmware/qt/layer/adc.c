#include "adc.h"
#include "calibration.h"
#include <string.h>

#define ADC_CHANNEL_VREF 7u
#define ADC_RESOLUTION_BITS 12

static uint32_t configuration = 0;

uint16_t aADCxConvertedValues[16];


void adc__init(void)
{
}


void adc__calibrate()
{
}

extern void adc__start();


/*

+----------------------+--------+-------+----------+
| Input                | in     | Vadc  | ADC conv |
+----------------------+--------+-------+----------+
| +5V                  | +5V    | 2.5V  | 3103     |
| +5V                  | +4V    | 2.0V  | 2482     |
| Current samplers     | 1A     | 0.333 | 414      |
| PSU                  | 15V    | 3.1637| 3926     |
| PSU                  | -15V   | 0.0058| 7        |
| PSU                  | 0V     | 1.5848| 1967     |
| Vpos                 | 12V    | 2.4489| 3039     |
| Vpos                 | 5V     | 1.0204| 1266     |
| Vneg                 | -12V   | 2.5532| 3169     |
| Vneg                 | -5V    | 1.0638| 1320     |
+----------------------+--------+-------+----------+

 */

fixed_t adc__rawtovoltage(uint32_t raw)
{
    unsigned vref = adc__getrawvalue(ADC_CHANNEL_VREF);
    if (vref==0) {
        vref = 1489;
    }
    /*
     * Ideal case: VCC=3.30000V, Vref=1.20000V.
     *
     *  adc_value = 4096 * (Vin/3.3)
     *
     *  Vin = (adc_value * Vadc) / 4096
     *
     *  Vadc is unknown.
     *  we know that ADC(VREF) = 4096* (1.2/Vadc) [1489@3.3V]
     *  Vadc = (4096*1.2) / ADC(VREF)
     *
     *
     *  Vadc = (4096*1.2) / ADC(VREF)
     *
     *  Vin = (adc_value * ((4096*1.2)/ADC(VREF) ) ) / 4096.
     *  Vin = adc_value * (1.2/ADC(VREF))
     *  VinS = adc_value * (S*1.2) / ADC(VREF)
     */

     unsigned scale = FLOAT2FP(1.2) / vref;
     return (raw * scale) + FLOAT2FP(0.00005);
}




void __attribute__((weak)) adc__conversion_complete_callback()
{
}

#if ENABLE_AVERAGE

static uint32_t samplebuf[NUM_CHANNELS] = {0};
static uint8_t sampcount = 0;

#endif

void HAL_ADC_ConvCpltCallback()
{
    //outstring("Adc\r\n");
    // Convert
    //uint32_t sample32;

    unsigned i;
#if !(ENABLE_AVERAGE)
#error a
    for (i=0;i<NUM_CHANNELS;i++) {
        uint64_t sample = (uint64_t)adc__rawtovoltage(aADCxConvertedValues[i]);
        sample *= (uint64_t)calibration[i].mul;
        sample32 = sample>>32;
        sample32 += calibration[i].offset;
        voltages[i] = sample32;
    }
    adc__conversion_complete_callback();
#else
    for (i=0;i<NUM_CHANNELS;i++) {
        uint32_t sample = (uint32_t)aADCxConvertedValues[i];
        samplebuf[i] += sample;
    }

    sampcount++;
    if (sampcount==SAMPLES_TO_AVERAGE_COUNT) {
        sampcount = 0;
        for (i=0;i<NUM_CHANNELS;i++) {
            samplebuf[i] = adc__rawtovoltage(samplebuf[i] >> SAMPLES_TO_AVERAGE_SHIFT);
        }

        // This is destructive. :)
        calibration__calculate_values( samplebuf, configuration );
        adc__conversion_complete_callback();

        for (i=0;i<NUM_CHANNELS;i++) {
            samplebuf[i] = 0;
        }

    }
#endif
    
}

fixed_t adc__getvadc(void)
{
    int64_t v = 4095*1.2*FLOAT2FP(1.0);

    uint64_t vref = adc__getrawvalue(ADC_CHANNEL_VREF);

    //printf("In compute VADC: sensor %d\n", vref);
    if (vref) {
        int32_t vadc = v / (int64_t)adc__getrawvalue(ADC_CHANNEL_VREF);
        return vadc;
    }
    return 0;
}

void adc__inject(uint16_t values[16])
{
    unsigned i;
    uint32_t buffer[8];
    memcpy(aADCxConvertedValues, values, sizeof(aADCxConvertedValues));

    for (i=0;i<8;i++) {
        buffer[i]  = adc__rawtovoltage(values[i]);
    }
    calibration__calculate_values( buffer, configuration );
    adc__conversion_complete_callback();
}

unsigned adc__getrawvalue(unsigned index)
{
    return aADCxConvertedValues[index];
}

