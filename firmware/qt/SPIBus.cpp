#include "SPIBus.h"
#include "dev_spi.h"
#include "dev_gpio.h"


SPIBus::SPIBus(SPI_TypeDef *h): spi(h)
{
}

void SPIBus::attach(SPIDevice *dev, GPIO_TypeDef *gpio, unsigned pin)
{
    dev_spi__registerhandler(spi,
                             &wrap_txrx,
                             dev);

    dev_gpio__registerhandler(gpio,
                              pin,
                              wrap_write,
                              wrap_read,
                              dev);
}

void SPIBus::wrap_txrx(void *data, uint8_t mode, const uint8_t *tx, uint8_t *rx, unsigned len)
{
    static_cast<SPIDevice*>(data)->txrx(mode, tx,rx,len);
}


uint8_t SPIBus::wrap_read(void*)
{
    return 1;
}

void SPIBus::wrap_write(void*data,uint8_t v)
{
    static_cast<SPIDevice*>(data)->select(!v);

}
