#include <inttypes.h>
#include <unistd.h>
#include <stdlib.h>
#include <fcntl.h>

#include "MCP45HV.h"
#include <QDebug>


MCP45HV::MCP45HV()
{
}

MCP45HV::~MCP45HV()
{
}

int MCP45HV::transmit(const uint8_t *pData, uint16_t Size)
{
    qDebug()<<"MCP45HV: TX size"<<Size;
    if (Size==2) {
        if (pData[0]==0) {
            // Wiper
            emit changed(pData[1]);
        }
    }
    return 0;
}

int MCP45HV::receive(uint8_t *pData, uint16_t Size)
{
    qDebug()<<"MCP45HV: RX size"<<Size;
    while (Size--) {
    }
    return 0;
}


