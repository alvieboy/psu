#ifndef __SPIBUS_H__
#define __SPIBUS_H__

#include "stm32f1xx_hal_spi.h"
#include "stm32f1xx_hal_gpio.h"
#include "SPIDevice.h"


class SPIBus
{
public:
    SPIBus(SPI_TypeDef*h);
    void attach(SPIDevice *dev, GPIO_TypeDef *gpio, unsigned pin);
private:
    static void wrap_txrx(void *, uint8_t mode, const uint8_t *tx, uint8_t *rx, unsigned len);
    void txrx(uint8_t mode, const uint8_t *tx, uint8_t *rx, unsigned len);
    static uint8_t wrap_read(void*);
    static void wrap_write(void*,uint8_t);
    SPI_TypeDef *spi;
};

#endif
