/****************************************************************************
** Meta object code from reading C++ file 'FrontPanel.h'
**
** Created by: The Qt Meta Object Compiler version 63 (Qt 4.8.7)
**
** WARNING! All changes made in this file will be lost!
*****************************************************************************/

#include "FrontPanel.h"
#if !defined(Q_MOC_OUTPUT_REVISION)
#error "The header file 'FrontPanel.h' doesn't include <QObject>."
#elif Q_MOC_OUTPUT_REVISION != 63
#error "This file was generated using the moc from 4.8.7. It"
#error "cannot be used with the include files from this version of Qt."
#error "(The moc has changed too much.)"
#endif

QT_BEGIN_MOC_NAMESPACE
static const uint qt_meta_data_FrontPanel[] = {

 // content:
       6,       // revision
       0,       // classname
       0,    0, // classinfo
      16,   14, // methods
       0,    0, // properties
       0,    0, // enums/sets
       0,    0, // constructors
       0,       // flags
       4,       // signalCount

 // signals: signature, parameters, type, tag, flags
      12,   11,   11,   11, 0x05,
      32,   11,   11,   11, 0x05,
      53,   11,   11,   11, 0x05,
      73,   11,   11,   11, 0x05,

 // slots: signature, parameters, type, tag, flags
      93,   11,   11,   11, 0x0a,
     124,   11,   11,   11, 0x0a,
     159,   11,   11,   11, 0x0a,
     191,   11,   11,   11, 0x0a,
     227,   11,   11,   11, 0x0a,
     261,   11,   11,   11, 0x0a,
     299,   11,   11,   11, 0x0a,
     334,   11,   11,   11, 0x0a,
     373,   11,   11,   11, 0x0a,
     398,   11,   11,   11, 0x0a,
     424,   11,   11,   11, 0x0a,
     451,   11,   11,   11, 0x0a,

       0        // eod
};

static const char qt_meta_stringdata_FrontPanel[] = {
    "FrontPanel\0\0LedP5VChanged(bool)\0"
    "LedVADJChanged(bool)\0LedPSUChanged(bool)\0"
    "BuzzerChanged(bool)\0onRotaryTopLeftChanged(double)\0"
    "onRotaryTopLeftButtonChanged(bool)\0"
    "onRotaryTopRightChanged(double)\0"
    "onRotaryTopRightButtonChanged(bool)\0"
    "onRotaryBottomLeftChanged(double)\0"
    "onRotaryBottomLeftButtonChanged(bool)\0"
    "onRotaryBottomRightChanged(double)\0"
    "onRotaryBottomRightButtonChanged(bool)\0"
    "onButtonP5VChanged(bool)\0"
    "onButtonVADJChanged(bool)\0"
    "onButtonExtraChanged(bool)\0"
    "onButtonPSUChanged(bool)\0"
};

void FrontPanel::qt_static_metacall(QObject *_o, QMetaObject::Call _c, int _id, void **_a)
{
    if (_c == QMetaObject::InvokeMetaMethod) {
        Q_ASSERT(staticMetaObject.cast(_o));
        FrontPanel *_t = static_cast<FrontPanel *>(_o);
        switch (_id) {
        case 0: _t->LedP5VChanged((*reinterpret_cast< bool(*)>(_a[1]))); break;
        case 1: _t->LedVADJChanged((*reinterpret_cast< bool(*)>(_a[1]))); break;
        case 2: _t->LedPSUChanged((*reinterpret_cast< bool(*)>(_a[1]))); break;
        case 3: _t->BuzzerChanged((*reinterpret_cast< bool(*)>(_a[1]))); break;
        case 4: _t->onRotaryTopLeftChanged((*reinterpret_cast< double(*)>(_a[1]))); break;
        case 5: _t->onRotaryTopLeftButtonChanged((*reinterpret_cast< bool(*)>(_a[1]))); break;
        case 6: _t->onRotaryTopRightChanged((*reinterpret_cast< double(*)>(_a[1]))); break;
        case 7: _t->onRotaryTopRightButtonChanged((*reinterpret_cast< bool(*)>(_a[1]))); break;
        case 8: _t->onRotaryBottomLeftChanged((*reinterpret_cast< double(*)>(_a[1]))); break;
        case 9: _t->onRotaryBottomLeftButtonChanged((*reinterpret_cast< bool(*)>(_a[1]))); break;
        case 10: _t->onRotaryBottomRightChanged((*reinterpret_cast< double(*)>(_a[1]))); break;
        case 11: _t->onRotaryBottomRightButtonChanged((*reinterpret_cast< bool(*)>(_a[1]))); break;
        case 12: _t->onButtonP5VChanged((*reinterpret_cast< bool(*)>(_a[1]))); break;
        case 13: _t->onButtonVADJChanged((*reinterpret_cast< bool(*)>(_a[1]))); break;
        case 14: _t->onButtonExtraChanged((*reinterpret_cast< bool(*)>(_a[1]))); break;
        case 15: _t->onButtonPSUChanged((*reinterpret_cast< bool(*)>(_a[1]))); break;
        default: ;
        }
    }
}

const QMetaObjectExtraData FrontPanel::staticMetaObjectExtraData = {
    0,  qt_static_metacall 
};

const QMetaObject FrontPanel::staticMetaObject = {
    { &I2CDevice::staticMetaObject, qt_meta_stringdata_FrontPanel,
      qt_meta_data_FrontPanel, &staticMetaObjectExtraData }
};

#ifdef Q_NO_DATA_RELOCATION
const QMetaObject &FrontPanel::getStaticMetaObject() { return staticMetaObject; }
#endif //Q_NO_DATA_RELOCATION

const QMetaObject *FrontPanel::metaObject() const
{
    return QObject::d_ptr->metaObject ? QObject::d_ptr->metaObject : &staticMetaObject;
}

void *FrontPanel::qt_metacast(const char *_clname)
{
    if (!_clname) return 0;
    if (!strcmp(_clname, qt_meta_stringdata_FrontPanel))
        return static_cast<void*>(const_cast< FrontPanel*>(this));
    return I2CDevice::qt_metacast(_clname);
}

int FrontPanel::qt_metacall(QMetaObject::Call _c, int _id, void **_a)
{
    _id = I2CDevice::qt_metacall(_c, _id, _a);
    if (_id < 0)
        return _id;
    if (_c == QMetaObject::InvokeMetaMethod) {
        if (_id < 16)
            qt_static_metacall(this, _c, _id, _a);
        _id -= 16;
    }
    return _id;
}

// SIGNAL 0
void FrontPanel::LedP5VChanged(bool _t1)
{
    void *_a[] = { 0, const_cast<void*>(reinterpret_cast<const void*>(&_t1)) };
    QMetaObject::activate(this, &staticMetaObject, 0, _a);
}

// SIGNAL 1
void FrontPanel::LedVADJChanged(bool _t1)
{
    void *_a[] = { 0, const_cast<void*>(reinterpret_cast<const void*>(&_t1)) };
    QMetaObject::activate(this, &staticMetaObject, 1, _a);
}

// SIGNAL 2
void FrontPanel::LedPSUChanged(bool _t1)
{
    void *_a[] = { 0, const_cast<void*>(reinterpret_cast<const void*>(&_t1)) };
    QMetaObject::activate(this, &staticMetaObject, 2, _a);
}

// SIGNAL 3
void FrontPanel::BuzzerChanged(bool _t1)
{
    void *_a[] = { 0, const_cast<void*>(reinterpret_cast<const void*>(&_t1)) };
    QMetaObject::activate(this, &staticMetaObject, 3, _a);
}
QT_END_MOC_NAMESPACE
