/****************************************************************************
** Meta object code from reading C++ file 'ADC.h'
**
** Created by: The Qt Meta Object Compiler version 63 (Qt 4.8.7)
**
** WARNING! All changes made in this file will be lost!
*****************************************************************************/

#include "ADC.h"
#if !defined(Q_MOC_OUTPUT_REVISION)
#error "The header file 'ADC.h' doesn't include <QObject>."
#elif Q_MOC_OUTPUT_REVISION != 63
#error "This file was generated using the moc from 4.8.7. It"
#error "cannot be used with the include files from this version of Qt."
#error "(The moc has changed too much.)"
#endif

QT_BEGIN_MOC_NAMESPACE
static const uint qt_meta_data_ADC[] = {

 // content:
       6,       // revision
       0,       // classname
       0,    0, // classinfo
      12,   14, // methods
       0,    0, // properties
       0,    0, // enums/sets
       0,    0, // constructors
       0,       // flags
       1,       // signalCount

 // signals: signature, parameters, type, tag, flags
       5,    4,    4,    4, 0x05,

 // slots: signature, parameters, type, tag, flags
      19,    4,    4,    4, 0x0a,
      34,    4,    4,    4, 0x0a,
      59,    4,    4,    4, 0x0a,
      84,    4,    4,    4, 0x0a,
     109,    4,    4,    4, 0x0a,
     134,    4,    4,    4, 0x0a,
     170,    4,    4,    4, 0x0a,
     205,    4,    4,    4, 0x0a,
     240,    4,    4,    4, 0x0a,
     275,    4,    4,    4, 0x0a,
     304,    4,    4,    4, 0x0a,

       0        // eod
};

static const char qt_meta_stringdata_ADC[] = {
    "ADC\0\0ADCComplete()\0timerExpired()\0"
    "VPSUWiperChanged(quint8)\0"
    "IPSUWiperChanged(quint8)\0"
    "VPOSWiperChanged(quint8)\0"
    "VNEGWiperChanged(quint8)\0"
    "onPSUVoltageCheckStateChanged(bool)\0"
    "onPSUCurrentCheckStateChanged(int)\0"
    "onP5VVoltageCheckStateChanged(int)\0"
    "onP5VCurrentCheckStateChanged(int)\0"
    "onVPOSCheckStateChanged(int)\0"
    "onVNEGCheckStateChanged(int)\0"
};

void ADC::qt_static_metacall(QObject *_o, QMetaObject::Call _c, int _id, void **_a)
{
    if (_c == QMetaObject::InvokeMetaMethod) {
        Q_ASSERT(staticMetaObject.cast(_o));
        ADC *_t = static_cast<ADC *>(_o);
        switch (_id) {
        case 0: _t->ADCComplete(); break;
        case 1: _t->timerExpired(); break;
        case 2: _t->VPSUWiperChanged((*reinterpret_cast< quint8(*)>(_a[1]))); break;
        case 3: _t->IPSUWiperChanged((*reinterpret_cast< quint8(*)>(_a[1]))); break;
        case 4: _t->VPOSWiperChanged((*reinterpret_cast< quint8(*)>(_a[1]))); break;
        case 5: _t->VNEGWiperChanged((*reinterpret_cast< quint8(*)>(_a[1]))); break;
        case 6: _t->onPSUVoltageCheckStateChanged((*reinterpret_cast< bool(*)>(_a[1]))); break;
        case 7: _t->onPSUCurrentCheckStateChanged((*reinterpret_cast< int(*)>(_a[1]))); break;
        case 8: _t->onP5VVoltageCheckStateChanged((*reinterpret_cast< int(*)>(_a[1]))); break;
        case 9: _t->onP5VCurrentCheckStateChanged((*reinterpret_cast< int(*)>(_a[1]))); break;
        case 10: _t->onVPOSCheckStateChanged((*reinterpret_cast< int(*)>(_a[1]))); break;
        case 11: _t->onVNEGCheckStateChanged((*reinterpret_cast< int(*)>(_a[1]))); break;
        default: ;
        }
    }
}

const QMetaObjectExtraData ADC::staticMetaObjectExtraData = {
    0,  qt_static_metacall 
};

const QMetaObject ADC::staticMetaObject = {
    { &QObject::staticMetaObject, qt_meta_stringdata_ADC,
      qt_meta_data_ADC, &staticMetaObjectExtraData }
};

#ifdef Q_NO_DATA_RELOCATION
const QMetaObject &ADC::getStaticMetaObject() { return staticMetaObject; }
#endif //Q_NO_DATA_RELOCATION

const QMetaObject *ADC::metaObject() const
{
    return QObject::d_ptr->metaObject ? QObject::d_ptr->metaObject : &staticMetaObject;
}

void *ADC::qt_metacast(const char *_clname)
{
    if (!_clname) return 0;
    if (!strcmp(_clname, qt_meta_stringdata_ADC))
        return static_cast<void*>(const_cast< ADC*>(this));
    return QObject::qt_metacast(_clname);
}

int ADC::qt_metacall(QMetaObject::Call _c, int _id, void **_a)
{
    _id = QObject::qt_metacall(_c, _id, _a);
    if (_id < 0)
        return _id;
    if (_c == QMetaObject::InvokeMetaMethod) {
        if (_id < 12)
            qt_static_metacall(this, _c, _id, _a);
        _id -= 12;
    }
    return _id;
}

// SIGNAL 0
void ADC::ADCComplete()
{
    QMetaObject::activate(this, &staticMetaObject, 0, 0);
}
QT_END_MOC_NAMESPACE
