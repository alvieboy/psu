#ifndef __RENDERAREA_H__
#define __RENDERAREA_H__

#include <QWidget>
#include <QImage>
#include <QTimer>
#include "ScreenDrawer.h"

class RenderArea : public QWidget, public ScreenDrawer
{
    Q_OBJECT

public:
    RenderArea(QWidget *parent = 0);

    QSize minimumSizeHint() const override;
    QSize sizeHint() const override;

    void startFrame();
    void finishFrame();
protected:
    void paintEvent(QPaintEvent *event) override;
    void drawPixel(int x, int y, uint32_t color) override;
    void drawImage(int x, int y, QImage *);
    void drawHLine(int x, int y, int width, uint32_t color);
    void drawVLine(int x, int y, int width, uint32_t color);

    QImage *image;
};

#endif
