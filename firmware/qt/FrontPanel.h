#ifndef __FRONTPANEL_H__
#define __FRONTPANEL_H__

#include "I2CDevice.h"
#include <QObject>

class FrontPanel: public I2CDevice
{
    Q_OBJECT
public:
    FrontPanel();
    virtual ~FrontPanel();
    virtual int transmit(const uint8_t *pData, uint16_t Size);
    virtual int receive(uint8_t *pData, uint16_t Size);
public slots:
    void onRotaryTopLeftChanged(double);
    void onRotaryTopLeftButtonChanged(bool);
    void onRotaryTopRightChanged(double);
    void onRotaryTopRightButtonChanged(bool);
    void onRotaryBottomLeftChanged(double);
    void onRotaryBottomLeftButtonChanged(bool);
    void onRotaryBottomRightChanged(double);
    void onRotaryBottomRightButtonChanged(bool);
    void onButtonP5VChanged(bool);
    void onButtonVADJChanged(bool);
    void onButtonExtraChanged(bool);
    void onButtonPSUChanged(bool);
signals:
    void LedP5VChanged(bool);
    void LedVADJChanged(bool);
    void LedPSUChanged(bool);
    void BuzzerChanged(bool);
protected:
    void setButton(unsigned button, bool val);
    void updateRotary(unsigned index, double value);
    void updateIndicators(uint8_t newvalue);



private:
    unsigned ptr;
    uint8_t regs[18];
};

#endif
