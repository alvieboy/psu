#ifndef __I2CBUS_H__
#define __I2CBUS_H__
#include "stm32f1xx_hal_i2c.h"
#include "dev_i2c.h"
#include <QMap>
#include "I2CDevice.h"

class I2CBus
{
public:
    I2CBus(I2C_TypeDef *i2c);
    void attach(uint16_t address, I2CDevice*);
private:


    static int wrap_transmit(void *, uint16_t DevAddress, const uint8_t *pData, uint16_t Size);
    static int wrap_receive(void *, uint16_t DevAddress, uint8_t *pData, uint16_t Size);

    int transmit(uint16_t DevAddress, const uint8_t *pData, uint16_t Size);
    int receive(uint16_t DevAddress, uint8_t *pData, uint16_t Size);

    I2C_TypeDef *i2c;
    QMap<uint16_t, I2CDevice*> devices;
};

#endif
