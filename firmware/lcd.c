/*
  ******************************************************************************
  * (C) 2018 Alvaro Lopes <alvieboy@alvie.com>
  *
  * Redistribution and use in source and binary forms, with or without modification,
  * are permitted provided that the following conditions are met:
  *   1. Redistributions of source code must retain the above copyright notice,
  *      this list of conditions and the following disclaimer.
  *   2. Redistributions in binary form must reproduce the above copyright notice,
  *      this list of conditions and the following disclaimer in the documentation
  *      and/or other materials provided with the distribution.
  *   3. Neither the name of Alvaro Lopes nor the names of contributors
  *      may be used to endorse or promote products derived from this software
  *      without specific prior written permission.
  *
  * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
  * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
  * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
  * DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
  * FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
  * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
  * SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
  * CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
  * OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
  * OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
  *
  ******************************************************************************
  */

#include "lcd.h"
#include "defs.h"
#include "spi.h"
#include <stm32f1xx_hal.h>
#include <stm32f1xx_hal_gpio.h>
#include "uart.h"
#include "font.h"

//extern const unsigned char __tomthumb_bitmap__[];



void lcd__fillScreen();

void lcd__setreset(unsigned val)
{
    HAL_GPIO_WritePin( LCD_RST_PORT, LCD_RST_PIN, val);
}

static void lcd__setdcrs(int value)
{
    HAL_GPIO_WritePin( LCD_DC_PORT, LCD_DC_PIN, value);
}

static void lcd__setcs(int value)
{
    HAL_GPIO_WritePin( LCD_CS_PORT, LCD_CS_PIN, value);
}

static void lcd__sendCommand(uint8_t index)
{
    lcd__setdcrs(0);
    lcd__setcs(0);
    spi__tx(&index,1);
    lcd__setcs(1);
}

static void lcd__sendData8(uint8_t data)
{
    lcd__setdcrs(1);
    lcd__setcs(0);
    spi__tx(&data,1);
    lcd__setcs(1);
}

static void lcd__sendData16(uint16_t data)
{
    uint8_t v;
    lcd__setdcrs(1);
    lcd__setcs(0);
    v=data>>8;
    spi__tx((uint8_t*)&v,1);
    v=data & 0xff;
    spi__tx((uint8_t*)&v,1);
    //SPI.transfer(data>>8);
    //SPI.transfer(data&0xff);
    lcd__setcs(1);
}
#if 0

static uint8_t lcd__readRegister(uint8_t address, uint8_t param)
{
    uint8_t data, txdata;

    lcd__sendCommand(0xd9);
    lcd__sendData8(0x10+param);
    lcd__setdcrs(0);
    lcd__setcs(0);
    spi__tx(&address,1);
    lcd__setdcrs(1);
    lcd__setcs(0);
    txdata = 0xa5;
    spi__txrx(&txdata, &data, 1);
    lcd__setcs(1);
    return data;
}
#endif

#if 0
static uint8_t spi__readID(void)
{
    int i;
    uint8_t data[3];
    uint8_t expectedId[3] = {0x00, 0x93, 0x41};
    int valid=1;
    outstring("Reading SPI id\r\n");
    for(i=0;i<3;i++)
    {
        data[i]=lcd__readRegister(0xd3,i+1);
        if(data[i] != expectedId[i])
        {
            valid=0;
        }
    }
    if(!valid)
    {
        outstring("Read TFT ID failed, ID should be 0x009341, but read ID = 0x");
        for(i=0;i<3;i++)
        {
            printhexbyte(data[i]);
        }
        outstring("\r\n");
    }
    return valid;
}
#endif

static void lcd__initLCD(void)
{
#if 1
    lcd__setreset(LCD_RESET_POLARITY);
    HAL_Delay(200);
    lcd__setreset(!LCD_RESET_POLARITY);
    HAL_Delay(150);
#endif

    lcd__setcs(1);
    lcd__setdcrs(1);

    //lcd__setled(0);
    HAL_Delay(100);
    //lcd__setled(1);

#if 0
    int i;
    for (i=0;i<30;i++) {
        if (spi__readID()==1)
            break;
        HAL_Delay(500);
    }
#endif
    lcd__sendCommand(0x01);
    HAL_Delay(200);

    lcd__sendCommand(0xCF);
    lcd__sendData8(0x00);
    lcd__sendData8(0x8B);
    //sendData8(0xC1);
    lcd__sendData8(0X30);

    lcd__sendCommand(0xED);
    lcd__sendData8(0x67);
    lcd__sendData8(0x03);
    lcd__sendData8(0X12);
    lcd__sendData8(0X81);

    lcd__sendCommand(0xE8);
    lcd__sendData8(0x85);
    lcd__sendData8(0x10);
    lcd__sendData8(0x7A);

    lcd__sendCommand(0xCB);
    lcd__sendData8(0x39);
    lcd__sendData8(0x2C);
    lcd__sendData8(0x00);
    lcd__sendData8(0x34);
    lcd__sendData8(0x02);

    lcd__sendCommand(0xF7);
    // Pump ratio: DDVDH=2*VCI
    lcd__sendData8(0x20);

    lcd__sendCommand(0xEA);
    lcd__sendData8(0x00);
    lcd__sendData8(0x00);

    lcd__sendCommand(0xC0);                                                      /* Power control                */
    //sendData8(0x1B);                                                   /* VRH[5:0]                     */
    // GVDD=4V
    lcd__sendData8(0x23);                                                   /* VRH[5:0]                     */

    lcd__sendCommand(0xC1);
    /* Power control                */
    //(DDVDH - 0.2) V.
    // DDVDH = VCI*2 (6.6V), but should be max. 5.8V? That gives 2.9V for VCI...
    // Note6 in page 236 states DDVDH=5.8 with VCI=3.3V... It also refers a 5% Voltage Drop.
    // GVDD should be 3.0-5.0V


    lcd__sendData8(0x10);                                                   /* SAP[2:0];BT[3:0]             */

    lcd__sendCommand(0xC5);                                                      /* VCM control                  */
    // VCOMH=4.275V
    lcd__sendData8(0x3F);
    // VCOML=-1V
    lcd__sendData8(0x3C);

    lcd__sendCommand(0xC7);                                                      /* VCM control2                 */
    // VCOM offset= -9
    // Adafruit uses 0x86 (-58)
    lcd__sendData8(0XB7);


#define MADCTL_MH  0x04
#define MADCTL_BGR 0x08
#define MADCTL_ML 0x10
#define MADCTL_MV 0x20
#define MADCTL_MX 0x40
#define MADCTL_MY 0x80

    lcd__sendCommand(0x36);                                                      /* Memory Access Control        */
    lcd__sendData8(MADCTL_MV | MADCTL_MY |MADCTL_BGR);

    lcd__sendCommand(0x3A);
    lcd__sendData8(0x55);

    // Frame rate.
    lcd__sendCommand(0xB1);
    lcd__sendData8(0x00);
    lcd__sendData8(0x1B);

    lcd__sendCommand(0xB6);                                                      /* Display Function Control     */
    lcd__sendData8(0x0A);
    lcd__sendData8(0xA2);

#if 1

    lcd__sendCommand(0xF2);                                                      /* 3Gamma Function Disable      */
    lcd__sendData8(0x00);

    lcd__sendCommand(0x26);                                                      /* Gamma curve selected         */
    lcd__sendData8(0x01);

    lcd__sendCommand(0xE0);                                                      /* Set Gamma                    */
#if GAMMA0
    lcd__sendData8(0x0F);
    lcd__sendData8(0x2A);
    lcd__sendData8(0x28);
    lcd__sendData8(0x08);
    lcd__sendData8(0x0E);
    lcd__sendData8(0x08);
    lcd__sendData8(0x54);
    lcd__sendData8(0XA9);
    lcd__sendData8(0x43);
    lcd__sendData8(0x0A);
    lcd__sendData8(0x0F);
    lcd__sendData8(0x00);
    lcd__sendData8(0x00);
    lcd__sendData8(0x00);
    lcd__sendData8(0x00);
#else
    lcd__sendData8(0x0F);
    lcd__sendData8(0x31);
    lcd__sendData8(0x2B);
    lcd__sendData8(0x0C);
    lcd__sendData8(0x0E);
    lcd__sendData8(0x08);
    lcd__sendData8(0x4E);
    lcd__sendData8(0xF1);
    lcd__sendData8(0x37);
    lcd__sendData8(0x07);
    lcd__sendData8(0x10);
    lcd__sendData8(0x03);
    lcd__sendData8(0x0E);
    lcd__sendData8(0x09);
    lcd__sendData8(0x00);
#endif

    lcd__sendCommand(0XE1);                                                      /* Set Gamma                    */
#if GAMMA0
    lcd__sendData8(0x00);
    lcd__sendData8(0x15);
    lcd__sendData8(0x17);
    lcd__sendData8(0x07);
    lcd__sendData8(0x11);
    lcd__sendData8(0x06);
    lcd__sendData8(0x2B);
    lcd__sendData8(0x56);
    lcd__sendData8(0x3C);
    lcd__sendData8(0x05);
    lcd__sendData8(0x10);
    lcd__sendData8(0x0F);
    lcd__sendData8(0x3F);
    lcd__sendData8(0x3F);
    lcd__sendData8(0x0F);
#else
    lcd__sendData8(0x00);
    lcd__sendData8(0x0E);
    lcd__sendData8(0x14);
    lcd__sendData8(0x03);
    lcd__sendData8(0x11);
    lcd__sendData8(0x07);
    lcd__sendData8(0x31);
    lcd__sendData8(0xC1);
    lcd__sendData8(0x48);
    lcd__sendData8(0x08);
    lcd__sendData8(0x0F);
    lcd__sendData8(0x0C);
    lcd__sendData8(0x31);
    lcd__sendData8(0x36);
    lcd__sendData8(0x0F);
#endif

#endif

    lcd__sendCommand(0x51);
    lcd__sendData8(0xff);
    lcd__sendCommand(0x20);

    lcd__sendCommand(0x11);
    /* Exit Sleep                   */
    HAL_Delay(120);

    lcd__sendCommand(0x29);
    /* Display on                   */

    //lcd__clear();
}

static void lcd__setCol(int StartCol,int EndCol)
{
    lcd__sendCommand(0x2A);                                                      /* Column Command address       */
    lcd__sendData16(StartCol);
    lcd__sendData16(EndCol);
}

static void lcd__setPage(int StartPage,int EndPage)
{
    lcd__sendCommand(0x2B);                                                      /* Column Command address       */
    lcd__sendData16(StartPage);
    lcd__sendData16(EndPage);
}

void lcd__clear(uint16_t color)
{
    int x, y;
    lcd__setPage(0, 239);
    lcd__setCol(0, 319);
    lcd__sendCommand(0x2c);
    lcd__setdcrs(1);
    lcd__setcs(0);
    spi__setmode16();
    for (y=0;y<240;y++) {
        for (x=0;x<320;x++) {
            spi__fasttx16(color);
        }
    }
    spi__waitnotbusy();
    lcd__setcs(1);
    spi__setmode8();
}

void lcd__drawChar16(const font_t *font, int x, int y, uint8_t ch, uint16_t fg, uint16_t bg)
{
    uint16_t v = 0xffff;
    ch -= font->start;

    uint16_t mask = (1<<(font->w-1));
    const uint8_t *ptr = &font->bitmap[(unsigned)ch*2*font->h];

    lcd__setCol(x, x+font->w-1);
    lcd__setPage(y, y+font->w-1);
    lcd__sendCommand(0x2c);
    lcd__setdcrs(1);
    lcd__setcs(0);
    spi__setmode16();
    for (int cy=0;cy<font->h;cy++) {
        uint16_t row = ((uint16_t)ptr[0]<<8) + ptr[1];
        ptr+=2;

        for (int cx=0;cx<font->w;cx++) {
            if (row&mask) v=fg; else v=bg;
            row<<=1;
            spi__fasttx16(v);
        }
        y++;
    }
    spi__waitnotbusy();

    lcd__setcs(1);
    spi__setmode8();
}

void lcd__drawChar8(const font_t *font, int x, int y, uint8_t ch, uint16_t fg, uint16_t bg)
{
    uint16_t v = 0xffff;
    ch -= font->start;

    const uint8_t *ptr = &font->bitmap[(unsigned)ch*font->h];

    lcd__setCol(x, x+font->w-1);
    lcd__setPage(y, y+font->h-1);
    lcd__sendCommand(0x2c);
    lcd__setdcrs(1);
    lcd__setcs(0);
    spi__setmode16();
    for (int cy=0;cy<font->h;cy++) {
        uint8_t row = *ptr++;
        for (int cx=0;cx<font->w;cx++) {
            if (row&0x80) v=fg; else v=bg;
            row<<=1;
            spi__fasttx16(v);
        }
        y++;
    }
    spi__waitnotbusy();

    lcd__setcs(1);
    spi__setmode8();
}

void lcd__drawChar32(const font_t *font, int x, int y, uint8_t ch, uint16_t fg, uint16_t bg)
{
    uint16_t v = 0xffff;

    ch -= '-';
    const uint8_t *ptr = &font->bitmap[(unsigned)ch*4*font->h];

    lcd__setCol(x, x+font->w-1);
    lcd__setPage(y, y+font->h-1);
    lcd__sendCommand(0x2c);
    lcd__setdcrs(1);
    lcd__setcs(0);
    spi__setmode16();
    for (int cy=0;cy<font->h;cy++) {
        uint32_t row =
            ((uint32_t)ptr[0]<<24) +
            ((uint32_t)ptr[1]<<16) +
            ((uint32_t)ptr[2]<<8) +
            ptr[3];
        ptr+=4;

        for (int cx=0;cx<font->h;cx++) {
            if (row&0x80000000) v=fg; else v=bg;
            row<<=1;
            spi__fasttx16(v);
        }
        //spi__wait();
        y++;
       // lcd__setPage(y, y+8);
    }
    spi__waitnotbusy();

    lcd__setcs(1);
    spi__setmode8();
}

void lcd__drawChar24(const font_t *font, int x, int y, uint8_t ch, uint16_t fg, uint16_t bg)
{
    uint16_t v = 0xffff;

    ch -= '-';
    const uint8_t *ptr = &font->bitmap[(unsigned)ch*3*font->h];

    lcd__setCol(x, x+font->w-1);
    lcd__setPage(y, y+font->h-1);
    lcd__sendCommand(0x2c);
    lcd__setdcrs(1);
    lcd__setcs(0);
    spi__setmode16();
    for (int cy=0;cy<font->h;cy++) {
        uint32_t row =
            ((uint32_t)ptr[0]<<16) +
            ((uint32_t)ptr[1]<<8) +
            ((uint32_t)ptr[2]<<0);
        ptr+=3;

        for (int cx=0;cx<font->w;cx++) {
            if (row&0x800000) v=fg; else v=bg;
            row<<=1;
            spi__fasttx16(v);
        }
        //spi__wait();
        y++;
       // lcd__setPage(y, y+8);
    }
    spi__waitnotbusy();
    lcd__setcs(1);
    spi__setmode8();
}

void lcd__drawString(const font_t*font,int x, int y, const char *ch, uint16_t fg, uint16_t bg)
{
    int xpos=x;
    while (*ch) {
        lcd__drawChar(font,xpos,y,(uint32_t)*ch,fg,bg);
        if (font->spacing) {
            xpos+=font->spacing(font, *ch);
        } else {
            xpos+=font->w;
        }
        ch++;
    }
}

void lcd__drawFilledRect(int x, int y, int w, int h, uint16_t color)
{
    unsigned count = w*h;
    lcd__setCol(x, x+w-1);
    lcd__setPage(y, y+h-1);
    lcd__sendCommand(0x2c);
    lcd__setdcrs(1);
    lcd__setcs(0);
    spi__setmode16();
    while (count--) {
        spi__fasttx16(color);
    }
    spi__waitnotbusy();
    lcd__setcs(1);
    spi__setmode8();
}

void lcd__drawRect(int x, int y, int w, int h, uint16_t color)
{
    lcd__drawFilledRect(x,y,w,1,color);   // H line
    lcd__drawFilledRect(x,y+h,w,1,color); // H line
    if (h>2) {
        lcd__drawFilledRect(x,y,1,h,color);   // V line
        lcd__drawFilledRect(x+w,y,1,h,color);   // V line
    }
}

void lcd__init()
{
    lcd__initLCD();
}
