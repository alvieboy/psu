/*
  ******************************************************************************
  * (C) 2018 Alvaro Lopes <alvieboy@alvie.com>
  *
  * Redistribution and use in source and binary forms, with or without modification,
  * are permitted provided that the following conditions are met:
  *   1. Redistributions of source code must retain the above copyright notice,
  *      this list of conditions and the following disclaimer.
  *   2. Redistributions in binary form must reproduce the above copyright notice,
  *      this list of conditions and the following disclaimer in the documentation
  *      and/or other materials provided with the distribution.
  *   3. Neither the name of Alvaro Lopes nor the names of contributors
  *      may be used to endorse or promote products derived from this software
  *      without specific prior written permission.
  *
  * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
  * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
  * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
  * DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
  * FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
  * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
  * SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
  * CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
  * OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
  * OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
  *
  ******************************************************************************
  */
#ifndef __CALIBRATION_H__
#define __CALIBRATION_H__

#include <stdbool.h>
#include "adc.h"

#define ADJUST_ALT 1

#define CALIBRATION_ENTRIES NUM_CHANNELS

typedef enum {
    MEASUREMENT_5V_VOLTAGE=0,
    MEASUREMENT_5V_CURRENT,
    MEASUREMENT_PSU_VOLTAGE,
    MEASUREMENT_PSU_CURRENT,
    MEASUREMENT_VPOS_VOLTAGE,
    MEASUREMENT_VNEG_VOLTAGE
} measurement_t;

#define CAL_NUM_MEASUREMENTS 6

struct cal_values {
    int32_t mul;
    int32_t offset;
};

typedef enum {
    V_MEASURE,
    A_MEASURE,
    A_ADJ_MEASURE
} calibration_type;

struct calibration
{
    calibration_type type;
    const char *name;
    void *data;
};

struct calibration_voltagecurrent_measurement
{
    uint8_t adc_input;
    int8_t rotary_adj_index;
    struct cal_values *cal;
};

struct calibration_current_adj_measurement
{
    uint8_t adc_current_input;
    int8_t rotary_current_adj_index;
    struct cal_values *ical0;
    struct cal_values *ical1;

    uint8_t adc_voltage_input;
    uint8_t rotary_voltage_adj_index;
    struct cal_values *vcal0;
    struct cal_values *vcal1;
};

void calibration__init(void);
int calibration__running(void);
void calibration__select(measurement_t);
void calibration__stop(void);
void calibration__start(void);
void calibration__redraw(void);
void calibration__draw_initial(void);
void calibration__on_rotary_changed_delta(uint8_t, int);
int calibration__get(uint8_t index, struct cal_values *dest);
void calibration__force_redraw(void);
int calibration__store(void);
int calibration__storedefaults(void);
void calibration__rotary_bottom_left_on_long_button_press(void);
void calibration__calculate_values(uint32_t *samples, uint32_t conf);
fixed_t calibration__get_measurement(measurement_t index);
void calibration__debugonce(void);
const struct calibration *calibration__getcal(unsigned);

#endif
