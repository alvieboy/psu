/*
  ******************************************************************************
  * (C) 2018 Alvaro Lopes <alvieboy@alvie.com>
  *
  * Redistribution and use in source and binary forms, with or without modification,
  * are permitted provided that the following conditions are met:
  *   1. Redistributions of source code must retain the above copyright notice,
  *      this list of conditions and the following disclaimer.
  *   2. Redistributions in binary form must reproduce the above copyright notice,
  *      this list of conditions and the following disclaimer in the documentation
  *      and/or other materials provided with the distribution.
  *   3. Neither the name of Alvaro Lopes nor the names of contributors
  *      may be used to endorse or promote products derived from this software
  *      without specific prior written permission.
  *
  * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
  * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
  * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
  * DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
  * FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
  * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
  * SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
  * CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
  * OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
  * OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
  *
  ******************************************************************************
  */

#include "panel.h"
#include "i2c.h"
#include <string.h>

static unsigned char panel_regs[17];
static unsigned char prev_panel_regs[17];
static int8_t scan_seq;
static int8_t indicator_latch;

int panel__init(void)
{
    scan_seq = -2;
    i2c__enable();
    // Clear all indicators
    i2c__writeregister8(0x06, 17, 0);
    indicator_latch=0;
    i2c__disable();
    return 0;
}


static int panel__readallregisters(void)
{
    int r = -1;
    memcpy(prev_panel_regs, panel_regs, sizeof(panel_regs));
    i2c__enable();
    if (i2c__readregister8block(0x07, 0, 17, panel_regs)==0) {
        if (scan_seq<0)
            scan_seq++;
        r=0;
#if 0
        {
            int i;
            printf("Regs: ");
            for (i=0;i<17;i++) {
                printf("%02x ", panel_regs[i]);
            }
            printf("\n");
        }
#endif
    }
    i2c__disable();
    return r;
}

int panel__buttonsvalid(void)
{
    return scan_seq==0;
}


int panel__read(void)
{
    if (panel__readallregisters()==0) {
        return 0;
    }
    return -1;
}

uint8_t panel__getreg(uint8_t reg)
{
    return panel_regs[reg];
}

uint8_t panel__getbuttonstate(void)
{
    return panel_regs[0];
}

#if 0
static uint8_t panel__getbuttonevents(uint8_t button)
{
    return panel_regs[button+5];
}
#endif

uint8_t panel__getbuttondeltaevents(uint8_t button)
{
    return panel_regs[button+5] - prev_panel_regs[button+5];
}

static void panel__sendindicators(uint8_t value)
{
    i2c__enable();
    i2c__writeregister8(0x06, 17, value);
    indicator_latch = value;
    i2c__disable();
}

void panel__setindicator(uint8_t indicator, uint8_t val)
{
    uint8_t new_indicator;
    if (val) {
        new_indicator = indicator_latch | (1<<indicator);
    } else {
        new_indicator = indicator_latch & ~(1<<indicator);
    }
    if (new_indicator!=indicator_latch) {
        panel__sendindicators(new_indicator);
    }

}

void panel__enableindicator(uint8_t indicator)
{
    panel__setindicator(indicator,1);
}

void panel__disableindicator(uint8_t indicator)
{
    panel__setindicator(indicator,0);
}

