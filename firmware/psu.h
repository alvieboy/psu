/*
  ******************************************************************************
  * (C) 2018 Alvaro Lopes <alvieboy@alvie.com>
  *
  * Redistribution and use in source and binary forms, with or without modification,
  * are permitted provided that the following conditions are met:
  *   1. Redistributions of source code must retain the above copyright notice,
  *      this list of conditions and the following disclaimer.
  *   2. Redistributions in binary form must reproduce the above copyright notice,
  *      this list of conditions and the following disclaimer in the documentation
  *      and/or other materials provided with the distribution.
  *   3. Neither the name of Alvaro Lopes nor the names of contributors
  *      may be used to endorse or promote products derived from this software
  *      without specific prior written permission.
  *
  * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
  * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
  * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
  * DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
  * FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
  * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
  * SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
  * CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
  * OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
  * OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
  *
  ******************************************************************************
  */

#ifndef __PSU_H__
#define __PSU_H__

/* Pin mappings. As per r1 PCB

NOTE NOTE NOTE: disable SWO and JTAG

+-----------------------------+------+-----+-----------+
| Name                        | Port | Pin | Conf      |
+-----------------------------+------+-----+-----------+
| PSU_VOLTAGE_SENSE           | A    | 0   | Analog    |
| 5V_CURRENT_SENSE            | A    | 1   | Analog    |
| 5V_SENSE                    | A    | 2   | Analog    |
| PSU_CURRENT_SENSE           | A    | 3   | Analog    |
| VPOS_SENSE                  | A    | 4   | Analog    |
| VNEG_SENSE                  | A    | 5   | Analog    |
| PSU_OUT_ON                  | A    | 6   | PP        |
| 5V_OUT_ON                   | A    | 7   | PP        |
| VOUT_ON                     | A    | 8   | PP        |
| UART_TX                     | A    | 9   | Alt/PP    |
| UART_RX                     | A    | 10  | Alt/PP    |
| USB_DM                      | A    | 11  | Alt/Input |
| USB_DP                      | A    | 12  | Alt/Input |
| LED_ ON                     | A    | 13  | PP        |
| ONEWIRE                     | A    | 14  | OD*       |
| SPI_CS0                     | A    | 15  | Remap/PP  |
+-----------------------------+------+-----+-----------+
|                             | B    | 0   | Input     |
|                             | B    | 1   | Input     |
| BOOT1                       | B    | 2   | --------- |
| SCK                         | B    | 3   | Remap/PP  |
| MISO                        | B    | 4   | Remap/PP  |
| MOSI                        | B    | 5   | Remap/PP  |
| SCL                         | B    | 6   | Alt/OD    |
| SDA                         | B    | 7   | Alt/OD    |
| DC                          | B    | 8   | PP        |
|                             | B    | 9   | --------- |
|                             | B    | 10  | Input     |
| RST                         | B    | 11  | PP        |
| FAN_PWM                     | B    | 12  | --------- |
| FAN_TACH                    | B    | 13  | --------- |
| SPI_CS2                     | B    | 14  | PP        |
| SPI_CS1                     | B    | 15  | PP        |
+-----------------------------+------+-----+-----------+
|                             | C    | 13  | --------- |
| BOOTLOAD                    | C    | 14  | Input     |
+-----------------------------+------+-----+-----------+

*/

#define PSU_OUT_ON_PORT GPIOA
#define PSU_OUT_ON_PIN  GPIO_PIN_6
#define P5V_OUT_ON_PORT GPIOA
#define P5V_OUT_ON_PIN  GPIO_PIN_7
#define VOUT_ON_PORT    GPIOA
#define VOUT_ON_PIN     GPIO_PIN_8
#define LED_ON_PORT     GPIOA
#define LED_ON_PIN      GPIO_PIN_13
#define ONEWIRE_PORT    GPIOA
#define ONEWIRE_PIN     GPIO_PIN_14
#define SPI_CS0_PORT    GPIOA
#define SPI_CS0_PIN     GPIO_PIN_15

#define LCD_DC_PORT     GPIOB
#define LCD_DC_PIN      GPIO_PIN_8
#define LCD_RST_PORT    GPIOB
#define LCD_RST_PIN     GPIO_PIN_11
#define SPI_CS2_PORT    GPIOB
#define SPI_CS2_PIN     GPIO_PIN_14
#define SPI_CS1_PORT    GPIOB
#define SPI_CS1_PIN     GPIO_PIN_15

#define I2C_SCL_PORT    GPIOB
#define I2C_SCL_PIN     GPIO_PIN_6
#define I2C_SDA_PORT    GPIOB
#define I2C_SDA_PIN     GPIO_PIN_7


#define LCD_CS_PORT     SPI_CS0_PORT
#define LCD_CS_PIN      SPI_CS0_PIN

#endif

