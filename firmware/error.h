#ifndef __ERROR_H__
#define __ERROR_H__

#include <inttypes.h>

void error__flag(uint8_t index);
const char *error__strerror(int errno);

#define ERROR_EEPROM_COMMS -2
#define ERROR_EEPROM_HEADER -3
#define ERROR_EEPROM_CRC -4



#endif
