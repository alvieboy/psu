/*
  ******************************************************************************
  * (C) 2018 Alvaro Lopes <alvieboy@alvie.com>
  *
  * Redistribution and use in source and binary forms, with or without modification,
  * are permitted provided that the following conditions are met:
  *   1. Redistributions of source code must retain the above copyright notice,
  *      this list of conditions and the following disclaimer.
  *   2. Redistributions in binary form must reproduce the above copyright notice,
  *      this list of conditions and the following disclaimer in the documentation
  *      and/or other materials provided with the distribution.
  *   3. Neither the name of Alvaro Lopes nor the names of contributors
  *      may be used to endorse or promote products derived from this software
  *      without specific prior written permission.
  *
  * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
  * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
  * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
  * DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
  * FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
  * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
  * SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
  * CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
  * OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
  * OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
  *
  ******************************************************************************
  */

#include "calibration.h"
#include "adc.h"
#include <string.h>
#include "lcd.h"
#include "input.h"
#include "store.h"
#include "uart.h"
#include "debug.h"
#include "input.h"

#undef TEST_VALUES

static bool cal_running;

#define ADC_INPUT_PSU_VOLTAGE  0
#define ADC_INPUT_PSU_CURRENT  3
#define ADC_INPUT_5V_VOLTAGE   2
#define ADC_INPUT_5V_CURRENT   1
#define ADC_INPUT_VPOS_VOLTAGE 4
#define ADC_INPUT_VNEG_VOLTAGE 5
#define ADC_INPUT_TEMP_VOLTAGE 6
#define ADC_INPUT_VREF_VOLTAGE 7

#define CAL_INPUT_PSU_VOLTAGE  0
#define CAL_INPUT_PSU_CURRENT  1
#define CAL_INPUT_5V_VOLTAGE   2
#define CAL_INPUT_5V_CURRENT   3
#define CAL_INPUT_VPOS_VOLTAGE 4
#define CAL_INPUT_VNEG_VOLTAGE 5
#define CAL_INPUT_PSU_VCURRADJ 6
#define CAL_INPUT_PSU_CURRENTX10 7
#define CAL_INPUT_PSU_VCURRADJX10 8

#define CAL_NUM_ENTRIES 9


static uint8_t cal_debug=0;

static struct cal_values calibration[CAL_NUM_ENTRIES];
static const struct cal_values default_calibration[CAL_NUM_ENTRIES] =
{
    { FLOAT2FP(9.53013865938645),  FLOAT2FP(-0.08) }, // PSU
    { FLOAT2FP(1.0), 0 },    // Current sense
    { FLOAT2FP(2.0), 0 }, // 5V
    { FLOAT2FP(1.838235294117647), FLOAT2FP(-0.012) }, // Current sense
    { FLOAT2FP(4.9), 0 },   // Vpos
    { FLOAT2FP(-4.699323931232229), 0}, // Vneg

    { 0x00000000, 0}, // PSU Current/Voltage adj
    { 0x01000000, 0}, // PSU current sense (x10)
    { 0x00000000, 0}, // PSU Current/Voltage adj (x10)
};

static fixed_t computed_values[CAL_NUM_MEASUREMENTS];

static struct calibration_voltagecurrent_measurement caldata_5v_voltage =
{
    ADC_INPUT_5V_VOLTAGE,
    -1,
    &calibration[CAL_INPUT_5V_VOLTAGE]
};

static struct calibration_voltagecurrent_measurement caldata_5v_current =
{
    .adc_input = ADC_INPUT_5V_CURRENT,
    .rotary_adj_index = -1,
    .cal = &calibration[CAL_INPUT_5V_CURRENT]
};

static struct calibration_voltagecurrent_measurement caldata_psu_voltage =
{
    ADC_INPUT_PSU_VOLTAGE,
    ROTARY_INDEX_TOP_LEFT,
    &calibration[CAL_INPUT_PSU_VOLTAGE]
};

static struct calibration_current_adj_measurement caldata_psu_current =
{
    ADC_INPUT_PSU_CURRENT,
    ROTARY_INDEX_TOP_RIGHT,
    &calibration[CAL_INPUT_PSU_CURRENT],
    &calibration[CAL_INPUT_PSU_CURRENTX10],
    ADC_INPUT_PSU_VOLTAGE,
    ROTARY_INDEX_TOP_LEFT,
    &calibration[CAL_INPUT_PSU_VCURRADJ],
    &calibration[CAL_INPUT_PSU_VCURRADJX10],
};

static struct calibration_voltagecurrent_measurement caldata_vpos_voltage =
{
    ADC_INPUT_VPOS_VOLTAGE,
    ROTARY_INDEX_BOTTOM_RIGHT,
    &calibration[CAL_INPUT_VPOS_VOLTAGE]
};

static struct calibration_voltagecurrent_measurement caldata_vneg_voltage =
{
    ADC_INPUT_VNEG_VOLTAGE,
    ROTARY_INDEX_BOTTOM_LEFT,
    &calibration[CAL_INPUT_VNEG_VOLTAGE]
};

static struct calibration cal_5v_voltage =
{
    .type = V_MEASURE,
    .name = "5V voltage output",
    .data = &caldata_5v_voltage
};

static struct calibration cal_5v_current =
{
    .type = A_MEASURE,
    .name = "5V current output",
    .data = &caldata_5v_current
};

static struct calibration cal_psu_voltage =
{
    .type = V_MEASURE,
    .name = "PSU voltage output",
    .data = &caldata_psu_voltage
};

static struct calibration cal_psu_current =
{
    .type = A_ADJ_MEASURE,
    .name = "PSU current",
    .data = &caldata_psu_current
};

static struct calibration cal_vpos_voltage =
{
    .type = V_MEASURE,
    .name = "VPOS voltage output",
    .data = &caldata_vpos_voltage
};

static struct calibration cal_vneg_voltage =
{
    .type = V_MEASURE,
    .name = "VNEG voltage output",
    .data = &caldata_vneg_voltage
};

static struct calibration *cal_meas[CAL_NUM_MEASUREMENTS] = {
    &cal_5v_voltage,
    &cal_5v_current,
    &cal_psu_voltage,
    &cal_psu_current,
    &cal_vpos_voltage,
    &cal_vneg_voltage
};

const struct calibration *calibration__getcal(unsigned index)
{
    if (index<sizeof(cal_meas)/sizeof(cal_meas[0])) {
        return cal_meas[index];
    }
    return NULL;
}

static void calibration__dump()
{
    outstring("Cal values: \r\n");
    int i;
    for (i=0;i<NUM_CHANNELS;i++) {
        printhexbyte(i);
        outstring(" M=");
        printhex( calibration[i].mul );
        outstring(" O=");
        printhex( calibration[i].offset );
        outstring("\r\n");

    }
}

int calibration__store(void)
{
    return store__set(STORE_OFFSET_CALIBRATION, (uint8_t*)calibration, sizeof(calibration));
}

int calibration__storedefaults(void)
{
    outstring("Calibration: store defaults\r\n");
    memcpy(calibration, default_calibration, sizeof(calibration));
    return calibration__store();
}


void calibration__init(void)
{
    if (store__valid()) {
        outstring("Loading cal values from store\r\n");
        store__get(STORE_OFFSET_CALIBRATION, (uint8_t*)
                   calibration, sizeof(calibration));
    } else {
        memcpy(calibration, default_calibration, sizeof(calibration));
    }
    outstring("Calibration loaded\r\n");
    calibration__dump();
}

int calibration__get(uint8_t index, struct cal_values *cal)
{
    if (index<NUM_CHANNELS) {
        *cal = calibration[index];
        return 0;
    }
    return -1;
}


int calibration__running(void)
{
    return cal_running;
}


static fixed_t calibration__calculate_simple( struct calibration_voltagecurrent_measurement *c, uint32_t *samples)
{
    // Get input
    uint32_t input = samples[ c->adc_input ];

    if (cal_debug) {
        debug__printf(" ADC input %d value %08x: ", c->adc_input, input);
        debug__putfp( input );
        debug__printf("\r\n  MUL 0x%08x offset 0x%08x\r\n", c->cal->mul, c->cal->offset);
    }
    // Multiply factor
    input = fmul(input, c->cal->mul);
    input += c->cal->offset;

    return input;
}

static fixed_t calibration__calculate_adjust( struct calibration_current_adj_measurement *c, uint32_t *samples, uint32_t configuration)
{
    // Get input
    uint32_t input = samples[ c->adc_current_input ];
    uint32_t input2 = samples[ c->adc_voltage_input ];

    if (cal_debug) {
        debug__printf(" ADC input current %d value %08x: ", c->adc_current_input, input);
        debug__putfp( input );
        debug__printf("\r\n");
        debug__printf(" ADC input voltage %d value %08x: ", c->adc_voltage_input, input2);
        debug__putfp( input2 );
        debug__printf("\r\n");
    }
    // Multiply factor
    if (configuration & ADJUST_ALT) {
        input = fmul(input, c->ical1->mul);
        input += c->ical1->offset;

        input2 = fmul(input2,c->vcal1->mul);
        input += input2;
    } else {
        input = fmul(input, c->ical0->mul);
        input += c->ical0->offset;
        input2 = fmul(input2, c->vcal0->mul);
        input += input2;
    }
    return input;
}


static fixed_t calibration__calculate(struct calibration *c, uint32_t *samples, uint32_t configuration)
{
    fixed_t val = 0;

    switch (c->type) {
    case V_MEASURE:
    case A_MEASURE:
        val = calibration__calculate_simple(c->data, samples);
        break;
    case A_ADJ_MEASURE:
        val = calibration__calculate_adjust(c->data, samples, configuration);
        break;
    default:
        break;
    }
    if (cal_debug)
        debug__printf(" Result: 0x%08x\r\n",val);

    return val;
}


fixed_t calibration__get_measurement(measurement_t c)
{
    if (c>=0 && c<CAL_NUM_MEASUREMENTS)
        return computed_values[c];
    return 0;
}

void calibration__calculate_values(uint32_t *samples, uint32_t configuration)
{
    unsigned measure;
    struct calibration *c;
    for (measure=0; measure<CAL_NUM_MEASUREMENTS; measure++) {
        // Compute values
        c = cal_meas[measure];
        if (cal_debug) {
            outstring("CL\r\n");
            debug__printf("Calibration for measurement %d (%s)\r\n",
                         measure, c->name);
        }
#ifdef TEST_VALUES
        switch(measure) {
        case MEASUREMENT_5V_VOLTAGE: computed_values[measure] = FLOAT2FP(29.2888); break;
        case MEASUREMENT_5V_CURRENT: computed_values[measure] = FLOAT2FP(6.2888); break;
        case MEASUREMENT_PSU_VOLTAGE:computed_values[measure] = FLOAT2FP(30.2888); break;
        case MEASUREMENT_PSU_CURRENT:computed_values[measure] = FLOAT2FP(31.2888); break;
        case MEASUREMENT_VPOS_VOLTAGE:computed_values[measure] = FLOAT2FP(28.2888); break;
        case MEASUREMENT_VNEG_VOLTAGE:computed_values[measure] = FLOAT2FP(-20.2888); break;

        }
#else
        computed_values[measure] = calibration__calculate(c, samples, configuration);
#endif
    }
    //cal_debug=0;
}

