/*
  ******************************************************************************
  * (C) 2018 Alvaro Lopes <alvieboy@alvie.com>
  *
  * Based on work Copyright (C) Tilen Majerle, 2015
  *
  * Redistribution and use in source and binary forms, with or without modification,
  * are permitted provided that the following conditions are met:
  *   1. Redistributions of source code must retain the above copyright notice,
  *      this list of conditions and the following disclaimer.
  *   2. Redistributions in binary form must reproduce the above copyright notice,
  *      this list of conditions and the following disclaimer in the documentation
  *      and/or other materials provided with the distribution.
  *   3. Neither the name of Alvaro Lopes nor the names of contributors
  *      may be used to endorse or promote products derived from this software
  *      without specific prior written permission.
  *
  * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
  * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
  * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
  * DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
  * FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
  * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
  * SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
  * CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
  * OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
  * OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
  *
  ******************************************************************************
  */

#include "ds18b20.h"
#include "onewire.h"

int ds18b20__start(const uint8_t *addr)
{
    /* Reset line */
    if (onewire__reset()!=0)
        return -1;

    onewire__select(addr);
    onewire__writebyte(DS18B20_CMD_CONVERTTEMP);

    return 0;
}

int ds18b20__start_all(void)
{
    /* Reset pulse */
    if (onewire__reset()!=0)
        return -1;
    /* Skip addr */
    onewire__writebyte(ONEWIRE_CMD_SKIPROM);
    /* Start conversion on all connected devices */
    onewire__writebyte(DS18B20_CMD_CONVERTTEMP);
    return 0;
}

int ds18b20__read(const uint8_t *addr, int16_t *dest)
{
    uint16_t temperature;
    unsigned i = 0;
    uint8_t data[9];
    /* Check if line is released, if it is, then conversion is complete */
    if (!onewire__readbit()) {
        /* Conversion is not finished yet */
        return 1;
    }

    /* Reset line */
    onewire__reset();
    /* Select ROM number */
    onewire__select(addr);
    /* Read scratchpad command by onewire protocol */
    onewire__writebyte(ONEWIRE_CMD_RSCRATCHPAD);

    /* Get data */
    for (i = 0; i < 9; i++) {
        /* Read byte by byte */
        data[i] = onewire__readbyte();
    }
#if 1
    /* Calculate CRC */
    uint8_t crc = onewire__crc8(data, 8);

    /* Check if CRC is ok */
    if (crc != data[8]) {
        /* CRC invalid */
        return -1;
    }
#endif
    /* First two bytes of scratchpad are temperature values */
    temperature = data[0] | (data[1] << 8);

    /* Reset line */
    onewire__reset();

    *dest = temperature;

    return 0;
}
#if 0
static int ds18b20__setresolution(const uint8_t *addr, uint8_t resolution)
{
    uint8_t th, tl, conf;

    if (onewire__reset()!=0)
        return -1;

    onewire__select(addr);
    onewire__writebyte(ONEWIRE_CMD_RSCRATCHPAD);

    /* Ignore first 2 bytes */
    (void)onewire__readbyte();
    (void)onewire__readbyte();

    th = onewire__readbyte();
    tl = onewire__readbyte();
    conf = onewire__readbyte();

    resolution &= 0x3;
    resolution <<= DS18B20_RESOLUTION_R0;

    conf &= ~(0x3 << DS18B20_RESOLUTION_R1);
    conf |= resolution;

    if (onewire__reset()!=0)
        return -1;

    onewire__select(addr);
    /* Write scratchpad command by onewire protocol, only th, tl and conf register can be written */
    onewire__writebyte(ONEWIRE_CMD_WSCRATCHPAD);

    /* Write bytes */
    onewire__writebyte(th);
    onewire__writebyte(tl);
    onewire__writebyte(conf);
    /* Reset line */
    if (onewire__reset()!=0)
        return -1;

    onewire__select(addr);
    onewire__writebyte(ONEWIRE_CMD_CPYSCRATCHPAD);

    return 0;
}
#endif

int ds18b20__address_match(const uint8_t *addr)
{
    /* Checks if first byte is equal to DS18B20's family code */
    if (addr[0] == DS18B20_FAMILY_CODE) {
        return 1;
    }
    return 0;
}

