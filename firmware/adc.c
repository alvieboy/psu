/*
  ******************************************************************************
  * (C) 2018 Alvaro Lopes <alvieboy@alvie.com>
  *
  * Redistribution and use in source and binary forms, with or without modification,
  * are permitted provided that the following conditions are met:
  *   1. Redistributions of source code must retain the above copyright notice,
  *      this list of conditions and the following disclaimer.
  *   2. Redistributions in binary form must reproduce the above copyright notice,
  *      this list of conditions and the following disclaimer in the documentation
  *      and/or other materials provided with the distribution.
  *   3. Neither the name of Alvaro Lopes nor the names of contributors
  *      may be used to endorse or promote products derived from this software
  *      without specific prior written permission.
  *
  * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
  * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
  * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
  * DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
  * FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
  * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
  * SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
  * CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
  * OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
  * OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
  *
  ******************************************************************************
  */

#include <stm32f1xx_hal_gpio.h>
#include <stm32f1xx_hal.h>
#include <stm32f1xx_hal_adc.h>
#include <stm32f1xx_hal_tim.h>
#include <stm32f1xx_hal_rcc.h>
#include "uart.h"
#include "defs.h"
#include "adc.h"
#include "calibration.h"

void ADC1_2_IRQHandler(void);
void DMA1_Channel1_IRQHandler(void);

static uint32_t configuration = 0;

static ADC_HandleTypeDef  AdcHandle;
static DMA_HandleTypeDef  AnalogDmaHandle;


TIM_HandleTypeDef    TimHandle;
#define TIMER_FREQUENCY                ((uint32_t) 1000)    /* Timer frequency (unit: Hz). With a timer 16 bits and time base freq min 1Hz, range is min=1Hz, max=32kHz. */
#define TIMER_FREQUENCY_RANGE_MIN      ((uint32_t)    1)    /* Timer minimum frequency (unit: Hz). With a timer 16 bits, maximum frequency will be 32000 times this value. */
#define TIMER_PRESCALER_MAX_VALUE      (0xFFFF-1)           /* Timer prescaler maximum value (0xFFFF for a timer 16 bits) */

#define ADC_TIMx                            TIM3    /* Caution: Timer instance must be on APB1 (clocked by PCLK1) due to frequency computation in function "TIM_Config()" */
#define ADC_TIMx_CLK_ENABLE()               __HAL_RCC_TIM3_CLK_ENABLE()

#define ADC_TIMx_FORCE_RESET()              __HAL_RCC_TIM3_FORCE_RESET()
#define ADC_TIMx_RELEASE_RESET()            __HAL_RCC_TIM3_RELEASE_RESET()

#define ADC_EXTERNALTRIGCONV_Tx_TRGO    ADC_EXTERNALTRIGCONV_T3_TRGO

static void TIM_Config(void);

void adc__init(void)
{
  ADC_ChannelConfTypeDef   sConfig;
  
  /* Configuration of ADCx init structure: ADC parameters and regular group */
  AdcHandle.Instance = ADC1;
  
  AdcHandle.Init.DataAlign             = ADC_DATAALIGN_RIGHT;
  AdcHandle.Init.ScanConvMode          = ADC_SCAN_ENABLE;               /* Sequencer disabled (ADC conversion on only 1 channel: channel set on rank 1) */
  AdcHandle.Init.ContinuousConvMode    = DISABLE;                       /* Continuous mode to have maximum conversion speed (no delay between conversions) */
  AdcHandle.Init.NbrOfConversion       = 8;
  AdcHandle.Init.DiscontinuousConvMode = DISABLE;
  AdcHandle.Init.NbrOfDiscConversion   = 8;

#if 0
  AdcHandle.Init.ExternalTrigConv      = ADC_EXTERNALTRIGCONV_Tx_TRGO;  /* Trig of conversion start done by external event */
#else
  AdcHandle.Init.ExternalTrigConv      = ADC_SOFTWARE_START;            /* Software start to trig the 1st conversion manually, without external event */
#endif

  if (HAL_ADC_Init(&AdcHandle) != HAL_OK)
  {
    /* ADC initialization error */
    Error_Handler();
  }
  
  /* Configuration of channel on ADCx regular group on sequencer rank 1 */
  /* Note: Considering IT occurring after each ADC conversion if ADC          */
  /*       conversion is out of the analog watchdog window selected (ADC IT   */
  /*       enabled), select sampling time and ADC clock with sufficient       */
  /*       duration to not create an overhead situation in IRQHandler.        */
  sConfig.Channel      = ADC_CHANNEL_0;
  sConfig.Rank         = ADC_REGULAR_RANK_1;
  sConfig.SamplingTime = ADC_SAMPLETIME_239CYCLES_5;//ADC_SAMPLETIME_71CYCLES_5; //ADC_SAMPLETIME_41CYCLES_5;
  
  if (HAL_ADC_ConfigChannel(&AdcHandle, &sConfig) != HAL_OK)
  {
    /* Channel Configuration Error */
    Error_Handler();
  }
#if 1
  sConfig.Channel      = ADC_CHANNEL_1;
  sConfig.Rank         = ADC_REGULAR_RANK_2;
  if (HAL_ADC_ConfigChannel(&AdcHandle, &sConfig) != HAL_OK)
  {
      /* Channel Configuration Error */
      Error_Handler();
  }

  sConfig.Channel      = ADC_CHANNEL_2;
  sConfig.Rank         = ADC_REGULAR_RANK_3;
  if (HAL_ADC_ConfigChannel(&AdcHandle, &sConfig) != HAL_OK)
  {
      /* Channel Configuration Error */
      Error_Handler();
  }

  sConfig.Channel      = ADC_CHANNEL_3;
  sConfig.Rank         = ADC_REGULAR_RANK_4;
  if (HAL_ADC_ConfigChannel(&AdcHandle, &sConfig) != HAL_OK)
  {
      /* Channel Configuration Error */
      Error_Handler();
  }

  sConfig.Channel      = ADC_CHANNEL_4;
  sConfig.Rank         = ADC_REGULAR_RANK_5;
  if (HAL_ADC_ConfigChannel(&AdcHandle, &sConfig) != HAL_OK)
  {
      /* Channel Configuration Error */
      Error_Handler();
  }

  sConfig.Channel      = ADC_CHANNEL_5;
  sConfig.Rank         = ADC_REGULAR_RANK_6;
  if (HAL_ADC_ConfigChannel(&AdcHandle, &sConfig) != HAL_OK)
  {
      /* Channel Configuration Error */
      Error_Handler();
  }

  sConfig.Channel      = ADC_CHANNEL_TEMPSENSOR;
  sConfig.Rank         = ADC_REGULAR_RANK_7;
  if (HAL_ADC_ConfigChannel(&AdcHandle, &sConfig) != HAL_OK)
  {
      /* Channel Configuration Error */
      Error_Handler();
  }

  sConfig.Channel      = ADC_CHANNEL_VREFINT;
  sConfig.Rank         = ADC_REGULAR_RANK_8;
  if (HAL_ADC_ConfigChannel(&AdcHandle, &sConfig) != HAL_OK)
  {
      /* Channel Configuration Error */
      Error_Handler();
  }
#endif
  TIM_Config();

}

static void TIM_Config(void)
{
  TIM_MasterConfigTypeDef master_timer_config;
  RCC_ClkInitTypeDef clk_init_struct = {0};       /* Temporary variable to retrieve RCC clock configuration */
  uint32_t latency;                               /* Temporary variable to retrieve Flash Latency */
  
  uint32_t timer_clock_frequency = 0;             /* Timer clock frequency */
  uint32_t timer_prescaler = 0;                   /* Time base prescaler to have timebase aligned on minimum frequency possible */
  
  /* Configuration of timer as time base:                                     */ 
  /* Caution: Computation of frequency is done for a timer instance on APB1   */
  /*          (clocked by PCLK1)                                              */
  /* Timer period can be adjusted by modifying the following constants:       */
  /* - TIMER_FREQUENCY: timer frequency (unit: Hz).                           */
  /* - TIMER_FREQUENCY_RANGE_MIN: timer minimum frequency (unit: Hz).         */
  ADC_TIMx_CLK_ENABLE();

  /* Retrieve timer clock source frequency */
  HAL_RCC_GetClockConfig(&clk_init_struct, &latency);
  /* If APB1 prescaler is different of 1, timers have a factor x2 on their    */
  /* clock source.                                                            */
  if (clk_init_struct.APB1CLKDivider == RCC_HCLK_DIV1)
  {
    timer_clock_frequency = HAL_RCC_GetPCLK1Freq();
  }
  else
  {
    timer_clock_frequency = HAL_RCC_GetPCLK1Freq() *2;
  }
  
  /* Timer prescaler calculation */
  /* (computation for timer 16 bits, additional + 1 to round the prescaler up) */
  timer_prescaler = (timer_clock_frequency / (TIMER_PRESCALER_MAX_VALUE * TIMER_FREQUENCY_RANGE_MIN)) +1;
  
  /* Set timer instance */
  TimHandle.Instance = ADC_TIMx;
  
  /* Configure timer parameters */
  TimHandle.Init.Period            = ((timer_clock_frequency / (timer_prescaler * TIMER_FREQUENCY)) - 1);
  TimHandle.Init.Prescaler         = (timer_prescaler - 1);
  TimHandle.Init.ClockDivision     = TIM_CLOCKDIVISION_DIV1;
  TimHandle.Init.CounterMode       = TIM_COUNTERMODE_UP;
  TimHandle.Init.RepetitionCounter = 0x0;
  
  if (HAL_TIM_Base_Init(&TimHandle) != HAL_OK)
  {
    /* Timer initialization Error */
    Error_Handler();
  }

  /* Timer TRGO selection */
  master_timer_config.MasterOutputTrigger = TIM_TRGO_UPDATE;
  master_timer_config.MasterSlaveMode = TIM_MASTERSLAVEMODE_DISABLE;

  if (HAL_TIMEx_MasterConfigSynchronization(&TimHandle, &master_timer_config) != HAL_OK)
  {
    /* Timer TRGO selection Error */
    Error_Handler();
  }
  outstring("Timer configured\r\n");
}


void adc__calibrate()
{
    /* Run the ADC calibration */
    if (HAL_ADCEx_Calibration_Start(&AdcHandle) != HAL_OK)
    {
        /* Calibration Error */
        Error_Handler();
    }
}

static __IO uint16_t   aADCxConvertedValues[ADCCONVERTEDVALUES_BUFFER_SIZE];

//static __IO fixed_t voltages[NUM_CHANNELS];


void adc__start()
{
    if (HAL_ADC_Start_DMA(&AdcHandle,
                          (uint32_t *)aADCxConvertedValues,
                          ADCCONVERTEDVALUES_BUFFER_SIZE
                         ) != HAL_OK)
    {
        /* Start Error */
        Error_Handler();
    }
}

void ADC1_2_IRQHandler(void)
{
    HAL_ADC_IRQHandler(&AdcHandle);
}

void DMA1_Channel1_IRQHandler(void)
{
    HAL_DMA_IRQHandler(AdcHandle.DMA_Handle);
}


/*

+----------------------+--------+-------+----------+
| Input                | in     | Vadc  | ADC conv |
+----------------------+--------+-------+----------+
| +5V                  | +5V    | 2.5V  | 3103     |
| +5V                  | +4V    | 2.0V  | 2482     |
| Current samplers     | 1A     | 0.333 | 414      |
| PSU                  | 15V    | 3.1637| 3926     |
| PSU                  | -15V   | 0.0058| 7        |
| PSU                  | 0V     | 1.5848| 1967     |
| Vpos                 | 12V    | 2.4489| 3039     |
| Vpos                 | 5V     | 1.0204| 1266     |
| Vneg                 | -12V   | 2.5532| 3169     |
| Vneg                 | -5V    | 1.0638| 1320     |
+----------------------+--------+-------+----------+

 */

#define ADC_CHANNEL_VREF 7u
#define ADC_RESOLUTION_BITS 12

fixed_t adc__rawtovoltage(uint32_t raw)
{
    unsigned vref = adc__getrawvalue(ADC_CHANNEL_VREF);

    /*
     * Ideal case: VCC=3.30000V, Vref=1.20000V.
     *
     *  adc_value = 4096 * (Vin/3.3)
     *
     *  Vin = (adc_value * Vadc) / 4096
     *
     *  Vadc is unknown.
     *  we know that ADC(VREF) = 4096* (1.2/Vadc) [1489@3.3V]
     *  Vadc = (4096*1.2) / ADC(VREF)
     *
     *
     *  Vadc = (4096*1.2) / ADC(VREF)
     *
     *  Vin = (adc_value * ((4096*1.2)/ADC(VREF) ) ) / 4096.
     *  Vin = adc_value * (1.2/ADC(VREF))
     *  VinS = adc_value * (S*1.2) / ADC(VREF)
     */

     unsigned scale = FLOAT2FP(1.2) / vref;
     return (raw * scale) + FLOAT2FP(0.00005);
}





#if ENABLE_AVERAGE

static uint32_t samplebuf[NUM_CHANNELS] = {0};
static uint8_t sampcount = 0;

#endif

void HAL_ADC_ConvCpltCallback(ADC_HandleTypeDef *AdcHandle)
{
    //outstring("Adc\r\n");
    // Convert
    //uint32_t sample32;

    unsigned i;
#if !(ENABLE_AVERAGE)
#error a
    for (i=0;i<NUM_CHANNELS;i++) {
        uint64_t sample = (uint64_t)adc__rawtovoltage(aADCxConvertedValues[i]);
        sample *= (uint64_t)calibration[i].mul;
        sample32 = sample>>32;
        sample32 += calibration[i].offset;
        voltages[i] = sample32;
    }
    adc__conversion_complete_callback();
#else
    for (i=0;i<NUM_CHANNELS;i++) {
        uint32_t sample = (uint32_t)aADCxConvertedValues[i];
        samplebuf[i] += sample;
    }

    sampcount++;
    if (sampcount==SAMPLES_TO_AVERAGE_COUNT) {
        sampcount = 0;
        for (i=0;i<NUM_CHANNELS;i++) {
            samplebuf[i] = adc__rawtovoltage(samplebuf[i] >> SAMPLES_TO_AVERAGE_SHIFT);
        }

        // This is destructive. :)
        calibration__calculate_values( samplebuf, configuration );
        adc__conversion_complete_callback();

        for (i=0;i<NUM_CHANNELS;i++) {
            samplebuf[i] = 0;
        }

    }


#endif
    
}

fixed_t adc__getvadc(void)
{
    int64_t v = 4095*1.2*FLOAT2FP(1.0);

    int32_t vadc = v / (int64_t)adc__getrawvalue(ADC_CHANNEL_VREF);
    return vadc;
}

unsigned adc__getrawvalue(unsigned index)
{
    return aADCxConvertedValues[index];
}

void HAL_ADC_MspInit(ADC_HandleTypeDef *hadc)
{
    RCC_PeriphCLKInitTypeDef  PeriphClkInit;

    /* Enable clock of ADCx peripheral */
    __HAL_RCC_ADC1_CLK_ENABLE();

    /* Configure ADCx clock prescaler */
    /* Caution: On STM32F1, ADC clock frequency max is 14MHz (refer to device   */
    /*          datasheet).                                                     */
    /*          Therefore, ADC clock prescaler must be configured in function   */
    /*          of ADC clock source frequency to remain below this maximum      */
    /*          frequency.                                                      */
    PeriphClkInit.PeriphClockSelection = RCC_PERIPHCLK_ADC;
    PeriphClkInit.AdcClockSelection = RCC_ADCPCLK2_DIV6;
    HAL_RCCEx_PeriphCLKConfig(&PeriphClkInit);

    /* Enable clock of DMA associated to the peripheral */
    __HAL_RCC_DMA1_CLK_ENABLE();

    /* Configure DMA parameters */
    AnalogDmaHandle.Instance = DMA1_Channel1;

    AnalogDmaHandle.Init.Direction           = DMA_PERIPH_TO_MEMORY;
    AnalogDmaHandle.Init.PeriphInc           = DMA_PINC_DISABLE;
    AnalogDmaHandle.Init.MemInc              = DMA_MINC_ENABLE;
    AnalogDmaHandle.Init.PeriphDataAlignment = DMA_PDATAALIGN_HALFWORD;   /* Transfer from ADC by half-word to match with ADC configuration: ADC resolution 10 or 12 bits */
    AnalogDmaHandle.Init.MemDataAlignment    = DMA_MDATAALIGN_HALFWORD;   /* Transfer to memory by half-word to match with buffer variable type: half-word */
    AnalogDmaHandle.Init.Mode                = DMA_CIRCULAR;              /* DMA in circular mode to match with ADC configuration: DMA continuous requests */
    AnalogDmaHandle.Init.Priority            = DMA_PRIORITY_HIGH;

    /* Deinitialize  & Initialize the DMA for new transfer */
    HAL_DMA_DeInit(&AnalogDmaHandle);
    HAL_DMA_Init(&AnalogDmaHandle);

    /* Associate the initialized DMA handle to the ADC handle */
    __HAL_LINKDMA(hadc, DMA_Handle, AnalogDmaHandle);

    /*##-4- Configure the NVIC #################################################*/

    /* NVIC configuration for DMA interrupt (transfer completion or error) */
    /* Priority: high-priority */
    HAL_NVIC_SetPriority(DMA1_Channel1_IRQn, 1, 0);
    HAL_NVIC_EnableIRQ(DMA1_Channel1_IRQn);


    /* NVIC configuration for ADC interrupt */
    /* Priority: high-priority */
    HAL_NVIC_SetPriority(DMA1_Channel1_IRQn, 0, 0);
    HAL_NVIC_EnableIRQ(DMA1_Channel1_IRQn);
    outstring("MSP init ADC\r\n");
}
