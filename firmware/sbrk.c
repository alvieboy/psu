#include "sbrk.h"
#include "uart.h"
#include <stm32f1xx_hal.h>

#define MEM_PAGESIZE 4096

extern int __bss_end__;
//extern int __Stack_Init;

static void *mend = 0;

static void brk__initialise()
{
    unsigned ptr = (unsigned)&__bss_end__;
    ptr+=(MEM_PAGESIZE-1);
    ptr&=~(MEM_PAGESIZE-1);

    mend = (void*)ptr;
}

void *_sbrk(int increment)
{
    if (mend==0)
        brk__initialise();

    void *ret = (void*)mend;
    increment += (MEM_PAGESIZE-1);
    increment &= (~(MEM_PAGESIZE-1));

    void *n_mend=(void*)((unsigned char*)mend + increment);
#if 0
    if (n_mend>(void*)&__Stack_Init)  {
        outstring("******** OUT OF MEMORY *********");
        outstring("Requested: 0x");
        printhex(increment);
        outstring("\r\n  mend: 0x");
        printhex((uint32_t)mend);
        outstring("\r\n  StackInit: 0x");
        printhex((uint32_t)__Stack_Init);

        return 0;
    }
#endif
    mend = n_mend;
    return ret;
}
