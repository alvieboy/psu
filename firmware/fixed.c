/*
  ******************************************************************************
  * (C) 2018 Alvaro Lopes <alvieboy@alvie.com>
  *
  * Redistribution and use in source and binary forms, with or without modification,
  * are permitted provided that the following conditions are met:
  *   1. Redistributions of source code must retain the above copyright notice,
  *      this list of conditions and the following disclaimer.
  *   2. Redistributions in binary form must reproduce the above copyright notice,
  *      this list of conditions and the following disclaimer in the documentation
  *      and/or other materials provided with the distribution.
  *   3. Neither the name of Alvaro Lopes nor the names of contributors
  *      may be used to endorse or promote products derived from this software
  *      without specific prior written permission.
  *
  * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
  * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
  * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
  * DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
  * FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
  * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
  * SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
  * CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
  * OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
  * OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
  *
  ******************************************************************************
  */

#include "fixed.h"

static char space = ' ';

void fixed2ascii_setspace(char c)
{
    space = c;
}

char* fixed2ascii(fixed_t fp, uint32_t fracbits, uint32_t decimal, int pad, char *dest)
{
    uint8_t chars[32];
    int32_t i, n;
    int needsign = 0;

    // Convert integer part
    i=0;
    if (fp<0) {
        fp*=-1;
        needsign=1;
    }
    n = fp >> fracbits;
    do {
        chars[i++] = '0' + n % 10;
        n /= 10;
    } while (n!=0);
    if (needsign)
        chars[i++]='-';

    if (pad>0) {
        for (n=i;n<=pad;n++) {
            *dest++=space;
        }
    }

    i--;
    for (; i >= 0; i--)
        *dest++=chars[i];
    if (decimal>0) {
        *dest++='.';

        // Convert fractional part
        for (i = 0; i < decimal; i++) {
            fp &= (1<<fracbits)-1;
            fp *= 10;
            *dest++=(fp >> fracbits) + '0';
        }
    }
    *dest='\0';
    return dest;
}



