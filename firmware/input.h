/*
  ******************************************************************************
  * (C) 2018 Alvaro Lopes <alvieboy@alvie.com>
  *
  * Redistribution and use in source and binary forms, with or without modification,
  * are permitted provided that the following conditions are met:
  *   1. Redistributions of source code must retain the above copyright notice,
  *      this list of conditions and the following disclaimer.
  *   2. Redistributions in binary form must reproduce the above copyright notice,
  *      this list of conditions and the following disclaimer in the documentation
  *      and/or other materials provided with the distribution.
  *   3. Neither the name of Alvaro Lopes nor the names of contributors
  *      may be used to endorse or promote products derived from this software
  *      without specific prior written permission.
  *
  * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
  * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
  * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
  * DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
  * FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
  * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
  * SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
  * CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
  * OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
  * OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
  *
  ******************************************************************************
  */

#ifndef __INPUT_H__
#define __INPUT_H__

#define ROTARY_INDEX_TOP_LEFT 3
#define ROTARY_INDEX_TOP_RIGHT 2
#define ROTARY_INDEX_BOTTOM_LEFT 0
#define ROTARY_INDEX_BOTTOM_RIGHT 1

#define BUTTON_ROTARY_TOP_LEFT 6
#define BUTTON_ROTARY_TOP_RIGHT 4
#define BUTTON_ROTARY_BOTTOM_LEFT 0
#define BUTTON_ROTARY_BOTTOM_RIGHT 2

#define BUTTON_LEFT 1
#define BUTTON_RIGHT 3
#define BUTTON_CENTER_LEFT 5
#define BUTTON_CENTER_RIGHT 7


void input__init(void);
void input__check(void);
void input__tick_10ms(void);
uint8_t input__getrawrotary(uint8_t index);
uint8_t input__getrotary(uint8_t index);

/* Callbacks */
int input__button_long_press_callback(uint8_t button);
int input__button_down_callback(uint8_t button);
//int input__button_up_callback(uint8_t button);
int input__button_clicked_callback(uint8_t button);
int input__rotary_changed_callback(uint8_t index, uint8_t value);
int input__rotary_changed_delta_callback(uint8_t index, int delta);

#endif
