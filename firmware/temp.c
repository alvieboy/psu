/*
  ******************************************************************************
  * (C) 2018 Alvaro Lopes <alvieboy@alvie.com>
  *
  * Redistribution and use in source and binary forms, with or without modification,
  * are permitted provided that the following conditions are met:
  *   1. Redistributions of source code must retain the above copyright notice,
  *      this list of conditions and the following disclaimer.
  *   2. Redistributions in binary form must reproduce the above copyright notice,
  *      this list of conditions and the following disclaimer in the documentation
  *      and/or other materials provided with the distribution.
  *   3. Neither the name of Alvaro Lopes nor the names of contributors
  *      may be used to endorse or promote products derived from this software
  *      without specific prior written permission.
  *
  * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
  * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
  * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
  * DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
  * FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
  * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
  * SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
  * CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
  * OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
  * OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
  *
  ******************************************************************************
  */

#include "temp.h"
#include "ds18b20.h"
#include "onewire.h"
#include "uart.h"
#include <string.h>

static int ds_chip_present_flag = 0;
static uint8_t ds_chip_address[8];
volatile uint8_t check_temp;
static uint8_t ds_errors = 0;
static int16_t ds_temp;

static int ds_chip_present()
{
    return ds_chip_present_flag;
}

int temp__get()
{
    if (ds_chip_present())
        return (int)ds_temp;
    return TEMP_INVALID;
}


void temp__enable_check(void)
{
    check_temp=1;
}

void temp__check()
{
    if (check_temp) {
        check_temp = 0;
        // Read DS chip
        if (ds_chip_present_flag) {
            if (ds18b20__read( ds_chip_address, &ds_temp) == 0) {
                ds_errors = 0;
                /*
                outstring("DS temperature: ");
                {
                    char buf[8];
                    fixed2ascii((int32_t)ds_temp, 4, 1, 1, buf);
                    outstring(buf);
                    outstring("C");
                }
                //printhex16(ds_temp);
                outstring("\r\n");
                */
                ds18b20__start_all();
            } else {
                ds_errors++;
                check_temp = 1;
                if (ds_errors > 16) {
                    outstring("Cannot read DS temperature\r\n");
                    ds_chip_present_flag = 0;
                }
            }
        }

    }
}

static void find_ds_chip()
{
    onewire_result_t v =onewire__first();
    if (v==ONEWIRE_OK) {
        if (ds18b20__address_match(onewire__getrom())) {
            outstring("DS18B20 IC found\r\n");
            memcpy( ds_chip_address, onewire__getrom(), 8);
            ds_chip_present_flag = 1;
        }
    }
}

void temp__init()
{
    find_ds_chip();
    if (ds_chip_present()) {
        ds18b20__start_all();
    }
}
