/*
  ******************************************************************************
  * (C) 2018 Alvaro Lopes <alvieboy@alvie.com>
  *
  * Redistribution and use in source and binary forms, with or without modification,
  * are permitted provided that the following conditions are met:
  *   1. Redistributions of source code must retain the above copyright notice,
  *      this list of conditions and the following disclaimer.
  *   2. Redistributions in binary form must reproduce the above copyright notice,
  *      this list of conditions and the following disclaimer in the documentation
  *      and/or other materials provided with the distribution.
  *   3. Neither the name of Alvaro Lopes nor the names of contributors
  *      may be used to endorse or promote products derived from this software
  *      without specific prior written permission.
  *
  * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
  * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
  * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
  * DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
  * FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
  * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
  * SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
  * CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
  * OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
  * OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
  *
  ******************************************************************************
  */

#include "usbd.h"
#include "usbd_core.h"
#include "usbd_desc.h"
#include "usbd_cdc.h" 
#include "usbd_cdc_interface.h"
#include "delay.h"
#include "uart.h"
#include "ringbuffer.h"
#include "vsnprintf.h"

USBD_HandleTypeDef  USBD_Device;
static uint8_t linestate = 0;

extern USBD_DescriptorsTypeDef VCP_Desc;

static ringbuffer_t txring;
static uint8_t txring_data[2048];
static uint8_t txbuf[64];

#if 0
static unsigned char usbbuf[512+64];

void *usb_malloc(unsigned size)
{
    outstring("USB alloc 0x");
    printhex(size);
    outstring("\r\n");
    return usbbuf;
}
extern void usb_free(void *ptr)
{

}
#endif

void usbd__init(void)
{
    ringbuffer__init(&txring, txring_data, sizeof(txring_data)-1);

    USBD_Init(	&USBD_Device, &VCP_Desc, 0);
    /* Add Supported Class */
    USBD_RegisterClass(&USBD_Device, USBD_CDC_CLASS);

    /* Add CDC Interface Class */
    USBD_CDC_RegisterInterface(&USBD_Device, &USBD_CDC_fops);

    /* Start Device Process */
    USBD_Start(&USBD_Device);

}

int usbd__isConnected()
{
    return linestate &1;
}

void usbd__checkTx(void)
{
    USBD_CDC_HandleTypeDef   *hcdc = (USBD_CDC_HandleTypeDef*) USBD_Device.pClassData;

    if (linestate &1) {
        if (hcdc->TxState == 0) {
            if (ringbuffer__size(&txring)>0) {
                unsigned len = ringbuffer__get(&txring, txbuf, sizeof(txbuf));
                USBD_CDC_SetTxBuffer( &USBD_Device, txbuf, len);
                USBD_CDC_TransmitPacket( &USBD_Device );
            }
        }
    }
}

void usbd__checkRx(void)
{
    usbd_cdc__check_data();

}
void usbd__transmit(uint8_t *buf, int len)
{
    if (linestate & 1) {
        USBD_CDC_SetTxBuffer( &USBD_Device, buf, len);
        while (USBD_CDC_TransmitPacket( &USBD_Device ) == USBD_BUSY) {
            delay__us(1000);
            if (linestate & 0)
                break;
        }
    }
}

#include <stdarg.h>

static char printf_line[128];

void usbd__printf(const char *fmt,...)
{
    va_list ap;
    va_start(ap, fmt);
    int len = __vsnprintf(printf_line, 128, fmt, ap);
    if (len>0) {
        ringbuffer__append(&txring, (uint8_t*)printf_line, len);
    }
    va_end(ap);
}

void usbd__puts(const char *str)
{
    ringbuffer__append(&txring, (uint8_t*)str, strlen(str));
}


void usbd__lineStateChanged(uint8_t state)
{
    linestate = state;
}

uint8_t usbd__getLineState()
{
    return linestate;
}

