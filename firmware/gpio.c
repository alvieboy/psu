/*
  ******************************************************************************
  * (C) 2018 Alvaro Lopes <alvieboy@alvie.com>
  *
  * Redistribution and use in source and binary forms, with or without modification,
  * are permitted provided that the following conditions are met:
  *   1. Redistributions of source code must retain the above copyright notice,
  *      this list of conditions and the following disclaimer.
  *   2. Redistributions in binary form must reproduce the above copyright notice,
  *      this list of conditions and the following disclaimer in the documentation
  *      and/or other materials provided with the distribution.
  *   3. Neither the name of Alvaro Lopes nor the names of contributors
  *      may be used to endorse or promote products derived from this software
  *      without specific prior written permission.
  *
  * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
  * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
  * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
  * DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
  * FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
  * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
  * SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
  * CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
  * OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
  * OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
  *
  ******************************************************************************
  */

#include "gpio.h"
#include <stm32f1xx_hal.h>
#include <stm32f1xx_hal_gpio.h>

#define PIN_INPUT     0
#define PIN_ANALOG    1
#define PIN_PP        2
#define PIN_AF_PP     3
#define PIN_USB       4
#define PIN_AF_OD     5
#define PIN_OD        6
#define PIN_SKIP      0xFF

static const struct {
    uint32_t mode;
    uint32_t pull;
    uint32_t speed;
} gpio_conf[] = {
    { GPIO_MODE_INPUT,     GPIO_NOPULL, GPIO_SPEED_FREQ_LOW }, // INPUT
    { GPIO_MODE_ANALOG,    GPIO_NOPULL, GPIO_SPEED_FREQ_HIGH }, // Analog
    { GPIO_MODE_OUTPUT_PP, GPIO_NOPULL, GPIO_SPEED_FREQ_LOW }, // PP
    { GPIO_MODE_AF_PP,     GPIO_NOPULL, GPIO_SPEED_FREQ_HIGH }, // AF_PP
    { GPIO_MODE_AF_INPUT,  GPIO_NOPULL, GPIO_SPEED_FREQ_HIGH }, // AF_INPUT (USB)
    { GPIO_MODE_AF_OD,     GPIO_NOPULL, GPIO_SPEED_FREQ_HIGH }, // AF_OC
    { GPIO_MODE_OUTPUT_OD, GPIO_NOPULL, GPIO_SPEED_FREQ_LOW }, // OD
};

static const uint8_t porta_conf[16] = {
    PIN_ANALOG,  // PSU_VOLTAGE_SENSE
    PIN_ANALOG,  // 5V_CURRENT_SENSE
    PIN_ANALOG,  // 5V_SENSE
    PIN_ANALOG,  // PSU_CURRENT_SENSE
    PIN_ANALOG,  // VPOS_SENSE
    PIN_ANALOG,  // VNEG_SENSE
    PIN_PP,      // PSU_OUT_ON
    PIN_PP,      // 5V_OUT_ON
    PIN_PP,      // VOUT_ON
    PIN_AF_PP,   // UART_TX
    PIN_AF_PP,   // UART_RX
    PIN_SKIP,    // USBDM
    PIN_SKIP,    // USBDP
    PIN_PP,      // LED_ON
    PIN_OD,      // ONEWIRE
    PIN_PP       // SPI_CS0
};

static const uint8_t portb_conf[16] = {
    PIN_INPUT,   // UNUSED
    PIN_INPUT,   // UNUSED
    PIN_INPUT,   // BOOT1/UNUSED
    PIN_AF_PP,   // SPI_SCK
    PIN_AF_PP,   // SPI_MISO (SPI extender)
    PIN_AF_PP,   // SPI_MOSI
    PIN_AF_OD,   // I2C_SCL
    PIN_AF_OD,   // I2C_SDA

    PIN_PP,      // DC
    PIN_INPUT,   // UNUSED
    PIN_INPUT,   // UNUSED
    PIN_PP,      // RST (to panel)
    PIN_INPUT,   // FAN_PWM (UNUSED)
    PIN_INPUT,   // FAN_TACH (UNUSED)
    PIN_PP,      // SPI_CS2
    PIN_PP       // SPI_CS1
};

static const uint8_t portc_conf[2] = { // Start at 13
    PIN_INPUT,   // UNUSED
    PIN_INPUT    // UNUSED
};

static void gpio_init_block(GPIO_TypeDef *port, const uint8_t *val, uint8_t start_pin, uint8_t count)
{
    GPIO_InitTypeDef  GPIO_InitStruct;
    uint32_t pin = (1<<start_pin);
    while (count--) {
        GPIO_InitStruct.Pin = pin;
        if (*val != PIN_SKIP) {
            GPIO_InitStruct.Mode  = gpio_conf[*val].mode;
            GPIO_InitStruct.Pull  = gpio_conf[*val].pull;
            GPIO_InitStruct.Speed = gpio_conf[*val].speed;
            HAL_GPIO_Init(port, &GPIO_InitStruct);
        }
        pin<<=1;
        val++;
    }
}

void gpio__preinit()
{
}

void gpio__init()
{
#ifdef STM32F103xB
    __HAL_RCC_GPIOA_CLK_ENABLE();
    __HAL_RCC_GPIOB_CLK_ENABLE();
    __HAL_RCC_GPIOC_CLK_ENABLE();

    __HAL_RCC_AFIO_CLK_ENABLE();
    __HAL_RCC_ADC1_CLK_ENABLE();

    __HAL_AFIO_REMAP_SWJ_DISABLE();
    __HAL_AFIO_REMAP_I2C1_DISABLE();
    __HAL_AFIO_REMAP_USART1_DISABLE();

    __HAL_AFIO_REMAP_SPI1_ENABLE();

    __HAL_RCC_USART1_CLK_ENABLE();
    __HAL_RCC_SPI1_CLK_ENABLE();
    __HAL_RCC_I2C1_CLK_DISABLE();
#endif
    gpio_init_block( GPIOA, porta_conf, 0, 16);
    gpio_init_block( GPIOB, portb_conf, 0, 16);
    gpio_init_block( GPIOC, portc_conf, 13, 2);
}
