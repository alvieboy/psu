/*
  ******************************************************************************
  * (C) 2018 Alvaro Lopes <alvieboy@alvie.com>
  *
  * Redistribution and use in source and binary forms, with or without modification,
  * are permitted provided that the following conditions are met:
  *   1. Redistributions of source code must retain the above copyright notice,
  *      this list of conditions and the following disclaimer.
  *   2. Redistributions in binary form must reproduce the above copyright notice,
  *      this list of conditions and the following disclaimer in the documentation
  *      and/or other materials provided with the distribution.
  *   3. Neither the name of Alvaro Lopes nor the names of contributors
  *      may be used to endorse or promote products derived from this software
  *      without specific prior written permission.
  *
  * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
  * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
  * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
  * DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
  * FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
  * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
  * SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
  * CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
  * OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
  * OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
  *
  ******************************************************************************
  */

#ifndef __I2C_H__
#define __I2C_H__

#include <stm32f1xx_hal.h>
#include <stm32f1xx_hal_gpio.h>
#include <stm32f1xx_hal_i2c.h>

#ifdef __cplusplus
extern "C" {
#endif


/*
 I2C Addresses

 On Board:

 VNEG MCP45 A0=0, A1=0                         0b0111100x   0x78
 VPOS MCP45 A0=1, A1=0                         0b0111101x   0x7A
 U$7  MCP45 A0=0, A1=1 (close to PSU send)     0b0111110x   0x7C
 U$8  MCP45 A0=1, A1=1 (close to USB)          0b0111111x   0x7E

*/


#define I2C_VNEG_ADJ_ADDRESS (0x78)
#define I2C_VPOS_ADJ_ADDRESS (0x7A)
#define I2C_VPSU_ADJ_ADDRESS (0x7C)
#define I2C_IPSU_ADJ_ADDRESS (0x7E)

#define I2C_PANEL_ADDRESS    (0x06)
#define I2C_EEPROM_ADDRESS   (0xA0)
#define I2C_FAN_ADDRESS      (0x5E)

void i2c__init(GPIO_TypeDef *scl_port,
               uint32_t scl_pin,
               GPIO_TypeDef *sda_port,
               uint32_t sda_pin);

int i2c__writeregister8(uint8_t address, uint8_t reg, uint8_t value);
int i2c__readregister8(uint8_t address, uint8_t reg);
int i2c__readregister8block(uint8_t address, uint8_t reg, uint16_t size, uint8_t *dest);
int i2c__readregister16block(uint8_t address, uint16_t reg, uint16_t size, uint8_t *dest);
int i2c__writeregister16(uint8_t address, uint16_t reg, uint8_t value);

void i2c__disable(void);
void i2c__enable(void);

void i2c__scan(uint8_t regno);

#ifdef __cplusplus
}
#endif

#endif
