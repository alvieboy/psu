/*
  ******************************************************************************
  * (C) 2018 Alvaro Lopes <alvieboy@alvie.com>
  *
  * Redistribution and use in source and binary forms, with or without modification,
  * are permitted provided that the following conditions are met:
  *   1. Redistributions of source code must retain the above copyright notice,
  *      this list of conditions and the following disclaimer.
  *   2. Redistributions in binary form must reproduce the above copyright notice,
  *      this list of conditions and the following disclaimer in the documentation
  *      and/or other materials provided with the distribution.
  *   3. Neither the name of Alvaro Lopes nor the names of contributors
  *      may be used to endorse or promote products derived from this software
  *      without specific prior written permission.
  *
  * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
  * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
  * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
  * DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
  * FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
  * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
  * SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
  * CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
  * OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
  * OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
  *
  ******************************************************************************
  */

#ifndef __OUTPUT_H__
#define __OUTPUT_H__

#include <stm32f1xx_hal.h>
#include <stm32f1xx_hal_gpio.h>

typedef uint8_t output_t;


#define OUTPUT_TYPE_SHIFT  4
#define OUTPUT_TYPE_MASK    0x70
#define OUTPUT_INDEX_MASK   0x0F


#define MAKEOUTPUT(type, index) ((type<<OUTPUT_TYPE_SHIFT) | (index))
#define OUTPUT_TYPE_GPIOA  0x0
#define OUTPUT_TYPE_PANEL  0x4
#define OUTPUT_TYPE_INVERT 0x8

#define OUTPUT_PSU_ON      MAKEOUTPUT(OUTPUT_TYPE_GPIOA, 6)
#define OUTPUT_P5V_ON      MAKEOUTPUT(OUTPUT_TYPE_GPIOA, 7)
#define OUTPUT_VADJ_ON     MAKEOUTPUT(OUTPUT_TYPE_GPIOA, 8)
#define OUTPUT_LED_PCB     MAKEOUTPUT(OUTPUT_TYPE_GPIOA, 13)

// Panel indicators
#define OUTPUT_LED_P5V     MAKEOUTPUT(OUTPUT_TYPE_PANEL, 2)
#define OUTPUT_LED_VADJ    MAKEOUTPUT(OUTPUT_TYPE_PANEL, 1)
#define OUTPUT_LED_PSU     MAKEOUTPUT(OUTPUT_TYPE_PANEL, 0)
#define OUTPUT_BUZZER      MAKEOUTPUT(OUTPUT_TYPE_PANEL, 3)

void output__init(void);
void output__enable(output_t output);
void output__disable(output_t output);
void output__set(output_t output, uint8_t val);


#endif
