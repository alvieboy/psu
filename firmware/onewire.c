/*
  ******************************************************************************
  * (C) 2018 Alvaro Lopes <alvieboy@alvie.com>
  *
  * Based on work Copyright (C) Tilen Majerle, 2015
  *
  * Redistribution and use in source and binary forms, with or without modification,
  * are permitted provided that the following conditions are met:
  *   1. Redistributions of source code must retain the above copyright notice,
  *      this list of conditions and the following disclaimer.
  *   2. Redistributions in binary form must reproduce the above copyright notice,
  *      this list of conditions and the following disclaimer in the documentation
  *      and/or other materials provided with the distribution.
  *   3. Neither the name of Alvaro Lopes nor the names of contributors
  *      may be used to endorse or promote products derived from this software
  *      without specific prior written permission.
  *
  * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
  * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
  * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
  * DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
  * FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
  * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
  * SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
  * CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
  * OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
  * OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
  *
  ******************************************************************************
  */

#include <stm32f1xx_hal.h>
#include <stm32f1xx_hal_gpio.h>

#include "onewire.h"

#include "psu.h"
#include "delay.h"

static GPIO_InitTypeDef  GPIO_InitStruct;
static uint8_t lastDiscrepancy = 0;
static uint8_t lastDeviceFlag = 0;
static uint8_t lastFamilyDiscrepancy = 0;
static uint8_t rom_address[8];

void onewire__init(void)
{
    GPIO_InitStruct.Pin = ONEWIRE_PIN;
    //GPIO_InitStruct.Mode  = gpio_conf[*val].mode;
    GPIO_InitStruct.Pull  = GPIO_NOPULL;
    GPIO_InitStruct.Speed = GPIO_SPEED_FREQ_LOW;
}

static uint8_t onewire__read(void)
{
    return HAL_GPIO_ReadPin( ONEWIRE_PORT, ONEWIRE_PIN);
}

static void onewire__input(void)
{
    GPIO_InitStruct.Mode  = GPIO_MODE_INPUT;
    HAL_GPIO_Init(ONEWIRE_PORT, &GPIO_InitStruct);
}

static void onewire__output(void)
{
    GPIO_InitStruct.Mode  = GPIO_MODE_OUTPUT_PP;
    HAL_GPIO_Init(ONEWIRE_PORT, &GPIO_InitStruct);
}

static void onewire__delay(uint16_t microsseconds)
{
    delay__us(microsseconds);
  //  HAL_Delay_Microseconds(microsseconds);
}

static void onewire__mark(uint32_t time)
{
    HAL_GPIO_WritePin( ONEWIRE_PORT, ONEWIRE_PIN, 0);
    onewire__output();
    onewire__delay(time);
    onewire__input();
}


int onewire__reset(void)
{
    int i;
    __disable_irq();
    onewire__mark( ONEWIRE_RESET_PULSE_WIDTH );
    onewire__delay( ONEWIRE_RESET_SAMPLEDLY );
    /* Check bit value */
    i = onewire__read();
    __enable_irq();
    /* Delay for 410 us */
    onewire__delay( ONEWIRE_RESET_WIDTH - (ONEWIRE_RESET_PULSE_WIDTH + ONEWIRE_RESET_SAMPLEDLY) );
    /* Return value of presence pulse, 0 = OK, 1 = ERROR */
    return i;
}

static void onewire__writebit(uint8_t bit)
{
    uint32_t delay = bit ? ONEWIRE_ONE_PULSE_WIDTH : ONEWIRE_ZERO_PULSE_WIDTH;
    __disable_irq();
    onewire__mark( delay );
    __enable_irq();
    onewire__delay( (ONEWIRE_BIT_WIDTH + ONEWIRE_RECOVERY_DELAY) - delay);
}

uint8_t onewire__readbit(void)
{
    uint8_t bit = 0;
    __disable_irq();
    onewire__mark(ONEWIRE_ONE_PULSE_WIDTH);
    onewire__delay(ONEWIRE_SAMPLE_DELAY);
    bit = onewire__read();
    __enable_irq();
    onewire__delay(ONEWIRE_BIT_WIDTH - (ONEWIRE_ONE_PULSE_WIDTH+ONEWIRE_SAMPLE_DELAY));
    return bit;
}

void onewire__writebyte(uint8_t byte)
{
    uint8_t i = 8;
    while (i--) {
        onewire__writebit(byte & 0x01);
        byte >>= 1;
    }
}

uint8_t onewire__readbyte(void)
{
    uint8_t i = 8;
    uint8_t byte = 0;
    while (i--) {
        byte >>= 1;
        byte |= ( onewire__readbit() << 7 );
    }
    return byte;
}


int onewire__first(void)
{
    /* Reset search values */
    onewire__resetsearch();
    /* Start with searching */
    return onewire__search(ONEWIRE_CMD_SEARCHROM);
}

int onewire__next(void)
{
    /* Leave the search state alone */
    return onewire__search(ONEWIRE_CMD_SEARCHROM);
}

void onewire__resetsearch(void)
{
    lastDiscrepancy = 0;
    lastDeviceFlag = 0;
    lastFamilyDiscrepancy = 0;
}

const uint8_t *onewire__getrom(void)
{
    return rom_address;
}

onewire_result_t onewire__search(uint8_t command)
{
    uint8_t id_bit_number;
    uint8_t last_zero, rom_byte_number, search_result;
    uint8_t id_bit, cmp_id_bit;
    uint8_t rom_byte_mask, search_direction;

    /* Initialize for search */
    id_bit_number = 1;
    last_zero = 0;
    rom_byte_number = 0;
    rom_byte_mask = 1;
    search_result = 0;

    // if the last call was not the last one
    if (!lastDeviceFlag) {
        // 1-Wire reset
        if (onewire__reset()!=ONEWIRE_OK) {
            /* Reset the search */
            lastDiscrepancy = 0;
            lastDeviceFlag = 0;
            lastFamilyDiscrepancy = 0;
            return ONEWIRE_ERR_NO_DEVICES;
        }

        // issue the search command
        onewire__writebyte(command);

        // loop to do the search
        do {
            // read a bit and its complement
            id_bit = onewire__readbit();
            cmp_id_bit = onewire__readbit();

            // check for no devices on 1-wire
            if ((id_bit == 1) && (cmp_id_bit == 1)) {
                break;
            } else {
                // all devices coupled have 0 or 1
                if (id_bit != cmp_id_bit) {
                    search_direction = id_bit;  // bit write value for search
                } else {
                    // if this discrepancy if before the Last Discrepancy
                    // on a previous next then pick the same as last time
                    if (id_bit_number < lastDiscrepancy) {
                        search_direction = ((rom_address[rom_byte_number] & rom_byte_mask) > 0);
                    } else {
                        // if equal to last pick 1, if not then pick 0
                        search_direction = (id_bit_number == lastDiscrepancy);
                    }

                    // if 0 was picked then record its position in LastZero
                    if (search_direction == 0) {
                        last_zero = id_bit_number;

                        // check for Last discrepancy in family
                        if (last_zero < 9) {
                            lastFamilyDiscrepancy = last_zero;
                        }
                    }
                }

                // set or clear the bit in the ROM byte rom_byte_number
                // with mask rom_byte_mask
                if (search_direction == 1) {
                    rom_address[rom_byte_number] |= rom_byte_mask;
                } else {
                    rom_address[rom_byte_number] &= ~rom_byte_mask;
                }

                // serial number search direction write bit
                onewire__writebit(search_direction);

                // increment the byte counter id_bit_number
                // and shift the mask rom_byte_mask
                id_bit_number++;
                rom_byte_mask <<= 1;

                // if the mask is 0 then go to new SerialNum byte rom_byte_number and reset mask
                if (rom_byte_mask == 0) {
                    //docrc8(rom_address[rom_byte_number]);  // accumulate the CRC
                    rom_byte_number++;
                    rom_byte_mask = 1;
                }
            }
        } while (rom_byte_number < 8);  // loop until through all ROM bytes 0-7

        // if the search was successful then
        if (!(id_bit_number < 65)) {
            // search successful so set LastDiscrepancy,LastDeviceFlag,search_result
            lastDiscrepancy = last_zero;

            // check for last device
            if (lastDiscrepancy == 0) {
                lastDeviceFlag = 1;
            }

            search_result = ONEWIRE_OK;
        }
    }

    if ((search_result!=ONEWIRE_OK) || !rom_address[0]) {
        lastDiscrepancy = 0;
        lastDeviceFlag = 0;
        lastFamilyDiscrepancy = 0;
        search_result = ONEWIRE_NO_MORE_DEVICES;
    }

    return search_result;
}

void onewire__select(const uint8_t* addr)
{
    uint8_t i;
    onewire__writebyte(ONEWIRE_CMD_MATCHROM);

    for (i=0; i<8; i++) {
        onewire__writebyte(addr[i]);
    }
}

uint8_t onewire__crc8(const uint8_t *data, unsigned len)
{
    uint8_t crc = 0, inbyte, i, mix;

    while (len--) {
        inbyte = *data++;
        for (i = 8; i; i--) {
            mix = (crc ^ inbyte) & 0x01;
            crc >>= 1;
            if (mix) {
                crc ^= 0x8C;
            }
            inbyte >>= 1;
        }
    }
    return crc;
}
