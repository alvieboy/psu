/*
  ******************************************************************************
  * (C) 2018 Alvaro Lopes <alvieboy@alvie.com>
  *
  * Redistribution and use in source and binary forms, with or without modification,
  * are permitted provided that the following conditions are met:
  *   1. Redistributions of source code must retain the above copyright notice,
  *      this list of conditions and the following disclaimer.
  *   2. Redistributions in binary form must reproduce the above copyright notice,
  *      this list of conditions and the following disclaimer in the documentation
  *      and/or other materials provided with the distribution.
  *   3. Neither the name of Alvaro Lopes nor the names of contributors
  *      may be used to endorse or promote products derived from this software
  *      without specific prior written permission.
  *
  * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
  * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
  * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
  * DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
  * FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
  * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
  * SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
  * CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
  * OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
  * OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
  *
  ******************************************************************************
  */

#include "i2c.h"
#include "defs.h"
#include "uart.h"

#define DEBUG_I2C
#define I2C_TIMEOUT 10

static I2C_HandleTypeDef I2cHandle;

void i2c__enable(void)
{
    __HAL_RCC_I2C1_CLK_ENABLE();
}

void i2c__disable(void)
{
    __HAL_RCC_I2C1_CLK_DISABLE();
}
#if 0

static int i2c__errata(I2C_HandleTypeDef *handle,
                       GPIO_TypeDef *scl_port,
                       uint32_t scl_pin,
                       GPIO_TypeDef *sda_port,
                       uint32_t sda_pin
                      )
{
    GPIO_InitTypeDef  gpio_init;
    /* As per errata 2.13.7 */

    /* ASSUMPTION: clock is enabled for I2C */

    /* 1. Disable the I2C peripheral by clearing the PE bit in I2Cx_CR1 register. */
    CLEAR_BIT(I2cHandle.Instance->CR1,  I2C_CR1_PE);

    /* 2. Configure the SCL and SDA I/Os as General Purpose Output Open-Drain, High level (Write 1 to GPIOx_ODR).*/
    gpio_init.Pin  = scl_pin;
    gpio_init.Mode = GPIO_MODE_OUTPUT_OD;
    gpio_init.Speed = GPIO_SPEED_FREQ_HIGH;
    HAL_GPIO_Init(scl_port, &gpio_init);

    gpio_init.Pin  = sda_pin;
    HAL_GPIO_Init(sda_port, &gpio_init);

    HAL_GPIO_WritePin(scl_port, scl_pin, 1);
    HAL_GPIO_WritePin(sda_port, sda_pin, 1);

    /* 3. Check SCL and SDA High level in GPIOx_IDR. */
    if (HAL_GPIO_ReadPin(scl_port, scl_pin)!=1)
        return -1;
    if (HAL_GPIO_ReadPin(sda_port, sda_pin)!=1)
        return -1;

    /* 4. Configure the SDA I/O as General Purpose Output Open-Drain, Low level (Write 0 to GPIOx_ODR). */
    HAL_GPIO_WritePin(sda_port, sda_pin, 0);

    /* 5. Check SDA Low level in GPIOx_IDR. */
    if (HAL_GPIO_ReadPin(sda_port, sda_pin)!=0)
        return -1;

    /* 6. Configure the SCL I/O as General Purpose Output Open-Drain, Low level (Write 0 to GPIOx_ODR). */
    HAL_GPIO_WritePin(scl_port, scl_pin, 0);

    /* 7. Check SCL Low level in GPIOx_IDR. */
    if (HAL_GPIO_ReadPin(scl_port, scl_pin)!=0)
        return -1;

    /* 8. Configure the SCL I/O as General Purpose Output Open-Drain, High level (Write 1 to GPIOx_ODR). */
    HAL_GPIO_WritePin(scl_port, scl_pin, 1);

    /* 9. Check SCL High level in GPIOx_IDR. */
    if (HAL_GPIO_ReadPin(scl_port, scl_pin)!=1)
        return -1;

    /* 10. Configure the SDA I/O as General Purpose Output Open-Drain , High level (Write 1 to GPIOx_ODR). */
    HAL_GPIO_WritePin(sda_port, sda_pin, 1);

    /* 11. Check SDA High level in GPIOx_IDR. */
    if (HAL_GPIO_ReadPin(sda_port, sda_pin)!=1)
        return -1;

    /* 12. Configure the SCL and SDA I/Os as Alternate function Open-Drain. */
    gpio_init.Pin  = scl_pin;
    gpio_init.Mode = GPIO_MODE_AF_OD;
    HAL_GPIO_Init(scl_port, &gpio_init);
    gpio_init.Pin  = sda_pin;
    HAL_GPIO_Init(sda_port, &gpio_init);

    /* 13. Set SWRST bit in I2Cx_CR1 register. */
    SET_BIT(I2cHandle.Instance->CR1,  I2C_CR1_SWRST);

    /* 14. Clear SWRST bit in I2Cx_CR1 register. */
    CLEAR_BIT(I2cHandle.Instance->CR1,  I2C_CR1_SWRST);

    /* 15. Enable the I2C peripheral by setting the PE bit in I2Cx_CR1 register. */
    SET_BIT(I2cHandle.Instance->CR1,  I2C_CR1_PE);

    return 0;
}
#endif

void i2c__init(GPIO_TypeDef *scl_port,
               uint32_t scl_pin,
               GPIO_TypeDef *sda_port,
               uint32_t sda_pin)
{
    // Enable clock
    i2c__enable();

    /*##-1- Configure the I2C peripheral ######################################*/
    I2cHandle.Instance             = I2C1;
    I2cHandle.Init.ClockSpeed      = 100000;
    I2cHandle.Init.DutyCycle       = I2C_DUTYCYCLE_2;
    I2cHandle.Init.OwnAddress1     = 0x00;
    I2cHandle.Init.AddressingMode  = I2C_ADDRESSINGMODE_7BIT;
    I2cHandle.Init.DualAddressMode = I2C_DUALADDRESS_DISABLE;
    I2cHandle.Init.OwnAddress2     = 0xFF;
    I2cHandle.Init.GeneralCallMode = I2C_GENERALCALL_DISABLE;
    I2cHandle.Init.NoStretchMode   = I2C_NOSTRETCH_DISABLE;

#ifdef STM32F103xB

    SET_BIT(I2cHandle.Instance->CR1,  I2C_CR1_SWRST);
    CLEAR_BIT(I2cHandle.Instance->CR1,  I2C_CR1_SWRST);
#endif

    if(HAL_I2C_Init(&I2cHandle) != HAL_OK)
    {
        /* Initialization Error */
        Error_Handler();
    }

    //i2c__errata(&I2cHandle, scl_port, scl_pin, sda_port, sda_pin);

    i2c__disable();
}

int i2c__writeregister8(uint8_t address, uint8_t reg, uint8_t value)
{
    unsigned char buffer[2];
    buffer[0] = reg;
    buffer[1] = value;

    while(HAL_I2C_Master_Transmit(&I2cHandle, (uint16_t)address, (uint8_t*)buffer, 2, I2C_TIMEOUT)!= HAL_OK)
    {
#ifdef DEBUG_I2C
        outstring("I2C write failed\r\n");
#endif
        return -1;
    }
    return 0;
}


int i2c__readregister8(uint8_t address, uint8_t reg)
{
    unsigned char buffer[1];
    buffer[0] = reg;

    while(HAL_I2C_Master_Transmit(&I2cHandle, (uint16_t)address, (uint8_t*)buffer, 1, I2C_TIMEOUT)!= HAL_OK)
    {
#ifdef DEBUG_I2C
        outstring("I2C read (setup) failed\r\n");
#endif
        return -1;
    }
    while(HAL_I2C_Master_Receive(&I2cHandle, (uint16_t)address, (uint8_t*)buffer, 1, I2C_TIMEOUT)!= HAL_OK)
    {
#ifdef DEBUG_I2C
        outstring("I2C read failed\r\n");
#endif
        return -1;
    }
    return (int)buffer[0];
}

void i2c__scan(uint8_t regno)
{
    unsigned address = 2;
    int i;
    outstring("Scanning I2C bus: \r\n");
//    SET_BIT(I2cHandle.Instance->CR1,  I2C_CR1_SWRST);
#if 0
    GPIO_InitTypeDef  GPIO_InitStruct;
    GPIO_InitStruct.Pin = 6;
    GPIO_InitStruct.Mode  = GPIO_MODE_AF_OD;
    GPIO_InitStruct.Pull  = GPIO_NOPULL;
    GPIO_InitStruct.Speed = GPIO_SPEED_FREQ_HIGH;

    HAL_GPIO_Init(GPIOB, &GPIO_InitStruct);
    GPIO_InitStruct.Pin = 7;
    HAL_GPIO_Init(GPIOB, &GPIO_InitStruct);
#endif
    i2c__enable();
   //     __HAL_I2C_ENABLE(&I2cHandle);


    for (i=0;i<126;i++) {
//        SET_BIT(I2cHandle.Instance->CR1,  I2C_CR1_SWRST);
 //       CLEAR_BIT(I2cHandle.Instance->CR1,  I2C_CR1_SWRST);
 //       SET_BIT(I2cHandle.Instance->CR1,  I2C_CR1_PE);
#if 0
        outstring("Scan address 0x");
        printhexbyte(address>>1);
        outstring("\r\n");
#endif
        if (i2c__readregister8(address,regno)>=0) {
            outstring("Positive reply from address ");
            printhexbyte(address);
            outstring("\r\n");
        } else {
#if 0
            outstring("No response.\r\n");
            outstring("SR1: ");
            printhex(I2cHandle.Instance->SR1);
            outstring("\r\n");
            outstring("SR2: ");
            printhex(I2cHandle.Instance->SR2);
            outstring("\r\n");
#endif
        }
        address+=2;
    }
    outstring("Finished I2C scan\r\n");
}

int i2c__readregister8block(uint8_t address, uint8_t reg, uint16_t size, uint8_t *dest)
{
    unsigned char buffer[1];
    buffer[0] = reg;

    while(HAL_I2C_Master_Transmit(&I2cHandle, (uint16_t)address, (uint8_t*)buffer, 1, I2C_TIMEOUT)!= HAL_OK)
    {
#ifdef DEBUG_I2C
        outstring("I2C read (setup) failed\r\n");
#endif
        return -1;
    }
    while(HAL_I2C_Master_Receive(&I2cHandle, (uint16_t)address, (uint8_t*)dest, size, I2C_TIMEOUT*size)!= HAL_OK)
    {
#ifdef DEBUG_I2C
        outstring("I2C read failed\r\n");
#endif
        return -1;
    }
    return 0;
}

int i2c__writeregister16(uint8_t address, uint16_t reg, uint8_t value)
{
    unsigned char buffer[3];
    buffer[0] = reg>>8;
    buffer[1] = reg;
    buffer[2] = value;

    while(HAL_I2C_Master_Transmit(&I2cHandle, (uint16_t)address, (uint8_t*)buffer, 3, I2C_TIMEOUT)!= HAL_OK)
    {
#ifdef DEBUG_I2C
        outstring("I2C write failed\r\n");
#endif
        return -1;
    }
    return 0;
}

int i2c__readregister16block(uint8_t address, uint16_t reg, uint16_t size, uint8_t *dest)
{
    unsigned char buffer[2];
    buffer[0] = reg>>8;
    buffer[1] = reg;

    while(HAL_I2C_Master_Transmit(&I2cHandle, (uint16_t)address, (uint8_t*)buffer, 2, I2C_TIMEOUT)!= HAL_OK)
    {
#ifdef DEBUG_I2C
        outstring("I2C read (setup) failed\r\n");
#endif
        return -1;
    }
    while(HAL_I2C_Master_Receive(&I2cHandle, (uint16_t)address, (uint8_t*)dest, size, I2C_TIMEOUT*size)!= HAL_OK)
    {
#ifdef DEBUG_I2C
        outstring("I2C block read failed\r\n");
#endif
        return -1;
    }
    return 0;
}

