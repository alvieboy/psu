/*
  ******************************************************************************
  * (C) 2018 Alvaro Lopes <alvieboy@alvie.com>
  *
  * Redistribution and use in source and binary forms, with or without modification,
  * are permitted provided that the following conditions are met:
  *   1. Redistributions of source code must retain the above copyright notice,
  *      this list of conditions and the following disclaimer.
  *   2. Redistributions in binary form must reproduce the above copyright notice,
  *      this list of conditions and the following disclaimer in the documentation
  *      and/or other materials provided with the distribution.
  *   3. Neither the name of Alvaro Lopes nor the names of contributors
  *      may be used to endorse or promote products derived from this software
  *      without specific prior written permission.
  *
  * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
  * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
  * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
  * DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
  * FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
  * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
  * SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
  * CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
  * OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
  * OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
  *
  ******************************************************************************
  */

#include <stm32f1xx_hal.h>
#include <stm32f1xx_hal_gpio.h>
#include <stdbool.h>
#include "adc.h"
#include "uart.h"
#include "panel.h"
#include "input.h"
#include "event.h"
#include "debug.h"


static uint8_t prev_button_state;

/*
 */
static uint8_t tick_counter_10ms = 0;
static uint8_t buttons_dc[8];

void input__init(void)
{
    unsigned int i;

    panel__init();

    for (i=0;i<8;i++) {
        buttons_dc[i] = 0;
    }
}

void input__tick_10ms()
{
    uint8_t i;
    tick_counter_10ms++;
    for (i=0;i<8;i++) {
        if (buttons_dc[i] == 2) {
            // Timer expired.
            input__button_long_press_callback(i);
        }
        if (buttons_dc[i]>1)
            buttons_dc[i]--;
    }
}

/*
 Timing example. 8-bit counter.

 0xFE (-2)

 expires at +10 (0x08)
 expires at +1 (0xFF)

 (-2)-8 = -10
 (-2)-1 = -1

*/

#define BUTTON_LONGPRESS_DELAY 50 /* half second */

static void input__buttonevent(uint8_t button, uint8_t state)
{
    outstring("Button  ");
    printhexbyte(button);
    outstring(state ? " pressed": " released");

    int cancel = 0;
    bool was_long_press = false;

    if (state) {
        buttons_dc[button] = BUTTON_LONGPRESS_DELAY;
    } else {
        if (buttons_dc[button] == 1) {
            // First release after long press.
            was_long_press=true;
        }
        buttons_dc[button] = 0;
    }

    if (state) {
        cancel = input__button_down_callback(button);
        if (cancel==EVENT_HANDLED)
            buttons_dc[button]=0;
    } else {
        // If releasing after a long press, ignore.
        if (!was_long_press) {
            //cancel = input__button_up_callback(button);
            cancel = input__button_clicked_callback(button);
        }
    }
}


static uint8_t rotary_zero[4];
static uint8_t rotary_prev[4];
static uint8_t rotary_values[4];

static void input__checkrotary()
{
    int i;
    uint8_t rotary_current;
    int rotary_next;
    int rotary_delta;

    for (i=0;i<4;i++) {
        rotary_current = input__getrawrotary(i);

        if (panel__buttonsvalid()) {
            if (rotary_current!=rotary_prev[i]) {
                // Corner cases:
                // Current: 0, prev = 255
                // Current: 255, prev = 0;

                rotary_delta = (int)rotary_current - (int)rotary_prev[i];
                if (rotary_delta < -128)
                    rotary_delta += 256;
                if (rotary_delta > 127)
                    rotary_delta -= 256;
                // Apply to current value.
                rotary_next = (int)rotary_values[i] + rotary_delta;
                input__rotary_changed_delta_callback(i, rotary_delta);
                if (rotary_next<0)
                    rotary_next=0;
                if (rotary_next>255)
                    rotary_next=255;

                if (rotary_values[i]!=rotary_next) {
                    input__rotary_changed_callback(i, rotary_next);
                }
                rotary_values[i] = rotary_next;
            }
        } else {
            // Zeroe position according to current initial value
            rotary_zero[i] = rotary_current;
            rotary_values[i] = 0;
        }
        rotary_prev[i] = rotary_current;
    }
}

uint8_t input__getrotary(uint8_t index)
{
    return rotary_values[index];
}

uint8_t input__getrawrotary(uint8_t index)
{
    return panel__getreg(1+index);
}

void input__check()
{
    panel__read();
    // Check rotary positions
    input__checkrotary();

    if (!(panel__buttonsvalid())) {
        // Save state anyway
        prev_button_state = panel__getbuttonstate();
        // Latch rotary values

        return;
    }


    //uint8_t state = panel__getbuttonstate();
    int i;
    for (i=0;i<8;i++) {
        uint8_t delta = panel__getbuttondeltaevents(i);
        uint8_t bmask = (1<<i);
        while (delta>0) {
            prev_button_state ^= bmask;
            input__buttonevent(i, !!(prev_button_state & bmask));
            delta--;
        }
    }
    prev_button_state = panel__getbuttonstate();
#if 0
    if (state&0x80) {
        panel__setindicators(0xFF);
    } else {
        panel__setindicators(0);
    }
#endif
}
