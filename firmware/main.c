/*
  ******************************************************************************
  * (C) 2018 Alvaro Lopes <alvieboy@alvie.com>
  *
  * Redistribution and use in source and binary forms, with or without modification,
  * are permitted provided that the following conditions are met:
  *   1. Redistributions of source code must retain the above copyright notice,
  *      this list of conditions and the following disclaimer.
  *   2. Redistributions in binary form must reproduce the above copyright notice,
  *      this list of conditions and the following disclaimer in the documentation
  *      and/or other materials provided with the distribution.
  *   3. Neither the name of Alvaro Lopes nor the names of contributors
  *      may be used to endorse or promote products derived from this software
  *      without specific prior written permission.
  *
  * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
  * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
  * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
  * DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
  * FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
  * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
  * SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
  * CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
  * OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
  * OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
  *
  ******************************************************************************
  */

#include <stm32f1xx_hal.h>

#include "defs.h"
#include "uart.h"
#include "spi.h"
#include "adc.h"
#include "lcd.h"
#include "input.h"
#include "gpio.h"
#include "conversions.h"
#include "i2c.h"
#include "mcp45hv.h"
#include "eeprom.h"
#include "panel.h"
#include "output.h"
#include "delay.h"
#include "onewire.h"
#include "temp.h"
#include "crc.h"
#include "store.h"
#include "usbd.h"
#include "error.h"
#include "debug.h"
#include "vsnprintf.h"
#include "screen.h"
#include "psucontrol.h"

#include <string.h>

#include "setup.h"
#include "calibration.h"

void tick__handler(void);

#ifndef STM32F103xB
extern void process_events(void);
#endif

static volatile uint8_t system_ready = 0;


#define COLOR_VOLTAGE_OFF COLOR_RGB(0x18,0x18,0x18)
#define COLOR_VOLTAGE_ON  COLOR_RGB(0x00,0xFF,0x00)

#define VC_XOFFSET (16)
#define VC_CXOFFSET 236-22-8
#define VC_DOTWIDTH 14


#if 0
static void draw_p5v_outline(int y, int selected)
{
    uint16_t fg = selected ? COLOR_YELLOW : COLOR_GRAY;
    int thickness = selected ? 3: 1;
    lcd__drawFilledRect(0,    y, 320, thickness, fg);
    lcd__drawFilledRect(0,    y, thickness, 52, fg);
    lcd__drawFilledRect(320-thickness,  y, thickness, 52, fg);
    lcd__drawFilledRect(0,  y+52, 320, thickness, fg);

}
#endif

#define OUTLINE_XOFFSET (100)
static void draw_big_outline(int y, int selected)
{
    uint16_t fg = selected ? COLOR_YELLOW : COLOR_GRAY;
    int thickness = selected ? 3: 1;
    lcd__drawFilledRect(0,  y, 318, thickness, fg);

    lcd__drawFilledRect(0,  y, thickness, 52+16, fg);

    lcd__drawFilledRect(318,y+1,thickness, 52+16+16, fg);
    lcd__drawFilledRect(OUTLINE_XOFFSET,y+52+16+16,319,thickness, fg);
    lcd__drawFilledRect(0,y+52+16,OUTLINE_XOFFSET,thickness, fg);
    lcd__drawFilledRect(OUTLINE_XOFFSET,y+52+16,thickness,16, fg);
}




#define P5V_ONOFF_YOFFSET 28
#define PSU_ONOFF_YOFFSET (28+76+16)
#define VADJ_ONOFF_YOFFSET (28+76+76+16)






enum {
    NORMAL,
    SENSORS,
    CALIBRATE,
    PANEL_DEBUG
} mode = NORMAL;






#if 0
static void redraw()
{
    switch(mode) {
    case NORMAL:
        redraw_normal();
        break;
    case SENSORS:
        redraw_sensors();
        break;
    case PANEL_DEBUG:
        redraw_paneldebug();
        break;
    case CALIBRATE:
        calibration__redraw();
        break;
    }
}
#endif


extern const screendef_t screen_main_def;
extern const screendef_t screen_sensors_def;
extern const screendef_t screen_calibration_def;

static const screendef_t *screens[3] = {
    &screen_main_def,
    &screen_sensors_def,
    &screen_calibration_def
};


static void draw_initial()
{
    /*
     switch(mode) {
    case NORMAL:
        draw_initial_normal();
        break;
    case SENSORS:
        draw_initial_sensors();
        break;
    case PANEL_DEBUG:
        draw_initial_paneldebug();
        break;
    case CALIBRATE:
        calibration__draw_initial();
        break;
        }
        */
}
#ifdef STM32F103xB
int main()
#else
int psu_main()
#endif
{
    int r;

    clk__setup();
    SystemCoreClockUpdate();

    HAL_Init();
    gpio__init();
    uart__init();
    usbd__init();

    usbd__printf("Starting application\n");
    i2c__init( I2C_SCL_PORT, I2C_SCL_PIN, I2C_SDA_PORT, I2C_SDA_PIN );
    spi__init();
    input__init();
    delay__init();
    onewire__init();

    HAL_GPIO_WritePin( LED_ON_PORT, LED_ON_PIN, 0);
    HAL_GPIO_WritePin( PSU_OUT_ON_PORT, PSU_OUT_ON_PIN, 0);
    HAL_GPIO_WritePin( P5V_OUT_ON_PORT, P5V_OUT_ON_PIN, 0);
    HAL_GPIO_WritePin( VOUT_ON_PORT, VOUT_ON_PIN, 0);
    HAL_GPIO_WritePin( SPI_CS0_PORT, SPI_CS0_PIN, 1);
    HAL_GPIO_WritePin( SPI_CS1_PORT, SPI_CS1_PIN, 1);
    HAL_GPIO_WritePin( SPI_CS2_PORT, SPI_CS2_PIN, 1);

    temp__init();
    i2c__enable();
    i2c__scan(12);
    mcp45hv__setwiper(I2C_VPSU_ADJ_ADDRESS, 0x80);
    mcp45hv__setwiper(I2C_IPSU_ADJ_ADDRESS, 0x80);
    mcp45hv__setwiper(I2C_VPOS_ADJ_ADDRESS, 0x00);
    mcp45hv__setwiper(I2C_VNEG_ADJ_ADDRESS, 0xFF);
    i2c__disable();

    eeprom__init();
    crc__init();

    do {
        outstring("Intializing store...\r\n");
        r=store__init();
        outstring("Eval\r\n");

        if (r==ERROR_EEPROM_COMMS) {
            outstring("Cannot initialize store: ");
            printhexbyte(r);
            outstring("\r\n");
            delay__us(10000);
        }
        if ((r==ERROR_EEPROM_CRC || (r==ERROR_EEPROM_HEADER))) {
            // Get defaults
            calibration__storedefaults();
            outstring("Saving new data into EEPROM: ");
            r = store__save();
            if (r==0)
                outstring("OK\r\n");
            else {
                outstring("ERROR!\r\n");
                while (1);
            }
        }
    } while (r==ERROR_EEPROM_COMMS);

    calibration__init();

    lcd__init();

    screen__init(screens, sizeof(screens)/sizeof(screens[0]));

    /* Configure the ADC peripheral */
    adc__init();
    adc__calibrate();

    draw_initial();

    system_ready=1;

    while (1) {
        screen__redraw();
        temp__check();
        usbd__checkTx();
        usbd__checkRx();
#ifndef STM32F103xB
        process_events();
#endif
        HAL_Delay(1);
    }
}


static int psu_on_button_changed(unsigned v)
{
    if (psucontrol__getoutputstate(OUTPUT_PSU)==OUTPUT_ON) {
        if (v) {
            // Turn off
            psucontrol__setoutputstate(OUTPUT_PSU, OUTPUT_OFF);
            return EVENT_HANDLED;
        }
    }
    return EVENT_NOT_HANDLED;
}

static int psu_on_long_button_press()
{
    psucontrol__setoutputstate(OUTPUT_PSU, OUTPUT_ON);
    return EVENT_HANDLED;
}

static int p5v_on_button_changed(unsigned v)
{
    if (psucontrol__getoutputstate(OUTPUT_P5V)==OUTPUT_ON) {
        if (v) {
            // Turn off
            psucontrol__setoutputstate(OUTPUT_P5V, OUTPUT_OFF);
            return EVENT_HANDLED;
        }
    }
    return EVENT_NOT_HANDLED;
}

static int p5v_on_long_button_press()
{
    psucontrol__setoutputstate(OUTPUT_P5V, OUTPUT_ON);
    return EVENT_HANDLED;
}

static int adj_on_button_changed(unsigned v)
{
    if (psucontrol__getoutputstate(OUTPUT_VADJ)==OUTPUT_ON) {
        if (v) {
            // Turn off
            psucontrol__setoutputstate(OUTPUT_VADJ, OUTPUT_OFF);
            return EVENT_HANDLED;
        }
    }
    return EVENT_NOT_HANDLED;
}

static int adj_on_long_button_press()
{
    psucontrol__setoutputstate(OUTPUT_VADJ, OUTPUT_ON);
    return EVENT_HANDLED;
}

int input__button_long_press_callback(uint8_t button)
{
    int r = EVENT_NOT_HANDLED;
    event_t event;

    switch (button) {
    case BUTTON_LEFT:
        r = p5v_on_long_button_press();
        break;
    case BUTTON_CENTER_LEFT:
        r = adj_on_long_button_press();
        break;
    case BUTTON_RIGHT:
        r = psu_on_long_button_press();
        break;
    case BUTTON_CENTER_RIGHT:
        screen__selectnext();
        r =EVENT_HANDLED;
        break;
    default:
        event.type = BUTTON_LONG_EVENT;
        event.data.button.button = button;
        r = screen__event(&event);
        break;
    }
    return r;
}

int input__button_down_callback(uint8_t button)
{
    int r = EVENT_NOT_HANDLED;
    switch(button) {
    case BUTTON_LEFT:
        r = p5v_on_button_changed(1);
        break;
    case BUTTON_CENTER_LEFT:
        r =  adj_on_button_changed(1);
        break;
    case BUTTON_RIGHT:
        r = psu_on_button_changed(1);
        break;
    }
    if (r==EVENT_NOT_HANDLED) {
        event_t event;
        event.type = BUTTON_DOWN_EVENT;
        event.data.button.button = button;
        r = screen__event(&event);
    }
    return r;
}

int input__button_clicked_callback(uint8_t button)
{
    return EVENT_NOT_HANDLED;
}

int input__rotary_changed_callback(uint8_t index, uint8_t value)
{
    event_t event;
    event.type = ROTARY_VALUE_CHANGED_EVENT;
    event.data.rotary_value.index = index;
    event.data.rotary_value.value = value;
    return screen__event(&event);
}

int input__rotary_changed_delta_callback(uint8_t index, int delta)
{
    event_t event;
    event.type = ROTARY_DELTA_CHANGED_EVENT;
    event.data.rotary_delta.index = index;
    event.data.rotary_delta.delta = delta;
    return screen__event(&event);
    return EVENT_NOT_HANDLED;
}


void event__send(event_t *event)
{
    screen__event(event);
}




#if 0
void rotary_rotation_changed(uint8_t index PARAM_UNUSED, unsigned v)
{
    if (v) {
        psu_mode++;
        if (psu_mode>PSU_MODE_CL)
            psu_mode=PSU_MODE_CL;
    } else {
        psu_mode--;
        if (psu_mode<PSU_MODE_CV)
            psu_mode=PSU_MODE_CV;
    }
    // Test
    vpos_incdec(v);
    force_redraw(REDRAW_PSU_MODE);
}
#endif

static int led=0;

static unsigned int adc_display = 0;
static unsigned int temp_counter = 0;

void adc__conversion_complete_callback(void)
{
    if ((adc_display & ((1<<(8-SAMPLES_TO_AVERAGE_SHIFT))-1)) == ((1<<(8-SAMPLES_TO_AVERAGE_SHIFT))-1)) {
        input__check();
    }
    adc_display++;

    if (adc_display==(1024/SAMPLES_TO_AVERAGE_COUNT)) {
        event_t event;
        adc_display=0;
        event.type = ADC_FINISHED_EVENT;
        screen__event(&event);

        //force_redraw(REDRAW_VOLTAGES);
        //calibration__force_redraw();
        temp_counter++;
        if (temp_counter==2) {
            temp_counter=0;
            temp__enable_check();
        }
#ifdef DEBUG_ADC
        {
            int i;
            outstring("ADC");
            for (i=0;i<8;i++) {
                unsigned v = adc__getrawvalue(i);                printhex16(v);
                outstring(" ");
            }
            outstring("\r\n");
        }
#endif
        led++;
        HAL_GPIO_WritePin( LED_ON_PORT, LED_ON_PIN, led & 1);
    }

}

static void handle_debug_char(const char c)
{
}

void usbd__incomingData(uint8_t *buf, unsigned len)
{
    while (len--) {
        handle_debug_char(*buf);
        buf++;
    }
}

#ifndef TICK_FREQUENCY
#define TICK_FREQUENCY 1000
#endif

static uint8_t ms_cnt = 0;

// Called each 10 milisseconds
static void ms10_tick()
{
    input__tick_10ms();
}

// Called each milissecond
static void ms_tick()
{
    ms_cnt++;
    if (ms_cnt>=(TICK_FREQUENCY/100)-1) {
        ms_cnt = 0;
        ms10_tick();
    }
}

void tick__handler(void)
{
    if (system_ready) {
        adc__start();
        ms_tick();
    }
}

