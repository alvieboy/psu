/*
  ******************************************************************************
  * (C) 2018 Alvaro Lopes <alvieboy@alvie.com>
  *
  * Redistribution and use in source and binary forms, with or without modification,
  * are permitted provided that the following conditions are met:
  *   1. Redistributions of source code must retain the above copyright notice,
  *      this list of conditions and the following disclaimer.
  *   2. Redistributions in binary form must reproduce the above copyright notice,
  *      this list of conditions and the following disclaimer in the documentation
  *      and/or other materials provided with the distribution.
  *   3. Neither the name of Alvaro Lopes nor the names of contributors
  *      may be used to endorse or promote products derived from this software
  *      without specific prior written permission.
  *
  * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
  * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
  * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
  * DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
  * FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
  * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
  * SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
  * CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
  * OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
  * OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
  *
  ******************************************************************************
  */

#include "store.h"
#include "eeprom.h"
#include "error.h"
#include "crc.h"
#include <string.h>
#include "uart.h"

#define EEPROM_OFFSET_HEADER 0u
#define EEPROM_OFFSET_DATA0  4u
#define EEPROM_OFFSET_CRC0   (4u+STORE_MAX_SPACE)

static uint32_t mem_store_u32[STORE_MAX_SPACE/sizeof(uint32_t)];
static uint8_t *mem_store = (uint8_t*)&mem_store_u32[0];

#define STORE_HEADER_VERSION 0x02u
#define STORE_HEADER_SIZE    STORE_MAX_SPACE

struct storage_header
{
    uint8_t version;
    uint8_t rsvd;
    uint16_t len;
};
static uint8_t storage_valid = 0;

int store__init(void)
{
    struct storage_header hdr;
    crc_t crc, computed_crc;

    if (eeprom__readblock( EEPROM_OFFSET_HEADER, (uint8_t*)&hdr, sizeof(struct storage_header) )<0)
    {
        return ERROR_EEPROM_COMMS;
    }

    if (hdr.version==STORE_HEADER_VERSION)
    {
        if (eeprom__readblock(EEPROM_OFFSET_DATA0, mem_store, STORE_MAX_SPACE)<0)
        {
            return ERROR_EEPROM_COMMS;
        }
        if (eeprom__readblock(EEPROM_OFFSET_CRC0, (uint8_t*)&crc, sizeof(crc))<0)
        {
            return ERROR_EEPROM_COMMS;
        }
        crc__enable();
        crc__calculate((uint32_t*)&hdr,
                       sizeof(hdr)/sizeof(uint32_t));

        computed_crc = crc__accumulate(mem_store_u32, sizeof(mem_store_u32)/sizeof(uint32_t));
        crc__disable();

        outstring("Data CRC: 0x");
        printhex(crc);
        outstring(" computed 0x");
        printhex(computed_crc);
        outstring("\r\n");

        outstring("Read store: ");
        int i;
        for (i=0;i<STORE_MAX_SPACE;i++) {
            printhexbyte(mem_store[i]);
            if ((i%16)==15)
                outstring("\r\n");
            else
                outstring(" ");
        }
        outstring("\r\n");


        if (crc != computed_crc) {
            return ERROR_EEPROM_CRC;
        }
    } else {
        outstring("7");
        return ERROR_EEPROM_HEADER;
    }
    outstring("8");
    storage_valid = 1;
    return 0;
}

int store__valid(void)
{
    return storage_valid;
}

int store__get(uint16_t address, uint8_t *dest, uint16_t len)
{
    if (!storage_valid)
        return -1;

    if (((uint32_t)address+(uint32_t)len) > STORE_MAX_SPACE)
        return -1;
    outstring("Store: read ");
    printhex(len);
    outstring(" bytes\r\n");
    printhex((unsigned)dest);
    outstring("\r\n");
    printhex((unsigned)mem_store);

    memcpy( dest, &mem_store[address], (uint32_t)len);

    outstring(" OK\r\n");

    return 0;
}

int store__set(uint16_t address, const uint8_t *data, uint16_t len)
{
    if (((uint32_t)address+(uint32_t)len) > STORE_MAX_SPACE)
        return -1;

    outstring("Storing @ ");
    printhex16( address);
    outstring(":\r\n");
    int i;
    for (i=0;i<len;i++) {
        printhexbyte(data[i]);
        if ((i%16)==15)
            outstring("\r\n");
        else
            outstring(" ");
    }
    outstring("\r\n");

    memcpy( &mem_store[address], data, len);
    storage_valid=1; // TODO
    return 0;

}

int store__save(void)
{
    struct storage_header hdr;
    crc_t crc;

    hdr.version = STORE_HEADER_VERSION;
    hdr.rsvd = 0u;
    hdr.len = STORE_MAX_SPACE;

    outstring("Saving store: ");
    int i;
    for (i=0;i<STORE_MAX_SPACE;i++) {
        printhexbyte(mem_store[i]);
        if ((i%16)==15)
            outstring("\r\n");
        else
            outstring(" ");
    }
    outstring("\r\n");


    outstring("Header ");
    if (eeprom__writeblock( EEPROM_OFFSET_HEADER,
                           (const uint8_t*)&hdr, sizeof(struct storage_header) )<0)
    {
        return ERROR_EEPROM_COMMS;
    }

    crc__enable();
    outstring("CRCH ");

    crc__calculate( (const uint32_t*)&hdr, sizeof(struct storage_header)/sizeof(uint32_t));

    outstring("Data0 ");

    if (eeprom__writeblock(EEPROM_OFFSET_DATA0,
                           mem_store, STORE_MAX_SPACE)<0)
    {
        return ERROR_EEPROM_COMMS;
    }
    outstring("CRCD ");

    crc = crc__accumulate(mem_store_u32, sizeof(mem_store_u32)/sizeof(uint32_t));
    crc__disable();
    outstring("CRC 0x");
    printhex(crc);
    outstring("\r\n");

    if (eeprom__writeblock(EEPROM_OFFSET_CRC0, (uint8_t*)&crc, sizeof(crc))<0)
    {
        return ERROR_EEPROM_COMMS;
    }
    outstring("Done ");
    storage_valid=1;
    return 0;
}

