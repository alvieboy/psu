#ifndef __SBRK_H__
#define __SBRK_H__

void *_sbrk(int increment);

#endif
